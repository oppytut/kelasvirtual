# KelasVirtual

KelasVirtual is web-based instructional media to improve students psychomotor competency on database learning.

## Installation
1. Clone this project
2. Install all package with `npm install`
3. Compile and run with `npm run dev`

## Source Code
Source code from this research :
<table>
    <tr>
        <td width="20%">Title</td>
        <td>Blended Learning Berbasis Web untuk Meningkatkan Kompetensi Keterampilan Peserta Didik pada Mata Pelajaran Basis Data</td>
    </tr>
    <tr>
        <td>Author</td>
        <td>Arief Novianto</td>
    </tr>
    <tr>
        <td>NIM</td>
        <td>1304482</td>
    </tr>
    <tr>
        <td>Departement</td>
        <td>Pendidikan Ilmu Komputer</td>
    </tr>
    <tr>
        <td>Faculty</td>
        <td>FPMIPA</td>
    </tr>
    <tr>
        <td>University</td>
        <td>Universitas Pendidikan Indonesia</td>
    </tr>
</table>

## Demo
A demo can be accessed on <a href="https://kelasvirtual.mazovi.com">kelasvirtual.mazovi.com</a>.

## Maintain
This project has not been maintained since January 24, 2018.
