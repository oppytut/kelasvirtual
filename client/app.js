import React from 'react';
import {render} from 'react-dom';
import {HashRouter} from 'react-router-dom';
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import App from '../share';
import appReducer from '../share/reducer';
import setAuthorizationToken from '../share/setAuthorizationToken.js';
import jwt from 'jsonwebtoken';
import {setCurrentUser} from '../share/action/auth.js';
import '../public/lib/semantic/dist/semantic.min.css';

// Dev
import {composeWithDevTools} from 'redux-devtools-extension';

const store = createStore(
	appReducer,
	composeWithDevTools(	//Dev
		applyMiddleware(thunk)
	)
);

if(localStorage.jwtToken) {
	setAuthorizationToken(localStorage.jwtToken);
	store.dispatch(setCurrentUser(jwt.decode(localStorage.jwtToken)));
}

render(
	<HashRouter>
		<Provider store={store}>
			<App />
		</Provider>
	</HashRouter>,
	document.getElementById('root')
);