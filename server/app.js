// Express
import express from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import Log from 'debug-logger';

// React SSR
// import React from 'react';
// import {renderToString} from 'react-dom/server';
// import Home from '../share/home';
// import MemberHome from '../share/memberHome';

// API
import authApi from './api/auth.js';
import basicCompetencyApi from './api/basicCompetency.js';
import cognitiveTestApi from './api/cognitiveTest.js';
import compilerApi from './api/compiler.js';
import coreCompetencyApi from './api/coreCompetency.js';
import courseApi from './api/course.js';
import departmentApi from './api/department.js';
import discussionApi from './api/discussion.js';
import essayQuestionApi from './api/essayQuestion.js';
import gradeApi from './api/grade.js';
import gradeDepartmentSchoolApi from './api/gradeDepartmentSchool.js';
import identityApi from './api/identity.js';
import indicatorApi from './api/indicator.js';
import matterApi from './api/matter.js';
import matterGroupApi from './api/matterGroup.js';
import matterRequireApi from './api/matterRequire.js';
import mcQuestionApi from './api/mcQuestion.js';
import mcQuestionOptionApi from './api/mcQuestionOption.js';
import meetingApi from './api/meeting.js';
import messageApi from './api/message.js';
import objectiveApi from './api/objective.js';
import pairCompetencyApi from './api/pairCompetency.js';
import participanApi from './api/participan.js';
import psychomotorTestApi from './api/psychomotorTest.js';
import readerApi from './api/reader.js';
import referenceApi from './api/reference.js';
import requestStudentApi from './api/requestStudent.js';
import schoolApi from './api/school.js';
import senderApi from './api/sender.js';
import studentApi from './api/student.js';
import studentCognitiveTestApi from './api/studentCognitiveTest.js';
import studentMatterApi from './api/studentMatter.js';
import studentMeetingApi from './api/studentMeeting.js';
import studentPsychomotorTestApi from './api/studentPsychomotorTest.js';
import studentTestApi from './api/studentTest.js';
import teacherApi from './api/teacher.js';
import teachingYearApi from './api/teachingYear.js';
import testApi from './api/test.js';
import testGroupApi from './api/testGroup.js';
import userApi from './api/user.js';
import userPictureApi from './api/userPicture.js';

// Database Config
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://mamas:sandinemamas@localhost:27017/ms_vc', {useMongoClient: true});

// Create App
const app = express();

// Debug
const log = Log('app');

// Middleware
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static('public'));

// Router API
app.use('/api/auth/', authApi);
app.use('/api/basicCompetency/', basicCompetencyApi);
app.use('/api/cognitiveTest/', cognitiveTestApi);
app.use('/api/compiler/', compilerApi);
app.use('/api/coreCompetency/', coreCompetencyApi);
app.use('/api/course/', courseApi);
app.use('/api/department/', departmentApi);
app.use('/api/discussion/', discussionApi);
app.use('/api/essayQuestion/', essayQuestionApi);
app.use('/api/grade/', gradeApi);
app.use('/api/gradeDepartmentSchool/', gradeDepartmentSchoolApi);
app.use('/api/identity/', identityApi);
app.use('/api/indicator/', indicatorApi);
app.use('/api/matter/', matterApi);
app.use('/api/matterGroup/', matterGroupApi);
app.use('/api/matterRequire/', matterRequireApi);
app.use('/api/mcQuestion/', mcQuestionApi);
app.use('/api/mcQuestionOption/', mcQuestionOptionApi);
app.use('/api/meeting/', meetingApi);
app.use('/api/message/', messageApi);
app.use('/api/objective/', objectiveApi);
app.use('/api/pairCompetency/', pairCompetencyApi);
app.use('/api/participan/', participanApi);
app.use('/api/psychomotorTest/', psychomotorTestApi);
app.use('/api/reader/', readerApi);
app.use('/api/reference/', referenceApi);
app.use('/api/requestStudent/', requestStudentApi);
app.use('/api/school/', schoolApi);
app.use('/api/sender/', senderApi);
app.use('/api/student/', studentApi);
app.use('/api/studentCognitiveTest/', studentCognitiveTestApi);
app.use('/api/studentMatter/', studentMatterApi);
app.use('/api/studentMeeting/', studentMeetingApi);
app.use('/api/studentPsychomotorTest/', studentPsychomotorTestApi);
app.use('/api/studentTest/', studentTestApi);
app.use('/api/teacher/', teacherApi);
app.use('/api/teachingYear/', teachingYearApi);
app.use('/api/test/', testApi);
app.use('/api/testGroup/', testGroupApi);
app.use('/api/user/', userApi);
app.use('/api/userPicture/', userPictureApi);

app.get('/', (req, res) => {
	log.trace('GET /');

	res.send(`
		<!DOCTYPE html>
		<html>
			<head>
				<!-- Hey, Dasar KEPO! -->
				<title>Kelas Virtual</title>
				<link rel="icon" href="/media/icon/16x16-square.png" type="image/png" sizes="16x16">
				<link rel="icon" href="/media/icon/32x32-square.png" type="image/png" sizes="32x32">

				<!-- Style -->
				<link rel="stylesheet" type="text/css" href="/css/style.css">

				<link rel="stylesheet" type="text/css" href="/bundle/css/app.css">
			</head>
			<body>
				<div id="root"></div>

				<script src="/bundle/js/app.js"></script>
			</body>
		</html>
	`);
});

// Error Handler
app.get('*', (req, res, next) => {
	log.warn('URL error:', req.url);

	var err = new Error();
	err.status = 404;
	next(err);
});
app.use((err, req, res, next) => {
	if (err.status !== 404) {
		return next();
	}
	res.status(err.status || 404);
	log.trace('Send respon URL error.');

	res.send(`
		<!DOCTYPE html>
		<html>
			<head>
				<!-- Hey, Dasar KEPO! -->
				<title>Kelas Virtual</title>
				<link rel="icon" href="/media/icon/16x16-square.png" type="image/png" sizes="16x16">
				<link rel="icon" href="/media/icon/32x32-square.png" type="image/png" sizes="32x32">

				<!-- Style -->
				<link rel="stylesheet" type="text/css" href="/css/style.css">
				<link rel="stylesheet" type="text/css" href="/lib/semantic/dist/semantic.min.css">
			</head>
			<body>
				<div>
					<div class="ui grid">
						<div class="row" style="padding: 26px 13px 26px 13px;">
							<div class="column">
								<center>
									<div class="ui message">
										<div class="header">
											Respon Error 404
										</div>
										<p>Halaman yang Anda tuju tidak tersedia!</p>
									</div>
								</center>
							</div>
						</div>
					</div>
				</div>

				<script
					src="https://code.jquery.com/jquery-3.1.1.min.js"
					integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
					crossorigin="anonymous">
				</script>
				<script src="/lib/semantic/dist/semantic.min.js"></script>
			</body>
		</html>
	`);
});

// Robot
app.get('/robots.txt', (req, res) => {
	res.type('text/plain');
	res.send('User-agent: *\nDisallow: /');
});

// Server
app.listen(process.env.PORT || 9001, () => {
	log.trace('Listening on port 9001!');
	console.log('KelasVirtual ready!');
});
