import matterModel from '../model/matter.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import {getMatterGroupsByCourse} from './matterGroup.js';

const log = Log('bridge:matter');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.matterGroup))
		if(!mongoose.Types.ObjectId.isValid(data.matterGroup))
			error.matterGroup = 'matterGroup is not valid!';

	if(!isEmpty(data.number))
		if(!validator.isNumeric(data.number))
			error.number = 'number is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.title))
		error.title = 'title is required!';

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.number))
		error.number = 'number is required!';

	return error;
}

export function getMatters({query, field, populate=[], limit=0}) {
	log.trace('getMatters');

	return new Promise((resolve, reject) => {
		matterModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, matter) => {
				if(err) {
					reject(err);
				} else {
					resolve(matter);
				}
			});
	});
}

export function getMatter({id}) {
	log.trace('getMatter');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			matterModel.findById(id, (err, matter) => {
				if(err) {
					reject(err);
				} else {
					resolve(matter);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postMatter({data}) {
	log.trace('postMatter');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {number, title, content, video} = data;
			var matter = new matterModel();

			matter.number = number;
			matter.title = title;
			matter.content = content;
			matter.video = video;

			matter.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(matter);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putMatter({id, data}) {
	log.trace('putMatter');

	return new Promise((resolve, reject) => {
		const {number, title, content, video, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			matterModel.findById(id, (err, matter) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(matter)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(number)) {
								log.trace('number changed');
								matter.number = number;
							}

							if(!isEmpty(title)) {
								log.trace('title changed');
								matter.title = title;
							}

							if(!isEmpty(content)) {
								log.trace('content changed');
								matter.content = content;
							}

							if(!isEmpty(video)) {
								log.trace('video changed');
								matter.video = video;
							}

							matter.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(matter);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'matter is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMatter({id}) {
	log.trace('delMatter');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			matterModel.findByIdAndRemove(id, (err, matter) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(matter)) {
						resolve(matter);
					} else {
						const error = 'matter is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMatters({query}) {
	log.trace('delMatters');

	return new Promise((resolve, reject) => {
		getMatters({query: query, field: '_id'})
			.then((matter) => {
				matterModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(matter);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}

export function getMattersByCourse({query, field, populate, limit}) {
	log.trace('getMattersByCourse');

	return new Promise((resolve, reject) => {
		getMatterGroupsByCourse({query: query.matterGroup, field: '_id'})
			.then((matterGroup) => {
				if(isEmpty(query.this))
					query.this = {};

				query.this.matterGroup = {$in: matterGroup};

				getMatters({query: query.this, field, populate, limit})
					.then((matter) => {
						resolve(matter);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}
