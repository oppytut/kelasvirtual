import objectiveModel from '../model/objective.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

const log = Log('bridge:objective');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.basicCompetency))
		if(!mongoose.Types.ObjectId.isValid(data.basicCompetency))
			error.basicCompetency = 'basicCompetency is not valid!';

	if(!isEmpty(data.number))
		if(!validator.isNumeric(data.number.toString()))
			error.number = 'number is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.number))
		error.number = 'number is required!';

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.basicCompetency))
		error.basicCompetency = 'basicCompetency is required!';

	return error;
}

export function getObjectives({query, field, populate=[], limit=0}) {
	log.trace('getObjectives');

	return new Promise((resolve, reject) => {
		objectiveModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, objective) => {
				if(err) {
					reject(err);
				} else {
					resolve(objective);
				}
			});
	});
}

export function getObjective({id}) {
	log.trace('getObjective');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			objectiveModel.findById(id, (err, objective) => {
				if(err) {
					reject(err);
				} else {
					resolve(objective);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postObjective({data}) {
	log.trace('postObjective');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {number, content, basicCompetency} = data;
			var objective = new objectiveModel();

			objective.number = number;
			objective.content = content;
			objective.basicCompetency = basicCompetency;

			objective.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(objective);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putObjective({id, data}) {
	log.trace('putObjective');

	return new Promise((resolve, reject) => {
		const {number, content, basicCompetency, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			objectiveModel.findById(id, (err, objective) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(objective)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(number)) {
								log.trace('number changed');
								objective.number = number;
							}

							if(!isEmpty(content)) {
								log.trace('content changed');
								objective.content = content;
							}

							if(!isEmpty(basicCompetency)) {
								log.trace('basicCompetency changed');
								objective.basicCompetency = basicCompetency;
							}

							objective.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(objective);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'objective is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delObjective({id}) {
	log.trace('delObjective');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			objectiveModel.findByIdAndRemove(id, (err, objective) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(objective)) {
						resolve(objective);
					} else {
						const error = 'objective is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delObjectives({query}) {
	log.trace('delObjectives');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			objectiveModel.find(query)
				.exec((err, objective) => {
					if(err) {
						reject(err);
					} else {
						resolve(objective);
					}
				});
		}).then((objective) => {
			objectiveModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(objective);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
