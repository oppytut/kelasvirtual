import studentMeetingModel from '../model/studentMeeting.js';
import Log from 'debug-logger';

const log = Log('bridge:studentMeeting');

export function getStudentMeetings({query, field, populate=[], limit=0}) {
	log.trace('getStudentMeetings');

	return new Promise((resolve, reject) => {
		studentMeetingModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, studentMeeting) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentMeeting);
				}
			});
	});
}

export function delStudentMeetings({query}) {
	log.trace('delStudentMeetings');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			studentMeetingModel.find(query)
				.exec((err, studentMeeting) => {
					if(err) {
						reject(err);
					} else {
						resolve(studentMeeting);
					}
				});
		}).then((studentMeeting) => {
			studentMeetingModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentMeeting);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
