import identityModel from '../model/identity.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

const log = Log('bridge:identity');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.number) || !isEmpty(data.type)) {
		if(!(!isEmpty(data.number) && !isEmpty(data.type))) {
			if(isEmpty(data.number))
				error.number = 'Anda belum mengisi nomor identitas!';
			if(isEmpty(data.type))
				error.type = 'Anda belum mengisi jenis identitas!';
		}
	}

	if(!isEmpty(data.user))
		if(!mongoose.Types.ObjectId.isValid(data.user))
			error.user = 'user is not valid!';

	if(!isEmpty(data.number))
		if(!validator.isNumeric(data.number))
			error.number = 'number is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.user))
		error.user = 'user is required!';

	return error;
}

export function getIdentitys({query, field, populate=[], limit=0}) {
	log.trace('getIdentitys');

	return new Promise((resolve, reject) => {
		identityModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, identity) => {
				if(err) {
					reject(err);
				} else {
					resolve(identity);
				}
			});
	});
}

export function getIdentity({id}) {
	log.trace('getIdentity');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			identityModel.findById(id, (err, identity) => {
				if(err) {
					reject(err);
				} else {
					resolve(identity);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postIdentity({data}) {
	log.trace('postIdentity');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {number, competency, coreCompetency} = data;
			var identity = new identityModel();

			identity.number = number;
			identity.competency = competency;
			identity.coreCompetency = coreCompetency;

			identity.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(identity);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putIdentity({id, data}) {
	log.trace('putIdentity');

	return new Promise((resolve, reject) => {
		const {number, competency, coreCompetency, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			identityModel.findById(id, (err, identity) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(identity)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(number)) {
								log.trace('number changed');
								identity.number = number;
							}

							if(!isEmpty(competency)) {
								log.trace('competency changed');
								identity.competency = competency;
							}

							if(!isEmpty(coreCompetency)) {
								log.trace('coreCompetency changed');
								identity.coreCompetency = coreCompetency;
							}

							identity.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(identity);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'identity is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delIdentity({id}) {
	log.trace('delIdentity');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			identityModel.findByIdAndRemove(id, (err, identity) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(identity)) {
						resolve(identity);
					} else {
						const error = 'identity is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delIdentitys({query}) {
	log.trace('delIdentitys');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			identityModel.find(query)
				.exec((err, identity) => {
					if(err) {
						reject(err);
					} else {
						resolve(identity);
					}
				});
		}).then((identity) => {
			identityModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(identity);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
