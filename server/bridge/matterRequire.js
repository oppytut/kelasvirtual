import matterRequireModel from '../model/matterRequire.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

const log = Log('bridge:matterRequire');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.matter))
		if(!mongoose.Types.ObjectId.isValid(data.matter))
			error.matter = 'matter is not valid!';

	if(!isEmpty(data.content))
		if(!mongoose.Types.ObjectId.isValid(data.content))
			error.content = 'content is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.matter))
		error.matter = 'matter is required!';

	return error;
}

export function getMatterRequires({query, field, populate=[], limit=0}) {
	log.trace('getMatterRequires');

	return new Promise((resolve, reject) => {
		matterRequireModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, matterRequire) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterRequire);
				}
			});
	});
}

export function getMatterRequire({id}) {
	log.trace('getMatterRequire');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			matterRequireModel.findById(id, (err, matterRequire) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterRequire);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postMatterRequire({data}) {
	log.trace('postMatterRequire');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {matter, content} = data;
			var matterRequire = new matterRequireModel();

			matterRequire.matter = matter;
			matterRequire.content = content;

			matterRequire.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterRequire);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putMatterRequire({id, data}) {
	log.trace('putMatterRequire');

	return new Promise((resolve, reject) => {
		const {matter, content, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			matterRequireModel.findById(id, (err, matterRequire) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(matterRequire)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(matter)) {
								log.trace('matter changed');
								matterRequire.matter = matter;
							}

							if(!isEmpty(content)) {
								log.trace('content changed');
								matterRequire.content = content;
							}

							matterRequire.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(matterRequire);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'matterRequire is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMatterRequire({id}) {
	log.trace('delMatterRequire');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			matterRequireModel.findByIdAndRemove(id, (err, matterRequire) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(matterRequire)) {
						resolve(matterRequire);
					} else {
						const error = 'matterRequire is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMatterRequires({query}) {
	log.trace('delMatterRequires');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			matterRequireModel.find(query)
				.exec((err, matterRequire) => {
					if(err) {
						reject(err);
					} else {
						resolve(matterRequire);
					}
				});
		}).then((matterRequire) => {
			matterRequireModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterRequire);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
