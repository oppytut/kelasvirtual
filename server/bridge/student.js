import studentModel from '../model/student.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import {getTeachers} from './teacher.js';

const log = Log('bridge:student');

function isAlreadyStudent(data) {
	log.trace('isAlreadyStudent');

	return new Promise((resolve) => { // data must not null
		studentModel.count({'user': data.user, 'course': data.course}, (err, count) => {
			if(err)
				log.error(err);

			if(count == 1) {
				resolve(true);
			} else {
				resolve(false);
			}
		});
	});
}

function validate(data) {
	log.trace('validate');

	return new Promise(async (resolve) => {
		var error = {};

		if(!(isEmpty(data.course) && isEmpty(data.user))) {
			if(!isEmpty(data.course) && !isEmpty(data.user)) {
				var asyncResult = await isAlreadyStudent(data);
				if(asyncResult) {
					error.user = 'course and student already connected!';
					error.course = 'course and student already connected!';
				}
			} else {
				error.user = 'course and student are interdependence!';
				error.course = 'course and student are interdependence!';
			}
		}

		if(!isEmpty(data.course))
			if(!mongoose.Types.ObjectId.isValid(data.course))
				error.course = 'course is not valid!';

		if(!isEmpty(data.user))
			if(!mongoose.Types.ObjectId.isValid(data.user))
				error.user = 'user is not valid!';

		if(!isEmpty(data.lastStudied))
			if(!mongoose.Types.ObjectId.isValid(data.lastStudied))
				error.lastStudied = 'lastStudied is not valid!';

		if(!isEmpty(data.deleted))
			if(!validator.isBoolean(data.deleted.toString()))
				error.deleted = 'deleted is not valid!';

		resolve(error);
	});
}

function validateAll(data) {
	log.trace('validateAll');

	return new Promise(async (resolve) => {
		var error = {};
		error = await validate(data);

		if(isEmpty(data.course))
			error.course = 'course is required!';

		if(isEmpty(data.user))
			error.user = 'user is required!';

		resolve(error);
	});
}

export function getStudents({query, field, populate=[], limit}) {
	log.trace('getStudents');

	return new Promise((resolve, reject) => {
		studentModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, student) => {
				if(err) {
					reject(err);
				} else {
					resolve(student);
				}
			});
	});
}

export function getStudent({id}) {
	log.trace('getStudent');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			studentModel.findById(id, (err, student) => {
				if(err) {
					reject(err);
				} else {
					resolve(student);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postStudent({data}) {
	log.trace('postStudent');

	return new Promise(async (resolve, reject) => {
		const error = await validateAll(data)	;

		if(isEmpty(error)) {
			const {course, user, lastStudied, deleted} = data;
			var student = new studentModel();

			student.course = course;
			student.user = user;
			student.lastStudied = lastStudied;
			student.deleted = deleted;

			student.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(student);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putStudent({id, data}) {
	log.trace('putStudent');

	return new Promise(async (resolve, reject) => {
		const {course, user, lastStudied, deleted, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			studentModel.findById(id, async (err, student) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(student)) {
						var error = {};

						if(_all) {
							error = await validateAll(data);
						} else {
							error = await validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(course)) {
								log.trace('course changed');
								student.course = course;
							}

							if(!isEmpty(user)) {
								log.trace('user changed');
								student.user = user;
							}

							if(!isEmpty(lastStudied)) {
								log.trace('lastStudied changed');
								student.lastStudied = lastStudied;
							}

							if(!isEmpty(deleted)) {
								log.trace('deleted changed');
								student.deleted = deleted;
							}

							student.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(student);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'student is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delStudent({id}) {
	log.trace('delStudent');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			studentModel.findByIdAndRemove(id, (err, student) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(student)) {
						resolve(student);
					} else {
						const error = 'student is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delStudents({query}) {
	log.trace('delStudents');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			studentModel.find(query)
				.exec((err, student) => {
					if(err) {
						reject(err);
					} else {
						resolve(student);
					}
				});
		}).then((student) => {
			studentModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(student);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}

export function getStudentsOnly({query, field, populate, limit}) {
	log.trace('getStudentsOnly');

	return new Promise((resolve, reject) => {
		getTeachers({query: query.teacher, field: 'user'})
			.then((teacher) => {
				if(isEmpty(query.this))
					query.this = {};

				var teacherUser = [];
				teacher.map((item) => {
					teacherUser.push(item.user);
				});

				query.this.user = {$nin: teacherUser};

				getStudents({query: query.this, field, populate, limit})
					.then((student) => {
						resolve(student);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}

export function putPostStudent({query, data}) {
	log.trace('putPostStudent');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			studentModel.find(query)
				.exec((err, student) => {
					if(err) {
						reject(err);
					} else {
						resolve(student);
					}
				});
		}).then((student) => {
			if(isEmpty(student)) {
				postStudent({data: data.post})
					.then((student) => {
						resolve(student);
					})
					.catch((err) => {
						reject(err);
					});
			} else {
				putStudent({
					id: student.reduce(item => item)._id,
					data: data.put
				})
					.then((student) => {
						resolve(student);
					})
					.catch((err) => {
						reject(err);
					});
			}
		}).catch((err) => {
			reject(err);
		});
	});
}
