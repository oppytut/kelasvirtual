import requestStudentModel from '../model/requestStudent.js';
import Log from 'debug-logger';

const log = Log('bridge:requestStudent');

export function getRequestStudents({query, field, populate=[], limit=0}) {
	log.trace('getRequestStudents');
	
	return new Promise((resolve, reject) => {
		requestStudentModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, requestStudent) => {
				if(err) {
					reject(err);
				} else {
					resolve(requestStudent);
				}
			});
	});
}

export function delRequestStudents({query}) {
	log.trace('delRequestStudents');
	
	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			requestStudentModel.find(query)
				.exec((err, requestStudent) => {
					if(err) {
						reject(err);
					} else {
						resolve(requestStudent);
					}
				});
		}).then((requestStudent) => {
			requestStudentModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(requestStudent);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}