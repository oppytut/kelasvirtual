import cognitiveTestModel from '../model/cognitiveTest.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

const log = Log('bridge:cognitiveTest');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(isEmpty(data.test))
		if(!mongoose.Types.ObjectId.isValid(data.test))
			error.test = 'test is not valid!';

	if(isEmpty(data.mcQuestion))
		if(!mongoose.Types.ObjectId.isValid(data.mcQuestion))
			error.mcQuestion = 'mcQuestion is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.test))
		error.test = 'test is required!';

	if(isEmpty(data.mcQuestion))
		error.mcQuestion = 'mcQuestion is required!';

	return error;
}

export function getCognitiveTests({query, field, populate=[], limit=0}) {
	log.trace('getCognitiveTests');

	return new Promise((resolve, reject) => {
		cognitiveTestModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, cognitiveTest) => {
				if(err) {
					reject(err);
				} else {
					resolve(cognitiveTest);
				}
			});
	});
}

export function getCognitiveTest({id}) {
	log.trace('getCognitiveTest');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			cognitiveTestModel.findById(id, (err, cognitiveTest) => {
				if(err) {
					reject(err);
				} else {
					resolve(cognitiveTest);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postCognitiveTest({data}) {
	log.trace('postCognitiveTest');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {number, option, test, mcQuestion} = data;
			var cognitiveTest = new cognitiveTestModel();

			cognitiveTest.number = number;
			cognitiveTest.option = option;
			cognitiveTest.test = test;
			cognitiveTest.mcQuestion = mcQuestion;

			cognitiveTest.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(cognitiveTest);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putCognitiveTest({id, data}) {
	log.trace('putCognitiveTest');

	return new Promise((resolve, reject) => {
		const {number, option, test, mcQuestion, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			cognitiveTestModel.findById(id, (err, cognitiveTest) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(cognitiveTest)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(number)) {
								log.trace('number changed');
								cognitiveTest.number = number;
							}

							if(!isEmpty(option)) {
								log.trace('option changed');
								cognitiveTest.option = option;
							}

							if(!isEmpty(test)) {
								log.trace('test changed');
								cognitiveTest.test = test;
							}

							if(!isEmpty(mcQuestion)) {
								log.trace('mcQuestion changed');
								cognitiveTest.mcQuestion = mcQuestion;
							}

							cognitiveTest.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(cognitiveTest);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'cognitiveTest is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delCognitiveTest({id}) {
	log.trace('delCognitiveTest');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			cognitiveTestModel.findByIdAndRemove(id, (err, cognitiveTest) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(cognitiveTest)) {
						resolve(cognitiveTest);
					} else {
						const error = 'cognitiveTest is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delCognitiveTests({query}) {
	log.trace('delCognitiveTests');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			cognitiveTestModel.find(query)
				.exec((err, cognitiveTest) => {
					if(err) {
						reject(err);
					} else {
						resolve(cognitiveTest);
					}
				});
		}).then((cognitiveTest) => {
			cognitiveTestModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(cognitiveTest);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
