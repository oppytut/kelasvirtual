import referenceModel from '../model/reference.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

const log = Log('bridge:reference');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.matter))
		if(!mongoose.Types.ObjectId.isValid(data.matter))
			error.matter = 'matter is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.matter))
		error.matter = 'matter is required!';

	return error;
}

export function getReferences({query, field, populate=[], limit=0}) {
	log.trace('getReferences');

	return new Promise((resolve, reject) => {
		referenceModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, reference) => {
				if(err) {
					reject(err);
				} else {
					resolve(reference);
				}
			});
	});
}

export function getReference({id}) {
	log.trace('getReference');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			referenceModel.findById(id, (err, reference) => {
				if(err) {
					reject(err);
				} else {
					resolve(reference);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postReference({data}) {
	log.trace('postReference');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {content, matter} = data;
			var reference = new referenceModel();

			reference.content = content;
			reference.matter = matter;

			reference.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(reference);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putReference({id, data}) {
	log.trace('putReference');

	return new Promise((resolve, reject) => {
		const {content, matter, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			referenceModel.findById(id, (err, reference) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(reference)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(content)) {
								log.trace('content changed');
								reference.content = content;
							}

							if(!isEmpty(matter)) {
								log.trace('matter changed');
								reference.matter = matter;
							}

							reference.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(reference);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'reference is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delReference({id}) {
	log.trace('delReference');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			referenceModel.findByIdAndRemove(id, (err, reference) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(reference)) {
						resolve(reference);
					} else {
						const error = 'reference is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delReferences({query}) {
	log.trace('delReferences');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			referenceModel.find(query)
				.exec((err, reference) => {
					if(err) {
						reject(err);
					} else {
						resolve(reference);
					}
				});
		}).then((reference) => {
			referenceModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(reference);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
