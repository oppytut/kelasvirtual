import studentCognitiveTestModel from '../model/studentCognitiveTest.js';
import Log from 'debug-logger';

const log = Log('bridge:studentCognitiveTest');

export function getStudentCognitiveTests({query, field, populate=[], limit=0}) {
	log.trace('getStudentCognitiveTests');

	return new Promise((resolve, reject) => {
		studentCognitiveTestModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, studentCognitiveTest) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentCognitiveTest);
				}
			});
	});
}

export function delStudentCognitiveTests({query}) {
	log.trace('delStudentCognitiveTests');

	return new Promise((resolve, reject) => {
		getStudentCognitiveTests({query: query, field: '_id'})
			.then((studentCognitiveTest) => {
				studentCognitiveTestModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(studentCognitiveTest);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}
