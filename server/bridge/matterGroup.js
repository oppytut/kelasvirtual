import matterGroupModel from '../model/matterGroup.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
// import validator from 'validator';

import {getPairCompetencysByCourse} from './pairCompetency.js';
import {getIndicatorsByCourse} from './indicator.js';
import {getMatters} from './matter.js';

const log = Log('bridge:matterGroup');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.indicator))
		if(!mongoose.Types.ObjectId.isValid(data.indicator))
			error.indicator = 'indicator is not valid!';

	if(!isEmpty(data.pairCompetency))
		if(!mongoose.Types.ObjectId.isValid(data.pairCompetency))
			error.pairCompetency = 'pairCompetency is not valid!';

	if(!isEmpty(data.course))
		if(!mongoose.Types.ObjectId.isValid(data.course))
			error.course = 'course is not valid!';

	if(!isEmpty(data.matterGroup))
		if(!mongoose.Types.ObjectId.isValid(data.matterGroup))
			error.matterGroup = 'matterGroup is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	return error;
}

export function getMatterGroups({query, field, populate=[], limit=0}) {
	log.trace('getMatterGroups');

	return new Promise((resolve, reject) => {
		matterGroupModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, matterGroup) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterGroup);
				}
			});
	});
}

export function getMatterGroup({id}) {
	log.trace('getMatterGroup');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			matterGroupModel.findById(id, (err, matterGroup) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterGroup);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postMatterGroup({data}) {
	log.trace('postMatterGroup');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {course, pairCompetency, indicator, name} = data;
			var matterGroup = new matterGroupModel();

			matterGroup.course = course;
			matterGroup.pairCompetency = pairCompetency;
			matterGroup.indicator = indicator;
			matterGroup.name = name;

			matterGroup.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterGroup);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putMatterGroup({id, data}) {
	log.trace('putMatterGroup');

	return new Promise((resolve, reject) => {
		const {course, pairCompetency, indicator, name, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			matterGroupModel.findById(id, (err, matterGroup) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(matterGroup)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(course)) {
								log.trace('course changed');
								matterGroup.course = course;
							}

							if(!isEmpty(pairCompetency)) {
								log.trace('pairCompetency changed');
								matterGroup.pairCompetency = pairCompetency;
							}

							if(!isEmpty(indicator)) {
								log.trace('indicator changed');
								matterGroup.indicator = indicator;
							}

							if(!isEmpty(name)) {
								log.trace('name changed');
								matterGroup.name = name;
							}

							matterGroup.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(matterGroup);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'matterGroup is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMatterGroup({id}) {
	log.trace('delMatterGroup');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			matterGroupModel.findByIdAndRemove(id, (err, matterGroup) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(matterGroup)) {
						resolve(matterGroup);
					} else {
						const error = 'matterGroup is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMatterGroups({query}) {
	log.trace('delMatterGroups');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			matterGroupModel.find(query)
				.exec((err, matterGroup) => {
					if(err) {
						reject(err);
					} else {
						resolve(matterGroup);
					}
				});
		}).then((matterGroup) => {
			matterGroupModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(matterGroup);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}

export function getMatterGroupsByCourse({query, field, populate, limit}) {
	log.trace('getMatterGroupsByCourse');

	return new Promise((resolve, reject) => {
		Promise.all([
			getPairCompetencysByCourse({query: query.pairCompetency, field: '_id'}),
			getIndicatorsByCourse({query: query.indicator, field: '_id'})
		])
			.then((item) => {
				if(isEmpty(query.this))
					query.this = {};

				const pairCompetency = item[0];
				const indicator = item[1];

				query.this.$or = [
					{pairCompetency: {$in: pairCompetency}},
					{indicator: {$in: indicator}},
					{course: query.course}
				];

				getMatterGroups({query: query.this, field, populate, limit})
					.then((matterGroup) => {
						resolve(matterGroup);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}

export function getMatterGroupsByMatter({query, field, populate, limit}) {
	log.trace('getMatterGroupsByMatter');

	return new Promise((resolve, reject) => {
		getMatters({query: query.matter, field: '_id matterGroup'})
			.then((matter) => {
				if(isEmpty(query.this))
					query.this = {};

				query.this._id = matter.reduce(item => item).matterGroup;

				getMatterGroups({query: query.this, field, populate, limit})
					.then((matterGroup) => {
						resolve(matterGroup);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}
