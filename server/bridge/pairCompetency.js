import pairCompetencyModel from '../model/pairCompetency.js';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import mongoose from 'mongoose';

import {
	delBasicCompetencys,
	getBasicCompetencysByCourse,
	postBasicCompetency
} from './basicCompetency.js';

import {
	delIndicators
} from './indicator.js';

import {
	delObjectives
} from './objective.js';

import {
	delMatterGroups
} from './matterGroup.js';

import {
	delMatters
} from './matter.js';

import {
	delMatterRequires
} from './matterRequire.js';

import {
	delReferences
} from './reference.js';

import {
	delStudentMatters
} from './studentMatter.js';

import {
	delTestGroups
} from './testGroup.js';

import {
	delTests
} from './test.js';

import {
	delPsychomotorTests
} from './psychomotorTest.js';

import {
	delStudentPsychomotorTests
} from './studentPsychomotorTest.js';

import {
	delEssayQuestions
} from './essayQuestion.js';

import {
	delCognitiveTests
} from './cognitiveTest.js';

import {
	delStudentCognitiveTests
} from './studentCognitiveTest.js';

import {
	delMcQuestions
} from './mcQuestion.js';

import {
	delMcQuestionOptions
} from './mcQuestionOption.js';

import {
	delStudentTests
} from './studentTest.js';

const log = Log('bridge:pairCompetency');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.cognitive))
		if(!mongoose.Types.ObjectId.isValid(data.cognitive))
			error.cognitive = 'cognitive is not valid!';

	if(!isEmpty(data.psychomotor))
		if(!mongoose.Types.ObjectId.isValid(data.psychomotor))
			error.psychomotor = 'psychomotor is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	return error;
}

export function getPairCompetencys({query, field, populate=[], limit=0}) {
	log.trace('getPairCompetencys');

	return new Promise((resolve, reject) => {
		pairCompetencyModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, pairCompetency) => {
				if(err) {
					log.error(err);
					reject(err);
				} else {
					resolve(pairCompetency);
				}
			});
	});
}

export function getPairCompetency({id}) {
	log.trace('getPairCompetency');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			pairCompetencyModel.findById(id, (err, pairCompetency) => {
				if(err) {
					reject(err);
				} else {
					resolve(pairCompetency);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postPairCompetency({data}) {
	log.trace('postPairCompetency');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {cognitive, psychomotor} = data;
			var pairCompetency = new pairCompetencyModel();

			pairCompetency.cognitive = cognitive;
			pairCompetency.psychomotor = psychomotor;

			pairCompetency.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(pairCompetency);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putPairCompetency({id, data}) {
	log.trace('putPairCompetency');

	return new Promise((resolve, reject) => {
		const {cognitive, psychomotor, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			pairCompetencyModel.findById(id, async (err, pairCompetency) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(pairCompetency)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(cognitive)) {
								log.trace('cognitive changed');
								pairCompetency.cognitive = cognitive;
							}

							if(!isEmpty(psychomotor)) {
								log.trace('psychomotor changed');
								pairCompetency.psychomotor = psychomotor;
							}

							pairCompetency.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(pairCompetency);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'pairCompetency is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delPairCompetency({id}) {
	log.trace('delPairCompetency');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			pairCompetencyModel.findByIdAndRemove(id, (err, pairCompetency) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(pairCompetency)) {
						resolve(pairCompetency);
					} else {
						const error = 'pairCompetency is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delPairCompetencys({query, field}) {
	log.trace('delPairCompetencys');

	return new Promise((resolve, reject) => {
		getPairCompetencys({query: query, field: field})
			.then((pairCompetency) => {
				pairCompetencyModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(pairCompetency);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}

export function getPairCompetencysByCourse({query, field, populate, limit}) {
	log.trace('getPairCompetencysByCourse');

	return new Promise((resolve, reject) => {
		getBasicCompetencysByCourse({query: query.basicCompetency, field: '_id'})
			.then((basicCompetency) => {
				if(isEmpty(query.this))
					query.this = {};

				query.this.cognitive = {$in: basicCompetency};
				query.this.psychomotor = {$in: basicCompetency};

				getPairCompetencys({query: query.this, field, populate, limit})
					.then((pairCompetency) => {
						resolve(pairCompetency);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}

export function postPairedBasicCompetency({data}) {
	log.trace('postPairedBasicCompetency');

	return new Promise((resolve, reject) => {
		const postCognitive = new Promise((resolve, reject) => {
			postBasicCompetency({data: data.cognitive})
				.then((basicCompetency) => {
					resolve(basicCompetency);
				})
				.catch((err) => {
					reject(err);
				});
		});

		const postPsychomotor = new Promise((resolve, reject) => {
			postBasicCompetency({data: data.psychomotor})
				.then((basicCompetency) => {
					resolve(basicCompetency);
				})
				.catch((err) => {
					reject(err);
				});
		});

		Promise.all([postCognitive, postPsychomotor]).then((item) => {
			postPairCompetency({data: {
				cognitive: item[0]._id,
				psychomotor: item[1]._id
			}}).then((pairCompetency) => {
				getPairCompetencys({
					query: {_id: pairCompetency._id},
					field: '_id',
					populate: [
						{
							path: 'cognitive',
							select: 'competency number'
						},
						{
							path: 'psychomotor',
							select: 'competency number'
						}
					]
				}).then((pairCompetency) => {
					resolve(pairCompetency);
				}).catch((err) => {
					reject(err);
				});
			}).catch((err) => {
				reject(err);
			});
		}).catch((err) => {
			reject(err);
		});
	});
}

export function delPairCompetencysChild({query}) {
	log.warn('delPairCompetencysChild');

	return new Promise((resolve, reject) => {
		delPairCompetencys({query: query, field: 'cognitive psychomotor'})
			.then((pairCompetency) => {
				log.warn('Delete Level 1');

				Promise.all([
					delBasicCompetencys({
						query: {
							_id: pairCompetency
								.reduce((prev, item) => item, {
									cognitive: undefined
								}).cognitive
						}
					}),
					delBasicCompetencys({
						query: {
							_id: pairCompetency
								.reduce((prev, item) => item, {
									psychomotor: undefined
								}).psychomotor
						}
					}),
					delMatterGroups({
						query: {
							pairCompetency: pairCompetency
								.reduce((prev, item) => item, {
									_id: undefined
								})._id
						}
					}),
					delTestGroups({
						query: {
							pairCompetency: pairCompetency
								.reduce((prev, item) => item, {
									_id: undefined
								})._id
						}
					})
				]).then(([
					cognitive,
					psychomotor,
					matterGroup,
					testGroup
				]) => {
					log.warn('Delete Level 2');

					Promise.all([
						/** basicCompetency **/
						delIndicators({
							query: {
								basicCompetency: cognitive
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),
						delIndicators({
							query: {
								basicCompetency: psychomotor
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),

						/** matterGroup **/
						delMatters({
							query: {
								matterGroup: matterGroup
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),

						/** testGroup **/
						delTests({
							query: {
								testGroup: testGroup
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),

						/** basicCompetency **/
						delObjectives({ // not have child
							query: {
								basicCompetency: cognitive
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),
						delObjectives({ // not have child
							query: {
								basicCompetency: psychomotor
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						})
					]).then(([
						cognitiveIndicator,
						psychomotorIndicator,
						matter,
						test
					]) => {
						log.warn('Delete Level 3');

						Promise.all([
							/** cognitiveIndicator **/
							delMatterGroups({
								query: {
									indicator: cognitiveIndicator
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),
							delTestGroups({
								query: {
									indicator: cognitiveIndicator
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),

							/** psychomotorIndicator **/
							delMatterGroups({
								query: {
									indicator: psychomotorIndicator
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),
							delTestGroups({
								query: {
									indicator: psychomotorIndicator
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),

							/** matter **/
							delTestGroups({
								query: {
									matter: matter
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),

							/** test **/
							delPsychomotorTests({
								query: {
									test: test
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),
							delCognitiveTests({
								query: {
									test: test
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),

							/** matter **/
							delMatterRequires({ // not have child
								query: {
									matter: matter
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),
							delReferences({ // not have child
								query: {
									matter: matter
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),
							delStudentMatters({ // not have child
								query: {
									matter: matter
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),

							/** test **/
							delStudentTests({ // not have child
								query: {
									test: test
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							})
						]).then(([
							matterGroupCognitive,
							testGroupCognitive,
							matterGroupPsychomotor,
							testGroupPsychomotor,
							testGroup,
							psychomotorTest,
							cognitiveTest
						]) => {
							log.warn('Delete Level 4');

							Promise.all([
								/** matterGroupCognitive **/
								delMatters({
									query: {
										matterGroup: matterGroupCognitive
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** testGroupCognitive **/
								delTests({
									query: {
										testGroup: testGroupCognitive
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** matterGroupPsychomotor **/
								delMatters({
									query: {
										matterGroup: matterGroupPsychomotor
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** testGroupPsychomotor **/
								delTests({
									query: {
										testGroup: testGroupPsychomotor
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** testGroup **/
								delTests({
									query: {
										testGroup: testGroup
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** cognitiveTest **/
								delMcQuestions({
									query: {
										_id: cognitiveTest
											.reduce((prev, item) => item, {
												mcQuestion: undefined
											}).mcQuestion
									}
								}),

								/** cognitiveTest **/
								delStudentCognitiveTests({ // not have child
									query: {
										cognitiveTest: cognitiveTest
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** psychomotorTest **/
								delStudentPsychomotorTests({ // not have child
									query: {
										psychomotorTest: psychomotorTest
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),
								delEssayQuestions({ // not have child
									query: {
										_id: psychomotorTest
											.reduce((prev, item) => item, {
												essayQuestion: undefined
											}).essayQuestion
									}
								})
							]).then(([
								matterCognitive,
								testCognitive,
								matterPsychomotor,
								testPsychomotor,
								test,
								mcQuestion
							]) => {
								log.warn('Delete Level 5');

								Promise.all([
									/** matterCognitive **/
									delTestGroups({
										query: {
											matter: matterCognitive
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** testCognitive **/
									delPsychomotorTests({
										query: {
											test: testCognitive
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delCognitiveTests({
										query: {
											test: testCognitive
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** matterPsychomotor **/
									delTestGroups({
										query: {
											matter: matterPsychomotor
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** testPsychomotor **/
									delPsychomotorTests({
										query: {
											test: testPsychomotor
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delCognitiveTests({
										query: {
											test: testPsychomotor
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** test **/
									delPsychomotorTests({
										query: {
											test: test
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delCognitiveTests({
										query: {
											test: test
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** matterCognitive **/
									delMatterRequires({ // not have child
										query: {
											matter: matterCognitive
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delReferences({ // not have child
										query: {
											matter: matterCognitive
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delStudentMatters({ // not have child
										query: {
											matter: matterCognitive
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** testCognitive **/
									delStudentTests({ // not have child
										query: {
											test: testCognitive
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** matterPsychomotor **/
									delMatterRequires({ // not have child
										query: {
											matter: matterPsychomotor
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delReferences({ // not have child
										query: {
											matter: matterPsychomotor
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delStudentMatters({ // not have child
										query: {
											matter: matterPsychomotor
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** testPsychomotor **/
									delStudentTests({ // not have child
										query: {
											test: testPsychomotor
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** test **/
									delStudentTests({ // not have child
										query: {
											test: test
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** mcQuestion **/
									delMcQuestionOptions({ // not have child
										query: {
											mcQuestion: mcQuestion
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									})
								]).then(([
									testGroupCognitive,
									psychomotorTestCognitive,
									cognitiveTestCognitive,
									testGroupPsychomotor,
									psychomotorTestPsychomotor,
									cognitiveTestPsychomotor,
									psychomotorTest,
									cognitiveTest
								]) => {
									log.warn('Delete Level 6');

									Promise.all([
										/** testGroupCognitive **/
										delTests({
											query: {
												testGroup: testGroupCognitive
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										}),

										/** cognitiveTestCognitive **/
										delMcQuestions({
											query: {
												_id: cognitiveTestCognitive
													.reduce((prev, item) => item, {
														mcQuestion: undefined
													}).mcQuestion
											}
										}),

										/** testGroupPsychomotor **/
										delTests({
											query: {
												testGroup: testGroupPsychomotor
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										}),

										/** cognitiveTestPsychomotor **/
										delMcQuestions({
											query: {
												_id: cognitiveTestPsychomotor
													.reduce((prev, item) => item, {
														mcQuestion: undefined
													}).mcQuestion
											}
										}),

										/** cognitiveTest **/
										delMcQuestions({
											query: {
												_id: cognitiveTest
													.reduce((prev, item) => item, {
														mcQuestion: undefined
													}).mcQuestion
											}
										}),

										/** psychomotorTestCognitive **/
										delStudentPsychomotorTests({ // not have child
											query: {
												psychomotorTest: psychomotorTestCognitive
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										}),
										delEssayQuestions({ // not have child
											query: {
												_id: psychomotorTestCognitive
													.reduce((prev, item) => item, {
														essayQuestion: undefined
													}).essayQuestion
											}
										}),

										/** cognitiveTestCognitive **/
										delStudentCognitiveTests({ // not have child
											query: {
												cognitiveTest: cognitiveTestCognitive
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										}),

										/** psychomotorTestPsychomotor **/
										delStudentPsychomotorTests({ // not have child
											query: {
												psychomotorTest: psychomotorTestPsychomotor
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										}),
										delEssayQuestions({ // not have child
											query: {
												_id: psychomotorTestPsychomotor
													.reduce((prev, item) => item, {
														essayQuestion: undefined
													}).essayQuestion
											}
										}),

										/** cognitiveTestPsychomotor **/
										delStudentCognitiveTests({ // not have child
											query: {
												cognitiveTest: cognitiveTestPsychomotor
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										}),

										/** psychomotorTest **/
										delStudentPsychomotorTests({ // not have child
											query: {
												psychomotorTest: psychomotorTest
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										}),
										delEssayQuestions({ // not have child
											query: {
												_id: psychomotorTest
													.reduce((prev, item) => item, {
														essayQuestion: undefined
													}).essayQuestion
											}
										}),

										/** cognitiveTest **/
										delStudentCognitiveTests({ // not have child
											query: {
												cognitiveTest: cognitiveTest
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										})
									]).then(([
										testCognitive,
										mcQuestionCognitive,
										testPsychomotor,
										mcQuestionPsychomotor,
										mcQuestion
									]) => {
										log.warn('Delete Level 7');

										Promise.all([
											/** testCognitive **/
											delPsychomotorTests({
												query: {
													test: testCognitive
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),
											delCognitiveTests({
												query: {
													test: testCognitive
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),

											/** testPsychomotor **/
											delPsychomotorTests({
												query: {
													test: testPsychomotor
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),
											delCognitiveTests({
												query: {
													test: testPsychomotor
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),

											/** testCognitive **/
											delStudentTests({ // not have child
												query: {
													test: testCognitive
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),

											/** testPsychomotor **/
											delStudentTests({ // not have child
												query: {
													test: testPsychomotor
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),

											/** mcQuestionCognitive **/
											delMcQuestionOptions({ // not have child
												query: {
													mcQuestion: mcQuestionCognitive
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),

											/** mcQuestionPsychomotor **/
											delMcQuestionOptions({ // not have child
												query: {
													mcQuestion: mcQuestionPsychomotor
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											}),

											/** mcQuestionPsychomotor **/
											delMcQuestionOptions({ // not have child
												query: {
													mcQuestion: mcQuestion
														.reduce((prev, item) => item, {
															_id: undefined
														})._id
												}
											})
										]).then(([
											psychomotorTestCognitive,
											cognitiveTestCognitive,
											psychomotorTestPsychomotor,
											cognitiveTestPsychomotor
										]) => {
											log.warn('Delete Level 8');

											Promise.all([
												/** cognitiveTestCognitive **/
												delMcQuestions({
													query: {
														_id: cognitiveTestCognitive
															.reduce((prev, item) => item, {
																mcQuestion: undefined
															}).mcQuestion
													}
												}),

												/** cognitiveTestPsychomotor **/
												delMcQuestions({
													query: {
														_id: cognitiveTestPsychomotor
															.reduce((prev, item) => item, {
																mcQuestion: undefined
															}).mcQuestion
													}
												}),

												/** psychomotorTestCognitive **/
												delStudentPsychomotorTests({ // not have child
													query: {
														psychomotorTest: psychomotorTestCognitive
															.reduce((prev, item) => item, {
																_id: undefined
															})._id
													}
												}),
												delEssayQuestions({ // not have child
													query: {
														_id: psychomotorTestCognitive
															.reduce((prev, item) => item, {
																essayQuestion: undefined
															}).essayQuestion
													}
												}),

												/** cognitiveTestCognitive **/
												delStudentCognitiveTests({ // not have child
													query: {
														cognitiveTest: cognitiveTestCognitive
															.reduce((prev, item) => item, {
																_id: undefined
															})._id
													}
												}),

												/** psychomotorTestPsychomotor **/
												delStudentPsychomotorTests({ // not have child
													query: {
														psychomotorTest: psychomotorTestPsychomotor
															.reduce((prev, item) => item, {
																_id: undefined
															})._id
													}
												}),
												delEssayQuestions({ // not have child
													query: {
														_id: psychomotorTestPsychomotor
															.reduce((prev, item) => item, {
																essayQuestion: undefined
															}).essayQuestion
													}
												}),

												/** cognitiveTestPsychomotor **/
												delStudentCognitiveTests({ // not have child
													query: {
														cognitiveTest: cognitiveTestPsychomotor
															.reduce((prev, item) => item, {
																_id: undefined
															})._id
													}
												}),
											]).then(([
												mcQuestionCognitive,
												mcQuestionPsychomotor
											]) => {
												log.warn('Delete Level 9');

												Promise.all([
													/** mcQuestionCognitive **/
													delMcQuestionOptions({ // not have child
														query: {
															mcQuestion: mcQuestionCognitive
																.reduce((prev, item) => item, {
																	_id: undefined
																})._id
														}
													}),

													/** mcQuestionPsychomotor **/
													delMcQuestionOptions({ // not have child
														query: {
															mcQuestion: mcQuestionPsychomotor
																.reduce((prev, item) => item, {
																	_id: undefined
																})._id
														}
													}),
												]).then(() => {
													resolve(pairCompetency);
												}).catch((err) => {
													reject(err);
												});
											}).catch((err) => {
												reject(err);
											});
										}).catch((err) => {
											reject(err);
										});
									}).catch((err) => {
										reject(err);
									});
								}).catch();
							}).catch((err) => {
								reject(err);
							});
						}).catch((err) => {
							reject(err);
						});
					}).catch((err) => {
						reject(err);
					});
				}).catch((err) => {
					reject(err);
				});
			}).catch((err) => {
				reject(err);
			});
	});
}
