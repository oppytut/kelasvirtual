import mcQuestionModel from '../model/mcQuestion.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

const log = Log('bridge:mcQuestion');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.letterAnswer))
		if(!validator.isNumeric(data.letterAnswer))
			error.letterAnswer = 'letterAnswer is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.question))
		error.question = 'question is required!';

	return error;
}

export function getMcQuestions({query, field, populate=[], limit=0}) {
	log.trace('getMcQuestions');

	return new Promise((resolve, reject) => {
		mcQuestionModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, mcQuestion) => {
				if(err) {
					reject(err);
				} else {
					resolve(mcQuestion);
				}
			});
	});
}

export function getMcQuestion({id}) {
	log.trace('getMcQuestion');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			mcQuestionModel.findById(id, (err, mcQuestion) => {
				if(err) {
					reject(err);
				} else {
					resolve(mcQuestion);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postMcQuestion({data}) {
	log.trace('postMcQuestion');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {question, answer, letterAnswer, info} = data;
			var mcQuestion = new mcQuestionModel();

			mcQuestion.question = question;
			mcQuestion.answer = answer;
			mcQuestion.letterAnswer = letterAnswer;
			mcQuestion.info = info;

			mcQuestion.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(mcQuestion);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putMcQuestion({id, data}) {
	log.trace('putMcQuestion');

	return new Promise((resolve, reject) => {
		const {question, answer, letterAnswer, info, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			mcQuestionModel.findById(id, (err, mcQuestion) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(mcQuestion)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(question)) {
								log.trace('question changed');
								mcQuestion.question = question;
							}

							if(!isEmpty(answer)) {
								log.trace('answer changed');
								mcQuestion.answer = answer;
							}

							if(!isEmpty(letterAnswer)) {
								log.trace('letterAnswer changed');
								mcQuestion.letterAnswer = letterAnswer;
							}

							if(!isEmpty(info)) {
								log.trace('info changed');
								mcQuestion.info = info;
							}

							mcQuestion.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(mcQuestion);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'mcQuestion is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMcQuestion({id}) {
	log.trace('delMcQuestion');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			mcQuestionModel.findByIdAndRemove(id, (err, mcQuestion) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(mcQuestion)) {
						resolve(mcQuestion);
					} else {
						const error = 'mcQuestion is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMcQuestions({query}) {
	log.trace('delMcQuestions');

	return new Promise((resolve, reject) => {
		getMcQuestions({query: query, field: '_id'})
			.then((mcQuestion) => {
				mcQuestionModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(mcQuestion);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}
