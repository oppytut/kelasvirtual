import userModel from '../model/user.js';
import Log from 'debug-logger';

const log = Log('bridge:user');

export function getUsers({query, field, populate=[], limit=0}) {
	log.trace('getUsers');
	
	return new Promise((resolve, reject) => {
		userModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, user) => {
				if(err) {
					reject(err);
				} else {
					resolve(user);
				}
			});
	});
}