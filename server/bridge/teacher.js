import teacherModel from '../model/teacher.js';
import Log from 'debug-logger';

const log = Log('bridge:teacher');

export function getTeachers({query, field, populate=[], limit}) {
	log.trace('getTeachers');
	
	return new Promise((resolve, reject) => {
		teacherModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, teacher) => {
				if(err) {
					reject(err);
				} else {
					resolve(teacher);
				}
			});
	});
}