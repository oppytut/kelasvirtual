import basicCompetencyModel from '../model/basicCompetency.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import {getCoreCompetencys} from './coreCompetency.js';

const log = Log('bridge:basicCompetency');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(isEmpty(data.number))
		error.number = 'number is required!';

	if(isEmpty(data.competency))
		error.competency = 'competency is required!';

	if(isEmpty(data.coreCompetency))
		error.coreCompetency = 'coreCompetency is required!';

	if(!isEmpty(data.number))
		if(!validator.isNumeric(data.number.toString()))
			error.number = 'number is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(!isEmpty(data.coreCompetency))
		if(!mongoose.Types.ObjectId.isValid(data.coreCompetency))
			error.coreCompetency = 'coreCompetency is not valid!';

	return error;
}

export function getBasicCompetencys({query, field, populate=[], limit=0}) {
	log.trace('getBasicCompetencys');

	return new Promise((resolve, reject) => {
		basicCompetencyModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, basicCompetency) => {
				if(err) {
					reject(err);
				} else {
					resolve(basicCompetency);
				}
			});
	});
}

export function getBasicCompetency({id}) {
	log.trace('getBasicCompetency');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			basicCompetencyModel.findById(id, (err, basicCompetency) => {
				if(err) {
					reject(err);
				} else {
					resolve(basicCompetency);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postBasicCompetency({data}) {
	log.trace('postBasicCompetency');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {number, competency, coreCompetency} = data;
			var basicCompetency = new basicCompetencyModel();

			basicCompetency.number = number;
			basicCompetency.competency = competency;
			basicCompetency.coreCompetency = coreCompetency;

			basicCompetency.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(basicCompetency);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putBasicCompetency({id, data}) {
	log.trace('putBasicCompetency');

	return new Promise((resolve, reject) => {
		const {number, competency, coreCompetency, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			basicCompetencyModel.findById(id, (err, basicCompetency) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(basicCompetency)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(number)) {
								log.trace('number changed');
								basicCompetency.number = number;
							}

							if(!isEmpty(competency)) {
								log.trace('competency changed');
								basicCompetency.competency = competency;
							}

							if(!isEmpty(coreCompetency)) {
								log.trace('coreCompetency changed');
								basicCompetency.coreCompetency = coreCompetency;
							}

							basicCompetency.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(basicCompetency);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'basicCompetency is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delBasicCompetency({id}) {
	log.trace('delBasicCompetency');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			basicCompetencyModel.findByIdAndRemove(id, (err, basicCompetency) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(basicCompetency)) {
						resolve(basicCompetency);
					} else {
						const error = 'basicCompetency is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delBasicCompetencys({query}) {
	log.trace('delBasicCompetencys');

	return new Promise((resolve, reject) => {
		getBasicCompetencys({query: query, field: '_id'})
			.then((basicCompetency) => {
				basicCompetencyModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(basicCompetency);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}

export function getBasicCompetencysByCourse({query, field, populate, limit}) {
	log.trace('getBasicCompetencysByCourse');

	return new Promise((resolve, reject) => {
		getCoreCompetencys({query: query.coreCompetency, field: '_id'})
			.then((coreCompetency) => {
				if(isEmpty(query.this))
					query.this = {};

				query.this.coreCompetency = {$in: coreCompetency};

				getBasicCompetencys({query: query.this, field, populate, limit})
					.then((basicCompetency) => {
						resolve(basicCompetency);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}
