import courseModel from '../model/course.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import bcrypt from 'bcrypt';

const log = Log('bridge:course');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.school))
		if(!mongoose.Types.ObjectId.isValid(data.school))
			error.school = 'school is not valid!';

	if(!isEmpty(data.department))
		if(!mongoose.Types.ObjectId.isValid(data.department))
			error.department = 'department is not valid!';

	if(!isEmpty(data.grade))
		if(!mongoose.Types.ObjectId.isValid(data.grade))
			error.grade = 'grade is not valid!';

	if(!isEmpty(data.teachingYear))
		if(!mongoose.Types.ObjectId.isValid(data.teachingYear))
			error.teachingYear = 'teachingYear is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.name))
		error.name = 'name is required!';

	return error;
}

export function getCourses({query, field, populate=[], limit=0}) {
	log.trace('getCourses');

	return new Promise((resolve, reject) => {
		courseModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, course) => {
				if(err) {
					reject(err);
				} else {
					resolve(course);
				}
			});
	});
}

export function getCourse({id}) {
	log.trace('getCourse');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			courseModel.findById(id, (err, course) => {
				if(err) {
					reject(err);
				} else {
					resolve(course);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postCourse({data}) {
	log.trace('postCourse');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {name, classname, password, school, department, grade, teachingYear} = data;
			var course = new courseModel();

			course.name = name;
			course.classname = classname;
			course.school = school;
			course.department = department;
			course.grade = grade;
			course.teachingYear = teachingYear;
			if(!isEmpty(password))
				course.password = bcrypt.hashSync(password, 10);

			course.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(course);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putCourse({id, data}) {
	log.trace('putCourse');

	return new Promise((resolve, reject) => {
		const {name, classname, password, school, department, grade, teachingYear, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			courseModel.findById(id, (err, course) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(course)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(name)) {
								log.trace('name changed');
								course.name = name;
							}

							if(!isEmpty(classname)) {
								log.trace('classname changed');
								course.classname = classname;
							}

							if(!isEmpty(password)) {
								log.trace('password changed');
								course.password = bcrypt.hashSync(password, 10);
							}

							if(!isEmpty(school)) {
								log.trace('school changed');
								course.school = school;
							}

							if(!isEmpty(department)) {
								log.trace('department changed');
								course.department = department;
							}

							if(!isEmpty(grade)) {
								log.trace('grade changed');
								course.grade = grade;
							}

							if(!isEmpty(teachingYear)) {
								log.trace('teachingYear changed');
								course.teachingYear = teachingYear;
							}

							course.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(course);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'course is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delCourse({id}) {
	log.trace('delCourse');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			courseModel.findByIdAndRemove(id, (err, course) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(course)) {
						resolve(course);
					} else {
						const error = 'course is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delCourses({query}) {
	log.trace('delCourses');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			courseModel.find(query)
				.exec((err, course) => {
					if(err) {
						reject(err);
					} else {
						resolve(course);
					}
				});
		}).then((course) => {
			courseModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(course);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}

export function checkPassword({id, data}) {
	log.trace('checkPassword');

	return new Promise((resolve, reject) => {
		const {password} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			courseModel.findById(id, (err, course) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(course)) {
						var isTrue = bcrypt.compareSync(password, course.password);

						resolve(isTrue);
					} else {
						const error = 'course is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}
