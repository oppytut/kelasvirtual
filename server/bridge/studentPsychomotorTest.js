import studentPsychomotorTestModel from '../model/studentPsychomotorTest.js';
import Log from 'debug-logger';

const log = Log('bridge:studentPsychomotorTest');

export function getStudentPsychomotorTests({query, field, populate=[], limit=0}) {
	log.trace('getStudentPsychomotorTests');

	return new Promise((resolve, reject) => {
		studentPsychomotorTestModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, studentPsychomotorTest) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentPsychomotorTest);
				}
			});
	});
}

export function delStudentPsychomotorTests({query}) {
	log.trace('delStudentPsychomotorTests');

	return new Promise((resolve, reject) => {
		getStudentPsychomotorTests({query: query, field: '_id'})
			.then((studentPsychomotorTest) => {
				studentPsychomotorTestModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(studentPsychomotorTest);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}
