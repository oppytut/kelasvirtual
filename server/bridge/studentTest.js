import studentTestModel from '../model/studentTest.js';
import Log from 'debug-logger';

const log = Log('bridge:studentTest');

export function getStudentTests({query, field, populate=[], limit=0}) {
	log.trace('getStudentTests');

	return new Promise((resolve, reject) => {
		studentTestModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, studentTest) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentTest);
				}
			});
	});
}

export function delStudentTests({query}) {
	log.trace('delStudentTests');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			studentTestModel.find(query)
				.exec((err, studentTest) => {
					if(err) {
						reject(err);
					} else {
						resolve(studentTest);
					}
				});
		}).then((studentTest) => {
			studentTestModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentTest);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
