import indicatorModel from '../model/indicator.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import {
	getBasicCompetencysByCourse
} from './basicCompetency.js';

import {
	delMatterGroups
} from './matterGroup.js';

import {
	delMatters
} from './matter.js';

import {
	delMatterRequires
} from './matterRequire.js';

import {
	delReferences
} from './reference.js';

import {
	delStudentMatters
} from './studentMatter.js';

import {
	delTestGroups
} from './testGroup.js';

import {
	delTests
} from './test.js';

import {
	delPsychomotorTests
} from './psychomotorTest.js';

import {
	delStudentPsychomotorTests
} from './studentPsychomotorTest.js';

import {
	delEssayQuestions
} from './essayQuestion.js';

import {
	delCognitiveTests
} from './cognitiveTest.js';

import {
	delStudentCognitiveTests
} from './studentCognitiveTest.js';

import {
	delMcQuestions
} from './mcQuestion.js';

import {
	delMcQuestionOptions
} from './mcQuestionOption.js';

import {
	delStudentTests
} from './studentTest.js';

const log = Log('bridge:indicator');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.basicCompetency))
		if(!mongoose.Types.ObjectId.isValid(data.basicCompetency))
			error.basicCompetency = 'basicCompetency is not valid!';

	if(!isEmpty(data.number))
		if(!validator.isNumeric(data.number.toString()))
			error.number = 'number is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.number))
		error.number = 'number is required!';

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.basicCompetency))
		error.basicCompetency = 'basicCompetency is required!';

	return error;
}

export function getIndicators({query, field, populate=[], limit=0}) {
	log.trace('getIndicators');

	return new Promise((resolve, reject) => {
		indicatorModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, indicator) => {
				if(err) {
					reject(err);
				} else {
					resolve(indicator);
				}
			});
	});
}

export function getIndicator({id}) {
	log.trace('getIndicator');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			indicatorModel.findById(id, (err, indicator) => {
				if(err) {
					reject(err);
				} else {
					resolve(indicator);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postIndicator({data}) {
	log.trace('postIndicator');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {number, content, basicCompetency} = data;
			var indicator = new indicatorModel();

			indicator.number = number;
			indicator.content = content;
			indicator.basicCompetency = basicCompetency;

			indicator.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(indicator);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putIndicator({id, data}) {
	log.trace('putIndicator');

	return new Promise((resolve, reject) => {
		const {number, content, basicCompetency, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			indicatorModel.findById(id, (err, indicator) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(indicator)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(number)) {
								log.trace('number changed');
								indicator.number = number;
							}

							if(!isEmpty(content)) {
								log.trace('content changed');
								indicator.content = content;
							}

							if(!isEmpty(basicCompetency)) {
								log.trace('basicCompetency changed');
								indicator.basicCompetency = basicCompetency;
							}

							indicator.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(indicator);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'indicator is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delIndicator({id}) {
	log.trace('delIndicator');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			indicatorModel.findByIdAndRemove(id, (err, indicator) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(indicator)) {
						resolve(indicator);
					} else {
						const error = 'indicator is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delIndicators({query}) {
	log.trace('delIndicators');

	return new Promise((resolve, reject) => {
		getIndicators({query}).then((indicator) => {
			indicatorModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(indicator);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}

export function getIndicatorsByCourse({query, field, populate, limit}) {
	log.trace('getIndicatorsByCourse');

	return new Promise((resolve, reject) => {
		getBasicCompetencysByCourse({query: query.basicCompetency, field: '_id'})
			.then((basicCompetency) => {
				if(isEmpty(query.this))
					query.this = {};

				query.this.basicCompetency = {$in: basicCompetency};

				getIndicators({query: query.this, field, populate, limit})
					.then((indicator) => {
						resolve(indicator);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}

export function delIndicatorsChild({query}) {
	log.warn('delIndicatorsChild');

	return new Promise((resolve, reject) => {
		delIndicators({query: query}).then((indicator) => {
			Promise.all([
				delMatterGroups({
					query: {
						indicator: indicator
							.reduce((prev, item) => item, {
								_id: undefined
							})._id
					}
				}),
				delTestGroups({
					query: {
						indicator: indicator
							.reduce((prev, item) => item, {
								_id: undefined
							})._id
					}
				})
			]).then(([
				matterGroup,
				testGroup
			]) => {
				Promise.all([
					/** matterGroup **/
					delMatters({
						query: {
							matterGroup: matterGroup
								.reduce((prev, item) => item, {
									_id: undefined
								})._id
						}
					}),
					/** testGroup **/
					delTests({
						query: {
							testGroup: testGroup
								.reduce((prev, item) => item, {
									_id: undefined
								})._id
						}
					})
				]).then(([
					matter,
					test
				]) => {
					Promise.all([
						/** matter **/
						delTestGroups({
							query: {
								matter: matter
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),

						/** test **/
						delPsychomotorTests({
							query: {
								test: test
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),
						delCognitiveTests({
							query: {
								test: test
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),

						/** matter **/
						delMatterRequires({ // not have child
							query: {
								matter: matter
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),
						delReferences({ // not have child
							query: {
								matter: matter
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),
						delStudentMatters({ // not have child
							query: {
								matter: matter
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						}),

						/** test **/
						delStudentTests({ // not have child
							query: {
								test: test
									.reduce((prev, item) => item, {
										_id: undefined
									})._id
							}
						})
					]).then(([
						testGroup,
						psychomotorTest,
						cognitiveTest
					]) => {
						Promise.all([
							/** testGroup **/
							delTests({
								query: {
									testGroup: testGroup
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),

							/** cognitiveTest **/
							delMcQuestions({
								query: {
									_id: cognitiveTest
										.reduce((prev, item) => item, {
											mcQuestion: undefined
										}).mcQuestion
								}
							}),

							/** cognitiveTest **/
							delStudentCognitiveTests({ // not have child
								query: {
									cognitiveTest: cognitiveTest
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),

							/** psychomotorTest **/
							delStudentPsychomotorTests({ // not have child
								query: {
									psychomotorTest: psychomotorTest
										.reduce((prev, item) => item, {
											_id: undefined
										})._id
								}
							}),
							delEssayQuestions({ // not have child
								query: {
									_id: psychomotorTest
										.reduce((prev, item) => item, {
											essayQuestion: undefined
										}).essayQuestion
								}
							})
						]).then(([
							test,
							mcQuestion
						]) => {
							Promise.all([
								/** test **/
								delPsychomotorTests({
									query: {
										test: test
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),
								delCognitiveTests({
									query: {
										test: test
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** test **/
								delStudentTests({ // not have child
									query: {
										test: test
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								}),

								/** mcQuestion **/
								delMcQuestionOptions({ // not have child
									query: {
										mcQuestion: mcQuestion
											.reduce((prev, item) => item, {
												_id: undefined
											})._id
									}
								})
							]).then(([
								psychomotorTest,
								cognitiveTest
							]) => {
								Promise.all([
									/** cognitiveTest **/
									delMcQuestions({
										query: {
											_id: cognitiveTest
												.reduce((prev, item) => item, {
													mcQuestion: undefined
												}).mcQuestion
										}
									}),

									/** cognitiveTest **/
									delStudentCognitiveTests({ // not have child
										query: {
											cognitiveTest: cognitiveTest
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),

									/** psychomotorTest **/
									delStudentPsychomotorTests({ // not have child
										query: {
											psychomotorTest: psychomotorTest
												.reduce((prev, item) => item, {
													_id: undefined
												})._id
										}
									}),
									delEssayQuestions({ // not have child
										query: {
											_id: psychomotorTest
												.reduce((prev, item) => item, {
													essayQuestion: undefined
												}).essayQuestion
										}
									})
								]).then(([
									mcQuestion
								]) => {
									Promise.all([
										/** mcQuestion **/
										delMcQuestionOptions({ // not have child
											query: {
												mcQuestion: mcQuestion
													.reduce((prev, item) => item, {
														_id: undefined
													})._id
											}
										})
									]).then(() => {
										resolve(indicator);
									}).catch((err) => {
										reject(err);
									});
								}).catch((err) => {
									reject(err);
								});
							}).catch((err) => {
								reject(err);
							});
						}).catch((err) => {
							reject(err);
						});
					}).catch((err) => {
						reject(err);
					});
				}).catch((err) => {
					reject(err);
				});
			}).catch((err) => {
				reject(err);
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
