import mcQuestionOptionModel from '../model/mcQuestionOption.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

const log = Log('bridge:mcQuestionOption');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.mcQuestion))
		if(!mongoose.Types.ObjectId.isValid(data.mcQuestion))
			error.mcQuestion = 'mcQuestion is not valid!';

	if(!isEmpty(data.letter))
		if(!validator.isNumeric(data.letter))
			error.letter = 'letter is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.mcQuestion))
		error.mcQuestion = 'mcQuestion is required!';

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.answer))
		error.answer = 'answer is required!';

	return error;
}

export function getMcQuestionOptions({query, field, populate=[], limit=0}) {
	log.trace('getMcQuestionOptions');

	return new Promise((resolve, reject) => {
		mcQuestionOptionModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, mcQuestionOption) => {
				if(err) {
					reject(err);
				} else {
					resolve(mcQuestionOption);
				}
			});
	});
}

export function getMcQuestionOption({id}) {
	log.trace('getMcQuestionOption');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			mcQuestionOptionModel.findById(id, (err, mcQuestionOption) => {
				if(err) {
					reject(err);
				} else {
					resolve(mcQuestionOption);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postMcQuestionOption({data}) {
	log.trace('postMcQuestionOption');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {mcQuestion, content, letter, answer} = data;
			var mcQuestionOption = new mcQuestionOptionModel();

			mcQuestionOption.mcQuestion = mcQuestion;
			mcQuestionOption.content = content;
			mcQuestionOption.letter = letter;
			mcQuestionOption.answer = answer;

			mcQuestionOption.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(mcQuestionOption);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putMcQuestionOption({id, data}) {
	log.trace('putMcQuestionOption');

	return new Promise((resolve, reject) => {
		const {mcQuestion, content, letter, answer, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			mcQuestionOptionModel.findById(id, (err, mcQuestionOption) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(mcQuestionOption)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(mcQuestion)) {
								log.trace('mcQuestion changed');
								mcQuestionOption.mcQuestion = mcQuestion;
							}

							if(!isEmpty(content)) {
								log.trace('content changed');
								mcQuestionOption.content = content;
							}

							if(!isEmpty(answer)) {
								log.trace('answer changed');
								mcQuestionOption.answer = answer;
							}

							if(!isEmpty(letter)) {
								log.trace('letter changed');
								mcQuestionOption.letter = letter;
							}

							mcQuestionOption.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(mcQuestionOption);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'mcQuestionOption is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMcQuestionOption({id}) {
	log.trace('delMcQuestionOption');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			mcQuestionOptionModel.findByIdAndRemove(id, (err, mcQuestionOption) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(mcQuestionOption)) {
						resolve(mcQuestionOption);
					} else {
						const error = 'mcQuestionOption is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delMcQuestionOptions({query}) {
	log.trace('delMcQuestionOptions');

	return new Promise((resolve, reject) => {
		getMcQuestionOptions({query: query, field: '_id'})
			.then((mcQuestionOption) => {
				mcQuestionOptionModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(mcQuestionOption);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}
