import psychomotorTestModel from '../model/psychomotorTest.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

const log = Log('bridge:psychomotorTest');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(isEmpty(data.test))
		if(!mongoose.Types.ObjectId.isValid(data.test))
			error.test = 'test is not valid!';

	if(isEmpty(data.essayQuestion))
		if(!mongoose.Types.ObjectId.isValid(data.essayQuestion))
			error.essayQuestion = 'essayQuestion is not valid!';

	if(!isEmpty(data.number))
		if(!validator.isNumeric(data.number))
			error.number = 'number is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.test))
		error.test = 'test is required!';

	if(isEmpty(data.essayQuestion))
		error.essayQuestion = 'essayQuestion is required!';

	return error;
}

export function getPsychomotorTests({query, field, populate=[], limit=0}) {
	log.trace('getPsychomotorTests');

	return new Promise((resolve, reject) => {
		psychomotorTestModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, psychomotorTest) => {
				if(err) {
					reject(err);
				} else {
					resolve(psychomotorTest);
				}
			});
	});
}

export function getPsychomotorTest({id}) {
	log.trace('getPsychomotorTest');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			psychomotorTestModel.findById(id, (err, psychomotorTest) => {
				if(err) {
					reject(err);
				} else {
					resolve(psychomotorTest);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postPsychomotorTest({data}) {
	log.trace('postPsychomotorTest');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {number, option, test, essayQuestion, answer, consoleAnswer} = data;
			var psychomotorTest = new psychomotorTestModel();

			psychomotorTest.number = number;
			psychomotorTest.option = option;
			psychomotorTest.test = test;
			psychomotorTest.essayQuestion = essayQuestion;
			psychomotorTest.answer = answer;
			psychomotorTest.consoleAnswer = consoleAnswer;

			psychomotorTest.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(psychomotorTest);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putPsychomotorTest({id, data}) {
	log.trace('putPsychomotorTest');

	return new Promise((resolve, reject) => {
		const {number, option, test, essayQuestion, answer, consoleAnswer, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			psychomotorTestModel.findById(id, (err, psychomotorTest) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(psychomotorTest)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(number)) {
								log.trace('number changed');
								psychomotorTest.number = number;
							}

							if(!isEmpty(option)) {
								log.trace('option changed');
								psychomotorTest.option = option;
							}

							if(!isEmpty(test)) {
								log.trace('test changed');
								psychomotorTest.test = test;
							}

							if(!isEmpty(essayQuestion)) {
								log.trace('essayQuestion changed');
								psychomotorTest.essayQuestion = essayQuestion;
							}

							if(!isEmpty(answer)) {
								log.trace('answer changed');
								psychomotorTest.answer = answer;
							}

							if(!isEmpty(consoleAnswer)) {
								log.trace('consoleAnswer changed');
								psychomotorTest.consoleAnswer = consoleAnswer;
							}

							psychomotorTest.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(psychomotorTest);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'psychomotorTest is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delPsychomotorTest({id}) {
	log.trace('delPsychomotorTest');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			psychomotorTestModel.findByIdAndRemove(id, (err, psychomotorTest) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(psychomotorTest)) {
						resolve(psychomotorTest);
					} else {
						const error = 'psychomotorTest is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delPsychomotorTests({query}) {
	log.trace('delPsychomotorTests');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			psychomotorTestModel.find(query)
				.exec((err, psychomotorTest) => {
					if(err) {
						reject(err);
					} else {
						resolve(psychomotorTest);
					}
				});
		}).then((psychomotorTest) => {
			psychomotorTestModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(psychomotorTest);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
