import studentMatterModel from '../model/studentMatter.js';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import {getStudents} from './student.js';

const log = Log('bridge:studentMatter');

export function getStudentMatters({query, field, populate=[], limit=0}) {
	log.trace('getStudentMatters');

	return new Promise((resolve, reject) => {
		studentMatterModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, studentMatter) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentMatter);
				}
			});
	});
}

export function getStudentMattersByCourseUser({query, field, populate, limit}) {
	log.trace('getStudentMattersByCourseUser');

	return new Promise((resolve, reject) => {
		getStudents({query: query.student, field: '_id'})
			.then((student) => {
				if(isEmpty(query.this))
					query.this = {};

				query.this.student = {$in: student};

				getStudentMatters({query: query.this, field, populate, limit})
					.then((studentMatter) => {
						resolve(studentMatter);
					})
					.catch((err) => {
						reject(err);
					});
			})
			.catch((err) => {
				reject(err);
			});
	});
}

export function delStudentMatters({query}) {
	log.trace('delStudentMatters');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			studentMatterModel.find(query)
				.exec((err, studentMatter) => {
					if(err) {
						reject(err);
					} else {
						resolve(studentMatter);
					}
				});
		}).then((studentMatter) => {
			studentMatterModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(studentMatter);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
