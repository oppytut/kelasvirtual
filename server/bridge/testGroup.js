import testGroupModel from '../model/testGroup.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

const log = Log('bridge:testGroup');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.course))
		if(!mongoose.Types.ObjectId.isValid(data.course))
			error.course = 'course is not valid!';

	if(!isEmpty(data.pairCompetency))
		if(!mongoose.Types.ObjectId.isValid(data.pairCompetency))
			error.pairCompetency = 'pairCompetency is not valid!';

	if(!isEmpty(data.indicator))
		if(!mongoose.Types.ObjectId.isValid(data.indicator))
			error.indicator = 'indicator is not valid!';

	if(!isEmpty(data.matter))
		if(!mongoose.Types.ObjectId.isValid(data.matter))
			error.matter = 'matter is not valid!';

	if(!isEmpty(data.name))
		if(!validator.isAlphanumeric(validator.blacklist(data.name, ' ')))
			error.name = 'name is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	return error;
}

export function getTestGroups({query, field, populate=[], limit=0}) {
	log.trace('getTestGroups');

	return new Promise((resolve, reject) => {
		testGroupModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, testGroup) => {
				if(err) {
					reject(err);
				} else {
					resolve(testGroup);
				}
			});
	});
}

export function getTestGroup({id}) {
	log.trace('getTestGroup');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			testGroupModel.findById(id, (err, testGroup) => {
				if(err) {
					reject(err);
				} else {
					resolve(testGroup);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postTestGroup({data}) {
	log.trace('postTestGroup');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {name, course, pairCompetency, indicator, matter} = data;
			var testGroup = new testGroupModel();

			testGroup.name = name;
			testGroup.course = course;
			testGroup.pairCompetency = pairCompetency;
			testGroup.indicator = indicator;
			testGroup.matter = matter;

			testGroup.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(testGroup);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putTestGroup({id, data}) {
	log.trace('putTestGroup');

	return new Promise((resolve, reject) => {
		const {name, course, pairCompetency, indicator, matter, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			testGroupModel.findById(id, (err, testGroup) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(testGroup)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(name)) {
								log.trace('name changed');
								testGroup.name = name;
							}

							if(!isEmpty(course)) {
								log.trace('course changed');
								testGroup.course = course;
							}

							if(!isEmpty(pairCompetency)) {
								log.trace('pairCompetency changed');
								testGroup.pairCompetency = pairCompetency;
							}

							if(!isEmpty(indicator)) {
								log.trace('indicator changed');
								testGroup.indicator = indicator;
							}

							if(!isEmpty(matter)) {
								log.trace('matter changed');
								testGroup.matter = matter;
							}

							testGroup.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(testGroup);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'testGroup is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delTestGroup({id}) {
	log.trace('delTestGroup');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			testGroupModel.findByIdAndRemove(id, (err, testGroup) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(testGroup)) {
						resolve(testGroup);
					} else {
						const error = 'testGroup is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delTestGroups({query}) {
	log.trace('delTestGroups');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			testGroupModel.find(query)
				.exec((err, testGroup) => {
					if(err) {
						reject(err);
					} else {
						resolve(testGroup);
					}
				});
		}).then((testGroup) => {
			testGroupModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(testGroup);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
