import coreCompetencyModel from '../model/coreCompetency.js';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import mongoose from 'mongoose';

const log = Log('bridge:coreCompetency');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.course))
		if(!mongoose.Types.ObjectId.isValid(data.course))
			error.course = 'course is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.domain))
		error.domain = 'domain is required!';

	if(isEmpty(data.competency))
		error.competency = 'competency is required!';

	if(isEmpty(data.course))
		error.course = 'course is required!';

	return error;
}

export function getCoreCompetencys({query, field, populate=[], limit=0}) {
	log.trace('getCoreCompetencys');

	return new Promise((resolve, reject) => {
		coreCompetencyModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, coreCompetency) => {
				if(err) {
					reject(err);
				} else {
					resolve(coreCompetency);
				}
			});
	});
}

export function getCoreCompetency({id}) {
	log.trace('getCoreCompetency');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			coreCompetencyModel.findById(id, (err, coreCompetency) => {
				if(err) {
					reject(err);
				} else {
					resolve(coreCompetency);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postCoreCompetency({data}) {
	log.trace('postCoreCompetency');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {domain, competency, course} = data;
			var coreCompetency = new coreCompetencyModel();

			coreCompetency.domain = domain;
			coreCompetency.competency = competency;
			coreCompetency.course = course;

			coreCompetency.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(coreCompetency);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putCoreCompetency({id, data}) {
	log.trace('putCoreCompetency');

	return new Promise((resolve, reject) => {
		const {domain, competency, course, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			coreCompetencyModel.findById(id, (err, coreCompetency) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(coreCompetency)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(domain)) {
								log.trace('domain changed');
								coreCompetency.domain = domain;
							}

							if(!isEmpty(competency)) {
								log.trace('competency changed');
								coreCompetency.competency = competency;
							}

							if(!isEmpty(course)) {
								log.trace('course changed');
								coreCompetency.course = course;
							}

							coreCompetency.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(coreCompetency);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'coreCompetency is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delCoreCompetency({id}) {
	log.trace('delCoreCompetency');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			coreCompetencyModel.findByIdAndRemove(id, (err, coreCompetency) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(coreCompetency)) {
						resolve(coreCompetency);
					} else {
						const error = 'coreCompetency is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delCoreCompetencys({query}) {
	log.trace('delCoreCompetencys');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			coreCompetencyModel.find(query)
				.exec((err, coreCompetency) => {
					if(err) {
						reject(err);
					} else {
						resolve(coreCompetency);
					}
				});
		}).then((coreCompetency) => {
			coreCompetencyModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(coreCompetency);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}

export function putPostCoreCompetency({query, data}) {
	log.trace('putPostCoreCompetency');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			coreCompetencyModel.find(query)
				.exec((err, coreCompetency) => {
					if(err) {
						reject(err);
					} else {
						resolve(coreCompetency);
					}
				});
		}).then((coreCompetency) => {
			if(isEmpty(coreCompetency)) {
				postCoreCompetency({data: data.post})
					.then((coreCompetency) => {
						resolve(coreCompetency);
					})
					.catch((err) => {
						reject(err);
					});
			} else {
				putCoreCompetency({
					id: coreCompetency.reduce(item => item)._id,
					data: data.put
				})
					.then((coreCompetency) => {
						resolve(coreCompetency);
					})
					.catch((err) => {
						reject(err);
					});
			}
		}).catch((err) => {
			reject(err);
		});
	});
}
