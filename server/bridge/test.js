import testModel from '../model/test.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

const log = Log('bridge:test');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.testGroup))
		if(!mongoose.Types.ObjectId.isValid(data.testGroup))
			error.testGroup = 'testGroup is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.name))
		error.name = 'name is required!';

	return error;
}

export function getTests({query, field, populate=[], limit=0}) {
	log.trace('getTests');

	return new Promise((resolve, reject) => {
		testModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, test) => {
				if(err) {
					reject(err);
				} else {
					resolve(test);
				}
			});
	});
}

export function getTest({id}) {
	log.trace('getTest');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			testModel.findById(id, (err, test) => {
				if(err) {
					reject(err);
				} else {
					resolve(test);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postTest({data}) {
	log.trace('postTest');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {name, testGroup} = data;
			var test = new testModel();

			test.name = name;
			test.testGroup = testGroup;

			test.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(test);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putTest({id, data}) {
	log.trace('putTest');

	return new Promise((resolve, reject) => {
		const {name, testGroup, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			testModel.findById(id, (err, test) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(test)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(name)) {
								log.trace('name changed');
								test.name = name;
							}

							if(!isEmpty(testGroup)) {
								log.trace('testGroup changed');
								test.testGroup = testGroup;
							}

							test.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(test);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'test is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delTest({id}) {
	log.trace('delTest');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			testModel.findByIdAndRemove(id, (err, test) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(test)) {
						resolve(test);
					} else {
						const error = 'test is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delTests({query}) {
	log.trace('delTests');

	return new Promise((resolve, reject) => {
		new Promise((resolve, reject) => {
			testModel.find(query)
				.exec((err, test) => {
					if(err) {
						reject(err);
					} else {
						resolve(test);
					}
				});
		}).then((test) => {
			testModel.remove(query, (err) => {
				if(err) {
					reject(err);
				} else {
					resolve(test);
				}
			});
		}).catch((err) => {
			reject(err);
		});
	});
}
