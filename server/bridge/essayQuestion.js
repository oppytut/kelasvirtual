import essayQuestionModel from '../model/essayQuestion.js';
import mongoose from 'mongoose';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
// import validator from 'validator';

const log = Log('bridge:essayQuestion');

// function validate(data) {
// 	log.trace('validate');
// 	var error = {};

// 	if(!isEmpty(data.question))


// 	return error;
// }

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	// error = validate(data);

	if(isEmpty(data.question))
		error.question = 'question is required!';

	return error;
}

export function getEssayQuestions({query, field, populate=[], limit=0}) {
	log.trace('getEssayQuestions');

	return new Promise((resolve, reject) => {
		essayQuestionModel.find(query, field)
			.populate(populate)
			.limit(limit)
			.exec((err, essayQuestion) => {
				if(err) {
					reject(err);
				} else {
					resolve(essayQuestion);
				}
			});
	});
}

export function getEssayQuestion({id}) {
	log.trace('getEssayQuestion');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			essayQuestionModel.findById(id, (err, essayQuestion) => {
				if(err) {
					reject(err);
				} else {
					resolve(essayQuestion);
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function postEssayQuestion({data}) {
	log.trace('postEssayQuestion');

	return new Promise((resolve, reject) => {
		const error = validateAll(data)	;

		if(isEmpty(error)) {
			const {question, answer, consoleAnswer, usedDB, info} = data;
			var essayQuestion = new essayQuestionModel();

			essayQuestion.question = question;
			essayQuestion.answer = answer;
			essayQuestion.consoleAnswer = consoleAnswer;
			essayQuestion.usedDB = usedDB;
			essayQuestion.info = info;

			essayQuestion.save((err) => {
				if(err) {
					reject(err);
				} else {
					resolve(essayQuestion);
				}
			});
		} else {
			reject({error});
		}
	});
}

export function putEssayQuestion({id, data}) {
	log.trace('putEssayQuestion');

	return new Promise((resolve, reject) => {
		const {question, answer, consoleAnswer, usedDB, info, _all} = data;

		if(mongoose.Types.ObjectId.isValid(id)) {
			essayQuestionModel.findById(id, (err, essayQuestion) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(essayQuestion)) {
						var error = {};

						if(_all) {
							error = validateAll(data);
						} else {
							// error = validate(data);
						}

						if(isEmpty(error)) {
							if(!isEmpty(question)) {
								log.trace('question changed');
								essayQuestion.question = question;
							}

							if(!isEmpty(answer)) {
								log.trace('answer changed');
								essayQuestion.answer = answer;
							}

							if(!isEmpty(consoleAnswer)) {
								log.trace('consoleAnswer changed');
								essayQuestion.consoleAnswer = consoleAnswer;
							}

							if(!isEmpty(usedDB)) {
								log.trace('usedDB changed');
								essayQuestion.usedDB = usedDB;
							}

							if(!isEmpty(info)) {
								log.trace('info changed');
								essayQuestion.info = info;
							}

							essayQuestion.save((err) => {
								if(err) {
									reject(err);
								} else {
									resolve(essayQuestion);
								}
							});
						} else {
							reject({error});
						}
					} else {
						const error = 'essayQuestion is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delEssayQuestion({id}) {
	log.trace('delEssayQuestion');

	return new Promise((resolve, reject) => {
		if(mongoose.Types.ObjectId.isValid(id)) {
			essayQuestionModel.findByIdAndRemove(id, (err, essayQuestion) => {
				if(err) {
					reject(err);
				} else {
					if(!isEmpty(essayQuestion)) {
						resolve(essayQuestion);
					} else {
						const error = 'essayQuestion is not found!';
						log.warn(error);
						reject({error});
					}
				}
			});
		} else {
			const error = 'id is not valid!';
			log.warn(error);
			reject({error});
		}
	});
}

export function delEssayQuestions({query}) {
	log.trace('delEssayQuestions');

	return new Promise((resolve, reject) => {
		getEssayQuestions({query: query, field: '_id'})
			.then((essayQuestion) => {
				essayQuestionModel.remove(query, (err) => {
					if(err) {
						reject(err);
					} else {
						resolve(essayQuestion);
					}
				});
			}).catch((err) => {
				reject(err);
			});
	});
}
