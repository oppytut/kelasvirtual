import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const departmentSchema = new Schema(
	{
		name: {type: String, required: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Department = mongoose.model('Department', departmentSchema);

export default Department;