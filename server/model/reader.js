import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const readerSchema = new Schema(
	{
		message: {type: Schema.Types.ObjectId, ref: 'Message', require: true},
		student: {type: Schema.Types.ObjectId, ref: 'Student'},
		teacher: {type: Schema.Types.ObjectId, ref: 'Teacher'}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Reader = mongoose.model('Reader', readerSchema);

export default Reader;