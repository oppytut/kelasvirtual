import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const basicCompetencySchema = new Schema(
	{
		number: {type: Number, require: true},
		competency: {type: String, require: true},
		coreCompetency: {type: Schema.Types.ObjectId, ref: 'CoreCompetency', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

basicCompetencySchema.virtual('indicator', {
	ref: 'Indicator',
	localField: '_id',
	foreignField: 'basicCompetency'
});

basicCompetencySchema.virtual('objective', {
	ref: 'Objective',
	localField: '_id',
	foreignField: 'basicCompetency'
});

basicCompetencySchema.virtual('cognitive', {
	ref: 'PairCompetency',
	localField: '_id',
	foreignField: 'cognitive'
});

basicCompetencySchema.virtual('psychomotor', {
	ref: 'PairCompetency',
	localField: '_id',
	foreignField: 'psychomotor'
});

const BasicCompetency = mongoose.model('BasicCompetency', basicCompetencySchema);

export default BasicCompetency;