import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const indicatorSchema = new Schema(
	{
		number: {type: Number, required: true},
		content: {type: String, required: true},
		basicCompetency: {type: Schema.Types.ObjectId, ref: 'BasicCompetency', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Indicator = mongoose.model('Indicator', indicatorSchema);

export default Indicator;