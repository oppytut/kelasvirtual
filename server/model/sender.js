import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const senderSchema = new Schema(
	{
		student: {type: Schema.Types.ObjectId, ref: 'Student'},
		teacher: {type: Schema.Types.ObjectId, ref: 'Teacher'}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Sender = mongoose.model('Sender', senderSchema);

export default Sender;