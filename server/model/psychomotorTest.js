import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const psychomotorTestSchema = new Schema(
	{
		number: {type: String},
		option: {type: String},
		answer: String,
		consoleAnswer: String,
		test: {type: Schema.Types.ObjectId, ref: 'Test', require: true},
		essayQuestion: {type: Schema.Types.ObjectId, ref: 'EssayQuestion', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

psychomotorTestSchema.virtual('studentPsychomotorTest', {
	ref: 'StudentPsychomotorTest',
	localField: '_id',
	foreignField: 'psychomotorTest'
});


const PsychomotorTest = mongoose.model('PsychomotorTest', psychomotorTestSchema);

export default PsychomotorTest;