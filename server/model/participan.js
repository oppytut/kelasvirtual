import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const participanSchema = new Schema(
	{
		discussion: {type: Schema.Types.ObjectId, ref: 'Discussion', require: true},
		student: {type: Schema.Types.ObjectId, ref: 'Student'},
		teacher: {type: Schema.Types.ObjectId, ref: 'Teacher'}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Participan = mongoose.model('Participan', participanSchema);

export default Participan;