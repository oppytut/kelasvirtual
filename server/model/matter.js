import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const matterSchema = new Schema(
	{
		number: {type: Number, require: true},
		title: {type: String, require: true},
		content: {type: String, require: true},
		video: {type: String},
		matterGroup: {type: Schema.Types.ObjectId, ref: 'MatterGroup', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

matterSchema.virtual('matterRequire', {
	ref: 'MatterRequire',
	localField: '_id',
	foreignField: 'matter'
});

matterSchema.virtual('studentMatter', {
	ref: 'StudentMatter',
	localField: '_id',
	foreignField: 'matter'
});

matterSchema.virtual('testGroup', {
	ref: 'TestGroup',
	localField: '_id',
	foreignField: 'matter'
});

matterSchema.virtual('reference', {
	ref: 'Reference',
	localField: '_id',
	foreignField: 'matter'
});

const Matter = mongoose.model('Matter', matterSchema);

export default Matter;