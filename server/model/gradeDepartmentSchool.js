import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const gradeDepartmentSchoolSchema = new Schema(
	{
		school: {type: Schema.Types.ObjectId, ref: 'School', required: true},
		department: {type: Schema.Types.ObjectId, ref: 'Department', required: true},
		grade: {type: Schema.Types.ObjectId, ref: 'Grade', required: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const GradeDepartmentSchool = mongoose.model('GradeDepartmentSchool', gradeDepartmentSchoolSchema);

export default GradeDepartmentSchool;