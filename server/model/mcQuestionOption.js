import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const mcQuestionOptionSchema = new Schema(
	{
		mcQuestion: {type: Schema.Types.ObjectId, ref: 'McQuestion', require: true},
		content: {type: String, require: true},
		answer: {type: String, require: true},
		letter: Number
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const McQuestionOption = mongoose.model('McQuestionOption', mcQuestionOptionSchema);

export default McQuestionOption;