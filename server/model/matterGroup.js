import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const matterGroupSchema = new Schema(
	{
		name: {type: String, require: true},
		course: {type: Schema.Types.ObjectId, ref: 'Course', require: true},
		pairCompetency: {type: Schema.Types.ObjectId, ref: 'PairCompetency', require: true},
		indicator: {type: Schema.Types.ObjectId, ref: 'Indicator', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

matterGroupSchema.virtual('matter', {
	ref: 'Matter',
	localField: '_id',
	foreignField: 'matterGroup'
});

const MatterGroup = mongoose.model('MatterGroup', matterGroupSchema);

export default MatterGroup;