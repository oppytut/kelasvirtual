import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const referenceSchema = new Schema(
	{
		content: {type: String, required: true},
		matter: {type: Schema.Types.ObjectId, ref: 'Matter', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Reference = mongoose.model('Reference', referenceSchema);

export default Reference;