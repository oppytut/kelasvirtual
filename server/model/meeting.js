import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const meetingSchema = new Schema(
	{
		content: {type: String, require: true},
		course: {type: Schema.Types.ObjectId, ref: 'Course', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Meeting = mongoose.model('Meeting', meetingSchema);

export default Meeting;