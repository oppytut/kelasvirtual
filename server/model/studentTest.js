import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const studentTestSchema = new Schema(
	{
		test: {type: Schema.Types.ObjectId, ref: 'Test', require: true},
		student: {type: Schema.Types.ObjectId, ref: 'Student', require: true},
		score: String,
		pass: Boolean,
		cognitiveScore: String,
		cognitivePass: Boolean,
		psychomotorScore: String,
		psychomotorPass: Boolean
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const StudentTest = mongoose.model('StudentTest', studentTestSchema);

export default StudentTest;