import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const essayQuestionSchema = new Schema(
	{
		question: {type: String, required: true},
		answer: String,
		consoleAnswer: String,
		usedDB: String, 
		info: String // dev
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const EssayQuestion = mongoose.model('EssayQuestion', essayQuestionSchema);

export default EssayQuestion;