import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const testSchema = new Schema(
	{
		name: {type: String, required: true},
		testGroup: {type: Schema.Types.ObjectId, ref: 'TestGroup', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

testSchema.virtual('cognitiveTest', {
	ref: 'CognitiveTest',
	localField: '_id',
	foreignField: 'test'
});

testSchema.virtual('psychomotorTest', {
	ref: 'PsychomotorTest',
	localField: '_id',
	foreignField: 'test'
});

testSchema.virtual('studentTest', {
	ref: 'StudentTest',
	localField: '_id',
	foreignField: 'test'
});

const Test = mongoose.model('Test', testSchema);

export default Test;