import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const studentMatterSchema = new Schema(
	{
		matter: {type: Schema.Types.ObjectId, ref: 'Matter', require: true},
		student: {type: Schema.Types.ObjectId, ref: 'Student', require: true},
		pass: Boolean,
		videoPass: Boolean
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const StudentMatter = mongoose.model('StudentMatter', studentMatterSchema);

export default StudentMatter;
