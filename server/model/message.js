import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const messageSchema = new Schema(
	{
		content: {type: String, required: true},
		time: {type: String, required: true},
		discussion: {type: Schema.Types.ObjectId, ref: 'Discussion', require: true},
		sender: {type: Schema.Types.ObjectId, ref: 'Sender', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Message = mongoose.model('Message', messageSchema);

export default Message;