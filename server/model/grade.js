import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const gradeSchema = new Schema(
	{
		name: {type: String, required: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Grade = mongoose.model('Grade', gradeSchema);

export default Grade;