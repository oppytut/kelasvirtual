import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const studentCognitiveTestSchema = new Schema(
	{
		cognitiveTest: {type: Schema.Types.ObjectId, ref: 'CognitiveTest', require: true},
		student: {type: Schema.Types.ObjectId, ref: 'Student', require: true},
		score: String,
		pass: Boolean,
		answer: String,
		letterAnswer: String
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const StudentCognitiveTest = mongoose.model('StudentCognitiveTest', studentCognitiveTestSchema);

export default StudentCognitiveTest;