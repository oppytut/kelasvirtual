import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const cognitiveTestSchema = new Schema(
	{
		number: {type: String},
		option: {type: String},
		test: {type: Schema.Types.ObjectId, ref: 'Test', require: true},
		mcQuestion: {type: Schema.Types.ObjectId, ref: 'McQuestion', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

cognitiveTestSchema.virtual('studentCognitiveTest', {
	ref: 'StudentCognitiveTest',
	localField: '_id',
	foreignField: 'cognitiveTest'
});

const CognitiveTest = mongoose.model('CognitiveTest', cognitiveTestSchema);

export default CognitiveTest;