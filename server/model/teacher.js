import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const teacherSchema = new Schema(
	{
		course: {type: Schema.Types.ObjectId, ref: 'Course', required: true},
		user: {type: Schema.Types.ObjectId, ref: 'User', required: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Teacher = mongoose.model('Teacher', teacherSchema);

export default Teacher;