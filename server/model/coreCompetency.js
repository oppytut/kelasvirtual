import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const coreCompetencySchema = new Schema(
	{
		domain: {type: String, require: true},
		competency: {type: String, require: true},
		course: {type: Schema.Types.ObjectId, ref: 'Course', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

coreCompetencySchema.virtual('basicCompetency', {
	ref: 'BasicCompetency',
	localField: '_id',
	foreignField: 'coreCompetency'
});

const CoreCompetency = mongoose.model('CoreCompetency', coreCompetencySchema);

export default CoreCompetency;