import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const pictureSchema = new Schema(
	{
		mini: String,
		tiny: String,
		small: String,
		medium: String,
		large: String,
		big: String,
		huge: String,
		massive: String
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Picture = mongoose.model('Picture', pictureSchema);

export default Picture;