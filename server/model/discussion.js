import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const discussionSchema = new Schema(
	{
		title: {type: String, required: true},
		course: {type: Schema.Types.ObjectId, ref: 'Course', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Discussion = mongoose.model('Discussion', discussionSchema);

export default Discussion;