import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const studentPsychomotorTestSchema = new Schema(
	{
		psychomotorTest: {type: Schema.Types.ObjectId, ref: 'PsychomotorTest', require: true},
		student: {type: Schema.Types.ObjectId, ref: 'Student', require: true},
		score: String,
		pass: Boolean
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const StudentPsychomotorTest = mongoose.model('StudentPsychomotorTest', studentPsychomotorTestSchema);

export default StudentPsychomotorTest;