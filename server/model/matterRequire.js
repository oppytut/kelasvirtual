import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const matterRequireSchema = new Schema(
	{
		matter: {type: Schema.Types.ObjectId, ref: 'Matter', require: true},
		content: {type: Schema.Types.ObjectId, ref: 'Matter', require: true},
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

matterRequireSchema.virtual('studentMatter', {
	ref: 'StudentMatter',
	localField: 'content',
	foreignField: 'matter'
});

const MatterRequire = mongoose.model('MatterRequire', matterRequireSchema);

export default MatterRequire;