import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const identitySchema = new Schema(
	{
		number: {type: String, required: true},
		type: {type: String, required: true},
		user: {type: Schema.Types.ObjectId, ref: 'User', required: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Identity = mongoose.model('Identity', identitySchema);

export default Identity;