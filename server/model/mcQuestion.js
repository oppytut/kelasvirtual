import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const mcQuestionSchema = new Schema(
	{
		question: {type: String, required: true},
		answer: String,
		letterAnswer: Number,
		info: String // dev
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

mcQuestionSchema.virtual('mcQuestionOption', {
	ref: 'McQuestionOption',
	localField: '_id',
	foreignField: 'mcQuestion'
});

const McQuestion = mongoose.model('McQuestion', mcQuestionSchema);

export default McQuestion;