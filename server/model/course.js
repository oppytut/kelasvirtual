import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const courseSchema = new Schema(
	{
		name: {type: String, required: true},
		classname: String,
		password: String,
		school: {type: Schema.Types.ObjectId, ref: 'School'},
		department: {type: Schema.Types.ObjectId, ref: 'Department'},
		grade: {type: Schema.Types.ObjectId, ref: 'Grade'},
		teachingYear: {type: Schema.Types.ObjectId, ref: 'TeachingYear'}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

courseSchema.virtual('coreCompetency', {
	ref: 'CoreCompetency',
	localField: '_id',
	foreignField: 'course'
});

courseSchema.virtual('discussion', {
	ref: 'Discussion',
	localField: '_id',
	foreignField: 'course'
});

courseSchema.virtual('meeting', {
	ref: 'Meeting',
	localField: '_id',
	foreignField: 'course'
});

courseSchema.virtual('matterGroup', {
	ref: 'MatterGroup',
	localField: '_id',
	foreignField: 'course'
});

courseSchema.virtual('testGroup', {
	ref: 'TestGroup',
	localField: '_id',
	foreignField: 'course'
});

courseSchema.virtual('teacher', {
	ref: 'Teacher',
	localField: '_id',
	foreignField: 'course'
});

courseSchema.virtual('student', {
	ref: 'Student',
	localField: '_id',
	foreignField: 'course'
});

courseSchema.virtual('requestStudent', {
	ref: 'RequestStudent',
	localField: '_id',
	foreignField: 'course'
});

const Course = mongoose.model('Course', courseSchema);

export default Course;