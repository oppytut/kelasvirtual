import mongoose from 'mongoose';

import './userPicture.js';

const Schema = mongoose.Schema;

const userSchema = new Schema(
	{
		username: {type: String, required: true, unique: true},
		password: {type: String, required: true},
		email: {type: String, required: true, unique: true},
		name: String,
		gender: String,
		userPicture: {type: Schema.Types.ObjectId, ref: 'UserPicture'}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

userSchema.virtual('student', {
	ref: 'Student',
	localField: '_id',
	foreignField: 'user'
});

userSchema.virtual('teacher', {
	ref: 'Teacher',
	localField: '_id',
	foreignField: 'user'
});

userSchema.virtual('identity', {
	ref: 'Identity',
	localField: '_id',
	foreignField: 'user'
});

const User = mongoose.model('User', userSchema);

export default User;
