import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const studentSchema = new Schema(
	{
		course: 	{type: Schema.Types.ObjectId, ref: 'Course', required: true},
		user: {type: Schema.Types.ObjectId, ref: 'User', required: true},
		lastStudied: {type: Schema.Types.ObjectId, ref: 'pairCompetency'},
		deleted: Boolean
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

studentSchema.virtual('studentMatter', {
	ref: 'StudentMatter',
	localField: '_id',
	foreignField: 'student'
});

const Student = mongoose.model('Student', studentSchema);

export default Student;
