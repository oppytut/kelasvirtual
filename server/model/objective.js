import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const objectiveSchema = new Schema(
	{
		number: {type: Number, required: true},
		content: {type: String, required: true},
		basicCompetency: {type: Schema.Types.ObjectId, ref: 'BasicCompetency', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const Objective = mongoose.model('Objective', objectiveSchema);

export default Objective;