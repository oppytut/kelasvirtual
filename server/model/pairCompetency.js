import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const pairCompetencySchema = new Schema(
	{
		cognitive: {type: Schema.Types.ObjectId, ref: 'BasicCompetency', require: true},
		psychomotor: {type: Schema.Types.ObjectId, ref: 'BasicCompetency', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const PairCompetency = mongoose.model('PairCompetency', pairCompetencySchema);

export default PairCompetency;