import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const teachingYearSchema = new Schema(
	{
		year: {type: String, required: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const TeachingYear = mongoose.model('TeachingYear', teachingYearSchema);

export default TeachingYear;