import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const requestStudentSchema = new Schema(
	{
		course: {type: Schema.Types.ObjectId, ref: 'Course', required: true},
		user: {type: Schema.Types.ObjectId, ref: 'User', required: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const RequestStudent = mongoose.model('RequestStudent', requestStudentSchema);

export default RequestStudent;