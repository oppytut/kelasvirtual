import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const testGroupSchema = new Schema(
	{
		name: {type: String},
		course: {type: Schema.Types.ObjectId, ref: 'Course'},
		pairCompetency: {type: Schema.Types.ObjectId, ref: 'PairCompetency'},
		indicator: {type: Schema.Types.ObjectId, ref: 'Indicator'},
		matter: {type: Schema.Types.ObjectId, ref: 'Matter'}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

testGroupSchema.virtual('test', {
	ref: 'Test',
	localField: '_id',
	foreignField: 'testGroup'
});

const TestGroup = mongoose.model('TestGroup', testGroupSchema);

export default TestGroup;