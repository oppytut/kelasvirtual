import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const studentMeetingSchema = new Schema(
	{
		meeting: {type: Schema.Types.ObjectId, ref: 'Meeting', require: true},
		student: {type: Schema.Types.ObjectId, ref: 'Student', require: true},
		presence: Boolean
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const StudentMeeting = mongoose.model('StudentMeeting', studentMeetingSchema);

export default StudentMeeting;