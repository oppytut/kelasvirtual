import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const schoolSchema = new Schema(
	{
		name: { type: String, required: true },
		address: String,
		phone: String,
		email: String,
		website: String,
		teachingYear: {type: Schema.Types.ObjectId, ref: 'TeachingYear', require: true}
	},
	{
		timestamps: false,
		toJSON: {virtuals: true}
	}
);

const School = mongoose.model('School', schoolSchema);

export default School;