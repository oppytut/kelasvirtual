import express from 'express';
import Log from 'debug-logger';

import {
	getIndicators,
	getIndicator,
	postIndicator,
	putIndicator,
	delIndicator,
	delIndicators,
	delIndicatorsChild
} from '../bridge/indicator.js';

const router = express.Router();
const log = Log('indicatorAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getIndicators({})
			.then((indicator) => {
				res.json({indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postIndicator({data: req.body})
			.then((indicator) => {
				const message = 'indicator created!';
				log.info(message);
				res.json({message, indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getIndicators({query, field, populate, limit})
			.then((indicator) => {
				res.json({indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getIndicator({id: req.params.id})
			.then((indicator) => {
				res.json({indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putIndicator({id: req.params.id, data: req.body})
			.then((indicator) => {
				const message = 'indicator updated!';
				log.info(message);
				res.json({message, indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delIndicator({id: req.params.id})
			.then((indicator) => {
				const message = 'indicator deleted!';
				res.json({message, indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delIndicators({query})
			.then((indicator) => {
				const message = 'indicator deleted!';
				res.json({message, indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/child/')
	.post((req, res) => {
		log.trace('DEL(POST) by query child');

		const {query} = req.body;

		delIndicatorsChild({query})
			.then((indicator) => {
				const message = 'indicator deleted!';
				res.json({message, indicator});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
