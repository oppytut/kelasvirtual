import express from 'express';
import Log from 'debug-logger';

import {
	getCognitiveTests,
	getCognitiveTest,
	postCognitiveTest,
	putCognitiveTest,
	delCognitiveTest,
	delCognitiveTests
} from '../bridge/cognitiveTest.js';

const router = express.Router();
const log = Log('cognitiveTestAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getCognitiveTests({})
			.then((cognitiveTest) => {
				res.json({cognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postCognitiveTest({data: req.body})
			.then((cognitiveTest) => {
				const message = 'cognitiveTest created!';
				log.info(message);
				res.json({message, cognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getCognitiveTests({query, field, populate, limit})
			.then((cognitiveTest) => {
				res.json({cognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getCognitiveTest({id: req.params.id})
			.then((cognitiveTest) => {
				res.json({cognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putCognitiveTest({id: req.params.id, data: req.body})
			.then((cognitiveTest) => {
				const message = 'cognitiveTest updated!';
				log.info(message);
				res.json({message, cognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delCognitiveTest({id: req.params.id})
			.then((cognitiveTest) => {
				const message = 'cognitiveTest deleted!';
				res.json({message, cognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delCognitiveTests({query})
			.then((cognitiveTest) => {
				const message = 'cognitiveTest deleted!';
				res.json({message, cognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
