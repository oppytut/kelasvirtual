import mongoose from 'mongoose';
import express from 'express';
import validator from 'validator';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import bcrypt from 'bcrypt';
// import mysql from 'mysql';

import userModel from '../model/user.js';
import {getUsers} from '../bridge/user.js';

const router = express.Router();
const log = Log('userAPI');

function checkRegistered(email) {
	log.trace('checkRegistered');

	return new Promise((resolve) => {
		userModel.count({'email' : email}, (err, count) => {
			if(err)
				log.error(err);

			if(count == 1) {
				resolve(true);
			} else {
				resolve(false);
			}
		});
	});
}

function checkUsernameUsed(username) {
	log.trace('checkUsernameUsed');

	return new Promise((resolve) => {
		userModel.count({'username' : username}, (err, count) => {
			if(err)
				log.error(err);

			if(count == 1) {
				resolve(true);
			} else {
				resolve(false);
			}
		});
	});
}

function validateEmail(email, oldEmail) {
	log.trace('validateEmail');

	return new Promise(async (resolve) => {
		if(!isEmpty(email)) {
			const checkRegisteredResult = checkRegistered(email);

			if(email === oldEmail) {
				resolve();
			} else if(!validator.isEmail(email)) {
				resolve('Format email yang Anda isi tidak valid!');
			}	else {
				const isRegistered = await checkRegisteredResult;

				if(isRegistered) {
					resolve('Email Anda sudah terdaftar!');
				} else {
					resolve();
				}
			}
		} else {
			resolve();
		}
	});
}

function validateUsername(username, oldUsername) {
	log.trace('validateUsername');

	return new Promise(async (resolve) => {
		if(!isEmpty(username)) {
			const checkUsernameUsedResult = checkUsernameUsed(username);

			if(username === oldUsername) {
				resolve();
			} else if(!validator.isAlphanumeric(username)) {
				resolve('Username hanya boleh menggunakan huruf dan angka!');
			} else if(!validator.isLength(username, {min: 5, max: 20})) {
				resolve('Username harus 5 sampai 20 karakter!');
			} else {
				const usernameIsUsed = await checkUsernameUsedResult;

				if(usernameIsUsed) {
					resolve('Username sudah digunakan orang lain!');
				} else {
					resolve();
				}
			}
		} else {
			resolve();
		}
	});
}

function validatePassword(password) {
	log.trace('validatePassword');

	if(!validator.isLength(password, {min: 8, max: 20})) {
		return 'Password harus 8 sampai 20 karakter!';
	} else if(/\s/.test(password)) {
		return 'Password tidak boleh mengandung spasi!';
	}
}

function validate(data, user={}) {
	log.trace('validate');

	return new Promise(async (resolve) => {
		var error = {};

		const asyncResult = await Promise.all([
			validateEmail(data.email, user.email),
			validateUsername(data.username, user.username)
		]);
		error.email = asyncResult[0];
		error.username = asyncResult[1];

		if(!isEmpty(data.password))
			error.password = validatePassword(data.password);

		if(!isEmpty(data.repassword))
			if(data.repassword != data.password)
				error.repassword = 'Password yang Anda isi tidak sama!';

		if(!isEmpty(data.name))
			if(!/^[a-zA-Z ]+$/.test(data.name)
				|| data.name[0] === ' '
				|| data.name[data.name.length-1] === ' '
			)
				error.name = 'name is not valid!';

		if(!isEmpty(data.picture))
			if(!mongoose.Types.ObjectId.isValid(data.picture))
				error.picture = 'picture is not valid!';

		// Delete undefined error
		for(var i in error) {
			if(isEmpty(error[i]))
				delete error[i];
		}

		resolve(error);
	});
}

function validateAll(data, user={}) {
	log.trace('validateAll');

	return new Promise(async (resolve) => {
		var error = {};
		error = await validate(data, user);

		if(isEmpty(data.email))
			error.email = 'Anda harus mengisi email!';

		if(isEmpty(data.username))
			error.username = 'Anda harus mengisi username!';

		if(isEmpty(data.password))
			error.password = 'Anda harus mengisi password!';

		if(isEmpty(data.repassword))
			error.repassword = 'Anda harus mengisi ulang password!';

		resolve(error);
	});
}

// function createMySQLUser(user) {
// 	log.trace('createMySQLUser');
//
// 	var ADMconn = mysql.createConnection({
// 		host: 'localhost',
// 		user: 'root',
// 		password: 'sukamto'
// 	});
//
// 	var comm = (conn, cmd, cb) => {
// 		new Promise((resolve) => {
// 			conn.query(cmd, (error, results, fields) => {
// 				if(error) {
// 					resolve({
// 						command: cmd,
// 						error: error
// 					});
// 				} else {
// 					resolve({
// 						command: cmd,
// 						results: results,
// 						fields: fields
// 					});
// 				}
// 			});
// 		}).then((data) => {
// 			cb(data);
// 		});
// 	};
//
// 	var createUser = (conn, name, host, pass, cb) => {
// 		var cmd = `CREATE USER '${name}'@'${host}' IDENTIFIED BY '${pass}';`;
//
// 		comm(conn, cmd, (data) => {
// 			var err = {};
//
// 			if(isEmpty(data.error)) {
// 				log.info(`Success create MySQL user: ${name}!`);
// 			} else {
// 				err = `Fail create MySQL user: ${name}. Error: ${data.error.sqlMessage}`;
// 				log.error(err);
// 			}
//
// 			cb(err, conn, name, host);
// 		});
// 	};
//
// 	var grantUserPermission = (conn, name, host, cb) => {
// 		var cmd = `GRANT ALL PRIVILEGES ON * . * TO '${name}'@'${host}';`;
//
// 		comm(conn, cmd, (data) => {
// 			var err = {};
//
// 			if(isEmpty(data.error)) {
// 				log.info(`Success grant permission to MySQL user: ${name}!`);
// 			} else {
// 				err = `Fail grant permission to MySQL user: ${name}. Error: ${data.error.sqlMessage}`;
// 				log.error(err);
// 			}
//
// 			cb(err);
// 		});
// 	};
//
// 	return new Promise((resolve, reject) => {
// 		createUser(ADMconn, user, 'localhost', '', (err, conn, name, host) => {
// 			if(isEmpty(err)) {
// 				grantUserPermission(conn, name, host, (err) => {
// 					if(isEmpty(err)) {
// 						resolve();
// 					} else {
// 						reject(err);
// 					}
// 				});
// 			} else {
// 				reject(err);
// 			}
// 		});
// 	});
// }

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		userModel.find((err, user) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({user});
			}
		});
	})
	.post(async (req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error  = await validateAll(req.body);

		if(isEmpty(error)) {
			log.trace('data is valid');

			const {username, email, password, name, gender, picture} = req.body;
			var user = new userModel();

			user.username = username;
			user.email = email;
			user.password = bcrypt.hashSync(password, 10);
			user.name = name;
			user.gender = gender;
			user.picture = picture;

			user.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'user created!';
					log.trace(message);

					res.json({message, user});

					/**
					 * Disable, use self compiler
					 */
					// createMySQLUser(user.username)
					// 	.then(() => {
					// 		res.json({message, user});
					// 	}).catch((error) => {
					// 		res.status(400).json({error});
					// 	});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('POST aka GET by query');

		const {query, field, populate, limit} = req.body;

		getUsers({query, field, populate, limit})
			.then((user) => {
				res.json({user});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			userModel.findById(req.params.id, (err, user) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({user});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {username, email, password, name, gender, picture, _all} = req.body;

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			userModel.findById(req.params.id, async (err, user) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(user)) {
						if(_all) {
							error = await validateAll(req.body, user);
						} else {
							error = await validate(req.body, user);
						}

						if(isEmpty(error)) {
							if(!isEmpty(username)) {
								log.trace('username changed');
								user.username = username;
							}

							if(!isEmpty(password)) {
								log.trace('password changed');
								user.password = bcrypt.hashSync(password, 10);
							}

							if(!isEmpty(email)) {
								log.trace('email changed');
								user.email = email;
							}

							if(!isEmpty(name)) {
								log.trace('name changed');
								user.name = name;
							}

							if(!isEmpty(gender)) {
								log.trace('gender changed');
								user.gender = gender;
							}

							if(!isEmpty(picture)) {
								log.trace('picture changed');
								user.picture = picture;
							}

							user.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'user updated!';
									log.info(message);
									res.json({message, user});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'user is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			userModel.findByIdAndRemove(req.params.id, (err, user) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(user)) {
						message = 'user deleted!';
						res.json({message, user});
					} else {
						error = 'user is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;
