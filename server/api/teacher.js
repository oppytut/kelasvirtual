import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import teacherModel from '../model/teacher.js';

const router = express.Router();
const log = Log('teacherAPI');

function isAlreadyTeacher(data) {
	log.trace('isAlreadyTeacher');

	return new Promise((resolve) => { // data must not null
		teacherModel.count({'user': data.user, 'course': data.course}, (err, count) => {
			if(err)
				log.error(err);
			
			if(count == 1) {
				resolve(true);
			} else {
				resolve(false);
			}
		});
	});
}

function validate(data) {
	log.trace('validate');

	return new Promise(async (resolve) => {
		var error = {};
		
		if(!(isEmpty(data.course) && isEmpty(data.user))) {
			if(!isEmpty(data.course) && !isEmpty(data.user)) {
				var asyncResult = await isAlreadyTeacher(data);
				if(asyncResult) {
					error.user = 'course and teacher already connected!';
					error.course = 'course and teacher already connected!';
				}
			} else {
				error.user = 'course and teacher are interdependence!';
				error.course = 'course and teacher are interdependence!';
			}
		}

		if(!isEmpty(data.course))
			if(!mongoose.Types.ObjectId.isValid(data.course))
				error.course = 'course is not valid!';
		
		if(!isEmpty(data.user))
			if(!mongoose.Types.ObjectId.isValid(data.user))
				error.user = 'user is not valid!';

		resolve(error);
	});
}

function validateAll(data) {
	log.trace('validateAll');

	return new Promise(async (resolve) => {
		var error = {};
		
		error = await validate(data);

		if(isEmpty(data.course))
			error.course = 'course is required!';

		if(isEmpty(data.user))
			error.user = 'user is required!';

		resolve(error);
	});
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		teacherModel.find((err, teacher) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({teacher});
			}
		});
	})
	.post(async (req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};
		
		error = await validateAll(req.body);	

		if(isEmpty(error)) {
			const {course, user} = req.body;
			var teacher = new teacherModel();

			teacher.course = course;
			teacher.user = user;

			teacher.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'teacher created!';
					log.info(message);
					res.json({message, teacher});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			teacherModel.findById(req.params.id, (err, teacher) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({teacher});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {course, user, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			teacherModel.findById(req.params.id, async (err, teacher) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(teacher)) {
						if(_all) {
							error = await validateAll(req.body);
						} else {
							error = await validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(course)) {
								log.trace('course changed');
								teacher.course = course;	
							}

							if(!isEmpty(user)) {
								log.trace('user changed');
								teacher.user = user;	
							}
							
							teacher.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'teacher updated!';
									log.info(message);
									res.json({message, teacher});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'teacher is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			teacherModel.findByIdAndRemove(req.params.id, (err, teacher) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(teacher)) {
						message = 'teacher deleted!';
						res.json({message, teacher});	
					} else {
						error = 'teacher is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;