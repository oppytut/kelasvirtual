import express from 'express';
import Log from 'debug-logger';

import {
	getMatters,
	getMatter,
	postMatter,
	putMatter,
	delMatter,
	delMatters
} from '../bridge/matter.js';

const router = express.Router();
const log = Log('matterAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getMatters({})
			.then((matter) => {
				res.json({matter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postMatter({data: req.body})
			.then((matter) => {
				const message = 'matter created!';
				log.info(message);
				res.json({message, matter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getMatters({query, field, populate, limit})
			.then((matter) => {
				res.json({matter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getMatter({id: req.params.id})
			.then((matter) => {
				res.json({matter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putMatter({id: req.params.id, data: req.body})
			.then((matter) => {
				const message = 'matter updated!';
				log.info(message);
				res.json({message, matter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delMatter({id: req.params.id})
			.then((matter) => {
				const message = 'matter deleted!';
				res.json({message, matter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delMatters({query})
			.then((matter) => {
				const message = 'matter deleted!';
				res.json({message, matter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
