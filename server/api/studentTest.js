import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import studentTestModel from '../model/studentTest.js';
import {
	getStudentTests,
	delStudentTests
} from '../bridge/studentTest.js';

const router = express.Router();
const log = Log('studentTestAPI');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.test))
		if(!mongoose.Types.ObjectId.isValid(data.test))
			error.test = 'test is not valid!';

	if(!isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';

	if(!isEmpty(data.score))
		if(!validator.isNumeric(data.score))
			error.score = 'score is not valid!';

	if(!isEmpty(data.pass))
		if(!validator.isBoolean(data.pass.toString()))
			error.pass = 'pass is not valid!';

	if(!isEmpty(data.cognitiveScore))
		if(!validator.isNumeric(data.cognitiveScore))
			error.cognitiveScore = 'cognitiveScore is not valid!';

	if(!isEmpty(data.cognitivePass))
		if(!validator.isBoolean(data.cognitivePass.toString()))
			error.cognitivePass = 'cognitivePass is not valid!';

	if(!isEmpty(data.psychomotorScore))
		if(!validator.isNumeric(data.psychomotorScore))
			error.psychomotorScore = 'psychomotorScore is not valid!';

	if(!isEmpty(data.psychomotorPass))
		if(!validator.isBoolean(data.psychomotorPass.toString()))
			error.psychomotorPass = 'psychomotorPass is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.test))
		error.test = 'test is required!';

	if(isEmpty(data.student))
		error.student = 'student is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		studentTestModel.find((err, studentTest) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({studentTest});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {
				test,
				student,
				score,
				pass,
				cognitiveScore,
				cognitivePass,
				psychomotorScore,
				psychomotorPass
			} = req.body;
			var studentTest = new studentTestModel();

			studentTest.test = test;
			studentTest.student = student;
			studentTest.score = score;
			studentTest.pass = pass;
			studentTest.cognitiveScore = cognitiveScore;
			studentTest.cognitivePass = cognitivePass;
			studentTest.psychomotorScore = psychomotorScore;
			studentTest.psychomotorPass = psychomotorPass;

			studentTest.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'studentTest created!';
					log.info(message);
					res.json({message, studentTest});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('POST aka GET by query');

		const {query, field, populate, limit} = req.body;

		getStudentTests({query, field, populate, limit})
			.then((studentTest) => {
				res.json({studentTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentTestModel.findById(req.params.id, (err, studentTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({studentTest});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {
			test,
			student,
			score,
			pass,
			cognitiveScore,
			cognitivePass,
			psychomotorScore,
			psychomotorPass,
			_all
		} = req.body;

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentTestModel.findById(req.params.id, (err, studentTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentTest)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(test)) {
								log.trace('test changed');
								studentTest.test = test;
							}

							if(!isEmpty(student)) {
								log.trace('student changed');
								studentTest.student = student;
							}

							if(!isEmpty(score)) {
								log.trace('score changed');
								studentTest.score = score;
							}

							if(!isEmpty(pass)) {
								log.trace('pass changed');
								studentTest.pass = pass;
							}

							if(!isEmpty(cognitiveScore)) {
								log.trace('cognitiveScore changed');
								studentTest.cognitiveScore = cognitiveScore;
							}

							if(!isEmpty(cognitivePass)) {
								log.trace('cognitivePass changed');
								studentTest.cognitivePass = cognitivePass;
							}

							if(!isEmpty(psychomotorScore)) {
								log.trace('psychomotorScore changed');
								studentTest.psychomotorScore = psychomotorScore;
							}

							if(!isEmpty(psychomotorPass)) {
								log.trace('psychomotorPass changed');
								studentTest.psychomotorPass = psychomotorPass;
							}

							studentTest.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'studentTest updated!';
									log.info(message);
									res.json({message, studentTest});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'studentTest is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentTestModel.findByIdAndRemove(req.params.id, (err, studentTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentTest)) {
						message = 'studentTest deleted!';
						res.json({message, studentTest});
					} else {
						error = 'studentTest is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('POST aka DEL by query');

		const {query} = req.body;

		delStudentTests({query})
			.then((studentTest) => {
				res.json({studentTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
