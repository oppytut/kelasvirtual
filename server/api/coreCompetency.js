import express from 'express';
import Log from 'debug-logger';

import {
	getCoreCompetencys,
	getCoreCompetency,
	postCoreCompetency,
	putCoreCompetency,
	delCoreCompetency,
	delCoreCompetencys,
	getCoreCompetencysOnly,
	putPostCoreCompetency
} from '../bridge/coreCompetency.js';

const router = express.Router();
const log = Log('coreCompetencyAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getCoreCompetencys({})
			.then((coreCompetency) => {
				res.json({coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postCoreCompetency({data: req.body})
			.then((coreCompetency) => {
				const message = 'coreCompetency created!';
				log.info(message);
				res.json({message, coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getCoreCompetencys({query, field, populate, limit})
			.then((coreCompetency) => {
				res.json({coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getCoreCompetency({id: req.params.id})
			.then((coreCompetency) => {
				res.json({coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putCoreCompetency({id: req.params.id, data: req.body})
			.then((coreCompetency) => {
				const message = 'coreCompetency updated!';
				log.info(message);
				res.json({message, coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delCoreCompetency({id: req.params.id})
			.then((coreCompetency) => {
				const message = 'coreCompetency deleted!';
				res.json({message, coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/only/')
	.post((req, res) => {
		log.trace('GET(POST) by query and coreCompetency only');

		const {query, field, populate, limit}	= req.body;

		getCoreCompetencysOnly({query, field, populate, limit})
			.then((coreCompetency) => {
				res.json({coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delCoreCompetencys({query})
			.then((coreCompetency) => {
				const message = 'coreCompetency deleted!';
				res.json({message, coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/putPost/')
	.post((req, res) => {
		log.trace('PUTPOST(POST) by query');

		const {query, data}	= req.body;

		putPostCoreCompetency({query, data})
			.then((coreCompetency) => {
				const message = 'coreCompetency created or updated!';
				res.json({message, coreCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
