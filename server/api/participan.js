import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import participanModel from '../model/participan.js';

const router = express.Router();
const log = Log('participanAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(isEmpty(data.discussion))	
		if(!mongoose.Types.ObjectId.isValid(data.discussion))
			error.discussion = 'discussion is not valid!';
	
	if(isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';
	
	if(isEmpty(data.teacher))
		if(!mongoose.Types.ObjectId.isValid(data.teacher))
			error.teacher = 'teacher is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	if(isEmpty(data.discussion))
		error.discussion = 'discussion is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		participanModel.find((err, participan) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({participan});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {discussion, student, teacher} = req.body;
			var participan = new participanModel();

			participan.discussion = discussion;
			participan.student = student;
			participan.teacher = teacher;

			participan.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'participan created!';
					log.info(message);
					res.json({message, participan});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			participanModel.findById(req.params.id, (err, participan) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({participan});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {discussion, student, teacher, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			participanModel.findById(req.params.id, (err, participan) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(participan)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}						

						if(isEmpty(error)) {
							if(!isEmpty(discussion)) {
								log.trace('discussion changed');
								participan.discussion = discussion;	
							}

							if(!isEmpty(student)) {
								log.trace('student changed');
								participan.student = student;	
							}

							if(!isEmpty(teacher)) {
								log.trace('teacher changed');
								participan.teacher = teacher;	
							}

							participan.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'participan updated!';
									log.info(message);
									res.json({message, participan});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'participan is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			participanModel.findByIdAndRemove(req.params.id, (err, participan) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(participan)) {
						message = 'participan deleted!';
						res.json({message, participan});	
					} else {
						error = 'participan is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;