import express from 'express';
import Log from 'debug-logger';

import {
	getMcQuestionOptions,
	getMcQuestionOption,
	postMcQuestionOption,
	putMcQuestionOption,
	delMcQuestionOption,
	delMcQuestionOptions
} from '../bridge/mcQuestionOption.js';

const router = express.Router();
const log = Log('mcQuestionOptionAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getMcQuestionOptions({})
			.then((mcQuestionOption) => {
				res.json({mcQuestionOption});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postMcQuestionOption({data: req.body})
			.then((mcQuestionOption) => {
				const message = 'mcQuestionOption created!';
				log.info(message);
				res.json({message, mcQuestionOption});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getMcQuestionOptions({query, field, populate, limit})
			.then((mcQuestionOption) => {
				res.json({mcQuestionOption});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getMcQuestionOption({id: req.params.id})
			.then((mcQuestionOption) => {
				res.json({mcQuestionOption});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putMcQuestionOption({id: req.params.id, data: req.body})
			.then((mcQuestionOption) => {
				const message = 'mcQuestionOption updated!';
				log.info(message);
				res.json({message, mcQuestionOption});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delMcQuestionOption({id: req.params.id})
			.then((mcQuestionOption) => {
				const message = 'mcQuestionOption deleted!';
				res.json({message, mcQuestionOption});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delMcQuestionOptions({query})
			.then((mcQuestionOption) => {
				const message = 'mcQuestionOption deleted!';
				res.json({message, mcQuestionOption});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
