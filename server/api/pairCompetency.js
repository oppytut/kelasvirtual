import express from 'express';
import Log from 'debug-logger';

import {
	getPairCompetencys,
	getPairCompetency,
	postPairCompetency,
	putPairCompetency,
	delPairCompetency,
	delPairCompetencys,
	getPairCompetencysByCourse,
	postPairedBasicCompetency,
	delPairCompetencysChild,
} from '../bridge/pairCompetency.js';

const router = express.Router();
const log = Log('pairCompetencyAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getPairCompetencys({})
			.then((pairCompetency) => {
				res.json({pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postPairCompetency({data: req.body})
			.then((pairCompetency) => {
				const message = 'pairCompetency created!';
				log.info(message);
				res.json({message, pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getPairCompetencys({query, field, populate, limit})
			.then((pairCompetency) => {
				res.json({pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/pairedBasicCompetency/')
	.post((req, res) => {
		log.trace('POST pairedBasicCompetency');

		postPairedBasicCompetency({data: req.body})
			.then((pairCompetency) => {
				const message = 'pairCompetency created!';
				log.info(message);
				res.json({message, pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getPairCompetency({id: req.params.id})
			.then((pairCompetency) => {
				res.json({pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putPairCompetency({id: req.params.id, data: req.body})
			.then((pairCompetency) => {
				const message = 'pairCompetency updated!';
				log.info(message);
				res.json({message, pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delPairCompetency({id: req.params.id})
			.then((pairCompetency) => {
				const message = 'pairCompetency deleted!';
				res.json({message, pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delPairCompetencys({query})
			.then((pairCompetency) => {
				const message = 'pairCompetency deleted!';
				res.json({message, pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/course/')
	.post((req, res) => {
		log.trace('POST aka GET by courseId');

		const {query, field, populate, limit}	= req.body;

		getPairCompetencysByCourse({query, field, populate, limit})
			.then((pairCompetency) => {
				res.json({pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/child/')
	.post((req, res) => {
		log.trace('DEL(POST) by query child');

		const {query} = req.body;

		delPairCompetencysChild({query})
			.then((pairCompetency) => {
				const message = 'pairCompetency deleted!';
				res.json({message, pairCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
