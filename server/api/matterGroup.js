import express from 'express';
import Log from 'debug-logger';

import {
	getMatterGroups,
	getMatterGroup,
	postMatterGroup,
	putMatterGroup,
	delMatterGroup,
	delMatterGroups,
	getMatterGroupsByCourse,
	getMatterGroupsByMatter
} from '../bridge/matterGroup.js';

const router = express.Router();
const log = Log('matterGroupAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getMatterGroups({})
			.then((matterGroup) => {
				res.json({matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postMatterGroup({data: req.body})
			.then((matterGroup) => {
				const message = 'matterGroup created!';
				log.info(message);
				res.json({message, matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getMatterGroups({query, field, populate, limit})
			.then((matterGroup) => {
				res.json({matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getMatterGroup({id: req.params.id})
			.then((matterGroup) => {
				res.json({matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putMatterGroup({id: req.params.id, data: req.body})
			.then((matterGroup) => {
				const message = 'matterGroup updated!';
				log.info(message);
				res.json({message, matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delMatterGroup({id: req.params.id})
			.then((matterGroup) => {
				const message = 'matterGroup deleted!';
				res.json({message, matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delMatterGroups({query})
			.then((matterGroup) => {
				const message = 'matterGroup deleted!';
				res.json({message, matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/course/')
	.post((req, res) => {
		log.trace('POST aka GET by courseId');

		const {query, field, populate, limit}	= req.body;

		getMatterGroupsByCourse({query, field, populate, limit})
			.then((matterGroup) => {
				res.json({matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/matter/')
	.post((req, res) => {
		log.trace('POST aka GET by matterId');

		const {query, field, populate, limit}	= req.body;

		getMatterGroupsByMatter({query, field, populate, limit})
			.then((matterGroup) => {
				res.json({matterGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
