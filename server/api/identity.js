import express from 'express';
import Log from 'debug-logger';

import {
	getIdentitys,
	getIdentity,
	postIdentity,
	putIdentity,
	delIdentity,
	delIdentitys
} from '../bridge/identity.js';

const router = express.Router();
const log = Log('identityAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getIdentitys({})
			.then((identity) => {
				res.json({identity});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postIdentity({data: req.body})
			.then((identity) => {
				const message = 'identity created!';
				log.info(message);
				res.json({message, identity});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getIdentitys({query, field, populate, limit})
			.then((identity) => {
				res.json({identity});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getIdentity({id: req.params.id})
			.then((identity) => {
				res.json({identity});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putIdentity({id: req.params.id, data: req.body})
			.then((identity) => {
				const message = 'identity updated!';
				log.info(message);
				res.json({message, identity});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delIdentity({id: req.params.id})
			.then((identity) => {
				const message = 'identity deleted!';
				res.json({message, identity});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delIdentitys({query})
			.then((identity) => {
				const message = 'identity deleted!';
				res.json({message, identity});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
