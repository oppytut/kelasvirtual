import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import studentMeetingModel from '../model/studentMeeting.js';
import {
	getStudentMeetings,
	delStudentMeetings
} from '../bridge/studentMeeting.js';

const router = express.Router();
const log = Log('studentMeetingAPI');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.meeting))
		if(!mongoose.Types.ObjectId.isValid(data.meeting))
			error.meeting = 'meeting is not valid!';

	if(!isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';

	if(!isEmpty(data.presence))
		if(!validator.isBoolean(data.presence.toString()))
			error.presence = 'presence is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.meeting))
		error.meeting = 'meeting is required!';

	if(isEmpty(data.student))
		error.student = 'student is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		studentMeetingModel.find((err, studentMeeting) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({studentMeeting});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {meeting, student, presence} = req.body;
			var studentMeeting = new studentMeetingModel();

			studentMeeting.meeting = meeting;
			studentMeeting.student = student;
			studentMeeting.presence = presence;

			studentMeeting.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'studentMeeting created!';
					log.info(message);
					res.json({message, studentMeeting});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('POST aka GET by query');

		const {query, field, populate, limit} = req.body;

		getStudentMeetings({query, field, populate, limit})
			.then((studentMeeting) => {
				res.json({studentMeeting});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentMeetingModel.findById(req.params.id, (err, studentMeeting) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({studentMeeting});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {meeting, student, presence, _all} = req.body;

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentMeetingModel.findById(req.params.id, (err, studentMeeting) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentMeeting)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(meeting)) {
								log.trace('meeting changed');
								studentMeeting.meeting = meeting;
							}

							if(!isEmpty(student)) {
								log.trace('student changed');
								studentMeeting.student = student;
							}

							if(!isEmpty(presence)) {
								log.trace('presence changed');
								studentMeeting.presence = presence;
							}

							studentMeeting.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'studentMeeting updated!';
									log.info(message);
									res.json({message, studentMeeting});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'studentMeeting is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentMeetingModel.findByIdAndRemove(req.params.id, (err, studentMeeting) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentMeeting)) {
						message = 'studentMeeting deleted!';
						res.json({message, studentMeeting});
					} else {
						error = 'studentMeeting is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('POST aka DEL by query');

		const {query} = req.body;

		delStudentMeetings({query})
			.then((studentMeeting) => {
				res.json({studentMeeting});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
