import express from 'express';
import Log from 'debug-logger';
import mysql from 'mysql';
import isEmpty from 'is-empty';

import userModel from '../model/user.js';

const router = express.Router();
const log = Log('compilerAPI');

function comm(conn, cmd, cb) {
	log.trace('comm');

	new Promise((resolve) => {
		conn.query(cmd, (error, results, fields) => {
			if(error) {
				resolve({
					command: cmd, 
					error: error
				});
			} else {
				resolve({
					command: cmd,
					results: results,
					fields: fields
				});
			}
		});
	}).then((data) => {
		cb(data);
	});
}

function useDatabase(conn, db, cb) {
	log.trace('useDatabase');

	const cmd = `USE ${db};`;
	var error = {};

	comm(conn, cmd, (data) => {
		if(isEmpty(data.error)) {
			log.trace(`database changed to: ${db}`);
			cb();
		} else {
			error= `fail change database: ${data.error.sqlMessage}`;
			log.error(error);
			cb(error);
		}
	});
}

function isPermitted(cmd) {
	log.trace('isPermitted');
	
	if(cmd.indexOf(';') == -1) {
		log.warn('not have semicolon');
		return false;
	} else {
		if(cmd.match(/;/gi).length > 1) {
			log.warn('multiple semicolon');
			return false;
		}
	}	

	const type = cmd.substr(0, cmd.indexOf(' '));
	const permitted = [
		'create',
		'drop',
		'show',
		'use',
		'alter',
		'insert',
		'select',
		'update',
		'delete'
	];

	for(var i in permitted) {
		if(type == permitted[i]) {
			log.trace('permitted');

			return true;
		}
	}
	
	log.warn('not permitted');
	return false;
}

router.route('/')
	.post((req, res) => {
		log.trace('POST');

		var error = {};
		var compiler = {};
		const {userId, db, cmd} = req.body;
				
		new Promise((resolve, reject) => {
			userModel.findById(userId, (err, user) => {
				log.trace('finding user');

				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(user)) {
						resolve(user);
					} else {
						reject();
					}
				}
			});
		}).then((user) => {
			log.trace('user found');

			const USRconn = mysql.createConnection({
				host: 'localhost',
				user: user.username,
				password: ''
			});
			
			new Promise((resolve, reject) => {
				if(!isEmpty(db)) {
					log.trace('change database');

					useDatabase(USRconn, db, (err) => {
						if(isEmpty(err)) {
							resolve();
						} else {
							reject();
						}
					});
				} else {
					log.trace('database not change');
					resolve();
				}	
			}).then(() => {
				if(isPermitted(cmd)) {
					log.trace('command is permitted');

					comm(USRconn, cmd, (data) => {
						res.json({compiler: data});
					});
				} else {
					log.trace('command is not permitted');

					compiler = {
						command: cmd,
						error: {
							sqlMessage: 'You have an error or not permitted command in your SQL syntax.'
						}
					};
					res.json({compiler});
				}
			}).catch(() => {
				log.trace('fail change database');

				compiler = {
					command: cmd,
					error: {
						sqlMessage: 'Database failed.'
					}
				};
				res.json({compiler});
			});
		}).catch(() => {
			error = 'user not found!';
			log.error(error);
			res.status(401).json({error});
		});
	});

export default router;