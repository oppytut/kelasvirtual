import express from 'express';
import Log from 'debug-logger';

import {
	getStudents,
	getStudent,
	postStudent,
	putStudent,
	delStudent,
	delStudents,
	getStudentsOnly,
	putPostStudent
} from '../bridge/student.js';

const router = express.Router();
const log = Log('studentAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getStudents({})
			.then((student) => {
				res.json({student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postStudent({data: req.body})
			.then((student) => {
				const message = 'student created!';
				log.info(message);
				res.json({message, student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getStudents({query, field, populate, limit})
			.then((student) => {
				res.json({student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getStudent({id: req.params.id})
			.then((student) => {
				res.json({student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putStudent({id: req.params.id, data: req.body})
			.then((student) => {
				const message = 'student updated!';
				log.info(message);
				res.json({message, student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delStudent({id: req.params.id})
			.then((student) => {
				const message = 'student deleted!';
				res.json({message, student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/only/')
	.post((req, res) => {
		log.trace('GET(POST) by query and student only');

		const {query, field, populate, limit}	= req.body;

		getStudentsOnly({query, field, populate, limit})
			.then((student) => {
				res.json({student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delStudents({query})
			.then((student) => {
				const message = 'student deleted!';
				res.json({message, student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/putPost/')
	.post((req, res) => {
		log.trace('PUTPOST(POST) by query');

		const {query, data}	= req.body;

		putPostStudent({query, data})
			.then((student) => {
				const message = 'student created or updated!';
				res.json({message, student});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
