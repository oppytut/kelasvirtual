import express from 'express';
import Log from 'debug-logger';

import {
	getEssayQuestions,
	getEssayQuestion,
	postEssayQuestion,
	putEssayQuestion,
	delEssayQuestion,
	delEssayQuestions
} from '../bridge/essayQuestion.js';

const router = express.Router();
const log = Log('essayQuestionAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getEssayQuestions({})
			.then((essayQuestion) => {
				res.json({essayQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postEssayQuestion({data: req.body})
			.then((essayQuestion) => {
				const message = 'essayQuestion created!';
				log.info(message);
				res.json({message, essayQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getEssayQuestions({query, field, populate, limit})
			.then((essayQuestion) => {
				res.json({essayQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getEssayQuestion({id: req.params.id})
			.then((essayQuestion) => {
				res.json({essayQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putEssayQuestion({id: req.params.id, data: req.body})
			.then((essayQuestion) => {
				const message = 'essayQuestion updated!';
				log.info(message);
				res.json({message, essayQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delEssayQuestion({id: req.params.id})
			.then((essayQuestion) => {
				const message = 'essayQuestion deleted!';
				res.json({message, essayQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delEssayQuestions({query})
			.then((essayQuestion) => {
				const message = 'essayQuestion deleted!';
				res.json({message, essayQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
