import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import messageModel from '../model/message.js';

const router = express.Router();
const log = Log('messageAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(!isEmpty(data.discussion))
		if(!mongoose.Types.ObjectId.isValid(data.discussion))
			error.discussion = 'discussion is not valid!';
	
	if(!isEmpty(data.sender))
		if(!mongoose.Types.ObjectId.isValid(data.sender))
			error.sender = 'sender is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.time))
		error.time = 'time is required!';

	if(isEmpty(data.discussion))
		error.discussion = 'discussion is required!';

	if(isEmpty(data.sender))
		error.sender = 'sender is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		messageModel.find((err, message) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({message});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var messageRes = {};
		var error = {};
		
		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {content, time, discussion, sender} = req.body;
			var message = new messageModel();

			message.content = content;
			message.time = time;
			message.discussion = discussion;
			message.sender = sender;

			message.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					messageRes = 'message created!';
					log.info(messageRes);
					res.json({messageRes, message});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			messageModel.findById(req.params.id, (err, message) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({message});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var messageRes = {};
		var error = {};
		const {content, time, discussion, sender, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			messageModel.findById(req.params.id, (err, message) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(message)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(content)) {
								log.trace('content changed');
								message.content = content;	
							}

							if(!isEmpty(time)) {
								log.trace('time changed');
								message.time = time;	
							}

							if(!isEmpty(discussion)) {
								log.trace('discussion changed');
								message.discussion = discussion;	
							}

							if(!isEmpty(sender)) {
								log.trace('sender changed');
								message.sender = sender;	
							}

							message.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									messageRes = 'message updated!';
									log.info(messageRes);
									res.json({messageRes, message});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'message is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var messageRes = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			messageModel.findByIdAndRemove(req.params.id, (err, message) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(message)) {
						messageRes = 'message deleted!';
						res.json({messageRes, message});	
					} else {
						error = 'message is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;