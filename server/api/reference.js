import express from 'express';
import Log from 'debug-logger';

import {
	getReferences,
	getReference,
	postReference,
	putReference,
	delReference,
	delReferences
} from '../bridge/reference.js';

const router = express.Router();
const log = Log('referenceAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getReferences({})
			.then((reference) => {
				res.json({reference});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postReference({data: req.body})
			.then((reference) => {
				const message = 'reference created!';
				log.info(message);
				res.json({message, reference});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getReferences({query, field, populate, limit})
			.then((reference) => {
				res.json({reference});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getReference({id: req.params.id})
			.then((reference) => {
				res.json({reference});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putReference({id: req.params.id, data: req.body})
			.then((reference) => {
				const message = 'reference updated!';
				log.info(message);
				res.json({message, reference});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delReference({id: req.params.id})
			.then((reference) => {
				const message = 'reference deleted!';
				res.json({message, reference});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delReferences({query})
			.then((reference) => {
				const message = 'reference deleted!';
				res.json({message, reference});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
