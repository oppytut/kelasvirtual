import express from 'express';
import Log from 'debug-logger';

import {
	getMatterRequires,
	getMatterRequire,
	postMatterRequire,
	putMatterRequire,
	delMatterRequire,
	delMatterRequires
} from '../bridge/matterRequire.js';

const router = express.Router();
const log = Log('matterRequireAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getMatterRequires({})
			.then((matterRequire) => {
				res.json({matterRequire});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postMatterRequire({data: req.body})
			.then((matterRequire) => {
				const message = 'matterRequire created!';
				log.info(message);
				res.json({message, matterRequire});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getMatterRequires({query, field, populate, limit})
			.then((matterRequire) => {
				res.json({matterRequire});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getMatterRequire({id: req.params.id})
			.then((matterRequire) => {
				res.json({matterRequire});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putMatterRequire({id: req.params.id, data: req.body})
			.then((matterRequire) => {
				const message = 'matterRequire updated!';
				log.info(message);
				res.json({message, matterRequire});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delMatterRequire({id: req.params.id})
			.then((matterRequire) => {
				const message = 'matterRequire deleted!';
				res.json({message, matterRequire});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delMatterRequires({query})
			.then((matterRequire) => {
				const message = 'matterRequire deleted!';
				res.json({message, matterRequire});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
