import express from 'express';
import Log from 'debug-logger';

import {
	getPsychomotorTests,
	getPsychomotorTest,
	postPsychomotorTest,
	putPsychomotorTest,
	delPsychomotorTest,
	delPsychomotorTests
} from '../bridge/psychomotorTest.js';

const router = express.Router();
const log = Log('psychomotorTestAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getPsychomotorTests({})
			.then((psychomotorTest) => {
				res.json({psychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postPsychomotorTest({data: req.body})
			.then((psychomotorTest) => {
				const message = 'psychomotorTest created!';
				log.info(message);
				res.json({message, psychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getPsychomotorTests({query, field, populate, limit})
			.then((psychomotorTest) => {
				res.json({psychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getPsychomotorTest({id: req.params.id})
			.then((psychomotorTest) => {
				res.json({psychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putPsychomotorTest({id: req.params.id, data: req.body})
			.then((psychomotorTest) => {
				const message = 'psychomotorTest updated!';
				log.info(message);
				res.json({message, psychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delPsychomotorTest({id: req.params.id})
			.then((psychomotorTest) => {
				const message = 'psychomotorTest deleted!';
				res.json({message, psychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delPsychomotorTests({query})
			.then((psychomotorTest) => {
				const message = 'psychomotorTest deleted!';
				res.json({message, psychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
