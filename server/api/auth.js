import express from 'express';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import userModel from '../model/user.js';
import jwtConfig from '../config/jwt.js';

const router = express.Router();
const log = Log('authAPI');

function getUserByEmail(email) {
	log.trace('getUserByEmail');

	return new Promise((resolve) => {
		userModel.findOne({'email': email}, (err, user) => {
			if(err)
				log.error(err);

			resolve(user);
		});
	});
}

function getUserByUsername(username) {
	log.trace('getUserByUsername');

	return new Promise((resolve) => {
		userModel.findOne({'username': username}, (err, user) => {
			if(err)
				log.error(err);
			
			resolve(user);
		});
	});
}

router.post('/', async (req, res) => {
	log.trace('AUTH');

	var error = {};
	const {id, password} = req.body;
	
	const asyncResult = await Promise.all([
		getUserByEmail(id),
		getUserByUsername(id)
	]);

	if(isEmpty(id)) {
		error.id = 'Anda harus mengisi Email atau Username!';
	} else if(isEmpty(asyncResult[0]) && isEmpty(asyncResult[1])) {
		error.id = 'Username/Email yang Anda masukan salah!';
	}

	if(isEmpty(password))
		error.password = 'Anda harus mengisi Password!';
	
	if(isEmpty(error)) {
		log.trace('data is valid');

		var user = {};

		if(!isEmpty(asyncResult[0])) 
		{
			log.trace('found user by Email');
			user = asyncResult[0];	
		} 
		else if(!isEmpty(asyncResult[1])) 
		{
			log.trace('found user by Username');
			user = asyncResult[1];	
		}
		
		if(bcrypt.compareSync(password, user.password)) {
			log.trace('password is true');

			const token = jwt.sign({
				id: user._id,
				username: user.username
			}, jwtConfig.jwtSecret);
			
			res.json({token});
		} else {
			log.trace('wrong password');
			error.password = 'Password yang Anda masukan salah!';
			res.status(401).json({error});
		}
	} else {
		log.trace('data not valid');
		res.status(401).json({error});
	}
});

export default router;