import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import studentMatterModel from '../model/studentMatter.js';
import {
	getStudentMatters,
	delStudentMatters
} from '../bridge/studentMatter.js';

const router = express.Router();
const log = Log('studentMatterAPI');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.matter))
		if(!mongoose.Types.ObjectId.isValid(data.matter))
			error.matter = 'matter is not valid!';

	if(!isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';

	if(!isEmpty(data.pass))
		if(!validator.isBoolean(data.pass.toString()))
			error.pass = 'pass is not valid!';

	if(!isEmpty(data.videoPass))
		if(!validator.isBoolean(data.videoPass.toString()))
			error.videoPass = 'videoPass is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.matter))
		error.matter = 'matter is required!';

	if(isEmpty(data.student))
		error.student = 'student is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		studentMatterModel.find((err, studentMatter) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({studentMatter});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {matter, student, pass, videoPass} = req.body;
			var studentMatter = new studentMatterModel();

			studentMatter.matter = matter;
			studentMatter.student = student;
			studentMatter.pass = pass;
			studentMatter.videoPass = videoPass;

			studentMatter.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'studentMatter created!';
					log.info(message);
					res.json({message, studentMatter});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('POST aka GET by query');

		const {query, field, populate, limit} = req.body;

		getStudentMatters({query, field, populate, limit})
			.then((studentMatter) => {
				res.json({studentMatter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentMatterModel.findById(req.params.id, (err, studentMatter) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({studentMatter});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {matter, student, pass, videoPass, _all} = req.body;

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentMatterModel.findById(req.params.id, (err, studentMatter) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentMatter)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(matter)) {
								log.trace('matter changed');
								studentMatter.matter = matter;
							}

							if(!isEmpty(student)) {
								log.trace('student changed');
								studentMatter.student = student;
							}

							if(!isEmpty(pass)) {
								log.trace('pass changed');
								studentMatter.pass = pass;
							}

							if(!isEmpty(videoPass)) {
								log.trace('videoPass changed');
								studentMatter.videoPass = videoPass;
							}

							studentMatter.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'studentMatter updated!';
									log.info(message);
									res.json({message, studentMatter});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'studentMatter is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentMatterModel.findByIdAndRemove(req.params.id, (err, studentMatter) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentMatter)) {
						message = 'studentMatter deleted!';
						res.json({message, studentMatter});
					} else {
						error = 'studentMatter is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('POST aka DEL by query');

		const {query} = req.body;

		delStudentMatters({query})
			.then((studentMatter) => {
				res.json({studentMatter});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
