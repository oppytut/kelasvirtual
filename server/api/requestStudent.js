import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import requestStudentModel from '../model/requestStudent.js';
import {
	getRequestStudents,
	delRequestStudents
} from '../bridge/requestStudent.js';

const router = express.Router();
const log = Log('requestStudentAPI');

function isAlreadyRequest(data) {
	log.trace('isAlreadyStudent');

	return new Promise((resolve) => { // data must not null
		requestStudentModel.count({'user': data.user, 'course': data.course}, (err, count) => {
			if(err)
				log.error(err);
			
			if(count == 1) {
				resolve(true);
			} else {
				resolve(false);
			}
		});
	});
}

function validate(data) {
	log.trace('validate');
	
	return new Promise(async (resolve) => {
		var error = {};
		
		if(!isEmpty(data.course) && !isEmpty(data.user)) {
			const asyncResult = await isAlreadyRequest(data);
			if(asyncResult) {
				error.user = 'Anda sudah mengirim permintaan!';
				error.course = 'course and student already connected!';
			}
		} else {
			error.user = 'course and student are interdependence!';
			error.course = 'course and student are interdependence!';
		}

		if(!isEmpty(data.course))
			if(!mongoose.Types.ObjectId.isValid(data.course))
				error.course = 'course is not valid!';

		if(!isEmpty(data.user))
			if(!mongoose.Types.ObjectId.isValid(data.user))
				error.user = 'user is not valid!';

		resolve(error);
	});
}

function validateAll(data) {
	log.trace('validateAll');
	
	return new Promise(async (resolve) => {
		var error = {};
		
		error = await validate(data);

		if(isEmpty(data.course))
			error.course = 'course is required!';

		if(isEmpty(data.user))
			error.user = 'user is required!';		

		resolve(error);
	});

}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		requestStudentModel.find((err, requestStudent) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({requestStudent});
			}
		});
	})
	.post(async (req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};
		
		error = await validateAll(req.body)	;
	
		if(isEmpty(error)) {
			const {course, user} = req.body;
			var requestStudent = new requestStudentModel();

			requestStudent.course = course;
			requestStudent.user = user;

			requestStudent.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'requestStudent created!';
					log.info(message);
					res.json({message, requestStudent});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('POST aka GET by query');

		const {query, field, populate, limit} = req.body;
	
		getRequestStudents({query, field, populate, limit})
			.then((requestStudent) => {
				res.json({requestStudent});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			requestStudentModel.findById(req.params.id, (err, requestStudent) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({requestStudent});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {course, user, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			requestStudentModel.findById(req.params.id, async (err, requestStudent) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(requestStudent)) {
						if(_all) {
							error = await validateAll(req.body);
						} else {
							error = await validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(course)) {
								log.trace('course changed');
								requestStudent.course = course;	
							}

							if(!isEmpty(user)) {
								log.trace('user changed');
								requestStudent.user = user;	
							}

							requestStudent.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'requestStudent updated!';
									log.info(message);
									res.json({message, requestStudent});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'requestStudent is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			requestStudentModel.findByIdAndRemove(req.params.id, (err, requestStudent) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(requestStudent)) {
						message = 'requestStudent deleted!';
						res.json({message, requestStudent});	
					} else {
						error = 'requestStudent is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('POST aka DEL by query');

		const {query} = req.body;
	
		delRequestStudents({query})
			.then((requestStudent) => {
				res.json({requestStudent});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;