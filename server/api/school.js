import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import schoolModel from '../model/school.js';

const router = express.Router();
const log = Log('schoolAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(!isEmpty(data.teachingYear))
		if(!mongoose.Types.ObjectId.isValid(data.teachingYear))
			error.teachingYear = 'teachingYear is not valid!';

	if(!isEmpty(data.name))
		if(!validator.isAlphanumeric(data.name))
			error.name = 'name is not valid!';

	if(!isEmpty(data.phone))
		if(!validator.isNumeric(data.phone))
			error.phone = 'phone is not valid!';

	if(!isEmpty(data.email))
		if(!validator.isEmail(data.email))
			error.email = 'email is not valid!';

	if(!isEmpty(data.website))
		if(!validator.isFQDN(data.website))
			error.website = 'website is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	if(isEmpty(data.name))
		error.name = 'name is required!';

	if(isEmpty(data.teachingYear))
		error.teachingYear = 'teachingYear is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		schoolModel.find((err, school) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({school});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};
		
		error = validateAll(req.body);	

		if(isEmpty(error)) {
			const {name, address, phone, email, website, teachingYear} = req.body;
			var school = new schoolModel();

			school.name = name;
			school.address = address;
			school.phone = phone;
			school.email = email;
			school.website = website;
			school.teachingYear = teachingYear;

			school.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'school created!';
					log.info(message);
					res.json({message, school});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			schoolModel.findById(req.params.id, (err, school) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({school});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {name, address, phone, email, website, teachingYear, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			schoolModel.findById(req.params.id, (err, school) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(school)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(name)) {
								log.trace('name changed');
								school.name = name;	
							}

							if(!isEmpty(address)) {
								log.trace('address changed');
								school.address = address;	
							}

							if(!isEmpty(phone)) {
								log.trace('phone changed');
								school.phone = phone;	
							}

							if(!isEmpty(email)) {
								log.trace('email changed');
								school.email = email;	
							}

							if(!isEmpty(website)) {
								log.trace('website changed');
								school.website = website;	
							}

							if(!isEmpty(teachingYear)) {
								log.trace('teachingYear changed');
								school.teachingYear = teachingYear;	
							}

							school.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'school updated!';
									log.info(message);
									res.json({message, school});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'school is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			schoolModel.findByIdAndRemove(req.params.id, (err, school) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(school)) {
						message = 'school deleted!';
						res.json({message, school});	
					} else {
						error = 'school is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;