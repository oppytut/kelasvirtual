import express from 'express';
import Log from 'debug-logger';

import {
	getCourses,
	getCourse,
	postCourse,
	putCourse,
	delCourse,
	delCourses,
	checkPassword
} from '../bridge/course.js';

const router = express.Router();
const log = Log('courseAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getCourses({})
			.then((course) => {
				res.json({course});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postCourse({data: req.body})
			.then((course) => {
				const message = 'course created!';
				log.info(message);
				res.json({message, course});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getCourses({query, field, populate, limit})
			.then((course) => {
				res.json({course});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getCourse({id: req.params.id})
			.then((course) => {
				res.json({course});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putCourse({id: req.params.id, data: req.body})
			.then((course) => {
				const message = 'course updated!';
				log.info(message);
				res.json({message, course});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delCourse({id: req.params.id})
			.then((course) => {
				const message = 'course deleted!';
				res.json({message, course});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delCourses({query})
			.then((course) => {
				const message = 'course deleted!';
				res.json({message, course});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/check-password/:id')
	.post((req, res) => {
		log.trace('POST check password');

		checkPassword({
			id: req.params.id,
			data: req.body
		}).then((isTrue) => {
			const message = `password is ${isTrue}!`;
			log.info(message);
			res.json({message, isTrue});
		}).catch((err) => {
			res.status(400).json(err);
		});
	});

export default router;
