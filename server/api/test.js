import express from 'express';
import Log from 'debug-logger';

import {
	getTests,
	getTest,
	postTest,
	putTest,
	delTest,
	delTests
} from '../bridge/test.js';

const router = express.Router();
const log = Log('testAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getTests({})
			.then((test) => {
				res.json({test});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postTest({data: req.body})
			.then((test) => {
				const message = 'test created!';
				log.info(message);
				res.json({message, test});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getTests({query, field, populate, limit})
			.then((test) => {
				res.json({test});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getTest({id: req.params.id})
			.then((test) => {
				res.json({test});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putTest({id: req.params.id, data: req.body})
			.then((test) => {
				const message = 'test updated!';
				log.info(message);
				res.json({message, test});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delTest({id: req.params.id})
			.then((test) => {
				const message = 'test deleted!';
				res.json({message, test});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delTests({query})
			.then((test) => {
				const message = 'test deleted!';
				res.json({message, test});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
