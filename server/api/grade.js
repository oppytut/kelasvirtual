import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import gradeModel from '../model/grade.js';

const router = express.Router();
const log = Log('gradeAPI');

// function validate(data) {
// 	log.trace('validate');
// 	var error = {};

// 	return error;
// }

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	// error = validate(data);

	if(isEmpty(data.name))
		error.name = 'name is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		gradeModel.find((err, grade) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({grade});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {name} = req.body;
			var grade = new gradeModel();

			grade.name = name;

			grade.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'grade created!';
					log.info(message);
					res.json({message, grade});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			gradeModel.findById(req.params.id, (err, grade) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({grade});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {name, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			gradeModel.findById(req.params.id, (err, grade) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(grade)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							// error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(name)) {
								log.trace('name changed');
								grade.name = name;	
							}

							grade.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'grade updated!';
									log.info(message);
									res.json({message, grade});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'grade is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			gradeModel.findByIdAndRemove(req.params.id, (err, grade) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(grade)) {
						message = 'grade deleted!';
						res.json({message, grade});	
					} else {
						error = 'grade is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;