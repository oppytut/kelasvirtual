import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import userPictureModel from '../model/userPicture.js';

const router = express.Router();
const log = Log('userPictureAPI');

// function validate(data) {
// 	log.trace('validate');
// 	var error = {};
	
// 	return error;
// }

// function validateAll(data) {
// 	log.trace('validateAll');
// 	var error = {};
	
// 	error = validate(data);

// 	return error;
// }

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		userPictureModel.find((err, userPicture) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({userPicture});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};
		
		if(isEmpty(error)) {
			const {mini, tiny, small, medium, large, big, huge, massive} = req.body;
			var userPicture = new userPictureModel();
			
			userPicture.mini = mini;
			userPicture.tiny = tiny;
			userPicture.small = small;
			userPicture.medium = medium;
			userPicture.large = large;
			userPicture.big = big;
			userPicture.huge = huge;
			userPicture.massive = massive;

			userPicture.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'userPicture created!';
					log.info(message);
					res.json({message, userPicture});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			userPictureModel.findById(req.params.id, (err, userPicture) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({userPicture});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {mini, tiny, small, medium, large, big, huge, massive, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			userPictureModel.findById(req.params.id, (err, userPicture) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(userPicture)) {				
						// if(_all) {
						// 	error = validateAll(req.body);
						// } else {
						// 	error = validate(req.body);
						// }
						
						if(isEmpty(error)) {
							if(!isEmpty(mini)) {
								log.trace('mini changed');
								userPicture.mini = mini;	
							}

							if(!isEmpty(tiny)) {
								log.trace('tiny changed');
								userPicture.tiny = tiny;	
							}

							if(!isEmpty(small)) {
								log.trace('small changed');
								userPicture.small = small;	
							}

							if(!isEmpty(medium)) {
								log.trace('medium changed');
								userPicture.medium = medium;	
							}

							if(!isEmpty(large)) {
								log.trace('large changed');
								userPicture.large = large;	
							}

							if(!isEmpty(big)) {
								log.trace('big changed');
								userPicture.big = big;	
							}

							if(!isEmpty(huge)) {
								log.trace('huge changed');
								userPicture.huge = huge;	
							}

							if(!isEmpty(massive)) {
								log.trace('massive changed');
								userPicture.massive = massive;	
							}

							userPicture.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'userPicture updated!';
									log.info(message);
									res.json({message, userPicture});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'userPicture is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			userPictureModel.findByIdAndRemove(req.params.id, (err, userPicture) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(userPicture)) {
						message = 'userPicture deleted!';
						res.json({message, userPicture});	
					} else {
						error = 'userPicture is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;