import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import senderModel from '../model/sender.js';

const router = express.Router();
const log = Log('senderAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(!isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';
	
	if(!isEmpty(data.teacher))
		if(!mongoose.Types.ObjectId.isValid(data.teacher))
			error.teacher = 'teacher is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		senderModel.find((err, sender) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({sender});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var messageRes = {};
		var error = {};
		
		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {student, teacher} = req.body;
			var sender = new senderModel();

			sender.student = student;
			sender.teacher = teacher;

			sender.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					messageRes = 'sender created!';
					log.info(messageRes);
					res.json({messageRes, sender});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			senderModel.findById(req.params.id, (err, sender) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({sender});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var messageRes = {};
		var error = {};
		const {student, teacher, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			senderModel.findById(req.params.id, (err, sender) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(sender)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(student)) {
								log.trace('student changed');
								sender.student = student;	
							}

							if(!isEmpty(teacher)) {
								log.trace('teacher changed');
								sender.teacher = teacher;	
							}

							sender.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									messageRes = 'sender updated!';
									log.info(messageRes);
									res.json({messageRes, sender});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'sender is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var messageRes = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			senderModel.findByIdAndRemove(req.params.id, (err, sender) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(sender)) {
						messageRes = 'sender deleted!';
						res.json({messageRes, sender});	
					} else {
						error = 'sender is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;