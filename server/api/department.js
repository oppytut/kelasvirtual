import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import departmentModel from '../model/department.js';

const router = express.Router();
const log = Log('departmentAPI');

// function validate(data) {
// 	log.trace('validate');
// 	var error = {};

// 	return error;
// }

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	// error = validate(data);

	if(isEmpty(data.name))
		error.name = 'name is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		departmentModel.find((err, department) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({department});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {name} = req.body;
			var department = new departmentModel();

			department.name = name;

			department.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'department created!';
					log.info(message);
					res.json({message, department});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			departmentModel.findById(req.params.id, (err, department) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({department});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {name, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			departmentModel.findById(req.params.id, (err, department) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(department)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							// error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(name)) {
								log.trace('name changed');
								department.name = name;	
							}

							department.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'department updated!';
									log.info(message);
									res.json({message, department});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'department is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			departmentModel.findByIdAndRemove(req.params.id, (err, department) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(department)) {
						message = 'department deleted!';
						res.json({message, department});	
					} else {
						error = 'department is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;