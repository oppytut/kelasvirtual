import express from 'express';
import Log from 'debug-logger';

import {
	getMcQuestions,
	getMcQuestion,
	postMcQuestion,
	putMcQuestion,
	delMcQuestion,
	delMcQuestions
} from '../bridge/mcQuestion.js';

const router = express.Router();
const log = Log('mcQuestionAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getMcQuestions({})
			.then((mcQuestion) => {
				res.json({mcQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postMcQuestion({data: req.body})
			.then((mcQuestion) => {
				const message = 'mcQuestion created!';
				log.info(message);
				res.json({message, mcQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getMcQuestions({query, field, populate, limit})
			.then((mcQuestion) => {
				res.json({mcQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getMcQuestion({id: req.params.id})
			.then((mcQuestion) => {
				res.json({mcQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putMcQuestion({id: req.params.id, data: req.body})
			.then((mcQuestion) => {
				const message = 'mcQuestion updated!';
				log.info(message);
				res.json({message, mcQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delMcQuestion({id: req.params.id})
			.then((mcQuestion) => {
				const message = 'mcQuestion deleted!';
				res.json({message, mcQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delMcQuestions({query})
			.then((mcQuestion) => {
				const message = 'mcQuestion deleted!';
				res.json({message, mcQuestion});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
