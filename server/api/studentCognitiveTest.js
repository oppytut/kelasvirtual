import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import studentCognitiveTestModel from '../model/studentCognitiveTest.js';
import {
	getStudentCognitiveTests,
	delStudentCognitiveTests
} from '../bridge/studentCognitiveTest.js';

const router = express.Router();
const log = Log('studentCognitiveTestAPI');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.cognitiveTest))
		if(!mongoose.Types.ObjectId.isValid(data.cognitiveTest))
			error.cognitiveTest = 'cognitiveTest is not valid!';

	if(!isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';

	if(!isEmpty(data.pass))
		if(!validator.isBoolean(data.pass.toString()))
			error.pass = 'pass is not valid!';

	if(!isEmpty(data.score))
		if(!validator.isNumeric(data.score))
			error.score = 'score is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.cognitiveTest))
		error.cognitiveTest = 'cognitiveTest is required!';

	if(isEmpty(data.student))
		error.student = 'student is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		studentCognitiveTestModel.find((err, studentCognitiveTest) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({studentCognitiveTest});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {cognitiveTest, student, score, pass, answer, letterAnswer} = req.body;
			var studentCognitiveTest = new studentCognitiveTestModel();

			studentCognitiveTest.cognitiveTest = cognitiveTest;
			studentCognitiveTest.student = student;
			studentCognitiveTest.score = score;
			studentCognitiveTest.pass = pass;
			studentCognitiveTest.answer = answer;
			studentCognitiveTest.letterAnswer = letterAnswer;

			studentCognitiveTest.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'studentCognitiveTest created!';
					log.info(message);
					res.json({message, studentCognitiveTest});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('POST aka GET by query');

		const {query, field, populate, limit} = req.body;

		getStudentCognitiveTests({query, field, populate, limit})
			.then((studentCognitiveTest) => {
				res.json({studentCognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentCognitiveTestModel.findById(req.params.id, (err, studentCognitiveTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({studentCognitiveTest});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {cognitiveTest, student, score, pass, answer, letterAnswer, _all} = req.body;

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentCognitiveTestModel.findById(req.params.id, (err, studentCognitiveTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentCognitiveTest)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(cognitiveTest)) {
								log.trace('cognitiveTest changed');
								studentCognitiveTest.cognitiveTest = cognitiveTest;
							}

							if(!isEmpty(student)) {
								log.trace('student changed');
								studentCognitiveTest.student = student;
							}

							if(!isEmpty(score)) {
								log.trace('score changed');
								studentCognitiveTest.score = score;
							}

							if(!isEmpty(pass)) {
								log.trace('pass changed');
								studentCognitiveTest.pass = pass;
							}

							if(!isEmpty(answer)) {
								log.trace('answer changed');
								studentCognitiveTest.answer = answer;
							}

							if(!isEmpty(letterAnswer)) {
								log.trace('letterAnswer changed');
								studentCognitiveTest.letterAnswer = letterAnswer;
							}

							studentCognitiveTest.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'studentCognitiveTest updated!';
									log.info(message);
									res.json({message, studentCognitiveTest});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'studentCognitiveTest is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentCognitiveTestModel.findByIdAndRemove(req.params.id, (err, studentCognitiveTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentCognitiveTest)) {
						message = 'studentCognitiveTest deleted!';
						res.json({message, studentCognitiveTest});
					} else {
						error = 'studentCognitiveTest is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('POST aka DEL by query');

		const {query} = req.body;

		delStudentCognitiveTests({query})
			.then((studentCognitiveTest) => {
				res.json({studentCognitiveTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
