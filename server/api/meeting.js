import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import meetingModel from '../model/meeting.js';

const router = express.Router();
const log = Log('meetingAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(!isEmpty(data.course))
		if(!mongoose.Types.ObjectId.isValid(data.course))
			error.course = 'course is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	if(isEmpty(data.content))
		error.content = 'content is required!';

	if(isEmpty(data.course))
		error.course = 'course is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		meetingModel.find((err, meeting) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({meeting});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);		

		if(isEmpty(error)) {
			const {content, course} = req.body;
			var meeting = new meetingModel();

			meeting.content = content;
			meeting.course = course;

			meeting.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'meeting created!';
					log.info(message);
					res.json({message, meeting});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			meetingModel.findById(req.params.id, (err, meeting) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({meeting});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {content, course, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			meetingModel.findById(req.params.id, (err, meeting) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(meeting)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}						
						
						if(isEmpty(error)) {
							if(!isEmpty(content)) {
								log.trace('content changed');
								meeting.content = content;	
							}

							if(!isEmpty(course)) {
								log.trace('course changed');
								meeting.course = course;	
							}

							meeting.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'meeting updated!';
									log.info(message);
									res.json({message, meeting});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'meeting is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			meetingModel.findByIdAndRemove(req.params.id, (err, meeting) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(meeting)) {
						message = 'meeting deleted!';
						res.json({message, meeting});	
					} else {
						error = 'meeting is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;