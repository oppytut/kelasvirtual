import express from 'express';
import Log from 'debug-logger';

import {
	getObjectives,
	getObjective,
	postObjective,
	putObjective,
	delObjective,
	delObjectives
} from '../bridge/objective.js';

const router = express.Router();
const log = Log('objectiveAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getObjectives({})
			.then((objective) => {
				res.json({objective});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postObjective({data: req.body})
			.then((objective) => {
				const message = 'objective created!';
				log.info(message);
				res.json({message, objective});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getObjectives({query, field, populate, limit})
			.then((objective) => {
				res.json({objective});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getObjective({id: req.params.id})
			.then((objective) => {
				res.json({objective});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putObjective({id: req.params.id, data: req.body})
			.then((objective) => {
				const message = 'objective updated!';
				log.info(message);
				res.json({message, objective});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delObjective({id: req.params.id})
			.then((objective) => {
				const message = 'objective deleted!';
				res.json({message, objective});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delObjectives({query})
			.then((objective) => {
				const message = 'objective deleted!';
				res.json({message, objective});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
