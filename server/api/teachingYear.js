import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import teachingYearModel from '../model/teachingYear.js';

const router = express.Router();
const log = Log('teachingYearAPI');

// function validate(data) {
// 	log.trace('validate');
// 	var error = {};

// 	return error;
// }

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	// error = validate(data);

	if(isEmpty(data.year))
		error.year = 'year is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		teachingYearModel.find((err, teachingYear) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({teachingYear});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {year} = req.body;
			var teachingYear = new teachingYearModel();

			teachingYear.year = year;

			teachingYear.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'teachingYear created!';
					log.info(message);
					res.json({message, teachingYear});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			teachingYearModel.findById(req.params.id, (err, teachingYear) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({teachingYear});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {year, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			teachingYearModel.findById(req.params.id, (err, teachingYear) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(teachingYear)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							// error = validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(year)) {
								log.trace('year changed');
								teachingYear.year = year;	
							}

							teachingYear.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'teachingYear updated!';
									log.info(message);
									res.json({message, teachingYear});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'teachingYear is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			teachingYearModel.findByIdAndRemove(req.params.id, (err, teachingYear) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(teachingYear)) {
						message = 'teachingYear deleted!';
						res.json({message, teachingYear});	
					} else {
						error = 'teachingYear is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;