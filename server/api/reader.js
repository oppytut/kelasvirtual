import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import readerModel from '../model/reader.js';

const router = express.Router();
const log = Log('readerAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(!isEmpty(data.message))	
		if(!mongoose.Types.ObjectId.isValid(data.message))
			error.message = 'message is not valid!';
	
	if(!isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';
	
	if(!isEmpty(data.teacher))
		if(!mongoose.Types.ObjectId.isValid(data.teacher))
			error.teacher = 'teacher is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	if(isEmpty(data.message))
		error.message = 'message is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		readerModel.find((err, reader) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({reader});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var messageRes = {};
		var error = {};
		
		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {message, student, teacher} = req.body;
			var reader = new readerModel();

			reader.message = message;
			reader.student = student;
			reader.teacher = teacher;

			reader.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					messageRes = 'reader created!';
					log.info(messageRes);
					res.json({messageRes, reader});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			readerModel.findById(req.params.id, (err, reader) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({reader});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var messageRes = {};
		var error = {};
		const {message, student, teacher, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			readerModel.findById(req.params.id, (err, reader) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(reader)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(message)) {
								log.trace('message changed');
								reader.message = message;	
							}

							if(!isEmpty(student)) {
								log.trace('student changed');
								reader.student = student;	
							}

							if(!isEmpty(teacher)) {
								log.trace('teacher changed');
								reader.teacher = teacher;	
							}

							reader.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									messageRes = 'reader updated!';
									log.info(messageRes);
									res.json({messageRes, reader});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'reader is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var messageRes = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			readerModel.findByIdAndRemove(req.params.id, (err, reader) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(reader)) {
						messageRes = 'reader deleted!';
						res.json({messageRes, reader});	
					} else {
						error = 'reader is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;