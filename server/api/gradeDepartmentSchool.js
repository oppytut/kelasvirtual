import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import gradeDepartmentSchoolModel from '../model/gradeDepartmentSchool.js';

const router = express.Router();
const log = Log('gradeDepartmentSchoolAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(!isEmpty(data.school))
		if(!mongoose.Types.ObjectId.isValid(data.school))
			error.school = 'school is not valid!';

	if(!isEmpty(data.department))
		if(!mongoose.Types.ObjectId.isValid(data.department))
			error.department = 'department is not valid!';
	
	if(!isEmpty(data.grade))
		if(!mongoose.Types.ObjectId.isValid(data.grade))
			error.grade = 'grade is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	if(isEmpty(data.school))
		error.school = 'school is required!';

	if(isEmpty(data.department))
		error.department = 'department is required!';

	if(isEmpty(data.grade))
		error.grade = 'grade is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		gradeDepartmentSchoolModel.find((err, gradeDepartmentSchool) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({gradeDepartmentSchool});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {school, department, grade} = req.body;
			var gradeDepartmentSchool = new gradeDepartmentSchoolModel();

			gradeDepartmentSchool.school = school;
			gradeDepartmentSchool.department = department;
			gradeDepartmentSchool.grade = grade;

			gradeDepartmentSchool.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'gradeDepartmentSchool created!';
					log.info(message);
					res.json({message, gradeDepartmentSchool});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			gradeDepartmentSchoolModel.findById(req.params.id, (err, gradeDepartmentSchool) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({gradeDepartmentSchool});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {school, department, grade, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			gradeDepartmentSchoolModel.findById(req.params.id, (err, gradeDepartmentSchool) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(gradeDepartmentSchool)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(school)) {
								log.trace('school changed');
								gradeDepartmentSchool.school = school;	
							}

							if(!isEmpty(department)) {
								log.trace('department changed');
								gradeDepartmentSchool.department = department;	
							}

							if(!isEmpty(grade)) {
								log.trace('grade changed');
								gradeDepartmentSchool.grade = grade;	
							}

							gradeDepartmentSchool.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'gradeDepartmentSchool updated!';
									log.info(message);
									res.json({message, gradeDepartmentSchool});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'gradeDepartmentSchool is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			gradeDepartmentSchoolModel.findByIdAndRemove(req.params.id, (err, gradeDepartmentSchool) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(gradeDepartmentSchool)) {
						message = 'gradeDepartmentSchool deleted!';
						res.json({message, gradeDepartmentSchool});	
					} else {
						error = 'gradeDepartmentSchool is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;