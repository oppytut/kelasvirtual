import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';
import validator from 'validator';

import studentPsychomotorTestModel from '../model/studentPsychomotorTest.js';
import {
	getStudentPsychomotorTests,
	delStudentPsychomotorTests
} from '../bridge/studentPsychomotorTest.js';

const router = express.Router();
const log = Log('studentPsychomotorTestAPI');

function validate(data) {
	log.trace('validate');
	var error = {};

	if(!isEmpty(data.psychomotorTest))
		if(!mongoose.Types.ObjectId.isValid(data.psychomotorTest))
			error.psychomotorTest = 'psychomotorTest is not valid!';

	if(!isEmpty(data.student))
		if(!mongoose.Types.ObjectId.isValid(data.student))
			error.student = 'student is not valid!';

	if(!isEmpty(data.score))
		if(!validator.isNumeric(data.score))
			error.student = 'score is not valid!';

	if(!isEmpty(data.pass))
		if(!validator.isBoolean(data.pass.toString()))
			error.pass = 'pass is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};

	error = validate(data);

	if(isEmpty(data.psychomotorTest))
		error.psychomotorTest = 'psychomotorTest is required!';

	if(isEmpty(data.student))
		error.student = 'student is required!';

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		studentPsychomotorTestModel.find((err, studentPsychomotorTest) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({studentPsychomotorTest});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {psychomotorTest, student, score, pass} = req.body;
			var studentPsychomotorTest = new studentPsychomotorTestModel();

			studentPsychomotorTest.psychomotorTest = psychomotorTest;
			studentPsychomotorTest.student = student;
			studentPsychomotorTest.score = score;
			studentPsychomotorTest.pass = pass;

			studentPsychomotorTest.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'studentPsychomotorTest created!';
					log.info(message);
					res.json({message, studentPsychomotorTest});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('POST aka GET by query');

		const {query, field, populate, limit} = req.body;

		getStudentPsychomotorTests({query, field, populate, limit})
			.then((studentPsychomotorTest) => {
				res.json({studentPsychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentPsychomotorTestModel.findById(req.params.id, (err, studentPsychomotorTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({studentPsychomotorTest});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {psychomotorTest, student, score, pass, _all} = req.body;

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentPsychomotorTestModel.findById(req.params.id, (err, studentPsychomotorTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentPsychomotorTest)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}

						if(isEmpty(error)) {
							if(!isEmpty(psychomotorTest)) {
								log.trace('psychomotorTest changed');
								studentPsychomotorTest.psychomotorTest = psychomotorTest;
							}

							if(!isEmpty(student)) {
								log.trace('student changed');
								studentPsychomotorTest.student = student;
							}

							if(!isEmpty(score)) {
								log.trace('score changed');
								studentPsychomotorTest.score = score;
							}

							if(!isEmpty(pass)) {
								log.trace('pass changed');
								studentPsychomotorTest.pass = pass;
							}

							studentPsychomotorTest.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'studentPsychomotorTest updated!';
									log.info(message);
									res.json({message, studentPsychomotorTest});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'studentPsychomotorTest is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			studentPsychomotorTestModel.findByIdAndRemove(req.params.id, (err, studentPsychomotorTest) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(studentPsychomotorTest)) {
						message = 'studentPsychomotorTest deleted!';
						res.json({message, studentPsychomotorTest});
					} else {
						error = 'studentPsychomotorTest is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('POST aka DEL by query');

		const {query} = req.body;

		delStudentPsychomotorTests({query})
			.then((studentPsychomotorTest) => {
				res.json({studentPsychomotorTest});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
