import mongoose from 'mongoose';
import express from 'express';
import Log from 'debug-logger';
import isEmpty from 'is-empty';

import discussionModel from '../model/discussion.js';

const router = express.Router();
const log = Log('discussionAPI');

function validate(data) {
	log.trace('validate');
	var error = {};
	
	if(!isEmpty(data.course))
		if(!mongoose.Types.ObjectId.isValid(data.course))
			error.course = 'course is not valid!';

	return error;
}

function validateAll(data) {
	log.trace('validateAll');
	var error = {};
	
	error = validate(data);

	if(isEmpty(data.title))
		error.title = 'title is required!';

	if(isEmpty(data.course))
		error.course = 'course is required!';	

	return error;
}

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		discussionModel.find((err, discussion) => {
			if(err) {
				log.error(err);
				res.send(err);
			} else {
				res.json({discussion});
			}
		});
	})
	.post((req, res) => {
		log.trace('POST');

		var message = {};
		var error = {};

		error = validateAll(req.body);

		if(isEmpty(error)) {
			const {title, course} = req.body;
			var discussion = new discussionModel();

			discussion.title = title;
			discussion.course = course;

			discussion.save((err) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					message = 'discussion created!';
					log.info(message);
					res.json({message, discussion});
				}
			});
		} else {
			res.status(400).json({error});
		}
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');
		
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			discussionModel.findById(req.params.id, (err, discussion) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					res.json({discussion});
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		var message = {};
		var error = {};
		const {title, course, _all} = req.body;
		
		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			discussionModel.findById(req.params.id, (err, discussion) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(discussion)) {
						if(_all) {
							error = validateAll(req.body);
						} else {
							error = validate(req.body);
						}
						
						if(isEmpty(error)) {
							if(!isEmpty(title)) {
								log.trace('title changed');
								discussion.title = title;	
							}

							if(!isEmpty(course)) {
								log.trace('course changed');
								discussion.course = course;	
							}

							discussion.save((err) => {
								if(err) {
									log.error(err);
									res.send(err);
								} else {
									message = 'discussion updated!';
									log.info(message);
									res.json({message, discussion});
								}
							});
						} else {
							res.status(400).json({error});
						}
					} else {
						error = 'discussion is not found!';
						log.warn(error);
						res.status(400).json({error});
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		var message = {};
		var error = {};

		if(mongoose.Types.ObjectId.isValid(req.params.id)) {
			discussionModel.findByIdAndRemove(req.params.id, (err, discussion) => {
				if(err) {
					log.error(err);
					res.send(err);
				} else {
					if(!isEmpty(discussion)) {
						message = 'discussion deleted!';
						res.json({message, discussion});	
					} else {
						error = 'discussion is not found!';
						log.warn(error);
						res.status(400).json({error});	
					}
				}
			});
		} else {
			error = 'id is not valid!';
			log.warn(error);
			res.status(400).json({error});
		}
	});

export default router;