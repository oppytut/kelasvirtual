import express from 'express';
import Log from 'debug-logger';

import {
	getBasicCompetencys,
	getBasicCompetency,
	postBasicCompetency,
	putBasicCompetency,
	delBasicCompetency,
	delBasicCompetencys,
	getBasicCompetencysByCourse
} from '../bridge/basicCompetency.js';

const router = express.Router();
const log = Log('basicCompetencyAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getBasicCompetencys({})
			.then((basicCompetency) => {
				res.json({basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postBasicCompetency({data: req.body})
			.then((basicCompetency) => {
				const message = 'basicCompetency created!';
				log.info(message);
				res.json({message, basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getBasicCompetencys({query, field, populate, limit})
			.then((basicCompetency) => {
				res.json({basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getBasicCompetency({id: req.params.id})
			.then((basicCompetency) => {
				res.json({basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putBasicCompetency({id: req.params.id, data: req.body})
			.then((basicCompetency) => {
				const message = 'basicCompetency updated!';
				log.info(message);
				res.json({message, basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delBasicCompetency({id: req.params.id})
			.then((basicCompetency) => {
				const message = 'basicCompetency deleted!';
				res.json({message, basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delBasicCompetencys({query})
			.then((basicCompetency) => {
				const message = 'basicCompetency deleted!';
				res.json({message, basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/course/')
	.post((req, res) => {
		log.trace('GET(POST) by courseId');

		const {query, field, populate, limit}	= req.body;

		getBasicCompetencysByCourse({query, field, populate, limit})
			.then((basicCompetency) => {
				res.json({basicCompetency});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
