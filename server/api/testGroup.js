import express from 'express';
import Log from 'debug-logger';

import {
	getTestGroups,
	getTestGroup,
	postTestGroup,
	putTestGroup,
	delTestGroup,
	delTestGroups
} from '../bridge/testGroup.js';

const router = express.Router();
const log = Log('testGroupAPI');

router.route('/')
	.get((req, res) => {
		log.trace('GET all');

		getTestGroups({})
			.then((testGroup) => {
				res.json({testGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.post((req, res) => {
		log.trace('POST');

		postTestGroup({data: req.body})
			.then((testGroup) => {
				const message = 'testGroup created!';
				log.info(message);
				res.json({message, testGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/')
	.post((req, res) => {
		log.trace('GET(POST) by query');

		const {query, field, populate, limit}	= req.body;

		getTestGroups({query, field, populate, limit})
			.then((testGroup) => {
				res.json({testGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/:id')
	.get((req, res) => {
		log.trace('GET by Id');

		getTestGroup({id: req.params.id})
			.then((testGroup) => {
				res.json({testGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.put((req, res) => {
		log.trace('PUT by Id');

		putTestGroup({id: req.params.id, data: req.body})
			.then((testGroup) => {
				const message = 'testGroup updated!';
				log.info(message);
				res.json({message, testGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	})
	.delete((req, res) => {
		log.trace('DELETE by Id');

		delTestGroup({id: req.params.id})
			.then((testGroup) => {
				const message = 'testGroup deleted!';
				res.json({message, testGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

router.route('/query/del/')
	.post((req, res) => {
		log.trace('DEL(POST) by query');

		const {query} = req.body;

		delTestGroups({query})
			.then((testGroup) => {
				const message = 'testGroup deleted!';
				res.json({message, testGroup});
			})
			.catch((err) => {
				res.status(400).json(err);
			});
	});

export default router;
