import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement DROP TABLE digunakan untuk menghapus sebuah entitas pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				DROP TABLE nama_entitas;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> drop table siswa;<br />
				Query OK, 0 rows affected (0,21 sec)
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;