import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement UPDATE digunakan untuk memperbarui data lama menjadi data terbaru. Perlu diingat perintah update ini harus dikuti dengan perintah WHERE sebagai kondisi untuk menentukan data mana yang akan diperbarui.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				UPDATE entitas SET <br />
				nama_kolom_1=nilai, nama_kolom_2=nilai, ... nama_kolom_n=nilai <br />
				WHERE kondisi; <br />
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				update siswa set nama='Zaen Fawaid' where nis='85439827';
			</div>
		`;
		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;