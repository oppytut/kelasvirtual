import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement SELECT dapat digunakan juga untuk menampilkan record dari beberapa entitas.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SELECT nama_entitas_1.nama_kolom, nama_entitas_2.nama_kolom, ... <br />
				FROM nama_entitas_1, nama_entitas_2, ... <br />
				WHERE kondisi;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				select siswa.nama_siswa, anggota_ekskul.nilai_ekskul <br />
				from siswa, anggota_ekskul <br />
				where siswa.id_siswa=anggota_ekskul.id_siswa;
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;