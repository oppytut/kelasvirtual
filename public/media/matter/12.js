import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Operator ORDER BY digunakan untuk mengurutkan data.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SELECT * FROM entitas ORDER BY field;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				select * from siswa order by tgl_lahir;
			</div>
		`;
		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;