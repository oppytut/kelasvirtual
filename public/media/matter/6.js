import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement SELECT digunakan untuk menampilkan record dari sebuah entitas pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SELECT nama_kolom_1, nama_kolom_2, nama_kolom_n FROM nama_entitas;<br />
				SELECT * FROM nama_entitas;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> select * from siswa;<br />
				+----------+--------+-----------------+<br />
				| id_siswa | nis&nbsp&nbsp&nbsp&nbsp| nama_siswa&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp|<br />
				+----------+--------+-----------------+<br />
				|&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp1 | 110118 | Rani Puspita&nbsp&nbsp&nbsp&nbsp|<br />
				|&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp2 | 110119 | Zaen Fawaid&nbsp&nbsp&nbsp&nbsp&nbsp|<br />
				|&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp3 | 110120 | Alya Nur Zakira |<br />
				+----------+--------+-----------------+<br />
				3 rows in set (0,00 sec)
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;