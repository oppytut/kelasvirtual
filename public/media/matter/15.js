import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement DELETE digunakan untuk menghapus atau menghilangkan baris data (record) dari entitas. Penggunaan perintah ini juga harus menggunakan WHERE sebagai kondisi untuk menentukan data mana yang akan dihapus.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				DELETE FROM entitas WHERE kondisi;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				delete from siswa where nis='110122';
			</div>
		`;
		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;