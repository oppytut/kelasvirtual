import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			<h3 style="margin: 20px 0px 5px 0px">Membuat Database</h3>
			Statement CREATE DATABASE digunakan untuk membuat database baru.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				CREATE DATABASE nama_database;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> create database kesiswaan;<br />
				Query OK, 1 row affected (0,00 sec)
			</div>

			<br />

			<h3 style="margin: 20px 0px 5px 0px">Menampilkan Daftar Database</h3>
			Statement SHOW DATABASE digunakan untuk melihat daftar database yang ada.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SHOW DATABASES;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> show databases;<br />
				+--------------------+<br />
				| Database&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp|<br />
				+--------------------+<br />
				| information_schema |<br />
				| bimbingan_koseling |<br />
				| hotel&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp|<br />
				| kesiswaan&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp|<br />
				| mysql&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp|<br />
				| performance_schema&nbsp|<br />
				| sys&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp|<br />
				+--------------------+<br />
				7 rows in set (0,00 sec)
			</div>

			<br />

			<h3 style="margin: 20px 0px 5px 0px">Menggunakan Database</h3>
			Statement USE digunakan untuk menggunakan suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				USE nama_database;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> use kesiswaan;<br />
				Database changed
			</div>

			<br />

			<h3 style="margin: 20px 0px 5px 0px">Menghapus Database</h3>
			Statement DROP DATABASE digunakan untuk menghapus suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				DROP DATABASE nama_database;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> drop database kesiswaan;<br />
				Query OK, 0 row affected (0,03 sec)
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;