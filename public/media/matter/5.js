import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement INSERT INTO digunakan untuk menambah record baru di sebuah entitas pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				INSERT INTO nama_entitas VALUES<br />
				&emsp;&emsp;&emsp;(nilai_1, nilai_2, ... nilai_n),<br />
				&emsp;&emsp;&emsp;(nilai_1, nilai_2, ... nilai_n),<br />
				&emsp;&emsp;&emsp;...<br />
				&emsp;&emsp;&emsp;(nilai_1, nilai_2, ... nilai_n);<br />
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> insert into siswa values <br />
				&emsp;&emsp;&emsp;-> (1,'110118','Rani Puspita'),<br />
				&emsp;&emsp;&emsp;-> (2,'110119','Zaen Fawaid'),<br />
				&emsp;&emsp;&emsp;-> (3,'110120','Alya Nur Zakira');<br />
				Query OK, 3 rows affected (0,05 sec)<br />
				Records: 3  Duplicates: 0  Warnings: 0
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;