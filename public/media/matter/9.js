import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement WHERE dengan operator OR dan AND digunakan juga untuk menampilkan record dengan dua kondisi.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SELECT nama_kolom_1, nama_kolom_2, ... nama_kolom_n <br />
				FROM entitas_1, entitas_2, ... entitas_n <br />
				WHERE kondisi_1 AND kondisi_2 <br /> <br />
				SELECT nama_kolom_1, nama_kolom_2, ... nama_kolom_n <br />
				FROM entitas_1, entitas_2, ... entitas_n <br />
				WHERE kondisi_1 OR kondisi_2
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				select nis, nama_siswa <br />
				from siswa <br />
				where jk='L' and tgl_lahir='1999-10-1';
			</div>
		`;
		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;