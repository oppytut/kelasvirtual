import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Operator LIKE digunakan untuk menseleksi data dengan kriteria mengandung kata atau klausa yang didefinisikan.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SELECT * FROM entitas WHERE field LIKE '% ... %';
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				select * from siswa where nama_siswa like '%arya%';
			</div>
		`;
		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;