import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			<h3 style="margin: 20px 0px 5px 0px">Mengubah Nama Entitas</h3>
			Statement ALTER TABLE dengan option RENAME TO digunakan untuk mengubah nama sebuah entitas pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				ALTER TABLE nama_lama RENAME TO nama_baru;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> alter table murid rename to siswa;<br />
				Query OK, 0 rows affected (0,21 sec)
			</div>

			<br />

			<h3 style="margin: 20px 0px 5px 0px">Menambah Kolom pada Entitas</h3>
			Statement ALTER TABLE dengan option ADD COLUMN digunakan untuk menambah kolom baru di sebuah entitas pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				ALTER TABLE nama_entitas ADD COLUMN (<br />
				&emsp;&emsp;&emsp;nama_kolom_1 tipe_data,<br />
				&emsp;&emsp;&emsp;nama_kolom_2 tipe_data,<br />
				&emsp;&emsp;&emsp;nama_kolom_3 tipe_data,<br />
				&emsp;&emsp;&emsp;...<br />
				);
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> alter table siswa add column ( <br />
				&emsp;&emsp;&emsp;-> jk varchar(3) not null,<br />
				&emsp;&emsp;&emsp;-> tgl_lahir date not null<br />
				&emsp;&emsp;&emsp;-> );<br />
				Query OK, 0 rows affected (0,53 sec)<br />
				Records: 0  Duplicates: 0  Warnings: 0
			</div>

			<br />

			<h3 style="margin: 20px 0px 5px 0px">Mengganti Nama Kolom</h3>
			Statement ALTER TABLE dengan option CHANGE COLUMN digunakan untuk mengganti nama kolom di sebuah entitas pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				ALTER TABLE nama_entitas CHANGE COLUMN nama_kolom_lama nama_kolom_baru tipe_data;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> alter table siswa change column jk jenis_kelamin varchar(3);<br />
				Query OK, 0 rows affected (0,08 sec)<br />
				Records: 0  Duplicates: 0  Warnings: 0
			</div>

			<br />

			<h3 style="margin: 20px 0px 5px 0px">Menghapus Kolom</h3>
			Statement ALTER TABLE dengan option DROP COLUMN digunakan untuk menghapus kolom di sebuah entitas pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				ALTER TABLE nama_entitas DROP COLUMN nama_kolom;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> alter table siswa drop column jenis_kelamin;<br />
				Query OK, 0 rows affected (0,67 sec)<br />
				Records: 0  Duplicates: 0  Warnings: 0
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;