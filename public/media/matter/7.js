import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement SELECT dengan WHERE digunakan untuk menampilkan record dari sebuah entitas menggunakan kondisi pada suatu database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SELECT nama_kolom FROM nama_entitas WHERE kondisi;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> select nama_siswa, nis from siswa where nis='110118';<br />
				+--------------+--------+<br />
				| nama_siswa&nbsp&nbsp&nbsp| nis&nbsp&nbsp&nbsp&nbsp|<br />
				+--------------+--------+<br />
				| Rani Puspita | 110118 |<br />
				+--------------+--------+<br />
				1 row in set (0,01 sec)
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;