import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Statement CREATE TABLE digunakan untuk membuat entitas baru pada sebuah database.

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				CREATE TABLE nama_entitas (<br />
				&emsp;&emsp;&emsp;nama_kolom_1 tipe_data,<br />
				&emsp;&emsp;&emsp;nama_kolom_2 tipe_data,<br />
				&emsp;&emsp;&emsp;nama_kolom_3 tipe_data,<br />
				&emsp;&emsp;&emsp;...<br />
				);
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				mysql> create table siswa (<br />
				&emsp;&emsp;&emsp;-> id_siswa int primary key auto_increment,<br />
				&emsp;&emsp;&emsp;-> nis varchar(13) not null,<br />
				&emsp;&emsp;&emsp;-> nama_siswa varchar(30) not null<br />
				&emsp;&emsp;&emsp;-> );<br />
				Query OK, 0 rows affected (0,25 sec)
			</div>
			<br />
			<div style="font-family: consolas">
				mysql>create table ekskul (<br />
				&emsp;&emsp;&emsp;-> id_ekskul int primary key auto_increment,<br />
				&emsp;&emsp;&emsp;-> nama_ekskul varchar(20) not null<br />
				&emsp;&emsp;&emsp;-> );<br />
				Query OK, 0 rows affected (0,24 sec)
			</div>
			<br />
			<div style="font-family: consolas">
				create table anggota_ekskul(<br />
				&emsp;&emsp;&emsp;-> id_anggota int primary key auto_increment,<br />
				&emsp;&emsp;&emsp;-> semester varchar(3) not null,<br />
				&emsp;&emsp;&emsp;-> nilai int not null,<br />
				&emsp;&emsp;&emsp;-> id_siswa int not null,<br />
				&emsp;&emsp;&emsp;-> id_ekskul int not null,<br />
				&emsp;&emsp;&emsp;-> foreign key (id_siswa) references siswa (id_siswa),<br />
				&emsp;&emsp;&emsp;-> foreign key (id_ekskul) references ekskul (id_ekskul)<br />
				&emsp;&emsp;&emsp;-> );<br />
				Query OK, 0 rows affected (0,24 sec)
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Keterangan:</h5>
			<div class="ui list" style="margin: 5px 0px 5px 0px">
				<div class="item">
					<i class="check icon"></i>
					<div class="content">
						<a class="header">
							primary key
						</a>
						<div class="description">
							Nilai field primary key harus berbeda dengan nilai field primary key pada setiap record dan tidak boleh kosong.
						</div>
						<div class="description">
							Field primary key akan digunakan untuk mengidentifikasi setiap pada record.
						</div>
						<div class="description">
							Setiap entitas hanya boleh memiliki satu field primary key.
						</div>
					</div>
				</div>
				<div class="item">
					<i class="check icon"></i>
					<div class="content">
						<a class="header">
							auto_increment
						</a>
						<div class="description">
							Field auto_increment akan terisi secara otomatis dan menghasilkan nilai bertambah secara terurut dari nilai field record terakhir.
						</div>
					</div>
				</div>
				<div class="item">
					<i class="check icon"></i>
					<div class="content">
						<a class="header">
							not null
						</a>
						<div class="description">
							Field harus terisi atau tidak boleh kosong.
						</div>
					</div>
				</div>
				<div class="item">
					<i class="check icon"></i>
					<div class="content">
						<a class="header">
							unique
						</a>
						<div class="description">
							Field unique harus berbeda dengan field unique pada setiap record.
						</div>
					</div>
				</div>
			</div>
		`;

		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;