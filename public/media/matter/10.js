import React, {Component} from 'react';

class Materi1 extends Component {
	render() {
		const content = `
			Operator BETWEEN digunakan untuk menyaring data dengan rentang tertentu (memiliki jangkauan).

			<h5 style="margin: 20px 0px 5px 0px">Sintaks:</h5>
			<div style="font-family: consolas">
				SELECT nama_kolom_1, nama_kolom_2, ... nama_kolom_n <br />
				FROM nama_entitas_1, nama_entitas_2, ... nama_entitas_n <br />
				WHERE field BETWEEN batas_awal AND batas_akhir;
			</div>

			<h5 style="margin: 20px 0px 5px 0px">Contoh:</h5>
			<div style="font-family: consolas">
				select * from siswa <br />
				where tgl_lahir between '1999-08-11' and '1998-04-19';
			</div>
		`;
		return(
			<div>
				{content}
			</div>
		);
	}
}

export default Materi1;