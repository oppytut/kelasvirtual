import React, {Component} from 'react';
import debug from 'debug';
import {Dimmer, Loader, Header} from 'semantic-ui-react';

import HeaderComp from './component/header';
import Body from './component/body';

const logName = 'App';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class App extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentDidMount() {
		log.trace('componentDidMount');

		this.setState({loading: false});
	}

	render() {
		log.trace('render');

		return(
			<div>
				{this.state.loading ?
					<div>
						<Dimmer active>
							<Loader>
								<Header as='h4' color='teal'>
									Sedang mempersiapkan ...
								</Header>
								Kelas Virtual Beta v0.01
							</Loader>
						</Dimmer>
					</div>
					:
					<div>
						<HeaderComp />
						<Body />
					</div>
				}
			</div>
		);
	}
}

export default App;