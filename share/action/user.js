/**
 * API
 */

import {
	SET_USER,
	SET_USERS,
	ADD_USER, 
	UPDATE_USER,
	DELETE_USER, 
	UPDATE_ADD_USER
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:USER';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setUser(user) {
	log.trace('setUsers');
	return {
		type: SET_USER,
		user
	};
}

export function setUsers(user) {
	log.trace('setUsers');
	return {
		type: SET_USERS,
		user
	};
}

export function addUser(user) {
	log.trace('addUser');
	return {
		type: ADD_USER,
		user
	};
}

export function updateUser(user) {
	log.trace('updateUser');
	return {
		type: UPDATE_USER,
		user
	};
}

export function deleteUser(user) {
	log.trace('deleteUser');
	return {
		type: DELETE_USER,
		user
	};
}

export function updateAddUser(user) {
	log.trace('updateAddUser');
	return {
		type: UPDATE_ADD_USER,
		user
	};
}

/**
 * Database
 */
export function getUsers({query, field, populate, limit}) {
	log.trace('getUsers');

	return () => {
		return fetch('/api/user/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getUser(id) {
	log.trace('getUser');

	return () => {
		return fetch(`/api/user/${id}`)
			.then(handleResponse);
	};
}

export function postUser(data) {
	log.trace('postUser');

	return () => {
		return fetch('/api/user', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putUser(data) {
	log.trace('putUser');

	return () => {
		return fetch(`/api/user/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delUser(id) {
	log.trace('delUser');

	return () => {
		return fetch(`/api/user/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}