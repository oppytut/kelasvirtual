/**
 * API
 */

import {
	SET_STUDENTMEETING,
	SET_STUDENTMEETINGS,
	ADD_STUDENTMEETING,
	UPDATE_STUDENTMEETING,
	DELETE_STUDENTMEETING,
	UPDATE_ADD_STUDENTMEETING
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:STUDENTMEETING';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setStudentMeeting(studentMeeting) {
	log.trace('setStudentMeetings');
	return {
		type: SET_STUDENTMEETING,
		studentMeeting
	};
}

export function setStudentMeetings(studentMeeting) {
	log.trace('setStudentMeetings');
	return {
		type: SET_STUDENTMEETINGS,
		studentMeeting
	};
}

export function addStudentMeeting(studentMeeting) {
	log.trace('addStudentMeeting');
	return {
		type: ADD_STUDENTMEETING,
		studentMeeting
	};
}

export function updateStudentMeeting(studentMeeting) {
	log.trace('updateStudentMeeting');
	return {
		type: UPDATE_STUDENTMEETING,
		studentMeeting
	};
}

export function deleteStudentMeeting(studentMeeting) {
	log.trace('deleteStudentMeeting');
	return {
		type: DELETE_STUDENTMEETING,
		studentMeeting
	};
}

export function updateAddStudentMeeting(studentMeeting) {
	log.trace('updateAddStudentMeeting');
	return {
		type: UPDATE_ADD_STUDENTMEETING,
		studentMeeting
	};
}

/**
 * Database
 */
export function getStudentMeetings({query, field, populate, limit}) {
	log.trace('getStudentMeetings');

	return () => {
		return fetch('/api/studentMeeting/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getStudentMeeting(id) {
	log.trace('getStudentMeeting');

	return () => {
		return fetch(`/api/studentMeeting/${id}`)
			.then(handleResponse);
	};
}

export function postStudentMeeting(data) {
	log.trace('postStudentMeeting');

	return () => {
		return fetch('/api/studentMeeting', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putStudentMeeting(data) {
	log.trace('putStudentMeeting');

	return () => {
		return fetch(`/api/studentMeeting/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentMeeting(id) {
	log.trace('delStudentMeeting');

	return () => {
		return fetch(`/api/studentMeeting/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentMeetings({query}) {
	log.trace('delStudentMeetings');

	return () => {
		return fetch('/api/studentMeeting/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
