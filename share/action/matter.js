/**
 * API
 */

import {
	SET_MATTER,
	SET_MATTERS,
	ADD_MATTER, 
	UPDATE_MATTER,
	DELETE_MATTER, 
	UPDATE_ADD_MATTER
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:MATTER';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setMatter(matter) {
	log.trace('setMatters');
	return {
		type: SET_MATTER,
		matter
	};
}

export function setMatters(matter) {
	log.trace('setMatters');
	return {
		type: SET_MATTERS,
		matter
	};
}

export function addMatter(matter) {
	log.trace('addMatter');
	return {
		type: ADD_MATTER,
		matter
	};
}

export function updateMatter(matter) {
	log.trace('updateMatter');
	return {
		type: UPDATE_MATTER,
		matter
	};
}

export function deleteMatter(matter) {
	log.trace('deleteMatter');
	return {
		type: DELETE_MATTER,
		matter
	};
}

export function updateAddMatter(matter) {
	log.trace('updateAddMatter');
	return {
		type: UPDATE_ADD_MATTER,
		matter
	};
}

/**
 * Database
 */
export function getMatters({query, field, populate, limit}) {
	log.trace('getMatters');

	return () => {
		return fetch('/api/matter/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getMatter(id) {
	log.trace('getMatter');

	return () => {
		return fetch(`/api/matter/${id}`)
			.then(handleResponse);
	};
}

export function postMatter(data) {
	log.trace('postMatter');

	return () => {
		return fetch('/api/matter', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putMatter(data) {
	log.trace('putMatter');

	return () => {
		return fetch(`/api/matter/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delMatter(id) {
	log.trace('delMatter');

	return () => {
		return fetch(`/api/matter/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}