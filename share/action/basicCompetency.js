/**
 * API
 */

import {
	SET_BASICCOMPETENCY,
	SET_BASICCOMPETENCYS,
	ADD_BASICCOMPETENCY,
	UPDATE_BASICCOMPETENCY,
	DELETE_BASICCOMPETENCY,
	UPDATE_ADD_BASICCOMPETENCY
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:BASICCOMPETENCY';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setBasicCompetency(basicCompetency) {
	log.trace('setBasicCompetencys');
	return {
		type: SET_BASICCOMPETENCY,
		basicCompetency
	};
}

export function setBasicCompetencys(basicCompetency) {
	log.trace('setBasicCompetencys');
	return {
		type: SET_BASICCOMPETENCYS,
		basicCompetency
	};
}

export function addBasicCompetency(basicCompetency) {
	log.trace('addBasicCompetency');
	return {
		type: ADD_BASICCOMPETENCY,
		basicCompetency
	};
}

export function updateBasicCompetency(basicCompetency) {
	log.trace('updateBasicCompetency');
	return {
		type: UPDATE_BASICCOMPETENCY,
		basicCompetency
	};
}

export function deleteBasicCompetency(basicCompetency) {
	log.trace('deleteBasicCompetency');
	return {
		type: DELETE_BASICCOMPETENCY,
		basicCompetency
	};
}

export function updateAddBasicCompetency(basicCompetency) {
	log.trace('updateAddBasicCompetency');
	return {
		type: UPDATE_ADD_BASICCOMPETENCY,
		basicCompetency
	};
}

/**
 * Database
 */
export function getBasicCompetencys({query, field, populate, limit}) {
	log.trace('getBasicCompetencys');

	return () => {
		return fetch('/api/basicCompetency/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getBasicCompetency(id) {
	log.trace('getBasicCompetency');

	return () => {
		return fetch(`/api/basicCompetency/${id}`)
			.then(handleResponse);
	};
}

export function postBasicCompetency(data) {
	log.trace('postBasicCompetency');

	return () => {
		return fetch('/api/basicCompetency', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putBasicCompetency(data) {
	log.trace('putBasicCompetency');

	return () => {
		return fetch(`/api/basicCompetency/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delBasicCompetency(id) {
	log.trace('delBasicCompetency');

	return () => {
		return fetch(`/api/basicCompetency/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getBasicCompetencysByCourse({query, field, populate, limit}) {
	log.trace('getBasicCompetencysByCourse');

	return () => {
		return fetch('/api/basicCompetency/query/course/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
