/**
 * API
 */

import {
	SET_OBJECTIVE,
	SET_OBJECTIVES,
	ADD_OBJECTIVE,
	UPDATE_OBJECTIVE,
	DELETE_OBJECTIVE,
	UPDATE_ADD_OBJECTIVE
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:OBJECTIVE';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setObjective(objective) {
	log.trace('setObjectives');
	return {
		type: SET_OBJECTIVE,
		objective
	};
}

export function setObjectives(objective) {
	log.trace('setObjectives');
	return {
		type: SET_OBJECTIVES,
		objective
	};
}

export function addObjective(objective) {
	log.trace('addObjective');
	return {
		type: ADD_OBJECTIVE,
		objective
	};
}

export function updateObjective(objective) {
	log.trace('updateObjective');
	return {
		type: UPDATE_OBJECTIVE,
		objective
	};
}

export function deleteObjective(objective) {
	log.trace('deleteObjective');
	return {
		type: DELETE_OBJECTIVE,
		objective
	};
}

export function updateAddObjective(objective) {
	log.trace('updateAddObjective');
	return {
		type: UPDATE_ADD_OBJECTIVE,
		objective
	};
}

/**
 * Database
 */
export function getObjectives({query, field, populate, limit}) {
	log.trace('getObjectives');

	return () => {
		return fetch('/api/objective/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getObjective(id) {
	log.trace('getObjective');

	return () => {
		return fetch(`/api/objective/${id}`)
			.then(handleResponse);
	};
}

export function postObjective(data) {
	log.trace('postObjective');

	return () => {
		return fetch('/api/objective', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putObjective(data) {
	log.trace('putObjective');

	return () => {
		return fetch(`/api/objective/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delObjective(id) {
	log.trace('delObjective');

	return () => {
		return fetch(`/api/objective/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delObjectives({query}) {
	log.trace('delObjectives');

	return () => {
		return fetch('/api/objective/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
