/**
 * API
 */

import {
	SET_REQUESTSTUDENT,
	SET_REQUESTSTUDENTS,
	ADD_REQUESTSTUDENT, 
	UPDATE_REQUESTSTUDENT,
	DELETE_REQUESTSTUDENT, 
	UPDATE_ADD_REQUESTSTUDENT
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:REQUESTSTUDENT';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setRequestStudent(requestStudent) {
	log.trace('setRequestStudents');
	return {
		type: SET_REQUESTSTUDENT,
		requestStudent
	};
}

export function setRequestStudents(requestStudent) {
	log.trace('setRequestStudents');
	return {
		type: SET_REQUESTSTUDENTS,
		requestStudent
	};
}

export function addRequestStudent(requestStudent) {
	log.trace('addRequestStudent');
	return {
		type: ADD_REQUESTSTUDENT,
		requestStudent
	};
}

export function updateRequestStudent(requestStudent) {
	log.trace('updateRequestStudent');
	return {
		type: UPDATE_REQUESTSTUDENT,
		requestStudent
	};
}

export function deleteRequestStudent(requestStudent) {
	log.trace('deleteRequestStudent');
	return {
		type: DELETE_REQUESTSTUDENT,
		requestStudent
	};
}

export function updateAddRequestStudent(requestStudent) {
	log.trace('updateAddRequestStudent');
	return {
		type: UPDATE_ADD_REQUESTSTUDENT,
		requestStudent
	};
}

/**
 * Database
 */
export function getRequestStudents({query, field, populate, limit}) {
	log.trace('getRequestStudents');

	return () => {
		return fetch('/api/requestStudent/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getRequestStudent(id) {
	log.trace('getRequestStudent');

	return () => {
		return fetch(`/api/requestStudent/${id}`)
			.then(handleResponse);
	};
}

export function postRequestStudent(data) {
	log.trace('postRequestStudent');

	return () => {
		return fetch('/api/requestStudent', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putRequestStudent(data) {
	log.trace('putRequestStudent');

	return () => {
		return fetch(`/api/requestStudent/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delRequestStudent(id) {
	log.trace('delRequestStudent');

	return () => {
		return fetch(`/api/requestStudent/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delRequestStudents({query}) {
	log.trace('delRequestStudents');

	return () => {
		return fetch('/api/requestStudent/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}