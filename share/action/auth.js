/**
 * Auth
 */

import {SET_CURRENT_USER} from './actionTypes.js';

import jwt from 'jsonwebtoken';
import setAuthorizationToken from '../setAuthorizationToken.js';

import debug from 'debug';

var logName = 'action:AUTH';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

export function setCurrentUser(user) {
	log.trace('setCurrentUser');

	return {
		type: SET_CURRENT_USER,
		user
	};
} 

export function login(data) {
	log.trace('login');

	return (dispatch) => {
		return fetch('/api/auth', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then((response) => {
			if(response.ok) {
				response.json().then((res) => {
					const token = res.token;

					localStorage.setItem('jwtToken', token);
					setAuthorizationToken(token);
					dispatch(setCurrentUser(jwt.decode(token)));
				});
			} else {
				var err = new Error(response.statusText);
				err.response = response;
				throw err;
			}
		});
	};
}

export function logout() {
	log.trace('logout');

	return (dispatch) => {
		localStorage.removeItem('jwtToken');
		setAuthorizationToken(false);
		dispatch(setCurrentUser({}));
	};
}