/**
 * API
 */

import {
	SET_STUDENTMATTER,
	SET_STUDENTMATTERS,
	ADD_STUDENTMATTER,
	UPDATE_STUDENTMATTER,
	DELETE_STUDENTMATTER,
	UPDATE_ADD_STUDENTMATTER
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:STUDENTMATTER';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setStudentMatter(studentMatter) {
	log.trace('setStudentMatters');
	return {
		type: SET_STUDENTMATTER,
		studentMatter
	};
}

export function setStudentMatters(studentMatter) {
	log.trace('setStudentMatters');
	return {
		type: SET_STUDENTMATTERS,
		studentMatter
	};
}

export function addStudentMatter(studentMatter) {
	log.trace('addStudentMatter');
	return {
		type: ADD_STUDENTMATTER,
		studentMatter
	};
}

export function updateStudentMatter(studentMatter) {
	log.trace('updateStudentMatter');
	return {
		type: UPDATE_STUDENTMATTER,
		studentMatter
	};
}

export function deleteStudentMatter(studentMatter) {
	log.trace('deleteStudentMatter');
	return {
		type: DELETE_STUDENTMATTER,
		studentMatter
	};
}

export function updateAddStudentMatter(studentMatter) {
	log.trace('updateAddStudentMatter');
	return {
		type: UPDATE_ADD_STUDENTMATTER,
		studentMatter
	};
}

/**
 * Database
 */
export function getStudentMatters({query, field, populate, limit}) {
	log.trace('getStudentMatters');

	return () => {
		return fetch('/api/studentMatter/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getStudentMatter(id) {
	log.trace('getStudentMatter');

	return () => {
		return fetch(`/api/studentMatter/${id}`)
			.then(handleResponse);
	};
}

export function postStudentMatter(data) {
	log.trace('postStudentMatter');

	return () => {
		return fetch('/api/studentMatter', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putStudentMatter(data) {
	log.trace('putStudentMatter');

	return () => {
		return fetch(`/api/studentMatter/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentMatter(id) {
	log.trace('delStudentMatter');

	return () => {
		return fetch(`/api/studentMatter/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentMatters({query}) {
	log.trace('delStudentMatters');

	return () => {
		return fetch('/api/studentMatter/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
