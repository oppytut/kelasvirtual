/**
 * API
 */

import debug from 'debug';

var logName = 'action:COMPILER';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

export function compile(data) {
	log.trace('compile');

	return () => {
		return fetch('/api/compiler/', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}