/**
 * Component
 */

import {
	LEARNING_SET_ACTIVE_MATTER,
	LEARNING_SET_MATTER_LIST
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:LEARNING';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

export function learningActiveMatter(value) {
	log.trace('learningActiveMatter');

	return {
		type: LEARNING_SET_ACTIVE_MATTER,
		value
	};
}

export function learningMatterList(value) {
	log.trace('learningMatterList');

	return {
		type: LEARNING_SET_MATTER_LIST,
		value
	};
}