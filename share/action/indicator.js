/**
 * API
 */

import {
	SET_INDICATOR,
	SET_INDICATORS,
	ADD_INDICATOR,
	UPDATE_INDICATOR,
	DELETE_INDICATOR,
	UPDATE_ADD_INDICATOR
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:INDICATOR';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setIndicator(indicator) {
	log.trace('setIndicators');
	return {
		type: SET_INDICATOR,
		indicator
	};
}

export function setIndicators(indicator) {
	log.trace('setIndicators');
	return {
		type: SET_INDICATORS,
		indicator
	};
}

export function addIndicator(indicator) {
	log.trace('addIndicator');
	return {
		type: ADD_INDICATOR,
		indicator
	};
}

export function updateIndicator(indicator) {
	log.trace('updateIndicator');
	return {
		type: UPDATE_INDICATOR,
		indicator
	};
}

export function deleteIndicator(indicator) {
	log.trace('deleteIndicator');
	return {
		type: DELETE_INDICATOR,
		indicator
	};
}

export function updateAddIndicator(indicator) {
	log.trace('updateAddIndicator');
	return {
		type: UPDATE_ADD_INDICATOR,
		indicator
	};
}

/**
 * Database
 */
export function getIndicators({query, field, populate, limit}) {
	log.trace('getIndicators');

	return () => {
		return fetch('/api/indicator/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getIndicator(id) {
	log.trace('getIndicator');

	return () => {
		return fetch(`/api/indicator/${id}`)
			.then(handleResponse);
	};
}

export function postIndicator(data) {
	log.trace('postIndicator');

	return () => {
		return fetch('/api/indicator', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putIndicator(data) {
	log.trace('putIndicator');

	return () => {
		return fetch(`/api/indicator/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delIndicator(id) {
	log.trace('delIndicator');

	return () => {
		return fetch(`/api/indicator/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delIndicators({query}) {
	log.trace('delIndicators');

	return () => {
		return fetch('/api/indicator/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delIndicatorsChild({query}) {
	log.trace('delIndicatorsChild');

	return () => {
		return fetch('/api/indicator/query/child/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
