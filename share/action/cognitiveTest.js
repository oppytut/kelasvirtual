/**
 * API
 */

import {
	SET_COGNITIVETEST,
	SET_COGNITIVETESTS,
	ADD_COGNITIVETEST, 
	UPDATE_COGNITIVETEST,
	DELETE_COGNITIVETEST, 
	UPDATE_ADD_COGNITIVETEST
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:COGNITIVETEST';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setCognitiveTest(cognitiveTest) {
	log.trace('setCognitiveTests');
	return {
		type: SET_COGNITIVETEST,
		cognitiveTest
	};
}

export function setCognitiveTests(cognitiveTest) {
	log.trace('setCognitiveTests');
	return {
		type: SET_COGNITIVETESTS,
		cognitiveTest
	};
}

export function addCognitiveTest(cognitiveTest) {
	log.trace('addCognitiveTest');
	return {
		type: ADD_COGNITIVETEST,
		cognitiveTest
	};
}

export function updateCognitiveTest(cognitiveTest) {
	log.trace('updateCognitiveTest');
	return {
		type: UPDATE_COGNITIVETEST,
		cognitiveTest
	};
}

export function deleteCognitiveTest(cognitiveTest) {
	log.trace('deleteCognitiveTest');
	return {
		type: DELETE_COGNITIVETEST,
		cognitiveTest
	};
}

export function updateAddCognitiveTest(cognitiveTest) {
	log.trace('updateAddCognitiveTest');
	return {
		type: UPDATE_ADD_COGNITIVETEST,
		cognitiveTest
	};
}

/**
 * Database
 */
export function getCognitiveTests({query, field, populate, limit}) {
	log.trace('getCognitiveTests');

	return () => {
		return fetch('/api/cognitiveTest/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getCognitiveTest(id) {
	log.trace('getCognitiveTest');

	return () => {
		return fetch(`/api/cognitiveTest/${id}`)
			.then(handleResponse);
	};
}

export function postCognitiveTest(data) {
	log.trace('postCognitiveTest');

	return () => {
		return fetch('/api/cognitiveTest', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putCognitiveTest(data) {
	log.trace('putCognitiveTest');

	return () => {
		return fetch(`/api/cognitiveTest/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delCognitiveTest(id) {
	log.trace('delCognitiveTest');

	return () => {
		return fetch(`/api/cognitiveTest/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}