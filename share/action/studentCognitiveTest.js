/**
 * API
 */

import {
	SET_STUDENTCOGNITIVETEST,
	SET_STUDENTCOGNITIVETESTS,
	ADD_STUDENTCOGNITIVETEST,
	UPDATE_STUDENTCOGNITIVETEST,
	DELETE_STUDENTCOGNITIVETEST,
	UPDATE_ADD_STUDENTCOGNITIVETEST
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:STUDENTCOGNITIVETEST';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setStudentCognitiveTest(studentCognitiveTest) {
	log.trace('setStudentCognitiveTests');
	return {
		type: SET_STUDENTCOGNITIVETEST,
		studentCognitiveTest
	};
}

export function setStudentCognitiveTests(studentCognitiveTest) {
	log.trace('setStudentCognitiveTests');
	return {
		type: SET_STUDENTCOGNITIVETESTS,
		studentCognitiveTest
	};
}

export function addStudentCognitiveTest(studentCognitiveTest) {
	log.trace('addStudentCognitiveTest');
	return {
		type: ADD_STUDENTCOGNITIVETEST,
		studentCognitiveTest
	};
}

export function updateStudentCognitiveTest(studentCognitiveTest) {
	log.trace('updateStudentCognitiveTest');
	return {
		type: UPDATE_STUDENTCOGNITIVETEST,
		studentCognitiveTest
	};
}

export function deleteStudentCognitiveTest(studentCognitiveTest) {
	log.trace('deleteStudentCognitiveTest');
	return {
		type: DELETE_STUDENTCOGNITIVETEST,
		studentCognitiveTest
	};
}

export function updateAddStudentCognitiveTest(studentCognitiveTest) {
	log.trace('updateAddStudentCognitiveTest');
	return {
		type: UPDATE_ADD_STUDENTCOGNITIVETEST,
		studentCognitiveTest
	};
}

/**
 * Database
 */
export function getStudentCognitiveTests({query, field, populate, limit}) {
	log.trace('getStudentCognitiveTests');

	return () => {
		return fetch('/api/studentCognitiveTest/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getStudentCognitiveTest(id) {
	log.trace('getStudentCognitiveTest');

	return () => {
		return fetch(`/api/studentCognitiveTest/${id}`)
			.then(handleResponse);
	};
}

export function postStudentCognitiveTest(data) {
	log.trace('postStudentCognitiveTest');

	return () => {
		return fetch('/api/studentCognitiveTest', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putStudentCognitiveTest(data) {
	log.trace('putStudentCognitiveTest');

	return () => {
		return fetch(`/api/studentCognitiveTest/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentCognitiveTest(id) {
	log.trace('delStudentCognitiveTest');

	return () => {
		return fetch(`/api/studentCognitiveTest/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentCognitiveTests({query}) {
	log.trace('delStudentCognitiveTests');

	return () => {
		return fetch('/api/studentCognitiveTest/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
