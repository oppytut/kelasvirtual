/**
 * API
 */

import {
	SET_MATTERGROUP,
	SET_MATTERGROUPS,
	ADD_MATTERGROUP, 
	UPDATE_MATTERGROUP,
	DELETE_MATTERGROUP, 
	UPDATE_ADD_MATTERGROUP
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:MATTERGROUP';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setMatterGroup(matterGroup) {
	log.trace('setMatterGroups');
	return {
		type: SET_MATTERGROUP,
		matterGroup
	};
}

export function setMatterGroups(matterGroup) {
	log.trace('setMatterGroups');
	return {
		type: SET_MATTERGROUPS,
		matterGroup
	};
}

export function addMatterGroup(matterGroup) {
	log.trace('addMatterGroup');
	return {
		type: ADD_MATTERGROUP,
		matterGroup
	};
}

export function updateMatterGroup(matterGroup) {
	log.trace('updateMatterGroup');
	return {
		type: UPDATE_MATTERGROUP,
		matterGroup
	};
}

export function deleteMatterGroup(matterGroup) {
	log.trace('deleteMatterGroup');
	return {
		type: DELETE_MATTERGROUP,
		matterGroup
	};
}

export function updateAddMatterGroup(matterGroup) {
	log.trace('updateAddMatterGroup');
	return {
		type: UPDATE_ADD_MATTERGROUP,
		matterGroup
	};
}

/**
 * Database
 */
export function getMatterGroups({query, field, populate, limit}) {
	log.trace('getMatterGroupsByQuery');

	return () => {
		return fetch('/api/matterGroup/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getMatterGroup(id) {
	log.trace('getMatterGroup');

	return () => {
		return fetch(`/api/matterGroup/${id}`)
			.then(handleResponse);
	};
}

export function postMatterGroup(data) {
	log.trace('postMatterGroup');

	return () => {
		return fetch('/api/matterGroup', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putMatterGroup(data) {
	log.trace('putMatterGroup');

	return () => {
		return fetch(`/api/matterGroup/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delMatterGroup(id) {
	log.trace('delMatterGroup');

	return () => {
		return fetch(`/api/matterGroup/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getMatterGroupsByCourse({query, field, populate, limit}) {
	log.trace('getMatterGroupsByCourse');

	return () => {
		return fetch('/api/matterGroup/query/course/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getMatterGroupsByMatter({query, field, populate, limit}) {
	log.trace('getMatterGroupsByMatter');

	return () => {
		return fetch('/api/matterGroup/query/matter/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}