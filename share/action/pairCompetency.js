/**
 * API
 */

import {
	SET_PAIRCOMPETENCY,
	SET_PAIRCOMPETENCYS,
	ADD_PAIRCOMPETENCY,
	UPDATE_PAIRCOMPETENCY,
	DELETE_PAIRCOMPETENCY,
	UPDATE_ADD_PAIRCOMPETENCY
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:PAIRCOMPETENCY';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setPairCompetency(pairCompetency) {
	log.trace('setPairCompetencys');
	return {
		type: SET_PAIRCOMPETENCY,
		pairCompetency
	};
}

export function setPairCompetencys(pairCompetency) {
	log.trace('setPairCompetencys');
	return {
		type: SET_PAIRCOMPETENCYS,
		pairCompetency
	};
}

export function addPairCompetency(pairCompetency) {
	log.trace('addPairCompetency');
	return {
		type: ADD_PAIRCOMPETENCY,
		pairCompetency
	};
}

export function updatePairCompetency(pairCompetency) {
	log.trace('updatePairCompetency');
	return {
		type: UPDATE_PAIRCOMPETENCY,
		pairCompetency
	};
}

export function deletePairCompetency(pairCompetency) {
	log.trace('deletePairCompetency');
	return {
		type: DELETE_PAIRCOMPETENCY,
		pairCompetency
	};
}

export function updateAddPairCompetency(pairCompetency) {
	log.trace('updateAddPairCompetency');
	return {
		type: UPDATE_ADD_PAIRCOMPETENCY,
		pairCompetency
	};
}

/**
 * Database
 */
export function getPairCompetencys({query, field, populate, limit}) {
	log.trace('getPairCompetencys');

	return () => {
		return fetch('/api/pairCompetency/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getPairCompetency(id) {
	log.trace('getPairCompetency');

	return () => {
		return fetch(`/api/pairCompetency/${id}`)
			.then(handleResponse);
	};
}

export function postPairCompetency(data) {
	log.trace('postPairCompetency');

	return () => {
		return fetch('/api/pairCompetency', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putPairCompetency(data) {
	log.trace('putPairCompetency');

	return () => {
		return fetch(`/api/pairCompetency/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delPairCompetency(id) {
	log.trace('delPairCompetency');

	return () => {
		return fetch(`/api/pairCompetency/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delPairCompetencys({query}) {
	log.trace('delPairCompetencys');

	return () => {
		return fetch('/api/pairCompetency/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delPairCompetencysChild({query}) {
	log.trace('delPairCompetencysChild');

	return () => {
		return fetch('/api/pairCompetency/query/child/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getPairCompetencysByCourse({query, field, populate, limit}) {
	log.trace('getPairCompetencys');

	return () => {
		return fetch('/api/pairCompetency/query/course/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function postPairedBasicCompetency(data) {
	log.trace('postPairedBasicCompetency');

	return () => {
		return fetch('/api/pairCompetency/pairedBasicCompetency', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
