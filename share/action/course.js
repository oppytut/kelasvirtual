/**
 * API
 */

import {
	SET_COURSE,
	SET_COURSES,
	ADD_COURSE,
	UPDATE_COURSE,
	DELETE_COURSE,
	UPDATE_ADD_COURSE
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:COURSE';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setCourse(course) {
	log.trace('setCourses');
	return {
		type: SET_COURSE,
		course
	};
}

export function setCourses(course) {
	log.trace('setCourses');
	return {
		type: SET_COURSES,
		course
	};
}

export function addCourse(course) {
	log.trace('addCourse');
	return {
		type: ADD_COURSE,
		course
	};
}

export function updateCourse(course) {
	log.trace('updateCourse');
	return {
		type: UPDATE_COURSE,
		course
	};
}

export function deleteCourse(course) {
	log.trace('deleteCourse');
	return {
		type: DELETE_COURSE,
		course
	};
}

export function updateAddCourse(course) {
	log.trace('updateAddCourse');
	return {
		type: UPDATE_ADD_COURSE,
		course
	};
}

/**
 * Database
 */
export function getCourses({query, field, populate, limit}) {
	log.trace('getCourses');

	return () => {
		return fetch('/api/course/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getCourse(id) {
	log.trace('getCourse');

	return () => {
		return fetch(`/api/course/${id}`)
			.then(handleResponse);
	};
}

export function postCourse(data) {
	log.trace('postCourse');

	return () => {
		return fetch('/api/course', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putCourse(data) {
	log.trace('putCourse');

	return () => {
		return fetch(`/api/course/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delCourse(id) {
	log.trace('delCourse');

	return () => {
		return fetch(`/api/course/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function checkPassword(data) {
	log.trace('checkPassword');

	return () => {
		return fetch(`/api/course/check-password/${data._id}`, {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
