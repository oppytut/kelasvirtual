/**
 * API
 */

import {
	SET_STUDENTTEST,
	SET_STUDENTTESTS,
	ADD_STUDENTTEST,
	UPDATE_STUDENTTEST,
	DELETE_STUDENTTEST,
	UPDATE_ADD_STUDENTTEST
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:STUDENTTEST';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setStudentTest(studentTest) {
	log.trace('setStudentTests');
	return {
		type: SET_STUDENTTEST,
		studentTest
	};
}

export function setStudentTests(studentTest) {
	log.trace('setStudentTests');
	return {
		type: SET_STUDENTTESTS,
		studentTest
	};
}

export function addStudentTest(studentTest) {
	log.trace('addStudentTest');
	return {
		type: ADD_STUDENTTEST,
		studentTest
	};
}

export function updateStudentTest(studentTest) {
	log.trace('updateStudentTest');
	return {
		type: UPDATE_STUDENTTEST,
		studentTest
	};
}

export function deleteStudentTest(studentTest) {
	log.trace('deleteStudentTest');
	return {
		type: DELETE_STUDENTTEST,
		studentTest
	};
}

export function updateAddStudentTest(studentTest) {
	log.trace('updateAddStudentTest');
	return {
		type: UPDATE_ADD_STUDENTTEST,
		studentTest
	};
}

/**
 * Database
 */
export function getStudentTests({query, field, populate, limit}) {
	log.trace('getStudentTests');

	return () => {
		return fetch('/api/studentTest/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getStudentTest(id) {
	log.trace('getStudentTest');

	return () => {
		return fetch(`/api/studentTest/${id}`)
			.then(handleResponse);
	};
}

export function postStudentTest(data) {
	log.trace('postStudentTest');

	return () => {
		return fetch('/api/studentTest', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putStudentTest(data) {
	log.trace('putStudentTest');

	return () => {
		return fetch(`/api/studentTest/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentTest(id) {
	log.trace('delStudentTest');

	return () => {
		return fetch(`/api/studentTest/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentTests({query}) {
	log.trace('delStudentTests');

	return () => {
		return fetch('/api/studentTest/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
