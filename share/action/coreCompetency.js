/**
 * API
 */

import {
	SET_CORECOMPETENCY,
	SET_CORECOMPETENCYS,
	ADD_CORECOMPETENCY,
	UPDATE_CORECOMPETENCY,
	DELETE_CORECOMPETENCY,
	UPDATE_ADD_CORECOMPETENCY
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:CORECOMPETENCY';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setCoreCompetency(coreCompetency) {
	log.trace('setCoreCompetencys');
	return {
		type: SET_CORECOMPETENCY,
		coreCompetency
	};
}

export function setCoreCompetencys(coreCompetency) {
	log.trace('setCoreCompetencys');
	return {
		type: SET_CORECOMPETENCYS,
		coreCompetency
	};
}

export function addCoreCompetency(coreCompetency) {
	log.trace('addCoreCompetency');
	return {
		type: ADD_CORECOMPETENCY,
		coreCompetency
	};
}

export function updateCoreCompetency(coreCompetency) {
	log.trace('updateCoreCompetency');
	return {
		type: UPDATE_CORECOMPETENCY,
		coreCompetency
	};
}

export function deleteCoreCompetency(coreCompetency) {
	log.trace('deleteCoreCompetency');
	return {
		type: DELETE_CORECOMPETENCY,
		coreCompetency
	};
}

export function updateAddCoreCompetency(coreCompetency) {
	log.trace('updateAddCoreCompetency');
	return {
		type: UPDATE_ADD_CORECOMPETENCY,
		coreCompetency
	};
}

/**
 * Database
 */
export function getCoreCompetencys({query, field, populate, limit}) {
	log.trace('getCoreCompetencys');

	return () => {
		return fetch('/api/coreCompetency/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getCoreCompetency(id) {
	log.trace('getCoreCompetency');

	return () => {
		return fetch(`/api/coreCompetency/${id}`)
			.then(handleResponse);
	};
}

export function postCoreCompetency(data) {
	log.trace('postCoreCompetency');

	return () => {
		return fetch('/api/coreCompetency', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putCoreCompetency(data) {
	log.trace('putCoreCompetency');

	return () => {
		return fetch(`/api/coreCompetency/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delCoreCompetency(id) {
	log.trace('delCoreCompetency');

	return () => {
		return fetch(`/api/coreCompetency/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putPostCoreCompetency({query, data}) {
	log.trace('putPostCoreCompetency');

	return () => {
		return fetch('/api/coreCompetency/query/putPost/', {
			method: 'post',
			body: JSON.stringify({query, data}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
