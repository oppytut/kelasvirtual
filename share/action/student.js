/**
 * API
 */

import {
	SET_STUDENT,
	SET_STUDENTS,
	ADD_STUDENT,
	UPDATE_STUDENT,
	DELETE_STUDENT,
	UPDATE_ADD_STUDENT
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:STUDENT';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setStudent(student) {
	log.trace('setStudents');
	return {
		type: SET_STUDENT,
		student
	};
}

export function setStudents(student) {
	log.trace('setStudents');
	return {
		type: SET_STUDENTS,
		student
	};
}

export function addStudent(student) {
	log.trace('addStudent');
	return {
		type: ADD_STUDENT,
		student
	};
}

export function updateStudent(student) {
	log.trace('updateStudent');
	return {
		type: UPDATE_STUDENT,
		student
	};
}

export function deleteStudent(student) {
	log.trace('deleteStudent');
	return {
		type: DELETE_STUDENT,
		student
	};
}

export function updateAddStudent(student) {
	log.trace('updateAddStudent');
	return {
		type: UPDATE_ADD_STUDENT,
		student
	};
}

/**
 * Database
 */
export function getStudents({query, field, populate, limit}) {
	log.trace('getStudents');

	return () => {
		return fetch('/api/student/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getStudent(id) {
	log.trace('getStudent');

	return () => {
		return fetch(`/api/student/${id}`)
			.then(handleResponse);
	};
}

export function postStudent(data) {
	log.trace('postStudent');

	return () => {
		return fetch('/api/student', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putStudent(data) {
	log.trace('putStudent');

	return () => {
		return fetch(`/api/student/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudent(id) {
	log.trace('delStudent');

	return () => {
		return fetch(`/api/student/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getStudentsOnly({query, field, populate, limit}) {
	log.trace('getStudentsOnly');

	return () => {
		return fetch('/api/student/query/only/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putPostStudent({query, data}) {
	log.trace('putPostStudent');

	return () => {
		return fetch('/api/student/query/putPost/', {
			method: 'post',
			body: JSON.stringify({query, data}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
