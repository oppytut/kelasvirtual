/**
 * API
 */

import {
	SET_IDENTITY,
	SET_IDENTITYS,
	ADD_IDENTITY, 
	UPDATE_IDENTITY,
	DELETE_IDENTITY, 
	UPDATE_ADD_IDENTITY
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:IDENTITY';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setIdentity(identity) {
	log.trace('setIdentitys');
	return {
		type: SET_IDENTITY,
		identity
	};
}

export function setIdentitys(identity) {
	log.trace('setIdentitys');
	return {
		type: SET_IDENTITYS,
		identity
	};
}

export function addIdentity(identity) {
	log.trace('addIdentity');
	return {
		type: ADD_IDENTITY,
		identity
	};
}

export function updateIdentity(identity) {
	log.trace('updateIdentity');
	return {
		type: UPDATE_IDENTITY,
		identity
	};
}

export function deleteIdentity(identity) {
	log.trace('deleteIdentity');
	return {
		type: DELETE_IDENTITY,
		identity
	};
}

export function updateAddIdentity(identity) {
	log.trace('updateAddIdentity');
	return {
		type: UPDATE_ADD_IDENTITY,
		identity
	};
}

/**
 * Database
 */
export function getIdentitys({query, field, populate, limit}) {
	log.trace('getIdentitys');

	return () => {
		return fetch('/api/identity/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getIdentity(id) {
	log.trace('getIdentity');

	return () => {
		return fetch(`/api/identity/${id}`)
			.then(handleResponse);
	};
}

export function postIdentity(data) {
	log.trace('postIdentity');

	return () => {
		return fetch('/api/identity', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putIdentity(data) {
	log.trace('putIdentity');

	return () => {
		return fetch(`/api/identity/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delIdentity(id) {
	log.trace('delIdentity');

	return () => {
		return fetch(`/api/identity/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}