/**
 * Component
 */

import {
	ADMINISTRATION_STUDENT_LIST,
	ADMINISTRATION_REQUEST_LIST
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:ADMINISTRATION';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

export function administrationStudentList(item) {
	log.trace('administrationStudentList');

	return {
		type: ADMINISTRATION_STUDENT_LIST,
		item
	};
}

export function administrationRequestList(item) {
	log.trace('administrationRequestList');

	return {
		type: ADMINISTRATION_REQUEST_LIST,
		item
	};
}
