/**
 * API
 */

import {
	SET_STUDENTPSYCHOMOTORTEST,
	SET_STUDENTPSYCHOMOTORTESTS,
	ADD_STUDENTPSYCHOMOTORTEST,
	UPDATE_STUDENTPSYCHOMOTORTEST,
	DELETE_STUDENTPSYCHOMOTORTEST,
	UPDATE_ADD_STUDENTPSYCHOMOTORTEST
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:STUDENTPSYCHOMOTORTEST';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function handleResponse(response) {
	log.trace('handleResponse');

	if(response.ok) {
		return response.json();
	} else {
		var err = new Error(response.statusText);
		err.response = response;
		throw err;
	}
}

/**
 * Store
 */
export function setStudentPsychomotorTest(studentPsychomotorTest) {
	log.trace('setStudentPsychomotorTests');
	return {
		type: SET_STUDENTPSYCHOMOTORTEST,
		studentPsychomotorTest
	};
}

export function setStudentPsychomotorTests(studentPsychomotorTest) {
	log.trace('setStudentPsychomotorTests');
	return {
		type: SET_STUDENTPSYCHOMOTORTESTS,
		studentPsychomotorTest
	};
}

export function addStudentPsychomotorTest(studentPsychomotorTest) {
	log.trace('addStudentPsychomotorTest');
	return {
		type: ADD_STUDENTPSYCHOMOTORTEST,
		studentPsychomotorTest
	};
}

export function updateStudentPsychomotorTest(studentPsychomotorTest) {
	log.trace('updateStudentPsychomotorTest');
	return {
		type: UPDATE_STUDENTPSYCHOMOTORTEST,
		studentPsychomotorTest
	};
}

export function deleteStudentPsychomotorTest(studentPsychomotorTest) {
	log.trace('deleteStudentPsychomotorTest');
	return {
		type: DELETE_STUDENTPSYCHOMOTORTEST,
		studentPsychomotorTest
	};
}

export function updateAddStudentPsychomotorTest(studentPsychomotorTest) {
	log.trace('updateAddStudentPsychomotorTest');
	return {
		type: UPDATE_ADD_STUDENTPSYCHOMOTORTEST,
		studentPsychomotorTest
	};
}

/**
 * Database
 */
export function getStudentPsychomotorTests({query, field, populate, limit}) {
	log.trace('getStudentPsychomotorTests');

	return () => {
		return fetch('/api/studentPsychomotorTest/query/', {
			method: 'post',
			body: JSON.stringify({query, field, populate, limit}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function getStudentPsychomotorTest(id) {
	log.trace('getStudentPsychomotorTest');

	return () => {
		return fetch(`/api/studentPsychomotorTest/${id}`)
			.then(handleResponse);
	};
}

export function postStudentPsychomotorTest(data) {
	log.trace('postStudentPsychomotorTest');

	return () => {
		return fetch('/api/studentPsychomotorTest', {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function putStudentPsychomotorTest(data) {
	log.trace('putStudentPsychomotorTest');

	return () => {
		return fetch(`/api/studentPsychomotorTest/${data._id}`, {
			method: 'put',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentPsychomotorTest(id) {
	log.trace('delStudentPsychomotorTest');

	return () => {
		return fetch(`/api/studentPsychomotorTest/${id}`, {
			method: 'delete',
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}

export function delStudentPsychomotorTests({query}) {
	log.trace('delStudentPsychomotorTests');

	return () => {
		return fetch('/api/studentPsychomotorTest/query/del/', {
			method: 'post',
			body: JSON.stringify({query}),
			headers: {
				'Content-Type': 'application/json'
			}
		}).then(handleResponse);
	};
}
