/**
 * Component
 */

import {
	COURSE_HOME_SET_ACTIVE_MENU,
	COURSE_HOME_SET_ACTIVE_BREADCRUMB
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:COURSEHOME';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

export function courseHomeActiveMenu(value) {
	log.trace('courseHomeActiveMenu');

	return {
		type: COURSE_HOME_SET_ACTIVE_MENU,
		value
	};
}

export function courseHomeActiveBreadcrumb(value) {
	log.trace('courseHomeActiveBreadcrumb');

	return {
		type: COURSE_HOME_SET_ACTIVE_BREADCRUMB,
		value
	};
}