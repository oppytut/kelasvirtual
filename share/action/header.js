/**
 * Component
 */

import {
	HEADER_SET_REGISTER_MODAL_OPEN, 
	HEADER_SET_LOGIN_MODAL_OPEN, 
	HEADER_SET_ACTIVE_MENU
} from './actionTypes.js';

import debug from 'debug';

var logName = 'action:HEADER';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

export function headerRegisterModalOpen(value) {
	log.trace('headerRegisterModalOpen');

	return {
		type: HEADER_SET_REGISTER_MODAL_OPEN,
		value
	};
}

export function headerLoginModalOpen(value) {
	log.trace('headerLoginModalOpen');

	return {
		type: HEADER_SET_LOGIN_MODAL_OPEN,
		value
	};
}

export function headerActiveMenu(value) {
	log.trace('headerActiveMenu');

	return {
		type: HEADER_SET_ACTIVE_MENU,
		value
	};
}