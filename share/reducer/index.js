import {combineReducers} from 'redux';

import administration from './administration.js';
import auth from './auth.js';
import basicCompetency from './basicCompetency.js';
import cognitiveTest from './cognitiveTest.js';
import coreCompetency from './coreCompetency.js';
import course from './course.js';
import courseHome from './courseHome.js';
import header from './header.js';
import identity from './identity.js';
import indicator from './indicator.js';
import learning from './learning.js';
import matter from './matter.js';
import matterGroup from './matterGroup.js';
import objective from './objective.js';
import pairCompetency from './pairCompetency.js';
import requestStudent from './requestStudent.js';
import student from './student.js';
import studentCognitiveTest from './studentCognitiveTest.js';
import studentMatter from './studentMatter.js';
import studentMeeting from './studentMeeting.js';
import studentPsychomotorTest from './studentPsychomotorTest.js';
import studentTest from './studentTest.js';
import user from './user.js';

export default combineReducers({
	administration,
	auth,
	basicCompetency,
	cognitiveTest,
	coreCompetency,
	course,
	courseHome,
	header,
	identity,
	indicator,
	learning,
	matter,
	matterGroup,
	objective,
	pairCompetency,
	requestStudent,
	student,
	studentCognitiveTest,
	studentMatter,
	studentMeeting,
	studentPsychomotorTest,
	studentTest,
	user
});
