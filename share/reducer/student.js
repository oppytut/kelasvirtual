/**
 * API
 */

import {
	SET_STUDENT,
	SET_STUDENTS,
	ADD_STUDENT, 
	UPDATE_STUDENT,
	DELETE_STUDENT, 
	UPDATE_ADD_STUDENT
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function student(state=[], action={}) {
	switch(action.type) {

	case SET_STUDENT:
		return action.student;
	
	case SET_STUDENTS:
		if(!isEmpty(action.student)) {
			return action.student;	
		} else {
			return [];
		}

	case ADD_STUDENT:
		return [
			...state,
			action.student
		];

	case UPDATE_STUDENT:
		return state.map(item => {
			if (item._id === action.student._id) return action.student;
			return item;
		});

	case DELETE_STUDENT:
		return state.filter(item => item._id !== action.student._id);

	case UPDATE_ADD_STUDENT:
		if(state.findIndex(item => item._id === action.student._id) > -1) {
			return state.map(item => {
				if (item._id === action.student._id) return action.student;
				return item;
			});
		} else {
			return [
				...state,
				action.student
			];
		}

	default: 
		return state;
		
	}
}
