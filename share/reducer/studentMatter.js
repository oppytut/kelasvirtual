/**
 * API
 */

import {
	SET_STUDENTMATTER,
	SET_STUDENTMATTERS,
	ADD_STUDENTMATTER, 
	UPDATE_STUDENTMATTER,
	DELETE_STUDENTMATTER, 
	UPDATE_ADD_STUDENTMATTER
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function studentMatter(state=[], action={}) {
	switch(action.type) {

	case SET_STUDENTMATTER:
		return action.studentMatter;
	
	case SET_STUDENTMATTERS:
		if(!isEmpty(action.studentMatter)) {
			return action.studentMatter;	
		} else {
			return [];
		}

	case ADD_STUDENTMATTER:
		return [
			...state,
			action.studentMatter
		];

	case UPDATE_STUDENTMATTER:
		return state.map(item => {
			if (item._id === action.studentMatter._id) return action.studentMatter;
			return item;
		});

	case DELETE_STUDENTMATTER:
		return state.filter(item => item._id !== action.studentMatter._id);

	case UPDATE_ADD_STUDENTMATTER:
		if(state.findIndex(item => item._id === action.studentMatter._id) > -1) {
			return state.map(item => {
				if (item._id === action.studentMatter._id) return action.studentMatter;
				return item;
			});
		} else {
			return [
				...state,
				action.studentMatter
			];
		}

	default: 
		return state;
		
	}
}
