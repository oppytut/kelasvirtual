/**
 * API
 */

import {
	SET_IDENTITY,
	SET_IDENTITYS,
	ADD_IDENTITY, 
	UPDATE_IDENTITY,
	DELETE_IDENTITY, 
	UPDATE_ADD_IDENTITY
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function identity(state=[], action={}) {
	switch(action.type) {

	case SET_IDENTITY:
		return action.identity;
	
	case SET_IDENTITYS:
		if(!isEmpty(action.identity)) {
			return action.identity;	
		} else {
			return [];
		}

	case ADD_IDENTITY:
		return [
			...state,
			action.identity
		];

	case UPDATE_IDENTITY:
		return state.map(item => {
			if (item._id === action.identity._id) return action.identity;
			return item;
		});

	case DELETE_IDENTITY:
		return state.filter(item => item._id !== action.identity._id);

	case UPDATE_ADD_IDENTITY:
		if(state.findIndex(item => item._id === action.identity._id) > -1) {
			return state.map(item => {
				if (item._id === action.identity._id) return action.identity;
				return item;
			});
		} else {
			return [
				...state,
				action.identity
			];
		}

	default: 
		return state;
		
	}
}
