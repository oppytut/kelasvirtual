/**
 * Component
 */

import {
	LEARNING_SET_ACTIVE_MATTER,
	LEARNING_SET_MATTER_LIST
} from '../action/actionTypes.js';

export default (state=[], action={}) => {
	switch(action.type) {

	case LEARNING_SET_ACTIVE_MATTER:
		return Object.assign({}, state, {
			activeMatter: action.value
		});

	case LEARNING_SET_MATTER_LIST:
		return Object.assign({}, state, {
			matterList: action.value
		});

	default: 
		return state;

	}
};