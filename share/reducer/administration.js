/**
 * Component
 */

import {
	ADMINISTRATION_STUDENT_LIST,
	ADMINISTRATION_REQUEST_LIST
} from '../action/actionTypes.js';

export default (state=[], action={}) => {
	switch(action.type) {

	case ADMINISTRATION_STUDENT_LIST:
		return Object.assign({}, state, {
			studentList: action.item
		});

	case ADMINISTRATION_REQUEST_LIST:
		return Object.assign({}, state, {
			requestList: action.item
		});

	default:
		return state;

	}
};
