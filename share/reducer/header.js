/**
 * Component
 */

import {
	HEADER_SET_REGISTER_MODAL_OPEN, 
	HEADER_SET_LOGIN_MODAL_OPEN, 
	HEADER_SET_ACTIVE_MENU
} from '../action/actionTypes.js';

export default (state=[], action={}) => {
	switch(action.type) {
		
	case HEADER_SET_REGISTER_MODAL_OPEN:
		return Object.assign({}, state, {
			registerModalOpen: action.value
		});

	case HEADER_SET_LOGIN_MODAL_OPEN:
		return Object.assign({}, state, {
			loginModalOpen: action.value
		});

	case HEADER_SET_ACTIVE_MENU:
		return Object.assign({}, state, {
			activeMenu: action.value
		});

	default: 
		return state;

	}
};