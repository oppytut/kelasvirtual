/**
 * API
 */

import {
	SET_BASICCOMPETENCY,
	SET_BASICCOMPETENCYS,
	ADD_BASICCOMPETENCY, 
	UPDATE_BASICCOMPETENCY,
	DELETE_BASICCOMPETENCY, 
	UPDATE_ADD_BASICCOMPETENCY
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function basicCompetency(state=[], action={}) {
	switch(action.type) {

	case SET_BASICCOMPETENCY:
		return action.basicCompetency;
	
	case SET_BASICCOMPETENCYS:
		if(!isEmpty(action.basicCompetency)) {
			return action.basicCompetency;	
		} else {
			return [];
		}

	case ADD_BASICCOMPETENCY:
		return [
			...state,
			action.basicCompetency
		];

	case UPDATE_BASICCOMPETENCY:
		return state.map(item => {
			if (item._id === action.basicCompetency._id) return action.basicCompetency;
			return item;
		});

	case DELETE_BASICCOMPETENCY:
		return state.filter(item => item._id !== action.basicCompetency._id);

	case UPDATE_ADD_BASICCOMPETENCY:
		if(state.findIndex(item => item._id === action.basicCompetency._id) > -1) {
			return state.map(item => {
				if (item._id === action.basicCompetency._id) return action.basicCompetency;
				return item;
			});
		} else {
			return [
				...state,
				action.basicCompetency
			];
		}

	default: 
		return state;
		
	}
}
