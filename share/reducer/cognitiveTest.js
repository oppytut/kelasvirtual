/**
 * API
 */

import {
	SET_COGNITIVETEST,
	SET_COGNITIVETESTS,
	ADD_COGNITIVETEST, 
	UPDATE_COGNITIVETEST,
	DELETE_COGNITIVETEST, 
	UPDATE_ADD_COGNITIVETEST
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function cognitiveTest(state=[], action={}) {
	switch(action.type) {

	case SET_COGNITIVETEST:
		return action.cognitiveTest;
	
	case SET_COGNITIVETESTS:
		if(!isEmpty(action.cognitiveTest)) {
			return action.cognitiveTest;	
		} else {
			return [];
		}

	case ADD_COGNITIVETEST:
		return [
			...state,
			action.cognitiveTest
		];

	case UPDATE_COGNITIVETEST:
		return state.map(item => {
			if (item._id === action.cognitiveTest._id) return action.cognitiveTest;
			return item;
		});

	case DELETE_COGNITIVETEST:
		return state.filter(item => item._id !== action.cognitiveTest._id);

	case UPDATE_ADD_COGNITIVETEST:
		if(state.findIndex(item => item._id === action.cognitiveTest._id) > -1) {
			return state.map(item => {
				if (item._id === action.cognitiveTest._id) return action.cognitiveTest;
				return item;
			});
		} else {
			return [
				...state,
				action.cognitiveTest
			];
		}

	default: 
		return state;
		
	}
}
