/**
 * API
 */

import {
	SET_CORECOMPETENCY,
	SET_CORECOMPETENCYS,
	ADD_CORECOMPETENCY, 
	UPDATE_CORECOMPETENCY,
	DELETE_CORECOMPETENCY, 
	UPDATE_ADD_CORECOMPETENCY
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function coreCompetency(state=[], action={}) {
	switch(action.type) {

	case SET_CORECOMPETENCY:
		return action.coreCompetency;
	
	case SET_CORECOMPETENCYS:
		if(!isEmpty(action.coreCompetency)) {
			return action.coreCompetency;	
		} else {
			return [];
		}

	case ADD_CORECOMPETENCY:
		return [
			...state,
			action.coreCompetency
		];

	case UPDATE_CORECOMPETENCY:
		return state.map(item => {
			if (item._id === action.coreCompetency._id) return action.coreCompetency;
			return item;
		});

	case DELETE_CORECOMPETENCY:
		return state.filter(item => item._id !== action.coreCompetency._id);

	case UPDATE_ADD_CORECOMPETENCY:
		if(state.findIndex(item => item._id === action.coreCompetency._id) > -1) {
			return state.map(item => {
				if (item._id === action.coreCompetency._id) return action.coreCompetency;
				return item;
			});
		} else {
			return [
				...state,
				action.coreCompetency
			];
		}

	default: 
		return state;
		
	}
}
