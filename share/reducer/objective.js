/**
 * API
 */

import {
	SET_OBJECTIVE,
	SET_OBJECTIVES,
	ADD_OBJECTIVE,
	UPDATE_OBJECTIVE,
	DELETE_OBJECTIVE,
	UPDATE_ADD_OBJECTIVE
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function objective(state=[], action={}) {
	switch(action.type) {

	case SET_OBJECTIVE:
		return action.objective;

	case SET_OBJECTIVES:
		if(!isEmpty(action.objective)) {
			return action.objective;
		} else {
			return [];
		}

	case ADD_OBJECTIVE:
		return [
			...state,
			action.objective
		];

	case UPDATE_OBJECTIVE:
		return state.map(item => {
			if (item._id === action.objective._id) return action.objective;
			return item;
		});

	case DELETE_OBJECTIVE:
		return state.filter(item => item._id !== action.objective._id);

	case UPDATE_ADD_OBJECTIVE:
		if(state.findIndex(item => item._id === action.objective._id) > -1) {
			return state.map(item => {
				if (item._id === action.objective._id) return action.objective;
				return item;
			});
		} else {
			return [
				...state,
				action.objective
			];
		}

	default:
		return state;

	}
}
