/**
 * Component
 */

import {
	COURSE_HOME_SET_ACTIVE_MENU, 
	COURSE_HOME_SET_ACTIVE_BREADCRUMB
} from '../action/actionTypes.js';

export default (state=[], action={}) => {
	switch(action.type) {

	case COURSE_HOME_SET_ACTIVE_MENU:
		return Object.assign({}, state, {
			activeMenu: action.value
		});

	case COURSE_HOME_SET_ACTIVE_BREADCRUMB:
		return Object.assign({}, state, {
			activeBreadcrumb: action.value
		});

	default: 
		return state;

	}
};