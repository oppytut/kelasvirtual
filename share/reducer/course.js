/**
 * API
 */

import {
	SET_COURSE,
	SET_COURSES,
	ADD_COURSE, 
	UPDATE_COURSE,
	DELETE_COURSE, 
	UPDATE_ADD_COURSE
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function course(state=[], action={}) {
	switch(action.type) {

	case SET_COURSE:
		return action.course;
	
	case SET_COURSES:
		if(!isEmpty(action.course)) {
			return action.course;	
		} else {
			return [];
		}

	case ADD_COURSE:
		return [
			...state,
			action.course
		];

	case UPDATE_COURSE:
		return state.map(item => {
			if (item._id === action.course._id) return action.course;
			return item;
		});

	case DELETE_COURSE:
		return state.filter(item => item._id !== action.course._id);

	case UPDATE_ADD_COURSE:
		if(state.findIndex(item => item._id === action.course._id) > -1) {
			return state.map(item => {
				if (item._id === action.course._id) return action.course;
				return item;
			});
		} else {
			return [
				...state,
				action.course
			];
		}

	default: 
		return state;
		
	}
}
