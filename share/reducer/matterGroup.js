/**
 * API
 */

import {
	SET_MATTERGROUP,
	SET_MATTERGROUPS,
	ADD_MATTERGROUP, 
	UPDATE_MATTERGROUP,
	DELETE_MATTERGROUP, 
	UPDATE_ADD_MATTERGROUP
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function matterGroup(state=[], action={}) {
	switch(action.type) {

	case SET_MATTERGROUP:
		return action.matterGroup;
	
	case SET_MATTERGROUPS:
		if(!isEmpty(action.matterGroup)) {
			return action.matterGroup;	
		} else {
			return [];
		}

	case ADD_MATTERGROUP:
		return [
			...state,
			action.matterGroup
		];

	case UPDATE_MATTERGROUP:
		return state.map(item => {
			if (item._id === action.matterGroup._id) return action.matterGroup;
			return item;
		});

	case DELETE_MATTERGROUP:
		return state.filter(item => item._id !== action.matterGroup._id);

	case UPDATE_ADD_MATTERGROUP:
		if(state.findIndex(item => item._id === action.matterGroup._id) > -1) {
			return state.map(item => {
				if (item._id === action.matterGroup._id) return action.matterGroup;
				return item;
			});
		} else {
			return [
				...state,
				action.matterGroup
			];
		}

	default: 
		return state;
		
	}
}
