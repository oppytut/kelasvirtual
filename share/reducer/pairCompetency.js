/**
 * API
 */

import {
	SET_PAIRCOMPETENCY,
	SET_PAIRCOMPETENCYS,
	ADD_PAIRCOMPETENCY, 
	UPDATE_PAIRCOMPETENCY,
	DELETE_PAIRCOMPETENCY, 
	UPDATE_ADD_PAIRCOMPETENCY
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function pairCompetency(state=[], action={}) {
	switch(action.type) {

	case SET_PAIRCOMPETENCY:
		return action.pairCompetency;
	
	case SET_PAIRCOMPETENCYS:
		if(!isEmpty(action.pairCompetency)) {
			return action.pairCompetency;	
		} else {
			return [];
		}

	case ADD_PAIRCOMPETENCY:
		return [
			...state,
			action.pairCompetency
		];

	case UPDATE_PAIRCOMPETENCY:
		return state.map(item => {
			if (item._id === action.pairCompetency._id) return action.pairCompetency;
			return item;
		});

	case DELETE_PAIRCOMPETENCY:
		return state.filter(item => item._id !== action.pairCompetency._id);

	case UPDATE_ADD_PAIRCOMPETENCY:
		if(state.findIndex(item => item._id === action.pairCompetency._id) > -1) {
			return state.map(item => {
				if (item._id === action.pairCompetency._id) return action.pairCompetency;
				return item;
			});
		} else {
			return [
				...state,
				action.pairCompetency
			];
		}

	default: 
		return state;
		
	}
}
