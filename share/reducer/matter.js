/**
 * API
 */

import {
	SET_MATTER,
	SET_MATTERS,
	ADD_MATTER, 
	UPDATE_MATTER,
	DELETE_MATTER, 
	UPDATE_ADD_MATTER
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function matter(state=[], action={}) {
	switch(action.type) {

	case SET_MATTER:
		return action.matter;
	
	case SET_MATTERS:
		if(!isEmpty(action.matter)) {
			return action.matter;	
		} else {
			return [];
		}

	case ADD_MATTER:
		return [
			...state,
			action.matter
		];

	case UPDATE_MATTER:
		return state.map(item => {
			if (item._id === action.matter._id) return action.matter;
			return item;
		});

	case DELETE_MATTER:
		return state.filter(item => item._id !== action.matter._id);

	case UPDATE_ADD_MATTER:
		if(state.findIndex(item => item._id === action.matter._id) > -1) {
			return state.map(item => {
				if (item._id === action.matter._id) return action.matter;
				return item;
			});
		} else {
			return [
				...state,
				action.matter
			];
		}

	default: 
		return state;
		
	}
}
