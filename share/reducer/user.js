/**
 * API
 */

import {
	SET_USER,
	SET_USERS,
	ADD_USER, 
	UPDATE_USER,
	DELETE_USER, 
	UPDATE_ADD_USER
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function user(state=[], action={}) {
	switch(action.type) {

	case SET_USER:
		return action.user;
	
	case SET_USERS:
		if(!isEmpty(action.user)) {
			return action.user;	
		} else {
			return [];
		}

	case ADD_USER:
		return [
			...state,
			action.user
		];

	case UPDATE_USER:
		return state.map(item => {
			if (item._id === action.user._id) return action.user;
			return item;
		});

	case DELETE_USER:
		return state.filter(item => item._id !== action.user._id);

	case UPDATE_ADD_USER:
		if(state.findIndex(item => item._id === action.user._id) > -1) {
			return state.map(item => {
				if (item._id === action.user._id) return action.user;
				return item;
			});
		} else {
			return [
				...state,
				action.user
			];
		}

	default: 
		return state;
		
	}
}
