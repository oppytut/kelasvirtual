/**
 * API
 */

import {
	SET_STUDENTTEST,
	SET_STUDENTTESTS,
	ADD_STUDENTTEST, 
	UPDATE_STUDENTTEST,
	DELETE_STUDENTTEST, 
	UPDATE_ADD_STUDENTTEST
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function studentTest(state=[], action={}) {
	switch(action.type) {

	case SET_STUDENTTEST:
		return action.studentTest;
	
	case SET_STUDENTTESTS:
		if(!isEmpty(action.studentTest)) {
			return action.studentTest;	
		} else {
			return [];
		}

	case ADD_STUDENTTEST:
		return [
			...state,
			action.studentTest
		];

	case UPDATE_STUDENTTEST:
		return state.map(item => {
			if (item._id === action.studentTest._id) return action.studentTest;
			return item;
		});

	case DELETE_STUDENTTEST:
		return state.filter(item => item._id !== action.studentTest._id);

	case UPDATE_ADD_STUDENTTEST:
		if(state.findIndex(item => item._id === action.studentTest._id) > -1) {
			return state.map(item => {
				if (item._id === action.studentTest._id) return action.studentTest;
				return item;
			});
		} else {
			return [
				...state,
				action.studentTest
			];
		}

	default: 
		return state;
		
	}
}
