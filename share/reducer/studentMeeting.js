/**
 * API
 */

import {
	SET_STUDENTMEETING,
	SET_STUDENTMEETINGS,
	ADD_STUDENTMEETING,
	UPDATE_STUDENTMEETING,
	DELETE_STUDENTMEETING,
	UPDATE_ADD_STUDENTMEETING
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function studentMeeting(state=[], action={}) {
	switch(action.type) {

	case SET_STUDENTMEETING:
		return action.studentMeeting;

	case SET_STUDENTMEETINGS:
		if(!isEmpty(action.studentMeeting)) {
			return action.studentMeeting;
		} else {
			return [];
		}

	case ADD_STUDENTMEETING:
		return [
			...state,
			action.studentMeeting
		];

	case UPDATE_STUDENTMEETING:
		return state.map(item => {
			if (item._id === action.studentMeeting._id) return action.studentMeeting;
			return item;
		});

	case DELETE_STUDENTMEETING:
		return state.filter(item => item._id !== action.studentMeeting._id);

	case UPDATE_ADD_STUDENTMEETING:
		if(state.findIndex(item => item._id === action.studentMeeting._id) > -1) {
			return state.map(item => {
				if (item._id === action.studentMeeting._id) return action.studentMeeting;
				return item;
			});
		} else {
			return [
				...state,
				action.studentMeeting
			];
		}

	default:
		return state;

	}
}
