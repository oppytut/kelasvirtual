/**
 * API
 */

import {
	SET_INDICATOR,
	SET_INDICATORS,
	ADD_INDICATOR, 
	UPDATE_INDICATOR,
	DELETE_INDICATOR, 
	UPDATE_ADD_INDICATOR
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function indicator(state=[], action={}) {
	switch(action.type) {

	case SET_INDICATOR:
		return action.indicator;
	
	case SET_INDICATORS:
		if(!isEmpty(action.indicator)) {
			return action.indicator;	
		} else {
			return [];
		}

	case ADD_INDICATOR:
		return [
			...state,
			action.indicator
		];

	case UPDATE_INDICATOR:
		return state.map(item => {
			if (item._id === action.indicator._id) return action.indicator;
			return item;
		});

	case DELETE_INDICATOR:
		return state.filter(item => item._id !== action.indicator._id);

	case UPDATE_ADD_INDICATOR:
		if(state.findIndex(item => item._id === action.indicator._id) > -1) {
			return state.map(item => {
				if (item._id === action.indicator._id) return action.indicator;
				return item;
			});
		} else {
			return [
				...state,
				action.indicator
			];
		}

	default: 
		return state;
		
	}
}
