/**
 * API
 */

import {
	SET_STUDENTCOGNITIVETEST,
	SET_STUDENTCOGNITIVETESTS,
	ADD_STUDENTCOGNITIVETEST,
	UPDATE_STUDENTCOGNITIVETEST,
	DELETE_STUDENTCOGNITIVETEST,
	UPDATE_ADD_STUDENTCOGNITIVETEST
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function studentCognitiveTest(state=[], action={}) {
	switch(action.type) {

	case SET_STUDENTCOGNITIVETEST:
		return action.studentCognitiveTest;

	case SET_STUDENTCOGNITIVETESTS:
		if(!isEmpty(action.studentCognitiveTest)) {
			return action.studentCognitiveTest;
		} else {
			return [];
		}

	case ADD_STUDENTCOGNITIVETEST:
		return [
			...state,
			action.studentCognitiveTest
		];

	case UPDATE_STUDENTCOGNITIVETEST:
		return state.map(item => {
			if (item._id === action.studentCognitiveTest._id) return action.studentCognitiveTest;
			return item;
		});

	case DELETE_STUDENTCOGNITIVETEST:
		return state.filter(item => item._id !== action.studentCognitiveTest._id);

	case UPDATE_ADD_STUDENTCOGNITIVETEST:
		if(state.findIndex(item => item._id === action.studentCognitiveTest._id) > -1) {
			return state.map(item => {
				if (item._id === action.studentCognitiveTest._id) return action.studentCognitiveTest;
				return item;
			});
		} else {
			return [
				...state,
				action.studentCognitiveTest
			];
		}

	default:
		return state;

	}
}
