/**
 * API
 */

import {
	SET_REQUESTSTUDENT,
	SET_REQUESTSTUDENTS,
	ADD_REQUESTSTUDENT, 
	UPDATE_REQUESTSTUDENT,
	DELETE_REQUESTSTUDENT, 
	UPDATE_ADD_REQUESTSTUDENT
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function requestStudent(state=[], action={}) {
	switch(action.type) {

	case SET_REQUESTSTUDENT:
		return action.requestStudent;
	
	case SET_REQUESTSTUDENTS:
		if(!isEmpty(action.requestStudent)) {
			return action.requestStudent;	
		} else {
			return [];
		}

	case ADD_REQUESTSTUDENT:
		return [
			...state,
			action.requestStudent
		];

	case UPDATE_REQUESTSTUDENT:
		return state.map(item => {
			if (item._id === action.requestStudent._id) return action.requestStudent;
			return item;
		});

	case DELETE_REQUESTSTUDENT:
		return state.filter(item => item._id !== action.requestStudent._id);

	case UPDATE_ADD_REQUESTSTUDENT:
		if(state.findIndex(item => item._id === action.requestStudent._id) > -1) {
			return state.map(item => {
				if (item._id === action.requestStudent._id) return action.requestStudent;
				return item;
			});
		} else {
			return [
				...state,
				action.requestStudent
			];
		}

	default: 
		return state;
		
	}
}
