/**
 * API
 */

import {
	SET_STUDENTPSYCHOMOTORTEST,
	SET_STUDENTPSYCHOMOTORTESTS,
	ADD_STUDENTPSYCHOMOTORTEST,
	UPDATE_STUDENTPSYCHOMOTORTEST,
	DELETE_STUDENTPSYCHOMOTORTEST,
	UPDATE_ADD_STUDENTPSYCHOMOTORTEST
} from '../action/actionTypes.js';

import isEmpty from 'is-empty';

export default function studentPsychomotorTest(state=[], action={}) {
	switch(action.type) {

	case SET_STUDENTPSYCHOMOTORTEST:
		return action.studentPsychomotorTest;

	case SET_STUDENTPSYCHOMOTORTESTS:
		if(!isEmpty(action.studentPsychomotorTest)) {
			return action.studentPsychomotorTest;
		} else {
			return [];
		}

	case ADD_STUDENTPSYCHOMOTORTEST:
		return [
			...state,
			action.studentPsychomotorTest
		];

	case UPDATE_STUDENTPSYCHOMOTORTEST:
		return state.map(item => {
			if (item._id === action.studentPsychomotorTest._id) return action.studentPsychomotorTest;
			return item;
		});

	case DELETE_STUDENTPSYCHOMOTORTEST:
		return state.filter(item => item._id !== action.studentPsychomotorTest._id);

	case UPDATE_ADD_STUDENTPSYCHOMOTORTEST:
		if(state.findIndex(item => item._id === action.studentPsychomotorTest._id) > -1) {
			return state.map(item => {
				if (item._id === action.studentPsychomotorTest._id) return action.studentPsychomotorTest;
				return item;
			});
		} else {
			return [
				...state,
				action.studentPsychomotorTest
			];
		}

	default:
		return state;

	}
}
