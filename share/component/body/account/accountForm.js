import React, {Component} from 'react';
import {Form, Button, Label, Message, Select, Divider, Loader} from 'semantic-ui-react';
import debug from 'debug';
import validator from 'validator';
import isEmpty from 'is-empty';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	putUser,
	setUsers
} from '../../../action/user.js';

import {
	postIdentity,
	putIdentity
} from '../../../action/identity.js';

const logName = 'AccountForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class AccountForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {},
			dataIdentity: {},
			errorIdentity: {},
			loading: true,
			message: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		/** Form Data */
		const user = this.props.user.reduce(item => item);

		const data = {
			name: user.name,
			gender: user.gender
		};

		const {number, type} = isEmpty(user.identity) ?
			{} : user.identity.reduce(item => item);

		const dataIdentity = {
			number: number,
			type: type
		};

		this.setState({
			data: data,
			dataIdentity: dataIdentity
		});
		/** End */

		this.setState({
			loading: false
		});
	}

	change(e) {
		log.trace('change');

		var {data} = this.state;
		data[e.target.name] = e.target.value;

		var {error} = this.state;
		delete error[e.target.name];

		this.setState({data, error});
	}

	selectChange(e, {name, value}) {
		log.trace('selectChange');

		var {data} = this.state;
		data[name] = value;

		var {error} = this.state;
		delete error[name];

		this.setState({data, error});
	}

	changeIdentity(e) {
		log.trace('changeIdentity');

		var {dataIdentity} = this.state;
		dataIdentity[e.target.name] = e.target.value;

		var {errorIdentity} = this.state;
		delete errorIdentity[e.target.name];

		this.setState({dataIdentity, errorIdentity});
	}

	selectChangeIdentity(e, {name, value}) {
		log.trace('selectChangeIdentity');

		var {dataIdentity} = this.state;
		dataIdentity[name] = value;

		var {errorIdentity} = this.state;
		delete errorIdentity[name];

		this.setState({dataIdentity, errorIdentity});
	}

	validateName() {
		log.trace('validateName');
		const {name} = this.state.data;
		var {error} = this.state;

		if(!isEmpty(name)) {
			if(!/^[a-zA-Z ]+$/.test(name)
				|| name[0] === ' '
				|| name[name.length-1] === ' '
			) {
				error.name = 'Format nama yang Anda isi tidak valid!';
			} else {
				delete error['name'];
			}
		} else {
			delete error['name'];
		}

		this.setState({error: error});
	}

	validateIdentityNumber() {
		log.trace('validateIdentityNumber');
		const {number} = this.state.dataIdentity;
		var {errorIdentity} = this.state;

		if(!isEmpty(number)) {
			if(!validator.isNumeric(number)) {
				errorIdentity.number = 'Format nomor identitas yang Anda isi tidak valid!';
			} else {
				delete errorIdentity['number'];
			}
		} else {
			delete errorIdentity['number'];
		}

		this.setState({errorIdentity: errorIdentity});
	}

	validate() {
		log.trace('validate');

		this.validateName();
		this.validateIdentityNumber();

		const {number, type} = this.state.dataIdentity;
		var {errorIdentity} = this.state;

		if(!isEmpty(number) || !isEmpty(type)) {
			if(!(!isEmpty(number) && !isEmpty(type))) {
				if(isEmpty(number))
					errorIdentity.number = 'Anda belum mengisi nomor identitas!';
				if(isEmpty(type))
					errorIdentity.type = 'Anda belum mengisi jenis identitas!';
			}
		}

		this.setState({errorIdentity: errorIdentity});
	}

	submit(e) {
		log.trace('submit');
		e.target.blur();

		if(!isEmpty(this.state.messageId)) {
			clearTimeout(this.state.messageId);
			this.setState({
				messageId: undefined,
				message: undefined
			});
		}

		this.setState({loading: true});

		this.validate();
		const validation = (isEmpty(this.state.error) && isEmpty(this.state.errorIdentity));

		if(validation) {
			log.info('data is valid');

			/** Save User */
			const savingUser = new Promise((resolve, reject) => {
				var {data} = this.state;
				data._id = this.props.user.reduce(item => item)._id;

				this.props.putUser(data).then(
					(res) => {resolve(res.user);},
					(err) => {
						err.response.json().then(({error}) => {
							this.setState({error});
							reject();
						});
					}
				);
			});
			/** End */

			/** Save Identity */
			const savingIdentity = new Promise((resolve, reject) => {
				const user = this.props.user.reduce(item => item);
				var {dataIdentity} = this.state;

				if(isEmpty(dataIdentity.number)
				&& isEmpty(dataIdentity.type)) {
					resolve();
				} else if(isEmpty(user.identity)) {
					dataIdentity.user = user._id;

					this.props.postIdentity(dataIdentity).then(
						(res) => {resolve(res.identity);},
						(err) => {
							err.response.json().then(({error}) => {
								const errorIdentity = error;
								this.setState({errorIdentity});
								reject();
							});
						}
					);
				} else {
					dataIdentity._id = user.identity.reduce(item => item)._id;

					this.props.putIdentity(dataIdentity).then(
						(res) => {resolve(res.identity);},
						(err) => {
							err.response.json().then(({error}) => {
								const errorIdentity = error;
								this.setState({errorIdentity});
								reject();
							});
						}
					);
				}
			});
			/** End */

			Promise.all([savingUser, savingIdentity]).then(
				(item) => {
					const message = {
						content: 'Data Anda berhasil disimpan!',
						color: 'green'
					};
					this.setState({
						loading: false,
						message: message
					});

					var messageId = setTimeout(() => {
						this.setState({message: undefined});
					}, 3000);
					this.setState({messageId: messageId});

					var user = item[0];
					const identity = item[1];

					user.identity = [];
					user.identity.push(identity);

					this.props.setUsers([user]);
				},
				() => {
					const message = {
						content: 'Data Anda gagal disimpan!',
						color: 'red'
					};
					this.setState({
						loading: false,
						message: message
					});

					var messageId = setTimeout(() => {
						this.setState({message: undefined});
					}, 3000);
					this.setState({messageId: messageId});
				}
			);
		} else {
			this.setState({loading: false});
		}
	}

	render() {
		log.trace('render');

		return(
			<div>
				{!isEmpty(this.state.message) &&
					<Message size='tiny' color={this.state.message.color}>
						{this.state.message.content}
					</Message>
				}
				{this.state.loading ?
					<div style={{margin: '400px 0px 400px 0px'}}>
						<Loader active />
					</div>
					:
					<Form size='small'>
						<Form.Field>
							<label>Nama Lengkap</label>
							<input
								placeholder='Nama Lengkap'
								name='name'
								onChange={this.change.bind(this)}
								value={isEmpty(this.state.data.name) ? '' : this.state.data.name}
							/>
							{this.state.error.name &&
								<Label pointing>{this.state.error.name}</Label>
							}
						</Form.Field>
						<Form.Field>
							<label>Jenis Kelamin</label>
							<Select
								placeholder='Jenis Kelamin'
								name='gender'
								options={[
									{key: 1, text: 'L (Laki-Laki)', value: 'L (Laki-Laki)'},
									{key: 2, text: 'P (Perempuan)', value: 'P (Perempuan)'}
								]}
								onChange={this.selectChange.bind(this)}
								value={this.state.data.gender}
							/>
							{this.state.error.gender &&
								<Label pointing>{this.state.error.gender}</Label>
							}
						</Form.Field>
						<Form.Field>
							<label>Nomor Identitas</label>
							<input
								placeholder='Nomor Indentitas'
								name='number'
								onChange={this.changeIdentity.bind(this)}
								value={isEmpty(this.state.dataIdentity.number) ? '' : this.state.dataIdentity.number}
							/>
							{this.state.errorIdentity.number &&
								<Label pointing>{this.state.errorIdentity.number}</Label>
							}
						</Form.Field>
						<Form.Field>
							<label>Jenis Identitas</label>
							<Select
								placeholder='Jenis Indentitas'
								name='type'
								options={[
									{key: 1, text: 'NIS (Nomor Induk Siswa)', value: 'NIS (Nomor Induk Siswa)'},
									{key: 2, text: 'NIP (Nomor Induk Pegawai)', value: 'NIP (Nomor Induk Pegawai)'}
								]}
								onChange={this.selectChangeIdentity.bind(this)}
								value={this.state.dataIdentity.type}
							/>
							{this.state.errorIdentity.type &&
								<Label pointing>{this.state.errorIdentity.type}</Label>
							}
						</Form.Field>
						<Form.Field>
							<Divider hidden />
							<center>
								<Button
									type='submit'
									onClick={this.submit.bind(this)}
									size='small'
									color='teal'
								>Simpan</Button>
							</center>
						</Form.Field>
					</Form>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		user: state.user
	};
}

AccountForm.propTypes = {
	putUser: PropTypes.func.isRequired,
	setUsers: PropTypes.func.isRequired,
	postIdentity: PropTypes.func.isRequired,
	putIdentity: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	putUser,
	setUsers,
	postIdentity,
	putIdentity
}) (AccountForm);
