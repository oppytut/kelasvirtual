import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Grid, Loader} from 'semantic-ui-react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import AccountForm from './accountForm.js';

import {
	getUsers,
	setUsers
} from '../../../action/user.js';
import {
	headerActiveMenu
} from '../../../action/header.js';

const logName = 'Account';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Account extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.headerActiveMenu('account');
		this.checkLogin();

		const userId = this.props.auth.user.id;

		this.props.getUsers({
			query: {_id: userId},
			field: 'email username name gender',
			populate: [
				{
					path: 'identity',
					select: 'number type'
				}
			]
		}).then((res) => {
			this.props.setUsers(res.user);
			this.setState({loading: false});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setUsers();
	}

	checkLogin() {
		log.trace('checkLogin');

		const {isAuthenticated} = this.props.auth;

		if(!isAuthenticated)
			this.props.history.push('/401');
	}

	render() {
		log.trace('render');

		const {loading} = this.state;
		const user = loading ? {} : this.props.user.reduce(item => item);

		return(
			<div>
				{loading ?
					<Grid style={{
						paddingTop: '70px',
						paddingBottom: '70px'
					}}>
						<Grid.Row style={{
							paddingTop: '100px',
							paddingBottom: '100px'
						}}>
							<Grid.Column>
								<Loader active />
							</Grid.Column>
						</Grid.Row>
					</Grid>
					:
					<Grid style={{
						paddingTop: '70px',
						paddingBottom: '70px'
					}}>
						<Grid.Row style={{
							background: '#00B5AD',
							paddingLeft: '13px',
							paddingRight: '13px'
						}}>
							<Grid.Column style={{color: 'white'}}>
								<div style={{fontSize: '20px'}}>
									{user.username}
								</div>
								<div style={{fontSize: '12px'}}>
									{user.email}
								</div>
							</Grid.Column>
						</Grid.Row>

						<Grid.Row style={{
							paddingLeft: '13px',
							paddingRight: '13px'
						}}>
							<Grid.Column>
								<center>
									<div style={{
										maxWidth: '500px',
										textAlign: 'left'
									}}>
										<AccountForm />
									</div>
								</center>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth,
		user: state.user
	};
}

Account.propTypes = {
	headerActiveMenu: PropTypes.func.isRequired,
	setUsers: PropTypes.func.isRequired,
	getUsers: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps, {
	headerActiveMenu,
	setUsers,
	getUsers
}) (Account));
