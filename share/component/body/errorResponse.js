import React, {Component} from 'react';
import {Grid, Message} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import debug from 'debug';

import {
	headerActiveMenu
} from '../../action/header.js';

const logName = 'ResponseError';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class ResponseError extends Component {
	componentWillMount() {
		log.trace('componentWillMount');

		this.props.headerActiveMenu('404');
	}

	render() {
		log.trace('render');

		const {title, message} = this.props;

		return(
			<div>
				<Grid style={{paddingTop: '70px', paddingBottom: '70px'}}>
					<Grid.Row style={{padding: '0px 13px 0px 13px'}}>
						<Grid.Column>
							<center>
								<Message>
									<Message.Header>
										{title}
									</Message.Header>
									<p>
										{message}
									</p>
								</Message>
							</center>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}

ResponseError.propTypes = {
	headerActiveMenu: PropTypes.func.isRequired
};

export default connect(null, {
	headerActiveMenu
}) (ResponseError);