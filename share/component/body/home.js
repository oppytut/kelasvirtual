import React, {Component} from 'react';
import {Button, Image, Divider, Grid, Responsive} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import Particles from 'react-particles-js';
import {Link} from 'react-router-dom';
import debug from 'debug';

import {
	headerRegisterModalOpen,
	headerLoginModalOpen,
	headerActiveMenu
} from '../../action/header.js';

const logName = 'Home';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

/** Running Text */
const TxtRotate = function(el, toRotate, period) {
	this.toRotate = toRotate;
	this.el = el;
	this.loopNum = 0;
	this.period = parseInt(period, 10) || 2000;
	this.txt = '';
	this.tick();
	this.isDeleting = false;
};

TxtRotate.prototype.tick = function() {
	const i = this.loopNum % this.toRotate.length;
	const fullTxt = this.toRotate[i];

	if (this.isDeleting) {
		this.txt = fullTxt.substring(0, this.txt.length - 1);
	} else {
		this.txt = fullTxt.substring(0, this.txt.length + 1);
	}

	this.el.innerHTML = '<span class="wrap">'+this.txt+'</span>';

	const that = this;
	var delta = 300 - Math.random() * 100;

	if (this.isDeleting) { delta /= 2; }

	if (!this.isDeleting && this.txt === fullTxt) {
		delta = this.period;
		this.isDeleting = true;
	} else if (this.isDeleting && this.txt === '') {
		this.isDeleting = false;
		this.loopNum++;
		delta = 500;
	}

	setTimeout(function() {
		that.tick();
	}, delta);
};
/** End */

class Home extends Component {
	componentWillMount() {
		log.trace('componentWillMount');
		this.props.headerActiveMenu('home');
	}

	componentDidMount() {
		log.trace('componentDidMount');

		this.activeTyping();
	}

	activeTyping() {
		log.trace('activeTyping');

		const elements = document.getElementsByClassName('txt-rotate');
		for (var i=0; i<elements.length; i++) {
			const toRotate = elements[i].getAttribute('data-rotate');
			const period = elements[i].getAttribute('data-period');
			if (toRotate) {
				new TxtRotate(elements[i], JSON.parse(toRotate), period);
			}
		}

		// Inject CSS
		var css = document.createElement('style');
		css.type = 'text/css';
		css.innerHTML = '.txt-rotate > .wrap { border-right: 0.08em solid #0DADFF }';
		document.body.appendChild(css);
	}

	register(e) {
		log.trace('register');
		e.target.blur();

		this.props.headerRegisterModalOpen(true);
	}

	login(e) {
		log.trace('login');
		e.target.blur();

		this.props.headerLoginModalOpen(true);
	}

	render() {
		log.trace('render');

		const {isAuthenticated} = this.props.auth;

		return(
			<div>
				<Grid style={{
					background: '#00B5AD',
					paddingTop: '80px',
					paddingBottom: '80px'
				}}>
					<Particles style={{
						position: 'fixed',
						top: 0,
						left: 0,
						height: '100%',
						width: '100%'
					}} />

					<Grid.Row style={{marginTop: '100px'}}>
						<Grid.Column>
							<Image
								src='/media/icon/300x300-transparent.png'
								size='small'
								centered
							/>
						</Grid.Column>
					</Grid.Row>

					<Grid.Row style={{color: 'white', marginTop: '40px'}}>
						<Grid.Column textAlign='center'>
							<p style={{fontSize: '40px'}}>
								Kelas Virtual,
								<span
									className='txt-rotate'
									data-period='2000'
									data-rotate='[
										" tambah pengetahuanmu!",
										" asah keterampilanmu!",
										" tingkatkan kemampuanmu!",
										" uji kompetensimu!",
										" wujudkan prestasimu!"
									]'
								></span>
							</p>
							<p style={{
								fontSize: '16px',
								marginTop: '-40px'
							}}>
								Media pembelajaran berbasis web untuk meningkatkan keterampilan pada pembelajaran Basis Data.
							</p>
						</Grid.Column>
					</Grid.Row>

					<Grid.Row style={{marginTop: '25px'}}>
						{isAuthenticated ?
							<Grid.Column textAlign='center'>
								<Button
									inverted
									animated='fade'
									as={Link}
									to='/course'
								>
									<Button.Content visible>
										Mulai Belajar Sekarang
									</Button.Content>
									<Button.Content hidden>
										Lihat Kelas
									</Button.Content>
								</Button>
							</Grid.Column>
							:
							<Grid.Column textAlign='center'>
								<Button
									inverted
									animated='fade'
									onClick={this.login.bind(this)}
								>
									<Button.Content visible>
										Mulai Belajar Sekarang
									</Button.Content>
									<Button.Content hidden>
										Masuk
									</Button.Content>
								</Button>
								<Responsive
									maxWidth={500}
									as={Divider}
									hidden
									fitted
								/>
								<Button
									inverted
									animated='fade'
									onClick={this.register.bind(this)}
								>
									<Button.Content visible>
										Belum punya akun?
									</Button.Content>
									<Button.Content hidden>
										Daftar
									</Button.Content>
								</Button>
							</Grid.Column>
						}
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth
	};
}

Home.propTypes = {
	headerRegisterModalOpen: PropTypes.func.isRequired,
	headerLoginModalOpen: PropTypes.func.isRequired,
	headerActiveMenu: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	headerRegisterModalOpen,
	headerLoginModalOpen,
	headerActiveMenu
}) (Home);
