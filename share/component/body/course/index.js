import React, {Component} from 'react';
import {Route, withRouter} from 'react-router-dom';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import CourseList from './courseList';
import CourseHome from './courseHome';

import {
	headerActiveMenu
} from '../../../action/header.js';

const logName = 'Course';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};	

class Course extends Component {
	componentWillMount() {
		log.trace('componentWillMount');

		this.checkLogin();
		this.props.headerActiveMenu('course');
	}

	checkLogin() {
		const {isAuthenticated} = this.props.auth;

		if(!isAuthenticated)
			this.props.history.push('/401');
	}

	render() {
		log.trace('render');

		return(
			<div>
				<Route path='/course' exact component={CourseList} />
				<Route path='/course/:courseId' component={CourseHome} />
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth
	};
}

Course.propTypes = {
	headerActiveMenu: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps, {
	headerActiveMenu
}) (Course));