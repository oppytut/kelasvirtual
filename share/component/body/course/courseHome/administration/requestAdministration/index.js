import React, {Component} from 'react';
import debug from 'debug';
import {Table, Button, Loader, Message} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'is-empty';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getRequestStudents,
	delRequestStudent
} from '../../../../../../action/requestStudent.js';

import {
	putPostStudent
} from '../../../../../../action/student.js';

import {
	administrationRequestList
} from '../../../../../../action/administration.js';

const logName = 'RequestAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class RequestAdministration extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			acceptLoading: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Request']);
		this.props.changeMenu('request');

		const course = this.props.course.reduce(item => item);
		this.props.getRequestStudents({
			query: {
				course: course._id
			},
			populate: [
				{
					path: 'user',
					select: 'name gender username email'
				}
			],
			field: '_id'
		}).then((res) => {
			this.props.administrationRequestList(res.requestStudent);

			const count = 10;
			var list = {
				count: count,
				page: 1,
				countPage: Math.ceil(Object.keys(res.requestStudent).length/count)
			};

			this.setState({
				loading: false,
				list
			});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.administrationRequestList();
	}

	/** Pagination */
	nextPage(e) {
		log.trace('nextPage');
		e.target.blur();

		var {list} = this.state;

		if(list.page < list.countPage) {
			list.page = list.page+1;
			this.setState({list});
		}
	}

	prevPage(e) {
		log.trace('prevPage');
		e.target.blur();

		var {list} = this.state;

		if(list.page > 1) {
			list.page = list.page-1;
			this.setState({list});
		}
	}

	goPage(page, e) {
		log.trace('goPage');
		e.target.blur();

		var {list} = this.state;
		list.page = page;
		this.setState({list});
	}
	/** End */

	/** Accept */
	acceptRequest(requestStudent, idx, e) {
		log.trace('acceptRequest');
		e.target.blur();

		if(!isEmpty(this.state.messageId)) {
			clearTimeout(this.state.messageId);
			this.setState({
				messageId: undefined,
				message: undefined
			});
		}

		this.changeAcceptLoading(idx, true);

		const deletingRequestStudent = new Promise((resolve, reject) => {
			this.props.delRequestStudent(requestStudent._id)
				.then(() => {
					resolve();
				})
				.catch(() => {
					reject();
				});
		});

		const putPostingStudent = new Promise((resolve, reject) => {
			const course = this.props.course.reduce(item => item);

			this.props.putPostStudent({
				query: {
					user: requestStudent.user._id,
					course: course._id
				},
				data: {
					post: {
						user: requestStudent.user._id,
						course: course._id
					},
					put: {deleted: false}
				}
			})
				.then(() => {
					resolve();
				})
				.catch(() => {
					reject();
				});
		});

		Promise.all([deletingRequestStudent, putPostingStudent]).then(() => {
			const requestList = this.props.administration.requestList
				.filter((item) => {
					return item._id !== requestStudent._id;
				});

			this.props.administrationRequestList(requestList);

			this.changeAcceptLoading(idx, false);

			const message = {
				content: 'Permintaan berhasil disetujui!',
				color: 'green'
			};
			this.setState({
				message: message
			});

			var messageId = setTimeout(() => {
				this.setState({message: undefined});
			}, 3000);
			this.setState({messageId: messageId});
		});
	}

	changeAcceptLoading(idx, sts) {
		log.trace('changeAcceptLoading');

		var {acceptLoading} = this.state;
		acceptLoading[idx] = sts;
		this.setState({acceptLoading: acceptLoading});
	}
	/** End */

	render() {
		log.trace('render');

		const {loading, acceptLoading} = this.state;
		const requestList = isEmpty(this.props.administration.requestList) ?
			[] : this.props.administration.requestList;
		const list = isEmpty(this.state.list) ?
			{} : this.state.list;

		/** PageButtion Component */
		var pageButton = [];
		for(var i = 1; i <= list.countPage; i++) {
			pageButton.push(<Button
				key={i}
				content={i}
				active={i == list.page ? true : undefined}
				onClick={this.goPage.bind(this, i)}
			/>);
		}
		/** End */

		return(
			<div>
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					<div>
						{!isEmpty(this.state.message) &&
							<Message color={this.state.message.color} size='tiny'>
								{this.state.message.content}
							</Message>
						}
						{isEmpty(requestList) ?
							<Message>
								<Message.Header>
									Belum ada permintaan peserta didik!
								</Message.Header>
								<p>
									Pengguna lain dapat mengajukan permintaan untuk menjadi peserta didik.
								</p>
							</Message>
							:
							<Table size='small' compact unstackable basic='very'>
								<Table.Header>
									<Table.Row>
										<Table.HeaderCell>No.</Table.HeaderCell>
										<Table.HeaderCell>Nama</Table.HeaderCell>
										<Table.HeaderCell>Jenis Kelamin</Table.HeaderCell>
										<Table.HeaderCell>Username</Table.HeaderCell>
										<Table.HeaderCell>Email</Table.HeaderCell>
										<Table.HeaderCell></Table.HeaderCell>
									</Table.Row>
								</Table.Header>
								<Table.Body>
									{requestList.map((requestStudent, index) => {
										if(
											(list.count*(list.page-1))+1 <= index+1
											&& index+1 <= list.count*list.page
										)
											return(
												<Table.Row key={index}>
													<Table.Cell style={{width: '25px'}}>{index+1}</Table.Cell>
													<Table.Cell>{isEmpty(requestStudent.user.name) ?
														<div style={{fontSize: '12px', color: 'grey'}}>Kosong</div> : requestStudent.user.name
													}</Table.Cell>
													<Table.Cell width={2}>{isEmpty(requestStudent.user.gender) ?
														<div style={{fontSize: '12px', color: 'grey'}}>Kosong</div> : requestStudent.user.gender
													}</Table.Cell>
													<Table.Cell width={2}>{requestStudent.user.username}</Table.Cell>
													<Table.Cell width={2}>{requestStudent.user.email}</Table.Cell>
													<Table.Cell width={3} textAlign='right'>
														<Button
															content='Setujui'
															size='tiny'
															color='teal'
															icon='checkmark'
															basic
															compact
															onClick={acceptLoading[index] ? undefined : this.acceptRequest.bind(this, requestStudent, index)}
															loading={acceptLoading[index]}
														/>
													</Table.Cell>
												</Table.Row>
											);
									})}
								</Table.Body>
								<Table.Footer>
									<Table.Row>
										<Table.HeaderCell colSpan='6' textAlign='right'>
											<Button.Group size='mini' color='teal' compact>
												<Button
													icon='left chevron'
													onClick={this.prevPage.bind(this)}
													disabled={list.page > 1 ? undefined : true}
												/>
												{pageButton}
												<Button
													icon='right chevron'
													onClick={this.nextPage.bind(this)}
													disabled={list.page < list.countPage ? undefined : true}
												/>
											</Button.Group>
										</Table.HeaderCell>
									</Table.Row>
								</Table.Footer>
							</Table>
						}
					</div>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		administration: state.administration
	};
}

RequestAdministration.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getRequestStudents: PropTypes.func.isRequired,
	delRequestStudent: PropTypes.func.isRequired,
	putPostStudent: PropTypes.func.isRequired,
	administrationRequestList: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getRequestStudents,
	delRequestStudent,
	putPostStudent,
	administrationRequestList
}) (RequestAdministration);
