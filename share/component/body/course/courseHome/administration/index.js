import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Grid} from 'semantic-ui-react';
import {Route, withRouter, Redirect, Switch} from 'react-router-dom';

import AdministrationMenu from './administrationMenu.js';
import CourseStatistic from './courseStatistic';
import StudentAdministration from './studentAdministration';
import RequestAdministration from './requestAdministration';
import TeacherAdministration from './teacherAdministration.js';
import CoreCompetencyAdministration from './coreCompetencyAdministration';
import BasicCompetencyAdministration from './basicCompetencyAdministration';
import IndicatorAdministration from './indicatorAdministration';
import ObjectiveAdministration from './objectiveAdministration';
import MatterAdministration from './matterAdministration';

import {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
} from '../../../../../action/courseHome.js';

const logName = 'Administration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Administration extends Component {
	constructor() {
		super();

		this.state = {
			activeMenu: 'statistic'
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveMenu('Administration');
		this.props.courseHomeActiveBreadcrumb(['Administration']);

	}

	changeMenu(item) {
		log.trace('changeMenu');

		this.setState({activeMenu: item});
	}

	getActiveMenu() {
		log.trace('getActiveMenu');

		return this.state.activeMenu;
	}

	render() {
		log.trace('render');

		return(
			<div>
				<Grid stackable textAlign='left'>
					<Grid.Row columns={2}>
						<Grid.Column width={3}>
							<AdministrationMenu
								getActiveMenu={this.getActiveMenu.bind(this)}
							/>
						</Grid.Column>
						<Grid.Column width={13}>
							<Switch>
								<Route
									path='/course/:courseId/administration'
									exact
									render={(props) => (
										<CourseStatistic {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/statistic'
									exact
									render={(props) => (
										<CourseStatistic {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/student'
									exact
									render={(props) => (
										<StudentAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/request'
									exact
									render={(props) => (
										<RequestAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/teacher'
									exact
									render={(props) => (
										<TeacherAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/coreCompetency'
									exact
									render={(props) => (
										<CoreCompetencyAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/basicCompetency'
									render={(props) => (
										<BasicCompetencyAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/indicator'
									exact
									render={(props) => (
										<IndicatorAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/objective'
									exact
									render={(props) => (
										<ObjectiveAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>
								<Route
									path='/course/:courseId/administration/matter'
									render={(props) => (
										<MatterAdministration {...props}
											changeMenu={this.changeMenu.bind(this)}
											getActiveMenu={this.getActiveMenu.bind(this)}
										/>
									)}
								/>

								<Redirect from='*' to='/404' />
							</Switch>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}

Administration.propTypes = {
	courseHomeActiveMenu: PropTypes.func.isRequired,
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired
};

export default withRouter(connect(null, {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
}) (Administration));
