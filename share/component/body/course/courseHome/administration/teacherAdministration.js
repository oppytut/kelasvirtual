import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../action/courseHome.js';

const logName = 'TeacherAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class TeacherAdministration extends Component {
	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Teacher']);
		this.props.changeMenu('teacher');

	}

	render() {
		log.trace('render');

		return(
			<div>
				TeacherAdministration
			</div>
		);
	}
}

TeacherAdministration.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired
};

export default connect(null, {
	courseHomeActiveBreadcrumb
}) (TeacherAdministration);
