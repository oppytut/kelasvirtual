import React, {Component} from 'react';
import {Table, Button, Loader, Modal, Message} from 'semantic-ui-react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'is-empty';

import AddStudentForm from './addStudentForm.js';
import EditStudentForm from './editStudentForm.js';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getStudentsOnly,
	putStudent
} from '../../../../../../action/student.js';

import {
	delStudentMatters
} from '../../../../../../action/studentMatter.js';

import {
	delStudentMeetings
} from '../../../../../../action/studentMeeting.js';

import {
	delStudentTests
} from '../../../../../../action/studentTest.js';

import {
	delStudentCognitiveTests
} from '../../../../../../action/studentCognitiveTest.js';

import {
	delStudentPsychomotorTests
} from '../../../../../../action/studentPsychomotorTest.js';

import {
	administrationStudentList
} from '../../../../../../action/administration.js';

const logName = 'MatterGroupListAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class MatterGroupListAdministration extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			deleteModalOpen: false,
			addModalOpen: false,
			editModalOpen: false
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Student']);
		this.props.changeMenu('student');

		const course = this.props.course.reduce(item => item);
		this.props.getStudentsOnly({
			query: {
				this: {
					course: course._id,
					deleted: {$ne: true}
				},
				teacher: {course: course._id}
			},
			populate: [
				{
					path: 'user',
					select: 'name gender username email'
				}
			],
			field: '_id'
		}).then((res) => {
			this.props.administrationStudentList(res.student);

			const count = 10;
			var list = {
				count: count,
				page: 1,
				countPage: Math.ceil(Object.keys(res.student).length/count)
			};

			this.setState({
				loading: false,
				list
			});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.administrationStudentList();
	}

	/** Remove Student */
	removeStudent(student, e) {
		log.trace('removeStudent');
		e.target.blur();

		this.setState({
			deleteModalOpen: true,
			studentDeleted: student
		});
	}

	closeDeleteModal() {
		log.trace('closeDeleteModal');

		this.setState({
			deleteModalOpen: false,
			studentDeleted: undefined
		});
	}

	handleDelete() {
		log.trace('handleDelete');

		if(!isEmpty(this.state.messageId)) {
			clearTimeout(this.state.messageId);
			this.setState({
				messageId: undefined,
				message: undefined
			});
		}

		var {studentList} = this.props.administration;
		var studentDeleted = this.state.studentDeleted;

		studentDeleted.deleted = true;

		studentList.map((item) => {
			if(item._id === studentDeleted._id)
				item = studentDeleted;
		});
		this.props.administrationStudentList(studentList);

		delete studentDeleted.user;
		delete studentDeleted.course;

		const puttingStudent = new Promise((resolve) => {
			this.props.putStudent(studentDeleted).then(() => {
				resolve();
			});
		});

		const deletingStudentTest = new Promise((resolve) => {
			this.props.delStudentTests({query: {student: studentDeleted._id}}).then(() => {
				resolve();
			});
		});

		const deletingStudentCognitiveTest = new Promise((resolve) => {
			this.props.delStudentCognitiveTests({query: {student: studentDeleted._id}}).then(() => {
				resolve();
			});
		});

		const deletingStudentPsychomotorTest = new Promise((resolve) => {
			this.props.delStudentPsychomotorTests({query: {student: studentDeleted._id}}).then(() => {
				resolve();
			});
		});

		const deletingStudentMatter = new Promise((resolve) => {
			this.props.delStudentMatters({query: {student: studentDeleted._id}}).then(() => {
				resolve();
			});
		});

		const deletingStudentMeeting = new Promise((resolve) => {
			this.props.delStudentMeetings({query: {student: studentDeleted._id}}).then(() => {
				resolve();
			});
		});

		Promise.all([
			puttingStudent,
			deletingStudentTest,
			deletingStudentCognitiveTest,
			deletingStudentPsychomotorTest,
			deletingStudentMatter,
			deletingStudentMeeting
		]).then(() => {
			this.setState({
				deleteModalOpen: false,
				studentDeleted: undefined
			});

			const message = {
				content: 'Peserta didik berhasil dihapus!',
				color: 'green'
			};
			this.setState({
				message: message
			});

			var messageId = setTimeout(() => {
				this.setState({message: undefined});
			}, 3000);
			this.setState({messageId: messageId});
		});
	}
	/** End */

	/** Add Student */
	addStudent(e) {
		log.trace('addStudent');
		e.target.blur();

		this.setState({addModalOpen: true});
	}

	closeAddModal() {
		log.trace('closeAddModal');

		this.setState({
			addModalOpen: false
		});
	}
	/** End */

	/** Edit Student */
	editStudent(student, e) {
		log.trace('editStudent');
		e.target.blur();

		this.setState({
			editModalOpen: true,
			studentEdited: student
		});
	}

	closeEditModal() {
		log.trace('closeEditModal');

		this.setState({
			editModalOpen: false,
			studentEdited: undefined
		});
	}

	handleEdit(e) {
		log.trace('handleEdit');
		e.target.blur();

	}

	getStudentEdited() {
		log.trace('getStudentEdited');

		return this.state.studentEdited;
	}
	/** End */

	/** Pagination */
	nextPage(e) {
		log.trace('nextPage');
		e.target.blur();

		var {list} = this.state;

		if(list.page < list.countPage) {
			list.page = list.page+1;
			this.setState({list});
		}
	}

	prevPage(e) {
		log.trace('prevPage');
		e.target.blur();

		var {list} = this.state;

		if(list.page > 1) {
			list.page = list.page-1;
			this.setState({list});
		}
	}

	goPage(page, e) {
		log.trace('goPage');
		e.target.blur();

		var {list} = this.state;
		list.page = page;
		this.setState({list});
	}
	/** End */

	render() {
		log.trace('render');

		const {loading} = this.state;
		const studentList = isEmpty(this.props.administration) ?
			[] : isEmpty(this.props.administration.studentList) ?
				[] : this.props.administration.studentList;
		const list = isEmpty(this.state.list) ?
			{} : this.state.list;

		/** PageButtion Component */
		var pageButton = [];
		for(var i = 1; i <= list.countPage; i++) {
			pageButton.push(<Button
				key={i}
				content={i}
				active={i == list.page ? true : undefined}
				onClick={this.goPage.bind(this, i)}
			/>);
		}
		/** End */

		const deleteModal = (
			<Modal
				size='tiny'
				open={this.state.deleteModalOpen}
				onClose={this.closeDeleteModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					Anda yakin ingin menghapus peserta didik tersebut dari kelas ini? <br />
					<Message size='small'>
						Proses pembelajaran yang telah dilakukan oleh peserta didik tersebut akan terhapus.
					</Message>
				</Modal.Content>

				<Modal.Actions>
					<Button size='small' onClick={this.closeDeleteModal.bind(this)}>
						Batal
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Ya'
						onClick={this.handleDelete.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const addModal = (
			<Modal
				size='tiny'
				open={this.state.addModalOpen}
				onClose={this.closeAddModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<AddStudentForm />
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeAddModal.bind(this)}
					>
						Tutup
					</Button>
				</Modal.Actions>
			</Modal>
		);

		const editModal = (
			<Modal
				size='tiny'
				open={this.state.editModalOpen}
				onClose={this.closeEditModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<EditStudentForm
						getStudentEdited={this.getStudentEdited.bind(this)}
						closeEditModal={this.closeEditModal.bind(this)}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeEditModal.bind(this)}
					>
						Tutup
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Simpan'
						type='submit'
						form='editStudentListForm'
						onClick={this.handleEdit.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		return(
			<div>
				{deleteModal}
				{addModal}
				{editModal}
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					<div>
						{!isEmpty(this.state.message) &&
							<Message color={this.state.message.color} size='tiny'>
								{this.state.message.content}
							</Message>
						}
						{isEmpty(studentList.filter((student) => {
							return student.deleted !== true;
						})) ?
							<Message>
								<Message.Header>
									Belum ada peserta didik!
								</Message.Header>
								<p>
									Silahkan tambahkan peserta didik terlebih dahulu.
									<br />
									<Button
										style={{marginTop: '15px'}}
										content='Tambah Peserta Didik'
										size='tiny'
										color='teal'
										icon='add'
										compact
										onClick={this.addStudent.bind(this)}
									/>
								</p>
							</Message>
							:
							<Table size='small' compact unstackable basic='very'>
								<Table.Header>
									<Table.Row>
										<Table.HeaderCell>No.</Table.HeaderCell>
										<Table.HeaderCell>Nama</Table.HeaderCell>
										<Table.HeaderCell>Jenis Kelamin</Table.HeaderCell>
										<Table.HeaderCell>Username</Table.HeaderCell>
										<Table.HeaderCell>Email</Table.HeaderCell>
										<Table.HeaderCell></Table.HeaderCell>
									</Table.Row>
								</Table.Header>
								<Table.Body>
									{studentList.filter((student) => {
										return student.deleted !== true;
									}).map((student, index) => {
										if(
											(list.count*(list.page-1))+1 <= index+1
											&& index+1 <= list.count*list.page
										)
											return(
												<Table.Row key={index}>
													<Table.Cell style={{width: '25px'}}>{index+1}</Table.Cell>
													<Table.Cell>{student.deleted}{isEmpty(student.user.name) ?
														<div style={{fontSize: '12px', color: 'grey'}}>Kosong</div> : student.user.name
													}</Table.Cell>
													<Table.Cell width={2}>{isEmpty(student.user.gender) ?
														<div style={{fontSize: '12px', color: 'grey'}}>Kosong</div> : student.user.gender
													}</Table.Cell>
													<Table.Cell width={2}>{student.user.username}</Table.Cell>
													<Table.Cell width={2}>{student.user.email}</Table.Cell>
													<Table.Cell width={3} textAlign='right'>
														<Button
															content='Edit'
															size='tiny'
															color='teal'
															icon='edit'
															basic
															compact
															onClick={this.editStudent.bind(this, student)}
														/>
														<Button
															content='Hapus'
															size='tiny'
															color='teal'
															icon='delete'
															basic
															compact
															onClick={this.removeStudent.bind(this, student)}
														/>
													</Table.Cell>
												</Table.Row>
											);
									})}
								</Table.Body>
								<Table.Footer>
									<Table.Row>
										<Table.HeaderCell colSpan='3' textAlign='left'>
											<Button
												content='Tambah'
												size='tiny'
												color='teal'
												icon='add'
												compact
												onClick={this.addStudent.bind(this)}
											/>
										</Table.HeaderCell>
										<Table.HeaderCell colSpan='3' textAlign='right'>
											<Button.Group size='mini' color='teal' compact>
												<Button
													icon='left chevron'
													onClick={this.prevPage.bind(this)}
													disabled={list.page > 1 ? undefined : true}
												/>
												{pageButton}
												<Button
													icon='right chevron'
													onClick={this.nextPage.bind(this)}
													disabled={list.page < list.countPage ? undefined : true}
												/>
											</Button.Group>
										</Table.HeaderCell>
									</Table.Row>
								</Table.Footer>
							</Table>
						}
					</div>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		administration: state.administration
	};
}

MatterGroupListAdministration.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getStudentsOnly: PropTypes.func.isRequired,
	putStudent: PropTypes.func.isRequired,
	administrationStudentList: PropTypes.func.isRequired,
	delStudentTests: PropTypes.func.isRequired,
	delStudentCognitiveTests: PropTypes.func.isRequired,
	delStudentPsychomotorTests: PropTypes.func.isRequired,
	delStudentMatters: PropTypes.func.isRequired,
	delStudentMeetings: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getStudentsOnly,
	putStudent,
	administrationStudentList,
	delStudentTests,
	delStudentCognitiveTests,
	delStudentPsychomotorTests,
	delStudentMatters,
	delStudentMeetings
}) (MatterGroupListAdministration);
