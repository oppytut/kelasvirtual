import React, {Component} from 'react';
import {Form, Label, Select, Message} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	putUser
} from '../../../../../../action/user.js';

import {
	administrationStudentList
} from '../../../../../../action/administration.js';

const logName = 'StudentListEditForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class StudentListEditForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {},
			loading: false,
			message: {}
		};

	}
	componentWillMount() {
		log.trace('componentWillMount');

		const student = this.props.getStudentEdited();
		this.setState({data:{
			name: student.user.name,
			gender: student.user.gender
		}});
	}

	/** Form */
	validateName() {
		log.trace('validateName');
		const {name} = this.state.data;
		var error = this.state.error;

		if(!isEmpty(name)) {
			if(!/^[a-zA-Z ]+$/.test(name)
				|| name[0] === ' '
				|| name[name.length-1] === ' '
			) {
				error.name = 'Format nama yang Anda isi tidak valid!';
			} else {
				delete error['name'];
			}
		} else {
			delete error['name'];
		}

		this.setState({error: error});
	}

	validate() {
		log.trace('validate');

		this.validateName();
	}

	change(e) {
		log.trace('change');

		var data = this.state.data;
		data[e.target.name] = e.target.value;

		var error = this.state.error;
		delete error[e.target.name];

		this.setState({data, error});
	}

	selectChange(e, {name, value}) {
		log.trace('selectChange');

		var data = this.state.data;
		data[name] = value;

		var error = this.state.error;
		delete error[name];

		this.setState({data, error});
	}

	submit(e) {
		log.trace('submit');
		e.target.blur();

		if(!isEmpty(this.state.messageId)) {
			clearTimeout(this.state.messageId);
			this.setState({
				messageId: undefined,
				message: undefined
			});
		}

		this.setState({loading: true});

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			/** Save User */
			const savingUser = new Promise((resolve, reject) => {
				var student = this.props.getStudentEdited();
				var data = this.state.data;
				data._id = student.user._id;

				this.props.putUser(data).then(
					(res) => {resolve(res.user);},
					(err) => {
						err.response.json().then(({error}) => {
							this.setState({error});
							reject();
						});
					}
				);
			});
			/** End */

			Promise.all([savingUser]).then(
				(item) => {
					const message = {
						content: 'Data Anda berhasil disimpan!',
						color: 'green'
					};
					this.setState({
						loading: false,
						message: message
					});

					var messageId = setTimeout(() => {
						this.setState({message: undefined});
					}, 3000);
					this.setState({messageId: messageId});

					var user = item[0];

					/** Upadate studentList */
					var {studentList} = this.props.administration;
					var student = this.props.getStudentEdited();

					student.user.name = user.name;
					student.user.gender = user.gender;

					studentList.map((item) => {
						if(item._id === student._id)
							item = student;
					});
					this.props.administrationStudentList(studentList);
					// this.props.closeEditModal();
					/** End */
				},
				() => {
					const message = {
						content: 'Data Anda gagal disimpan!',
						color: 'red'
					};
					this.setState({
						loading: false,
						message: message
					});

					var messageId = setTimeout(() => {
						this.setState({message: undefined});
					}, 3000);
					this.setState({messageId: messageId});
				}
			);
		} else {
			this.setState({loading: false});
		}
	}
	/** End */

	render() {
		log.trace('render');

		const {loading} = this.state;

		return(
			<div>
				{!isEmpty(this.state.message) &&
					<Message size='tiny' color={this.state.message.color}>
						{this.state.message.content}
					</Message>
				}
				<Form
					id='editStudentListForm'
					size='small'
					loading={loading}
					onSubmit={loading ? undefined : this.submit.bind(this)}
				>
					<Form.Field>
						<label>Nama Lengkap</label>
						<input
							placeholder='Nama Lengkap'
							name='name'
							onChange={this.change.bind(this)}
							value={isEmpty(this.state.data.name) ? '' : this.state.data.name}
						/>
						{this.state.error.name &&
							<Label pointing>{this.state.error.name}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Jenis Kelamin</label>
						<Select
							placeholder='Jenis Kelamin'
							name='gender'
							options={[
								{key: 1, text: 'L (Laki-Laki)', value: 'L (Laki-Laki)'},
								{key: 2, text: 'P (Perempuan)', value: 'P (Perempuan)'}
							]}
							onChange={this.selectChange.bind(this)}
							value={this.state.data.gender}
						/>
						{this.state.error.gender &&
							<Label pointing>{this.state.error.gender}</Label>
						}
					</Form.Field>
				</Form>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		administration: state.administration
	};
}

StudentListEditForm.propTypes = {
	putUser: PropTypes.func.isRequired,
	administrationStudentList: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	putUser,
	administrationStudentList
}) (StudentListEditForm);
