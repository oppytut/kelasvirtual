import React, {Component} from 'react';
import {Input, Form, Button, Table, Message, Label} from 'semantic-ui-react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'is-empty';

import {
	getUsers,
	setUsers
} from '../../../../../../action/user.js';

import {
	getStudents,
	postStudent,
	putStudent
} from '../../../../../../action/student.js';

import {
	administrationStudentList
} from '../../../../../../action/administration.js';

const logName = 'AddStudentForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class AddStudentForm extends Component {
	constructor() {
		super();

		this.state = {
			searchLoading: false,
			searched: false,
			chooseLoading: {},
			choosed: false
		};
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setUsers();
	}

	/** Search Field */
	changeKeyword(e) {
		log.trace('changeKeyword');

		this.setState({keyword: e.target.value});
	}

	search() {
		log.trace('search');

		this.setState({
			searchLoading: true,
			choosed: false
		});

		const {keyword} = this.state;
		const course = this.props.course.reduce(item => item);

		this.props.getUsers({
			query: {
				$or: [
					{email: keyword},
					{username: keyword}
				]
			},
			field: 'name gender username email',
			populate: [
				{
					path: 'student',
					select: 'deleted',
					match: {course: course._id}
				},
				{
					path: 'teacher',
					select: '_id',
					match: {course: course._id}
				}
			]
		}).then((res) => {
			this.props.setUsers(res.user);
			this.setState({
				searchLoading: false,
				searched: true
			});
		});
	}
	/** End */

	/** Button Handle */
	handleSearchIcon() {
		log.trace('handleSearchIcon');

		if(!this.state.searchLoading)
			this.search();
	}

	handleSearchButton(e) {
		log.trace('handleSearchButton');
		e.target.blur();

		this.search();
	}
	/** End */

	/** Choose */
	choose(user, idx, e) {
		log.trace('choose');
		e.target.blur();

		this.changeChooseLoading(idx, true);

		const course = this.props.course.reduce(item => item);

		new Promise((resolve) => {
			this.props.getStudents({
				query: {
					user: user._id,
					course: course._id
				},
				field: '_id'
			}).then((res) => {
				var student = isEmpty(res.student) ?
					{} : res.student.reduce(item => item);

				if(isEmpty(student)) {
					student = {
						user: user._id,
						course: course._id
					};

					this.props.postStudent(student).then((res) => {
						resolve(res.student);
					});
				} else {
					student.deleted = false;

					this.props.putStudent(student).then((res) => {
						resolve(res.student);
					});
				}
			});
		}).then((student) => {
			var {studentList} = this.props.administration;

			user.student = null;
			student.user = user;
			studentList.push(student);

			this.props.administrationStudentList(studentList);
			this.changeChooseLoading(idx, false);
			this.setState({choosed: true});
		});
	}

	changeChooseLoading(idx, sts) {
		log.trace('changeChooseLoading');

		var {chooseLoading} = this.state;
		chooseLoading[idx] = sts;
		this.setState({chooseLoading: chooseLoading});
	}
	/** End */

	render() {
		log.trace('render');

		const {searchLoading, searched, chooseLoading, choosed} = this.state;
		const user = isEmpty(this.props.user) ?
			{} : this.props.user;

		return(
			<div>
				<Form
					size='small'
				>
					<Form.Field>
						<label>Cari Username atau Email</label>
						<Input
							name='search'
							loading={searchLoading}
							onChange={this.changeKeyword.bind(this)}
							icon={{
								name: 'search',
								circular: true,
								link: true,
								onClick: this.handleSearchIcon.bind(this)
							}}
							placeholder='Cari Username atau Email'
						/>
						<div style={{marginTop: '10px', width: '100%', textAlign: 'left'}}>
							<Button
								content='Cari'
								size='small'
								color='teal'
								onClick={searchLoading ? undefined : this.handleSearchButton.bind(this)}
							/>
						</div>
					</Form.Field>

					{searched &&
						<div>
							{isEmpty(user) ?
								<Form.Field>
									<Message size='tiny' style={{marginTop: '10px'}}>
										Pengguna tidak ditemukan!
									</Message>
								</Form.Field>
								:
								<Form.Field>
									<Table size='small' compact unstackable basic='very' style={{marginTop: '30px'}}>
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell>Username</Table.HeaderCell>
												<Table.HeaderCell>Email</Table.HeaderCell>
												<Table.HeaderCell></Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											{user.map((user, index) => {
												return(
													<Table.Row key={index}>
														<Table.Cell style={{fontSize: '13px'}} width={5}>
															{user.username}
														</Table.Cell>
														<Table.Cell style={{fontSize: '13px'}} width={5}>
															{user.email}
														</Table.Cell>
														<Table.Cell width={6} textAlign='right'>
															{choosed ?
																<div>
																	<Label
																		content='Terdaftar'
																		color='green'
																		icon='checkmark'
																		circular
																		size='small'
																	/>
																	{!isEmpty(user.teacher) &&
																		<Label
																			content='Pendidik'
																			color='yellow'
																			icon='checkmark'
																			circular
																			size='small'
																		/>
																	}
																</div>
																:
																isEmpty(user.student) ?
																	<Button
																		content='Pilih'
																		size='mini'
																		color='teal'
																		icon='plus'
																		basic
																		compact
																		onClick={chooseLoading[index] ? undefined : this.choose.bind(this, user, index)}
																		loading={chooseLoading[index]}
																	/>
																	:
																	user.student.reduce(item => item).deleted !== true ?
																		<div>
																			<Label
																				content='Terdaftar'
																				color='green'
																				icon='checkmark'
																				circular
																				size='small'
																			/>
																			{!isEmpty(user.teacher) &&
																				<Label
																					content='Pendidik'
																					color='yellow'
																					icon='checkmark'
																					circular
																					size='small'
																				/>
																			}
																		</div>
																		:
																		<Button
																			content='Pilih'
																			size='mini'
																			color='teal'
																			icon='plus'
																			basic
																			compact
																			onClick={chooseLoading[index] ? undefined : this.choose.bind(this, user, index)}
																			loading={chooseLoading[index]}
																		/>
															}
														</Table.Cell>
													</Table.Row>
												);
											})}
										</Table.Body>
									</Table>
								</Form.Field>
							}
						</div>
					}

				</Form>
			</div>
		);
	}
}

AddStudentForm.propTypes = {
	getUsers: PropTypes.func.isRequired,
	setUsers: PropTypes.func.isRequired,
	getStudents: PropTypes.func.isRequired,
	postStudent: PropTypes.func.isRequired,
	putStudent: PropTypes.func.isRequired,
	administrationStudentList: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		user: state.user,
		course: state.course,
		administration: state.administration
	};
}

export default connect(mapStateToProps, {
	getUsers,
	setUsers,
	getStudents,
	postStudent,
	putStudent,
	administrationStudentList
}) (AddStudentForm);
