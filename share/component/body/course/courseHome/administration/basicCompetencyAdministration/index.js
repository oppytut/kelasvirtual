import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {List, Loader, Message, Button, Table, Modal} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {Link} from 'react-router-dom';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getCoreCompetencys,
	setCoreCompetencys
} from '../../../../../../action/coreCompetency.js';

import {
	getPairCompetencysByCourse,
	setPairCompetencys,
	delPairCompetencysChild
} from '../../../../../../action/pairCompetency.js';

import BasicCompetencyForm from './basicCompetencyForm.js';

const logName = 'BasicCompetencyAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class BasicCompetencyAdministration extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			addEditLoading: false,
			deleteModalOpen: false,
			addModalOpen: false,
			editModalOpen: false
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'BasicCompetency']);
		this.props.changeMenu('basicCompetency');

		const course = this.props.course.reduce(item => item);

		const gettingCoreCompetencys = new Promise((resolve, reject) => {
			this.props.getCoreCompetencys({
				query: {course: course._id},
				field: 'domain competency'
			}).then(
				(res) => {
					this.props.setCoreCompetencys(res.coreCompetency);
					resolve();
				},
				(err) => {
					reject(err);
				});
		});

		const gettingPairCompetencys = new Promise((resolve, reject) => {
			this.props.getPairCompetencysByCourse({
				query: {
					basicCompetency: {coreCompetency: {course: {_id: course._id}}}
				},
				field: '_id',
				populate: [
					{
						path: 'cognitive',
						select: 'competency number'
					},
					{
						path: 'psychomotor',
						select: 'competency number'
					}
				]
			}).then(
				(res) => {
					this.props.setPairCompetencys(res.pairCompetency);
					resolve();
				},
				(err) => {
					reject(err);
				});
		});

		Promise.all([gettingCoreCompetencys, gettingPairCompetencys]).then(() => {
			this.setState({loading: false});
		}).catch((err) => {
			err.response.json().then(({error}) => {
				this.setState({loading: false});
				this.setState({error});
			});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setPairCompetencys();
		this.props.setCoreCompetencys();
	}

	/** Form Add and Edit **/
	setAddEditLoading(value) {
		log.trace('setAddEditLoading');

		this.setState({addEditLoading: value});
	}

	getAddEditLoading() {
		log.trace('getAddEditLoading');

		return this.state.addEditLoading;
	}

	handleSave(e) {
		log.trace('handleSave');
		e.target.blur();

	}
	/** End **/

	/** Add basicCompetency */
	addBasicCompetency(e) {
		log.trace('addBasicCompetency');
		e.target.blur();

		this.setState({addModalOpen: true});
	}

	closeAddModal() {
		log.trace('closeAddModal');

		this.setState({
			addModalOpen: false
		});
	}
	/** End */

	/** Edit basicCompetency */
	editBasicCompetency(pairCompetency, e) {
		log.trace('editBasicCompetency');
		e.target.blur();

		this.setState({
			editModalOpen: true,
			pairCompetencyEdited: pairCompetency
		});
	}

	closeEditModal() {
		log.trace('closeEditModal');

		this.setState({
			editModalOpen: false,
			pairCompetencyEdited: undefined
		});
	}

	getPairCompetencyEdited() {
		log.trace('getPairCompetencyEdited');

		return this.state.pairCompetencyEdited;
	}
	/** End */

	/** Delete basicCompetency */
	deleteBasicCompetency(pairCompetency, e) {
		log.trace('deleteBasicCompetency');
		e.target.blur();

		this.setState({
			deleteModalOpen: true,
			pairCompetencyDeleted: pairCompetency
		});
	}

	closeDeleteModal() {
		log.trace('closeDeleteModal');

		this.setState({
			deleteModalOpen: false,
			pairCompetencyDeleted: undefined
		});
	}

	handleDeleteButton(e) {
		log.trace('handleDeleteButton');
		e.target.blur();

		this.cleanMessage();

		const {pairCompetencyDeleted} = this.state;

		this.props.delPairCompetencysChild({
			query: {_id: pairCompetencyDeleted._id}
		}).then(() => {
			const {pairCompetency} = this.props;
			this.props.setPairCompetencys(pairCompetency.filter((item) => {
				return item._id !== pairCompetencyDeleted._id;
			}));

			this.setState({
				deleteModalOpen: false,
				pairCompetencyDeleted: undefined
			});
		});
	}
	/** End */

	/** Message */
	cleanMessage() {
		if(!isEmpty(this.state.messageId)) {
			clearTimeout(this.state.messageId);
			this.setState({
				messageId: undefined,
				message: undefined
			});
		}
	}

	setMessage(msg, clr) {
		log.trace('setMessage');

		const message = {
			content: msg,
			color: clr
		};
		this.setState({
			message: message
		});

		var messageId = setTimeout(() => {
			this.setState({message: undefined});
		}, 3000);
		this.setState({messageId: messageId});
	}
	/** End */

	render() {
		log.trace('render');

		const {loading} = this.state;
		const {pairCompetency, coreCompetency} = this.props;
		const course = this.props.course.reduce(item => item);

		const addModal = (
			<Modal
				size='tiny'
				open={this.state.addModalOpen}
				onClose={this.closeAddModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<BasicCompetencyForm
						mode='add'
						getAddEditLoading={this.getAddEditLoading.bind(this)}
						setAddEditLoading={this.setAddEditLoading.bind(this)}
						closeAddModal={this.closeAddModal.bind(this)}
						setMessage={this.setMessage.bind(this)}
						cleanMessage={this.cleanMessage.bind(this)}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeAddModal.bind(this)}
					>
						Tutup
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Simpan'
						type='submit'
						form='basicCompetencyForm'
						onClick={this.handleSave.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const editModal = (
			<Modal
				size='tiny'
				open={this.state.editModalOpen}
				onClose={this.closeEditModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<BasicCompetencyForm
						mode='edit'
						getPairCompetencyEdited={this.getPairCompetencyEdited.bind(this)}
						getAddEditLoading={this.getAddEditLoading.bind(this)}
						setAddEditLoading={this.setAddEditLoading.bind(this)}
						closeEditModal={this.closeEditModal.bind(this)}
						setMessage={this.setMessage.bind(this)}
						cleanMessage={this.cleanMessage.bind(this)}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeEditModal.bind(this)}
					>
						Tutup
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Simpan'
						type='submit'
						form='basicCompetencyForm'
						onClick={this.handleSave.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const deleteModal = (
			<Modal
				size='tiny'
				open={this.state.deleteModalOpen}
				onClose={this.closeDeleteModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					Anda yakin ingin menghapus kompetensi dasar tersebut?
					<Message size='small' style={{marginTop: '15px'}}>
						Data berikut yang terkait pada kompetensi dasar tersebut juga akan terhapus.
						<List bulleted style={{marginTop: '5px'}}>
							<List.Item>Indikator</List.Item>
							<List.Item>Tujuan Pembelajaran</List.Item>
							<List.Item>Materi</List.Item>
							<List.Item>Ujian</List.Item>
							<List.Item>Proses Pembelajaran</List.Item>
						</List>
					</Message>
				</Modal.Content>

				<Modal.Actions>
					<Button size='small' onClick={this.closeDeleteModal.bind(this)}>
						Batal
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Ya'
						onClick={this.handleDeleteButton.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		return(
			<div>
				{addModal}
				{editModal}
				{deleteModal}
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					<div>
						{!isEmpty(this.state.message) &&
							<Message color={this.state.message.color} size='tiny'>
								{this.state.message.content}
							</Message>
						}
						{isEmpty(coreCompetency) ?
							<Message>
								<Message.Header>
									Anda belum melengkapi kompetensi inti!
								</Message.Header>
								<p>
									Sebelum menambahkan kompetensi dasar, Anda harus melengkapi kompetensi inti terlebih dahulu.
									<br />
									<Button
										style={{marginTop: '15px'}}
										content='Atur Kompetensi Inti'
										size='tiny'
										color='teal'
										icon='checkmark'
										compact
										as={Link}
										to={'/course/'+course._id+'/administration/coreCompetency'}
									/>
								</p>
							</Message>
							:
							isEmpty(pairCompetency) ?
								<Message>
									<Message.Header>
										Belum ada kompetensi dasar!
									</Message.Header>
									<p>
										Silahkan tambahkan kompetensi dasar terlebih dahulu.
										<br />
										<Button
											style={{marginTop: '15px'}}
											content='Tambah Kompetensi Dasar'
											size='tiny'
											color='teal'
											icon='add'
											compact
											onClick={this.addBasicCompetency.bind(this)}
										/>
									</p>
								</Message>
								:
								<div>
									<Table size='small' style={{borderRadius: '0px'}} compact unstackable basic='very'>
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell style={{width: '25px'}}>No.</Table.HeaderCell>
												<Table.HeaderCell>Kompetensi</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
									</Table>
									{pairCompetency.map((pairCompetency, index) => {
										return (
											<Table size='small' style={{marginTop: '7px'}} compact unstackable basic key={index}>
												<Table.Body>
													<Table.Row>
														<Table.Cell style={{width: '25px'}}>3.{pairCompetency.cognitive.number}</Table.Cell>
														<Table.Cell><div dangerouslySetInnerHTML={{__html: pairCompetency.cognitive.competency}} /></Table.Cell>
														<Table.Cell width={3} textAlign='right' rowSpan='2'>
															<Button
																content='Edit'
																size='tiny'
																color='teal'
																icon='edit'
																basic
																compact
																onClick={this.editBasicCompetency.bind(this, pairCompetency)}
															/>
															<Button
																content='Hapus'
																size='tiny'
																color='teal'
																icon='delete'
																basic
																compact
																onClick={this.deleteBasicCompetency.bind(this, pairCompetency)}
															/>
														</Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell>4.{pairCompetency.psychomotor.number}</Table.Cell>
														<Table.Cell><div dangerouslySetInnerHTML={{__html: pairCompetency.psychomotor.competency}} /></Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
										);
									})}
									<Table size='small' style={{borderRadius: '0px'}} compact unstackable basic='very'>
										<Table.Footer>
											<Table.Row>
												<Table.HeaderCell textAlign='left'>
													<Button
														content='Tambah'
														size='tiny'
														color='teal'
														icon='add'
														compact
														onClick={this.addBasicCompetency.bind(this)}
													/>
												</Table.HeaderCell>
											</Table.Row>
										</Table.Footer>
									</Table>
								</div>
						}
					</div>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		coreCompetency: state.coreCompetency,
		pairCompetency: state.pairCompetency
	};
}

BasicCompetencyAdministration.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getCoreCompetencys: PropTypes.func.isRequired,
	setCoreCompetencys: PropTypes.func.isRequired,
	getPairCompetencysByCourse: PropTypes.func.isRequired,
	setPairCompetencys: PropTypes.func.isRequired,
	delPairCompetencysChild: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getCoreCompetencys,
	setCoreCompetencys,
	getPairCompetencysByCourse,
	setPairCompetencys,
	delPairCompetencysChild
}) (BasicCompetencyAdministration);
