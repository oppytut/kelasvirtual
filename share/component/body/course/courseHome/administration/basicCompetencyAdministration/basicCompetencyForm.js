import React, {Component} from 'react';
import {Input, Form, Label, Divider, TextArea, Header} from 'semantic-ui-react';
import debug from 'debug';
import validator from 'validator';
import isEmpty from 'is-empty';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	postPairedBasicCompetency,
	setPairCompetencys
} from '../../../../../../action/pairCompetency.js';

import {
	putBasicCompetency
} from '../../../../../../action/basicCompetency.js';

const logName = 'BasicCompetencyForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class BasicCompetencyForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		const {mode} = this.props;

		if(mode === 'add') {
			this.props.setAddEditLoading(false);
		} else if(mode === 'edit') {
			const pairCompetencyEdited = this.props.getPairCompetencyEdited();
			var {data} = this.state;

			data.number = pairCompetencyEdited.cognitive.number;
			data.cognitive = pairCompetencyEdited.cognitive.competency;
			data.psychomotor = pairCompetencyEdited.psychomotor.competency;

			this.setState({data: data});

			this.props.setAddEditLoading(false);
		}
	}

	/** Validation */
	validateNumber() {
		log.trace('validateNumber');
		const {number} = this.state.data;
		var {error} = this.state;

		if(isEmpty(number)) {
			error.number = 'Anda belum mengisi nomor kompetensi!';
		} else {
			if(!validator.isNumeric(number.toString())) {
				error.number = 'Format nomor kompetensi yang Anda isi tidak valid!';
			} else {
				delete error['number'];
			}
		}

		this.setState({error: error});
	}

	validateCognitive() {
		log.trace('validateCognitive');
		const {cognitive} = this.state.data;
		var {error} = this.state;

		if(isEmpty(cognitive)) {
			error.cognitive = 'Anda belum mengisi kompetensi pengetahuan!';
		} else {
			delete error['cognitive'];
		}

		this.setState({error: error});
	}

	validatePsychomotor() {
		log.trace('validatePsychomotor');
		const {psychomotor} = this.state.data;
		var {error} = this.state;

		if(isEmpty(psychomotor)) {
			error.psychomotor = 'Anda belum mengisi kompetensi keterampilan!';
		} else {
			delete error['psychomotor'];
		}

		this.setState({error: error});
	}

	validate() {
		log.trace('validate');

		this.validateNumber();
		this.validateCognitive();
		this.validatePsychomotor();
	}
	/** End */

	/** Form */
	changeNumber(e) {
		log.trace('changeNumber');

		var {data} = this.state;
		data.number = e.target.value;
		this.setState({data: data});
	}

	changeCognitive(e) {
		log.trace('changeCognitive');

		var {data} = this.state;
		data.cognitive = e.target.value;
		this.setState({data: data});
	}

	changePsychomotor(e) {
		log.trace('changePsychomotor');

		var {data} = this.state;
		data.psychomotor = e.target.value;
		this.setState({data: data});
	}

	submit(e) {
		log.trace('submit');
		e.target.blur();

		const {mode} = this.props;
		if(mode === 'edit') {
			this.editSubmit();
		} else {
			this.addSubmit();
		}
	}

	editSubmit() {
		log.trace('editSubmit');

		this.props.cleanMessage();
		this.props.setAddEditLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {data} = this.state;
			const {coreCompetency} = this.props;
			const pairCompetencyEdited = this.props.getPairCompetencyEdited();
			var cognitiveCoreCompetency = {};
			var psychomotorCoreCompetency = {};

			coreCompetency.map((item) => {
				if(item.domain === 'cognitive')
					cognitiveCoreCompetency = item;

				if(item.domain === 'psychomotor')
					psychomotorCoreCompetency = item;
			});

			const updated = {
				cognitive: {
					_id: pairCompetencyEdited.cognitive._id,
					coreCompetency: cognitiveCoreCompetency._id,
					competency: data.cognitive,
					number: data.number
				},
				psychomotor: {
					_id: pairCompetencyEdited.psychomotor._id,
					coreCompetency: psychomotorCoreCompetency._id,
					competency: data.psychomotor,
					number: data.number
				}
			};

			const puttingCognitive = new Promise((resolve, reject) => {
				this.props.putBasicCompetency(
					updated.cognitive
				).then(
					(res) => {
						resolve(res.basicCompetency);
					},
					(err) => {
						reject(err);
					}
				);
			});

			const puttingPsychomotor = new Promise((resolve, reject) => {
				this.props.putBasicCompetency(updated.psychomotor
				).then(
					(res) => {
						resolve(res.basicCompetency);
					},
					(err) => {
						reject(err);
					}
				);
			});

			Promise.all([
				puttingCognitive,
				puttingPsychomotor
			]).then(([cognitive, psychomotor]) => {
				var pairCompetency = this.props.getPairCompetencyEdited();
				pairCompetency.map((item) => {
					if(item._id === pairCompetencyEdited._id) {
						item.psychomotor = psychomotor;
						item.cognitive = cognitive;
					}
				});
				this.props.setPairCompetencys(pairCompetency);

				this.props.setMessage('Kompetensi berhasil diperbaharui!', 'green');
				this.props.setAddEditLoading(false);
				this.props.closeEditModal();
			}).catch((err) => {
				err.response.json().then(({error}) => {
					this.props.setAddEditLoading(false);
					this.setState({error});
				});
			});
			/** End */
		} else {
			this.props.setAddEditLoading(false);
		}
	}

	addSubmit() {
		log.trace('addSubmit');

		this.props.cleanMessage();
		this.props.setAddEditLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {data} = this.state;
			const {coreCompetency} = this.props;
			var cognitiveCoreCompetency = {};
			var psychomotorCoreCompetency = {};

			coreCompetency.map((item) => {
				if(item.domain === 'cognitive')
					cognitiveCoreCompetency = item;

				if(item.domain === 'psychomotor')
					psychomotorCoreCompetency = item;
			});

			const paired = {
				cognitive: {
					coreCompetency: cognitiveCoreCompetency._id,
					competency: data.cognitive,
					number: data.number
				},
				psychomotor: {
					coreCompetency: psychomotorCoreCompetency._id,
					competency: data.psychomotor,
					number: data.number
				}
			};

			this.props.postPairedBasicCompetency(paired).then(
				(res) => {
					var {pairCompetency} = this.props;
					pairCompetency.push(res.pairCompetency.reduce(item => item));
					this.props.setPairCompetencys(pairCompetency);

					this.props.setMessage('Kompetensi berhasil ditambahkan!', 'green');
					this.props.setAddEditLoading(false);
					this.props.closeAddModal();
				},
				(err) => {
					err.response.json().then(({error}) => {
						this.props.setAddEditLoading(false);
						this.setState({error});
					});
				}
			);
			/** End */
		} else {
			this.props.setAddEditLoading(false);
		}
	}
	/** End */

	render() {
		log.trace('render');

		var loading = this.props.getAddEditLoading();

		return(
			<div>
				<Form
					size='small'
					id='basicCompetencyForm'
					onSubmit={this.submit.bind(this)}
					loading={loading}
				>
					<Form.Field>
						<Header as='h4'>Ranah Pengetahuan</Header>
					</Form.Field>
					<Form.Field>
						<label>Nomor</label>
						<Input
							label='3.'
							placeholder='Nomor Kompetensi'
							onChange={this.changeNumber.bind(this)}
							value={isEmpty(this.state.data.number) ?
								'' : this.state.data.number}
						/>
						{this.state.error.number &&
							<Label pointing>{this.state.error.number}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Kompetensi</label>
						<TextArea
							placeholder='Kompetensi Dasar Ranah Pengetahuan'
							autoHeight
							spellCheck='false'
							onChange={this.changeCognitive.bind(this)}
							value={this.state.data.cognitive}
						/>
						{this.state.error.cognitive &&
							<Label pointing>{this.state.error.cognitive}</Label>
						}
					</Form.Field>
					<Form.Field>
						<Divider fitted />
					</Form.Field>
					<Form.Field>
						<Header as='h4'>Ranah Keterampilan</Header>
					</Form.Field>
					<Form.Field>
						<label>Nomor</label>
						<Input
							label='4.'
							placeholder='Nomor Kompetensi'
							onChange={this.changeNumber.bind(this)}
							value={isEmpty(this.state.data.number) ?
								'' : this.state.data.number}
						/>
						{this.state.error.number &&
							<Label pointing>{this.state.error.number}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Kompetensi</label>
						<TextArea
							placeholder='Kompetensi Dasar Ranah Keterampilan'
							autoHeight
							spellCheck='false'
							onChange={this.changePsychomotor.bind(this)}
							value={this.state.data.psychomotor}
						/>
						{this.state.error.psychomotor &&
							<Label pointing>{this.state.error.psychomotor}</Label>
						}
					</Form.Field>
				</Form>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		coreCompetency: state.coreCompetency,
		pairCompetency: state.pairCompetency
	};
}

BasicCompetencyForm.propTypes = {
	postPairedBasicCompetency: PropTypes.func.isRequired,
	putBasicCompetency: PropTypes.func.isRequired,
	setPairCompetencys: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	postPairedBasicCompetency,
	putBasicCompetency,
	setPairCompetencys
}) (BasicCompetencyForm);
