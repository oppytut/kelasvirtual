import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Label, Loader, Message, Button, Table, Modal, Grid, Segment} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {Link} from 'react-router-dom';

import ObjectiveForm from './objectiveForm.js';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getPairCompetencysByCourse,
	setPairCompetencys
} from '../../../../../../action/pairCompetency.js';

import {
	delObjective
} from '../../../../../../action/objective.js';

const logName = 'ObjectiveAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class ObjectiveAdministration extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			addEditLoading: false,
			deleteModalOpen: false,
			addModalOpen: false,
			editModalOpen: false
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Objective']);
		this.props.changeMenu('objective');

		const course = this.props.course.reduce(item => item);

		this.props.getPairCompetencysByCourse({
			query: {
				basicCompetency: {coreCompetency: {course: {_id: course._id}}}
			},
			field: '_id',
			populate: [
				{
					path: 'cognitive',
					select: 'competency number',
					populate: [
						{
							path: 'objective',
							select: 'number content'
						}
					]
				},
				{
					path: 'psychomotor',
					select: 'competency number',
					populate: [
						{
							path: 'objective',
							select: 'number content'
						}
					]
				}
			]
		}).then(
			(res) => {
				const pairCompetency = res.pairCompetency;
				this.props.setPairCompetencys(pairCompetency);

				this.updateList();

				this.setState({
					loading: false
				});
			},
			(err) => {
				err.response.json().then(({error}) => {
					this.setState({loading: false});
					this.setState({error});
				});
			});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setPairCompetencys();
	}

	/** List */
	updateList() {
		log.trace('updateList');

		const {pairCompetency} = this.props;

		const count = 5;
		var listCog = [];
		var listPsy = [];

		pairCompetency.map((item, index) => {
			listCog[index] = {
				count: count,
				page: 1,
				countPage: Math.ceil(Object.keys(item.cognitive.objective).length/count)
			};

			listPsy[index] = {
				count: count,
				page: 1,
				countPage: Math.ceil(Object.keys(item.psychomotor.objective).length/count)
			};
		});

		this.setState({
			listCog: listCog,
			listPsy: listPsy
		});
	}
	/** End */

	/** Form Add and Edit **/
	setAddEditLoading(value) {
		log.trace('setAddEditLoading');

		this.setState({addEditLoading: value});
	}

	getAddEditLoading() {
		log.trace('getAddEditLoading');

		return this.state.addEditLoading;
	}

	getDomainUpdated() {
		log.trace('getDomainUpdated');

		return this.state.domainUpdated;
	}

	getBasicCompetencyUpdated() {
		log.trace('getBasicCompetencyUpdated');

		return this.state.basicCompetencyUpdated;
	}

	getObjectiveUpdated() {
		log.trace('getObjectiveUpdated');

		return this.state.objectiveUpdated;
	}

	handleSave(e) {
		log.trace('handleSave');
		e.target.blur();

	}
	/** End **/

	/** Add Objective */
	addObjective(domain, basicCompetency, e) {
		log.trace('addObjective');
		e.target.blur();

		this.setState({
			addModalOpen: true,
			basicCompetencyUpdated: basicCompetency,
			domainUpdated: domain
		});
	}

	closeAddModal() {
		log.trace('closeAddModal');

		this.setState({
			addModalOpen: false,
			basicCompetencyUpdated: undefined,
			domainUpdated: undefined
		});
	}
	/** End */

	/** Edit Objective */
	editObjective(domain, basicCompetency, objective, e) {
		log.trace('editObjective');
		e.target.blur();

		this.setState({
			editModalOpen: true,
			basicCompetencyUpdated: basicCompetency,
			objectiveUpdated: objective,
			domainUpdated: domain
		});
	}

	closeEditModal() {
		log.trace('closeEditModal');

		this.setState({
			editModalOpen: false,
			basicCompetencyUpdated: undefined,
			objectiveUpdated: undefined,
			domainUpdated: undefined
		});
	}
	/** End */

	/** Pagination */
	nextPage(idx, domain, e) {
		log.trace('nextPage');
		e.target.blur();

		var list = domain === 'cognitive' ?
			this.state.listCog : this.state.listPsy;

		if(list[idx].page < list[idx].countPage) {
			list[idx].page = list[idx].page+1;

			if(domain === 'cognitive') {
				this.setState({listCog: list});
			} else {
				this.setState({listPsy: list});
			}
		}
	}

	prevPage(idx, domain, e) {
		log.trace('prevPage');
		e.target.blur();

		var list = domain === 'cognitive' ?
			this.state.listCog : this.state.listPsy;

		if(list[idx].page > 1) {
			list[idx].page = list[idx].page-1;

			if(domain === 'cognitive') {
				this.setState({listCog: list});
			} else {
				this.setState({listPsy: list});
			}
		}
	}

	goPage(idx, page, domain, e) {
		log.trace('goPage');
		e.target.blur();

		var list = domain === 'cognitive' ?
			this.state.listCog : this.state.listPsy;

		list[idx].page = page;

		if(domain === 'cognitive') {
			this.setState({listCog: list});
		} else {
			this.setState({listPsy: list});
		}
	}
	/** End */

	/** Message */
	cleanMessage() {
		if(!isEmpty(this.state.messageId)) {
			clearTimeout(this.state.messageId);
			this.setState({
				messageId: undefined,
				message: undefined
			});
		}
	}

	setMessage(msg, clr) {
		log.trace('setMessage');

		const message = {
			content: msg,
			color: clr
		};
		this.setState({
			message: message
		});

		var messageId = setTimeout(() => {
			this.setState({message: undefined});
		}, 3000);
		this.setState({messageId: messageId});
	}
	/** End */

	/** Delete Objective */
	deleteObjective(domain, basicCompetency, objective, e) {
		log.trace('deleteObjective');
		e.target.blur();

		this.setState({
			deleteModalOpen: true,
			basicCompetencyDeleted: basicCompetency,
			objectiveDeleted: objective,
			domainDeleted: domain
		});
	}

	closeDeleteModal() {
		log.trace('closeDeleteModal');

		this.setState({
			deleteModalOpen: false,
			objective: undefined,
			domainDeleted: undefined
		});
	}

	handleDeleteButton(e) {
		log.trace('handleDeleteButton');
		e.target.blur();

		this.cleanMessage();

		const {objectiveDeleted} = this.state;

		this.props.delObjective(objectiveDeleted._id).then(() => {
			const {domainDeleted, basicCompetencyDeleted} = this.state;
			const {pairCompetency} = this.props;

			pairCompetency.map((item) => {
				if(domainDeleted === 'cognitive') {
					if(item.cognitive._id === basicCompetencyDeleted._id) {
						item.cognitive.objective.map((deletedObjective, index) => {
							if(deletedObjective._id === objectiveDeleted._id) {
								item.cognitive.objective.splice(index, 1);
							}
						});
					}
				} else if(domainDeleted === 'psychomotor') {
					if(item.psychomotor._id === basicCompetencyDeleted._id) {
						item.psychomotor.objective.map((deletedObjective, index) => {
							if(deletedObjective._id === objectiveDeleted._id) {
								item.psychomotor.objective.splice(index, 1);
							}
						});
					}
				}
			});
			this.props.setPairCompetencys(pairCompetency);
			this.updateList();

			this.setState({
				deleteModalOpen: false,
				basicCompetencyDeleted: undefined,
				objectiveDeleted: undefined,
				domainDeleted: undefined
			});
		});
	}
	/** End */

	render() {
		log.trace('render');

		const {loading} = this.state;
		const {course, pairCompetency} = this.props;

		const listCog = isEmpty(this.state.listCog) ?
			{} : this.state.listCog;
		const listPsy = isEmpty(this.state.listPsy) ?
			{} : this.state.listPsy;

		/** PageButtion Component */
		const pageButtonCog = (idx) => {
			var pageButton = [];
			for(var i = 1; i <= listCog[idx].countPage; i++) {
				pageButton.push(<Button
					key={i}
					content={i}
					active={i == listCog[idx].page ? true : undefined}
					onClick={this.goPage.bind(this, idx, i)}
				/>);
			}

			return pageButton;
		};

		const pageButtonPsy = (idx) => {
			var pageButton = [];
			for(var i = 1; i <= listPsy[idx].countPage; i++) {
				pageButton.push(<Button
					key={i}
					content={i}
					active={i == listPsy[idx].page ? true : undefined}
					onClick={this.goPage.bind(this, idx, i)}
				/>);
			}

			return pageButton;
		};
		/** End */

		const addModal = (
			<Modal
				size='tiny'
				open={this.state.addModalOpen}
				onClose={this.closeAddModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<ObjectiveForm
						mode='add'
						getBasicCompetencyUpdated={this.getBasicCompetencyUpdated.bind(this)}
						getDomainUpdated={this.getDomainUpdated.bind(this)}
						getAddEditLoading={this.getAddEditLoading.bind(this)}
						setAddEditLoading={this.setAddEditLoading.bind(this)}
						closeAddModal={this.closeAddModal.bind(this)}
						setMessage={this.setMessage.bind(this)}
						cleanMessage={this.cleanMessage.bind(this)}
						updateList={this.updateList.bind(this)}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeAddModal.bind(this)}
					>
						Tutup
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Simpan'
						type='submit'
						form='objectiveForm'
						onClick={this.handleSave.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const editModal = (
			<Modal
				size='tiny'
				open={this.state.editModalOpen}
				onClose={this.closeEditModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<ObjectiveForm
						mode='edit'
						getBasicCompetencyUpdated={this.getBasicCompetencyUpdated.bind(this)}
						getObjectiveUpdated={this.getObjectiveUpdated.bind(this)}
						getDomainUpdated={this.getDomainUpdated.bind(this)}
						getAddEditLoading={this.getAddEditLoading.bind(this)}
						setAddEditLoading={this.setAddEditLoading.bind(this)}
						closeEditModal={this.closeEditModal.bind(this)}
						setMessage={this.setMessage.bind(this)}
						cleanMessage={this.cleanMessage.bind(this)}
						updateList={this.updateList.bind(this)}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeEditModal.bind(this)}
					>
						Tutup
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Simpan'
						type='submit'
						form='objectiveForm'
						onClick={this.handleSave.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const deleteModal = (
			<Modal
				size='tiny'
				open={this.state.deleteModalOpen}
				onClose={this.closeDeleteModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					Anda yakin ingin menghapus tujuan pembelajaran tersebut?
				</Modal.Content>

				<Modal.Actions>
					<Button size='small' onClick={this.closeDeleteModal.bind(this)}>
						Batal
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Ya'
						onClick={this.handleDeleteButton.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		return(
			<div>
				{addModal}
				{editModal}
				{deleteModal}
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					<div>
						{!isEmpty(this.state.message) &&
							<Message color={this.state.message.color} size='tiny'>
								{this.state.message.content}
							</Message>
						}
						{isEmpty(pairCompetency) ?
							<Message>
								<Message.Header>
									Anda belum melengkapi kompetensi dasar!
								</Message.Header>
								<p>
									Sebelum menambahkan tujuan pembelajaran, Anda harus melengkapi kompetensi dasar terlebih dahulu.
									<br />
									<Button
										style={{marginTop: '15px'}}
										content='Atur Kompetensi Dasar'
										size='tiny'
										color='teal'
										icon='checkmark'
										compact
										as={Link}
										to={'/course/'+course._id+'/administration/basicCompetency'}
									/>
								</p>
							</Message>
							:
							<Grid>
								{pairCompetency.map((item, index) => {
									return(
										<Grid.Row key={index}>
											<Grid.Column>
												<Segment style={{
													borderRadius: '0px',
													boxShadow: 'none'
												}}>
													<Grid>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																<div style={{fontSize: '12px'}}>
																	Kompetensi Dasar
																</div>
															</Grid.Column>
														</Grid.Row>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																<Label ribbon>
																	<div style={{display: 'inline-block'}}>3.{item.cognitive.number}</div>
																	<div style={{marginLeft: '10px', display: 'inline-block'}}>{item.cognitive.competency}</div>
																</Label>
															</Grid.Column>
														</Grid.Row>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																{isEmpty(item.cognitive.objective) ?
																	<Message size='tiny'>
																		<p>
																			Belum ada tujuan pembelajaran untuk kompetensi ini.
																			<br />
																			<Button
																				style={{marginTop: '15px'}}
																				content='Tambah Tujuan Pembelajaran'
																				size='tiny'
																				color='teal'
																				icon='add'
																				compact
																				onClick={this.addObjective.bind(this, 'cognitive', item.cognitive)}
																			/>
																		</p>
																	</Message>
																	:
																	<Table size='small' compact unstackable basic style={{marginBottom: '20px'}}>
																		<Table.Body>
																			{item.cognitive.objective.map((objective, idxObjective) => {
																				if(
																					(listCog[index].count*(listCog[index].page-1))+1 <= idxObjective+1
																					&& idxObjective+1 <= listCog[index].count*listCog[index].page
																				)
																					return(
																						<Table.Row key={idxObjective}>
																							<Table.Cell style={{width: '50px'}}>{idxObjective+1}</Table.Cell>
																							<Table.Cell colSpan='2'><div dangerouslySetInnerHTML={{__html: objective.content}} /></Table.Cell>
																							<Table.Cell width={3} textAlign='right'>
																								<Button
																									content='Edit'
																									size='tiny'
																									color='teal'
																									icon='edit'
																									basic
																									compact
																									onClick={this.editObjective.bind(this, 'cognitive', item.cognitive, objective)}
																								/>
																								<Button
																									content='Hapus'
																									size='tiny'
																									color='teal'
																									icon='delete'
																									basic
																									compact
																									onClick={this.deleteObjective.bind(this, 'cognitive', item.cognitive, objective)}
																								/>
																							</Table.Cell>
																						</Table.Row>
																					);
																			})}
																		</Table.Body>
																		<Table.Footer>
																			<Table.Row>
																				<Table.HeaderCell colSpan='3' textAlign='left'>
																					<Button
																						content='Tambah'
																						size='tiny'
																						color='teal'
																						icon='add'
																						compact
																						onClick={this.addObjective.bind(this, 'cognitive', item.cognitive)}
																					/>
																				</Table.HeaderCell>
																				<Table.HeaderCell colSpan='3' textAlign='right'>
																					<Button.Group size='mini' color='teal' compact>
																						<Button
																							icon='left chevron'
																							onClick={this.prevPage.bind(this, index, 'cognitive')}
																							disabled={listCog.page > 1 ? undefined : true}
																						/>
																						{pageButtonCog(index)}
																						<Button
																							icon='right chevron'
																							onClick={this.nextPage.bind(this, index, 'cognitive')}
																							disabled={listCog.page < listCog.countPage ? undefined : true}
																						/>
																					</Button.Group>
																				</Table.HeaderCell>
																			</Table.Row>
																		</Table.Footer>
																	</Table>
																}
															</Grid.Column>
														</Grid.Row>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																<Label ribbon>
																	<div style={{display: 'inline-block'}}>4.{item.psychomotor.number}</div>
																	<div style={{marginLeft: '10px', display: 'inline-block'}}>{item.psychomotor.competency}</div>
																</Label>
															</Grid.Column>
														</Grid.Row>
														<Grid.Row>
															<Grid.Column>
																{isEmpty(item.psychomotor.objective) ?
																	<Message size='tiny'>
																		<p>
																			Belum ada tujuan pembelajaran untuk kompetensi ini.
																			<br />
																			<Button
																				style={{marginTop: '15px'}}
																				content='Tambah Tujuan Pembelajaran'
																				size='tiny'
																				color='teal'
																				icon='add'
																				compact
																				onClick={this.addObjective.bind(this, 'psychomotor', item.psychomotor)}
																			/>
																		</p>
																	</Message>
																	:
																	<Table size='small' compact unstackable basic>
																		<Table.Body>
																			{item.psychomotor.objective.map((objective, idxObjective) => {
																				if(
																					(listPsy[index].count*(listPsy[index].page-1))+1 <= idxObjective+1
																					&& idxObjective+1 <= listPsy[index].count*listPsy[index].page
																				)
																					return(
																						<Table.Row key={idxObjective}>
																							<Table.Cell style={{width: '50px'}}>{idxObjective+1}</Table.Cell>
																							<Table.Cell colSpan='2'><div dangerouslySetInnerHTML={{__html: objective.content}} /></Table.Cell>
																							<Table.Cell width={3} textAlign='right'>
																								<Button
																									content='Edit'
																									size='tiny'
																									color='teal'
																									icon='edit'
																									basic
																									compact
																									onClick={this.editObjective.bind(this, 'psychomotor', item.psychomotor, objective)}
																								/>
																								<Button
																									content='Hapus'
																									size='tiny'
																									color='teal'
																									icon='delete'
																									basic
																									compact
																									onClick={this.deleteObjective.bind(this, 'psychomotor', item.psychomotor, objective)}
																								/>
																							</Table.Cell>
																						</Table.Row>
																					);
																			})}
																		</Table.Body>
																		<Table.Footer>
																			<Table.Row>
																				<Table.HeaderCell colSpan='3' textAlign='left'>
																					<Button
																						content='Tambah'
																						size='tiny'
																						color='teal'
																						icon='add'
																						compact
																						onClick={this.addObjective.bind(this, 'psychomotor', item.psychomotor)}
																					/>
																				</Table.HeaderCell>
																				<Table.HeaderCell colSpan='3' textAlign='right'>
																					<Button.Group size='mini' color='teal' compact>
																						<Button
																							icon='left chevron'
																							onClick={this.prevPage.bind(this, index, 'psychomotor')}
																							disabled={listPsy.page > 1 ? undefined : true}
																						/>
																						{pageButtonPsy(index)}
																						<Button
																							icon='right chevron'
																							onClick={this.nextPage.bind(this, index, 'psychomotor')}
																							disabled={listPsy.page < listPsy.countPage ? undefined : true}
																						/>
																					</Button.Group>
																				</Table.HeaderCell>
																			</Table.Row>
																		</Table.Footer>
																	</Table>
																}
															</Grid.Column>
														</Grid.Row>
													</Grid>
												</Segment>
											</Grid.Column>
										</Grid.Row>
									);
								})}
							</Grid>
						}
					</div>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		pairCompetency: state.pairCompetency
	};
}

ObjectiveAdministration.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getPairCompetencysByCourse: PropTypes.func.isRequired,
	setPairCompetencys: PropTypes.func.isRequired,
	delObjective: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getPairCompetencysByCourse,
	setPairCompetencys,
	delObjective
}) (ObjectiveAdministration);
