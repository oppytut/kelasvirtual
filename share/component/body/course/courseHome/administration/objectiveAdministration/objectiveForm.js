import React, {Component} from 'react';
import {Input, Form, Label, TextArea} from 'semantic-ui-react';
import debug from 'debug';
import validator from 'validator';
import isEmpty from 'is-empty';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	postObjective,
	putObjective
} from '../../../../../../action/objective.js';

import {
	setPairCompetencys
} from '../../../../../../action/pairCompetency.js';

const logName = 'ObjectiveForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class ObjectiveForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		const {mode} = this.props;

		if(mode === 'add') {
			this.props.setAddEditLoading(false);
		} else if(mode === 'edit') {
			const objectiveUpdated = this.props.getObjectiveUpdated();
			var {data} = this.state;

			data.number = objectiveUpdated.number;
			data.objective = objectiveUpdated.content;

			this.setState({data: data});

			this.props.setAddEditLoading(false);
		}
	}

	/** Validation */
	validateNumber() {
		log.trace('validateNumber');
		const {number} = this.state.data;
		var {error} = this.state;

		if(isEmpty(number)) {
			error.number = 'Anda belum mengisi nomor tujuan pembelajaran!';
		} else {
			if(!validator.isNumeric(number.toString())) {
				error.number = 'Format nomor tujuan pembelajaran yang Anda isi tidak valid!';
			} else {
				delete error['number'];
			}
		}

		this.setState({error: error});
	}

	validateObjective() {
		log.trace('validateObjective');
		const {objective} = this.state.data;
		var {error} = this.state;

		if(isEmpty(objective)) {
			error.objective = 'Anda belum mengisi tujuan pembelajaran!';
		} else {
			delete error['objective'];
		}

		this.setState({error: error});
	}

	validate() {
		log.trace('validate');

		this.validateNumber();
		this.validateObjective();
	}
	/** End */

	/** Form */
	changeNumber(e) {
		log.trace('changeNumber');

		var {data} = this.state;
		data.number = e.target.value;
		this.setState({data: data});
	}

	changeObjective(e) {
		log.trace('changeObjective');

		var {data} = this.state;
		data.objective = e.target.value;
		this.setState({data: data});
	}

	submit(e) {
		log.trace('submit');
		e.target.blur();

		const {mode} = this.props;
		if(mode === 'edit') {
			this.editSubmit();
		} else {
			this.addSubmit();
		}
	}

	editSubmit() {
		log.trace('editSubmit');

		this.props.cleanMessage();
		this.props.setAddEditLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {data} = this.state;
			const objectiveUpdated = this.props.getObjectiveUpdated();

			const objective = {
				_id: objectiveUpdated._id,
				number: data.number,
				content: data.objective
			};

			this.props.putObjective(objective).then(
				(res) => {
					const domainUpdated = this.props.getDomainUpdated();
					const basicCompetencyUpdated = this.props.getBasicCompetencyUpdated();
					var {pairCompetency} = this.props;

					pairCompetency.map((item) => {
						if(domainUpdated === 'cognitive') {
							if(item.cognitive._id === basicCompetencyUpdated._id) {
								item.cognitive.objective.map((updatedObjective, index) => {
									if(updatedObjective._id === objective._id) {
										item.cognitive.objective[index] = res.objective;
									}
								});
							}
						} else if(domainUpdated === 'psychomotor') {
							if(item.psychomotor._id === basicCompetencyUpdated._id) {
								item.psychomotor.objective.map((updatedObjective, index) => {
									if(updatedObjective._id === objective._id) {
										item.psychomotor.objective[index] = res.objective;
									}
								});
							}
						}
					});
					this.props.setPairCompetencys(pairCompetency);

					this.props.setMessage('Tujuan pembelajaran berhasil diperbaharui!', 'green');
					this.props.setAddEditLoading(false);
					this.props.closeEditModal();
				},
				(err) => {
					err.response.json().then(({error}) => {
						this.props.setAddEditLoading(false);
						this.setState({error});
					});
				}
			);
			/** End */
		} else {
			this.props.setAddEditLoading(false);
		}
	}

	addSubmit() {
		log.trace('addSubmit');

		this.props.cleanMessage();
		this.props.setAddEditLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {data} = this.state;
			const basicCompetencyUpdated = this.props.getBasicCompetencyUpdated();

			const objective = {
				number: data.number,
				content: data.objective,
				basicCompetency: basicCompetencyUpdated._id
			};

			this.props.postObjective(objective).then(
				(res) => {
					const domainUpdated = this.props.getDomainUpdated();
					var {pairCompetency} = this.props;

					pairCompetency.map((item) => {
						if(domainUpdated === 'cognitive') {
							if(item.cognitive._id === basicCompetencyUpdated._id) {
								item.cognitive.objective.push(res.objective);
							}
						} else if(domainUpdated === 'psychomotor') {
							if(item.psychomotor._id === basicCompetencyUpdated._id) {
								item.psychomotor.objective.push(res.objective);
							}
						}
					});
					this.props.setPairCompetencys(pairCompetency);
					this.props.updateList();

					this.props.setMessage('Tujuan pembelajaran berhasil ditambahkan!', 'green');
					this.props.setAddEditLoading(false);
					this.props.closeAddModal();
				},
				(err) => {
					err.response.json().then(({error}) => {
						this.props.setAddEditLoading(false);
						this.setState({error});
					});
				}
			);
			/** End */
		} else {
			this.props.setAddEditLoading(false);
		}
	}
	/** End */

	render() {
		log.trace('render');

		var loading = this.props.getAddEditLoading();

		return(
			<div>
				<Form
					size='small'
					id='objectiveForm'
					onSubmit={this.submit.bind(this)}
					loading={loading}
				>
					<Form.Field>
						<label>Nomor</label>
						<Input
							placeholder='Nomor Tujuan Pembelajaran'
							onChange={this.changeNumber.bind(this)}
							value={isEmpty(this.state.data.number) ?
								'' : this.state.data.number}
						/>
						{this.state.error.number &&
							<Label pointing>{this.state.error.number}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Tujuan Pembelajaran</label>
						<TextArea
							placeholder='Tujuan Pembelajaran'
							autoHeight
							spellCheck='false'
							onChange={this.changeObjective.bind(this)}
							value={this.state.data.objective}
						/>
						{this.state.error.objective &&
							<Label pointing>{this.state.error.objective}</Label>
						}
					</Form.Field>
				</Form>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		pairCompetency: state.pairCompetency
	};
}

ObjectiveForm.propTypes = {
	postObjective: PropTypes.func.isRequired,
	putObjective: PropTypes.func.isRequired,
	setPairCompetencys: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	postObjective,
	putObjective,
	setPairCompetencys
}) (ObjectiveForm);
