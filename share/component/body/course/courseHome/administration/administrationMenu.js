import React, {Component} from 'react';
import {Menu} from 'semantic-ui-react';
import debug from 'debug';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

const logName = 'AdministrationMenu';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class AdministrationMenu extends Component {
	render() {
		log.trace('render');

		const activeMenu = this.props.getActiveMenu();
		const course = this.props.course.reduce(item => item);

		return(
			<div>
				<Menu
					fluid
					vertical
					size='tiny'
					style={{boxShadow: 'none', borderRadius: '0px'}}
				>
					<Menu.Item
						icon='area chart'
						content='Statistik'
						name='statistic'
						active={activeMenu === 'statistic'}
						as={Link}
						to={'/course/'+course._id+'/administration/statistic'}
					/>
					<Menu.Item
						icon='users'
						content='Peserta Didik'
						name='student'
						active={activeMenu === 'student'}
						as={Link}
						to={'/course/'+course._id+'/administration/student'}
					/>
					<Menu.Item
						icon='send'
						content='Permintaan'
						name='request'
						active={activeMenu === 'request'}
						as={Link}
						to={'/course/'+course._id+'/administration/request'}
					/>
					{/*
					<Menu.Item
						icon='user'
						content='Pendidik'
						name='teacher'
						active={activeMenu === 'teacher'}
						as={Link}
						to={'/course/'+course._id+'/administration/teacher'}
					/>
					*/}
					<Menu.Item
						icon='ordered list'
						content='Kompetensi Inti'
						name='coreCompetency'
						active={activeMenu === 'coreCompetency'}
						as={Link}
						to={'/course/'+course._id+'/administration/coreCompetency'}
					/>
					<Menu.Item
						icon='ordered list'
						content='Kompetensi Dasar'
						name='basicCompetency'
						active={activeMenu === 'basicCompetency'}
						as={Link}
						to={'/course/'+course._id+'/administration/basicCompetency'}
					/>
					<Menu.Item
						icon='ordered list'
						content='IPK'
						name='indicator'
						active={activeMenu === 'indicator'}
						as={Link}
						to={'/course/'+course._id+'/administration/indicator'}
					/>
					<Menu.Item
						icon='ordered list'
						content='Tujuan Pembelajaran'
						name='objective'
						active={activeMenu === 'objective'}
						as={Link}
						to={'/course/'+course._id+'/administration/objective'}
					/>
					{/*
					<Menu.Item
						icon='ordered list'
						content='Materi'
						name='matter'
						active={activeMenu === 'matter'}
						as={Link}
						to={'/course/'+course._id+'/administration/matter'}
					/>
					*/}
				</Menu>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course
	};
}

export default connect(mapStateToProps, {}) (AdministrationMenu);
