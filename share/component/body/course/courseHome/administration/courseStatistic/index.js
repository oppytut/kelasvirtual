import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Loader, Grid, Table, Button, Message, Progress} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {Link} from 'react-router-dom';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getStudentsOnly,
	putStudent
} from '../../../../../../action/student.js';

import {
	administrationStudentList
} from '../../../../../../action/administration.js';

import {
	getMatterGroupsByCourse,
	setMatterGroups
} from '../../../../../../action/matterGroup.js';

const logName = 'CourseStatistic';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class CourseStatistic extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			progress: {
				student: []
			}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Statistic']);
		this.props.changeMenu('statistic');

		const course = this.props.course.reduce(item => item);

		const gettingStudent = new Promise((resolve) => {
			this.props.getStudentsOnly({
				query: {
					this: {
						course: course._id,
						deleted: {$ne: true}
					},
					teacher: {course: course._id}
				},
				populate: [
					{
						path: 'user',
						select: 'name gender username email'
					},
					{
						path: 'studentMatter',
						select: 'pass videoPass'
					}
				],
				field: '_id'
			}).then((res) => {
				const student = res.student;
				var {progress} = this.state;

				student.map((item, index) => {
					progress.student[index] = {
						count: Object.keys(item.studentMatter.filter((item) => {
							return item.pass === true;
						})).length
					};

					log.trace('hello', item.studentMatter.filter((x) => {
						return x.pass !== true;
					}));
				});

				const count = 10;
				var list = {
					count: count,
					page: 1,
					countPage: Math.ceil(Object.keys(student).length/count)
				};

				this.setState({
					list: list,
					progress: progress
				});

				this.props.administrationStudentList(student);
				resolve();
			});
		});

		const gettingMatterGroup = new Promise((resolve) => {
			this.props.getMatterGroupsByCourse({
				query: {
					course: course._id, // Exception
					pairCompetency: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}},
					indicator: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}}
				},
				field: 'name',
				populate: [{
					path: 'matter',
					select: 'number title'
				}]
			}).then((res) => {
				const matterGroup = res.matterGroup;
				var {progress} = this.state;

				progress.total = 0;
				matterGroup.map((item) => {
					progress.total += Object.keys(item.matter).length;
				});

				if(progress.total === 0)
					progress.total = 1;

				this.props.setMatterGroups(matterGroup);
				this.setState({progress: progress});
				resolve();
			});
		});

		Promise.all([
			gettingStudent,
			gettingMatterGroup
		]).then(() => {
			this.setState({loading: false});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.administrationStudentList();
		this.props.setMatterGroups();
	}

	/** Pagination */
	nextPage(e) {
		log.trace('nextPage');
		e.target.blur();

		var {list} = this.state;

		if(list.page < list.countPage) {
			list.page = list.page+1;
			this.setState({list});
		}
	}

	prevPage(e) {
		log.trace('prevPage');
		e.target.blur();

		var {list} = this.state;

		if(list.page > 1) {
			list.page = list.page-1;
			this.setState({list});
		}
	}

	goPage(page, e) {
		log.trace('goPage');
		e.target.blur();

		var {list} = this.state;
		list.page = page;
		this.setState({list});
	}
	/** End */

	render() {
		log.trace('render');

		const {loading, progress} = this.state;
		const {course} = this.props;
		const studentList = isEmpty(this.props.administration) ?
			[] : isEmpty(this.props.administration.studentList) ?
				[] : this.props.administration.studentList;
		const list = isEmpty(this.state.list) ?
			{} : this.state.list;

		/** PageButtion Component */
		var pageButton = [];
		for(var i = 1; i <= list.countPage; i++) {
			pageButton.push(<Button
				key={i}
				content={i}
				active={i == list.page ? true : undefined}
				onClick={this.goPage.bind(this, i)}
			/>);
		}
		/** End */

		return(
			<div>
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					<div>
						<Grid>
							<Grid.Row>
								<Grid.Column>
									{isEmpty(studentList.filter((student) => {
										return student.deleted !== true;
									})) ?
										<Message>
											<Message.Header>
												Belum ada peserta didik!
											</Message.Header>
											<p>
												Silahkan tambahkan peserta didik terlebih dahulu.
												<br />
												<Button
													style={{marginTop: '15px'}}
													content='Atur Peserta Didik'
													size='tiny'
													color='teal'
													icon='checkmark'
													compact
													as={Link}
													to={'/course/'+course._id+'/administration/student'}
												/>
											</p>
										</Message>
										:
										<Table size='small' compact unstackable basic='very'>
											<Table.Header>
												<Table.Row>
													<Table.HeaderCell>No.</Table.HeaderCell>
													<Table.HeaderCell>Nama</Table.HeaderCell>
													<Table.HeaderCell>Jenis Kelamin</Table.HeaderCell>
													<Table.HeaderCell>Username</Table.HeaderCell>
													<Table.HeaderCell>Progres</Table.HeaderCell>
												</Table.Row>
											</Table.Header>
											<Table.Body>
												{studentList.filter((student) => {
													return student.deleted !== true;
												}).map((student, index) => {
													if(
														(list.count*(list.page-1))+1 <= index+1
														&& index+1 <= list.count*list.page
													)
														return(
															<Table.Row key={index}>
																<Table.Cell style={{width: '25px'}}>{index+1}</Table.Cell>
																<Table.Cell>{student.deleted}{isEmpty(student.user.name) ?
																	<div style={{fontSize: '12px', color: 'grey'}}>Kosong</div> : student.user.name
																}</Table.Cell>
																<Table.Cell width={2}>{isEmpty(student.user.gender) ?
																	<div style={{fontSize: '12px', color: 'grey'}}>Kosong</div> : student.user.gender
																}</Table.Cell>
																<Table.Cell width={2}>{student.user.username}</Table.Cell>
																<Table.Cell width={5}>
																	{Math.ceil(progress.student[index].count/progress.total*100)+'%'}
																	<Progress
																		active
																		size='tiny'
																		color='green'
																		style={{marginBottom: '0px'}}
																	/>
																</Table.Cell>
															</Table.Row>
														);
												})}
											</Table.Body>
											<Table.Footer>
												<Table.Row>
													<Table.HeaderCell colSpan='6' textAlign='right'>
														<Button.Group size='mini' color='teal' compact>
															<Button
																icon='left chevron'
																onClick={this.prevPage.bind(this)}
																disabled={list.page > 1 ? undefined : true}
															/>
															{pageButton}
															<Button
																icon='right chevron'
																onClick={this.nextPage.bind(this)}
																disabled={list.page < list.countPage ? undefined : true}
															/>
														</Button.Group>
													</Table.HeaderCell>
												</Table.Row>
											</Table.Footer>
										</Table>
									}
								</Grid.Column>
							</Grid.Row>
						</Grid>
					</div>
				}
			</div>
		);
	}
}

CourseStatistic.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getStudentsOnly: PropTypes.func.isRequired,
	putStudent: PropTypes.func.isRequired,
	administrationStudentList: PropTypes.func.isRequired,
	getMatterGroupsByCourse: PropTypes.func.isRequired,
	setMatterGroups: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		course: state.course,
		administration: state.administration
	};
}

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getStudentsOnly,
	putStudent,
	administrationStudentList,
	getMatterGroupsByCourse,
	setMatterGroups
}) (CourseStatistic);
