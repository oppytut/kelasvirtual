import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Loader, Message, Button, Table, Modal, Grid, Segment, List, Label} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {Link} from 'react-router-dom';

import IndicatorForm from './indicatorForm.js';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getPairCompetencysByCourse,
	setPairCompetencys
} from '../../../../../../action/pairCompetency.js';

import {
	delIndicatorsChild
} from '../../../../../../action/indicator.js';

const logName = 'IndicatorAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class IndicatorAdministration extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			addEditLoading: false,
			deleteModalOpen: false,
			addModalOpen: false,
			editModalOpen: false
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Indicator']);
		this.props.changeMenu('indicator');

		const course = this.props.course.reduce(item => item);

		this.props.getPairCompetencysByCourse({
			query: {
				basicCompetency: {coreCompetency: {course: {_id: course._id}}}
			},
			field: '_id',
			populate: [
				{
					path: 'cognitive',
					select: 'competency number',
					populate: [
						{
							path: 'indicator',
							select: 'number content'
						}
					]
				},
				{
					path: 'psychomotor',
					select: 'competency number',
					populate: [
						{
							path: 'indicator',
							select: 'number content'
						}
					]
				}
			]
		}).then(
			(res) => {
				const pairCompetency = res.pairCompetency;
				this.props.setPairCompetencys(pairCompetency);

				this.updateList();

				this.setState({
					loading: false
				});
			},
			(err) => {
				err.response.json().then(({error}) => {
					this.setState({loading: false});
					this.setState({error});
				});
			});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setPairCompetencys();
	}

	/** List */
	updateList() {
		log.trace('updateList');

		const {pairCompetency} = this.props;

		const count = 5;
		var listCog = [];
		var listPsy = [];

		pairCompetency.map((item, index) => {
			listCog[index] = {
				count: count,
				page: 1,
				countPage: Math.ceil(Object.keys(item.cognitive.indicator).length/count)
			};

			listPsy[index] = {
				count: count,
				page: 1,
				countPage: Math.ceil(Object.keys(item.psychomotor.indicator).length/count)
			};
		});

		this.setState({
			listCog: listCog,
			listPsy: listPsy
		});
	}
	/** End */

	/** Form Add and Edit **/
	setAddEditLoading(value) {
		log.trace('setAddEditLoading');

		this.setState({addEditLoading: value});
	}

	getAddEditLoading() {
		log.trace('getAddEditLoading');

		return this.state.addEditLoading;
	}

	getDomainUpdated() {
		log.trace('getDomainUpdated');

		return this.state.domainUpdated;
	}

	getBasicCompetencyUpdated() {
		log.trace('getBasicCompetencyUpdated');

		return this.state.basicCompetencyUpdated;
	}

	getIndicatorUpdated() {
		log.trace('getIndicatorUpdated');

		return this.state.indicatorUpdated;
	}

	handleSave(e) {
		log.trace('handleSave');
		e.target.blur();

	}
	/** End **/

	/** Add Indicator */
	addIndicator(domain, basicCompetency, e) {
		log.trace('addIndicator');
		e.target.blur();

		this.setState({
			addModalOpen: true,
			basicCompetencyUpdated: basicCompetency,
			domainUpdated: domain
		});
	}

	closeAddModal() {
		log.trace('closeAddModal');

		this.setState({
			addModalOpen: false,
			basicCompetencyUpdated: undefined,
			domainUpdated: undefined
		});
	}
	/** End */

	/** Edit Indicator */
	editIndicator(domain, basicCompetency, indicator, e) {
		log.trace('editIndicator');
		e.target.blur();

		this.setState({
			editModalOpen: true,
			basicCompetencyUpdated: basicCompetency,
			indicatorUpdated: indicator,
			domainUpdated: domain
		});
	}

	closeEditModal() {
		log.trace('closeEditModal');

		this.setState({
			editModalOpen: false,
			basicCompetencyUpdated: undefined,
			indicatorUpdated: undefined,
			domainUpdated: undefined
		});
	}
	/** End */

	/** Pagination */
	nextPage(idx, domain, e) {
		log.trace('nextPage');
		e.target.blur();

		var list = domain === 'cognitive' ?
			this.state.listCog : this.state.listPsy;

		if(list[idx].page < list[idx].countPage) {
			list[idx].page = list[idx].page+1;

			if(domain === 'cognitive') {
				this.setState({listCog: list});
			} else {
				this.setState({listPsy: list});
			}
		}
	}

	prevPage(idx, domain, e) {
		log.trace('prevPage');
		e.target.blur();

		var list = domain === 'cognitive' ?
			this.state.listCog : this.state.listPsy;

		if(list[idx].page > 1) {
			list[idx].page = list[idx].page-1;

			if(domain === 'cognitive') {
				this.setState({listCog: list});
			} else {
				this.setState({listPsy: list});
			}
		}
	}

	goPage(idx, page, domain, e) {
		log.trace('goPage');
		e.target.blur();

		var list = domain === 'cognitive' ?
			this.state.listCog : this.state.listPsy;

		list[idx].page = page;

		if(domain === 'cognitive') {
			this.setState({listCog: list});
		} else {
			this.setState({listPsy: list});
		}
	}
	/** End */

	/** Message */
	cleanMessage() {
		if(!isEmpty(this.state.messageId)) {
			clearTimeout(this.state.messageId);
			this.setState({
				messageId: undefined,
				message: undefined
			});
		}
	}

	setMessage(msg, clr) {
		log.trace('setMessage');

		const message = {
			content: msg,
			color: clr
		};
		this.setState({
			message: message
		});

		var messageId = setTimeout(() => {
			this.setState({message: undefined});
		}, 3000);
		this.setState({messageId: messageId});
	}
	/** End */

	/** Delete Indicator */
	deleteIndicator(domain, basicCompetency, indicator, e) {
		log.trace('deleteIndicator');
		e.target.blur();

		this.setState({
			deleteModalOpen: true,
			basicCompetencyDeleted: basicCompetency,
			indicatorDeleted: indicator,
			domainDeleted: domain
		});
	}

	closeDeleteModal() {
		log.trace('closeDeleteModal');

		this.setState({
			deleteModalOpen: false,
			indicator: undefined,
			domainDeleted: undefined
		});
	}

	handleDeleteButton(e) {
		log.trace('handleDeleteButton');
		e.target.blur();

		this.cleanMessage();

		const {indicatorDeleted} = this.state;

		this.props.delIndicatorsChild({
			query: {_id: indicatorDeleted._id}
		}).then(() => {
			const {domainDeleted, basicCompetencyDeleted} = this.state;
			const {pairCompetency} = this.props;

			pairCompetency.map((item) => {
				if(domainDeleted === 'cognitive') {
					if(item.cognitive._id === basicCompetencyDeleted._id) {
						item.cognitive.indicator.map((deletedIndicator, index) => {
							if(deletedIndicator._id === indicatorDeleted._id) {
								item.cognitive.indicator.splice(index, 1);
							}
						});
					}
				} else if(domainDeleted === 'psychomotor') {
					if(item.psychomotor._id === basicCompetencyDeleted._id) {
						item.psychomotor.indicator.map((deletedIndicator, index) => {
							if(deletedIndicator._id === indicatorDeleted._id) {
								item.psychomotor.indicator.splice(index, 1);
							}
						});
					}
				}
			});
			this.props.setPairCompetencys(pairCompetency);
			this.updateList();

			this.setState({
				deleteModalOpen: false,
				basicCompetencyDeleted: undefined,
				indicatorDeleted: undefined,
				domainDeleted: undefined
			});
		});
	}
	/** End */

	render() {
		log.trace('render');

		const {loading} = this.state;
		const {course, pairCompetency} = this.props;

		const listCog = isEmpty(this.state.listCog) ?
			{} : this.state.listCog;
		const listPsy = isEmpty(this.state.listPsy) ?
			{} : this.state.listPsy;

		/** PageButtion Component */
		const pageButtonCog = (idx) => {
			var pageButton = [];
			for(var i = 1; i <= listCog[idx].countPage; i++) {
				pageButton.push(<Button
					key={i}
					content={i}
					active={i == listCog[idx].page ? true : undefined}
					onClick={this.goPage.bind(this, idx, i, 'cognitive')}
				/>);
			}

			return pageButton;
		};

		const pageButtonPsy = (idx) => {
			var pageButton = [];
			for(var i = 1; i <= listPsy[idx].countPage; i++) {
				pageButton.push(<Button
					key={i}
					content={i}
					active={i == listPsy[idx].page ? true : undefined}
					onClick={this.goPage.bind(this, idx, i, 'psychomotor')}
				/>);
			}

			return pageButton;
		};
		/** End */

		const addModal = (
			<Modal
				size='tiny'
				open={this.state.addModalOpen}
				onClose={this.closeAddModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<IndicatorForm
						mode='add'
						getBasicCompetencyUpdated={this.getBasicCompetencyUpdated.bind(this)}
						getDomainUpdated={this.getDomainUpdated.bind(this)}
						getAddEditLoading={this.getAddEditLoading.bind(this)}
						setAddEditLoading={this.setAddEditLoading.bind(this)}
						closeAddModal={this.closeAddModal.bind(this)}
						setMessage={this.setMessage.bind(this)}
						cleanMessage={this.cleanMessage.bind(this)}
						updateList={this.updateList.bind(this)}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeAddModal.bind(this)}
					>
						Tutup
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Simpan'
						type='submit'
						form='indicatorForm'
						onClick={this.handleSave.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const editModal = (
			<Modal
				size='tiny'
				open={this.state.editModalOpen}
				onClose={this.closeEditModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<IndicatorForm
						mode='edit'
						getBasicCompetencyUpdated={this.getBasicCompetencyUpdated.bind(this)}
						getIndicatorUpdated={this.getIndicatorUpdated.bind(this)}
						getDomainUpdated={this.getDomainUpdated.bind(this)}
						getAddEditLoading={this.getAddEditLoading.bind(this)}
						setAddEditLoading={this.setAddEditLoading.bind(this)}
						closeEditModal={this.closeEditModal.bind(this)}
						setMessage={this.setMessage.bind(this)}
						cleanMessage={this.cleanMessage.bind(this)}
						updateList={this.updateList.bind(this)}
					/>
				</Modal.Content>
				<Modal.Actions>
					<Button
						size='small'
						onClick={this.closeEditModal.bind(this)}
					>
						Tutup
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Simpan'
						type='submit'
						form='indicatorForm'
						onClick={this.handleSave.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const deleteModal = (
			<Modal
				size='tiny'
				open={this.state.deleteModalOpen}
				onClose={this.closeDeleteModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					Anda yakin ingin menghapus indikator tersebut?
					<Message size='small' style={{marginTop: '15px'}}>
						Data berikut yang terkait pada indikator tersebut juga akan terhapus.
						<List bulleted style={{marginTop: '5px'}}>
							<List.Item>Materi</List.Item>
							<List.Item>Ujian</List.Item>
							<List.Item>Proses Pembelajaran</List.Item>
						</List>
					</Message>
				</Modal.Content>

				<Modal.Actions>
					<Button size='small' onClick={this.closeDeleteModal.bind(this)}>
						Batal
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Ya'
						onClick={this.handleDeleteButton.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		return(
			<div>
				{addModal}
				{editModal}
				{deleteModal}
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					<div>
						{!isEmpty(this.state.message) &&
							<Message color={this.state.message.color} size='tiny'>
								{this.state.message.content}
							</Message>
						}
						{isEmpty(pairCompetency) ?
							<Message>
								<Message.Header>
									Anda belum melengkapi kompetensi dasar!
								</Message.Header>
								<p>
									Sebelum menambahkan indikator, Anda harus melengkapi kompetensi dasar terlebih dahulu.
									<br />
									<Button
										style={{marginTop: '15px'}}
										content='Atur Kompetensi Dasar'
										size='tiny'
										color='teal'
										icon='checkmark'
										compact
										as={Link}
										to={'/course/'+course._id+'/administration/basicCompetency'}
									/>
								</p>
							</Message>
							:
							<Grid>
								{pairCompetency.map((item, index) => {
									return(
										<Grid.Row key={index}>
											<Grid.Column>
												<Segment style={{
													borderRadius: '0px',
													boxShadow: 'none'
												}}>
													<Grid>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																<div style={{fontSize: '12px'}}>
																	Kompetensi Dasar
																</div>
															</Grid.Column>
														</Grid.Row>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																<Label ribbon>
																	<div style={{display: 'inline-block'}}>3.{item.cognitive.number}</div>
																	<div style={{marginLeft: '10px', display: 'inline-block'}}>{item.cognitive.competency}</div>
																</Label>
															</Grid.Column>
														</Grid.Row>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																{isEmpty(item.cognitive.indicator) ?
																	<Message size='tiny'>
																		<p>
																			Belum ada indikator untuk kompetensi ini.
																			<br />
																			<Button
																				style={{marginTop: '15px'}}
																				content='Tambah Indikator'
																				size='tiny'
																				color='teal'
																				icon='add'
																				compact
																				onClick={this.addIndicator.bind(this, 'cognitive', item.cognitive)}
																			/>
																		</p>
																	</Message>
																	:
																	<Table size='small' compact unstackable basic>
																		<Table.Body>
																			{item.cognitive.indicator.map((indicator, idxIndicator) => {
																				if(
																					(listCog[index].count*(listCog[index].page-1))+1 <= idxIndicator+1
																					&& idxIndicator+1 <= listCog[index].count*listCog[index].page
																				)
																					return(
																						<Table.Row key={idxIndicator}>
																							<Table.Cell style={{width: '50px'}}>3.{item.cognitive.number}.{indicator.number}</Table.Cell>
																							<Table.Cell colSpan='2'><div dangerouslySetInnerHTML={{__html: indicator.content}} /></Table.Cell>
																							<Table.Cell width={3} textAlign='right'>
																								<Button
																									content='Edit'
																									size='tiny'
																									color='teal'
																									icon='edit'
																									basic
																									compact
																									onClick={this.editIndicator.bind(this, 'cognitive', item.cognitive, indicator)}
																								/>
																								<Button
																									content='Hapus'
																									size='tiny'
																									color='teal'
																									icon='delete'
																									basic
																									compact
																									onClick={this.deleteIndicator.bind(this, 'cognitive', item.cognitive, indicator)}
																								/>
																							</Table.Cell>
																						</Table.Row>
																					);
																			})}
																		</Table.Body>
																		<Table.Footer>
																			<Table.Row>
																				<Table.HeaderCell colSpan='3' textAlign='left'>
																					<Button
																						content='Tambah'
																						size='tiny'
																						color='teal'
																						icon='add'
																						compact
																						onClick={this.addIndicator.bind(this, 'cognitive', item.cognitive)}
																					/>
																				</Table.HeaderCell>
																				<Table.HeaderCell colSpan='3' textAlign='right'>
																					<Button.Group size='mini' color='teal' compact>
																						<Button
																							icon='left chevron'
																							onClick={this.prevPage.bind(this, index, 'cognitive')}
																							disabled={listCog[index].page > 1 ? undefined : true}
																						/>
																						{pageButtonCog(index)}
																						<Button
																							icon='right chevron'
																							onClick={this.nextPage.bind(this, index, 'cognitive')}
																							disabled={listCog[index].page < listCog[index].countPage ? undefined : true}
																						/>
																					</Button.Group>
																				</Table.HeaderCell>
																			</Table.Row>
																		</Table.Footer>
																	</Table>
																}
															</Grid.Column>
														</Grid.Row>
														<Grid.Row style={{paddingBottom: '0px'}}>
															<Grid.Column>
																<Label ribbon>
																	<div style={{display: 'inline-block'}}>4.{item.psychomotor.number}</div>
																	<div style={{marginLeft: '10px', display: 'inline-block'}}>{item.psychomotor.competency}</div>
																</Label>
															</Grid.Column>
														</Grid.Row>
														<Grid.Row>
															<Grid.Column>
																{isEmpty(item.psychomotor.indicator) ?
																	<Message size='tiny'>
																		<p>
																			Belum ada indikator untuk kompetensi ini.
																			<br />
																			<Button
																				style={{marginTop: '15px'}}
																				content='Tambah Indikator'
																				size='tiny'
																				color='teal'
																				icon='add'
																				compact
																				onClick={this.addIndicator.bind(this, 'psychomotor', item.psychomotor)}
																			/>
																		</p>
																	</Message>
																	:
																	<Table size='small' compact unstackable basic>
																		<Table.Body>
																			{item.psychomotor.indicator.map((indicator, idxIndicator) => {
																				if(
																					(listPsy[index].count*(listPsy[index].page-1))+1 <= idxIndicator+1
																					&& idxIndicator+1 <= listPsy[index].count*listPsy[index].page
																				)
																					return(
																						<Table.Row key={idxIndicator}>
																							<Table.Cell style={{width: '50px'}}>4.{item.psychomotor.number}.{indicator.number}</Table.Cell>
																							<Table.Cell colSpan='2'><div dangerouslySetInnerHTML={{__html: indicator.content}} /></Table.Cell>
																							<Table.Cell width={3} textAlign='right'>
																								<Button
																									content='Edit'
																									size='tiny'
																									color='teal'
																									icon='edit'
																									basic
																									compact
																									onClick={this.editIndicator.bind(this, 'psychomotor', item.psychomotor, indicator)}
																								/>
																								<Button
																									content='Hapus'
																									size='tiny'
																									color='teal'
																									icon='delete'
																									basic
																									compact
																									onClick={this.deleteIndicator.bind(this, 'psychomotor', item.psychomotor, indicator)}
																								/>
																							</Table.Cell>
																						</Table.Row>
																					);
																			})}
																		</Table.Body>
																		<Table.Footer>
																			<Table.Row>
																				<Table.HeaderCell colSpan='3' textAlign='left'>
																					<Button
																						content='Tambah'
																						size='tiny'
																						color='teal'
																						icon='add'
																						compact
																						onClick={this.addIndicator.bind(this, 'psychomotor', item.psychomotor)}
																					/>
																				</Table.HeaderCell>
																				<Table.HeaderCell colSpan='3' textAlign='right'>
																					<Button.Group size='mini' color='teal' compact>
																						<Button
																							icon='left chevron'
																							onClick={this.prevPage.bind(this, index, 'psychomotor')}
																							disabled={listPsy[index].page > 1 ? undefined : true}
																						/>
																						{pageButtonPsy(index)}
																						<Button
																							icon='right chevron'
																							onClick={this.nextPage.bind(this, index, 'psychomotor')}
																							disabled={listPsy[index].page < listPsy[index].countPage ? undefined : true}
																						/>
																					</Button.Group>
																				</Table.HeaderCell>
																			</Table.Row>
																		</Table.Footer>
																	</Table>
																}
															</Grid.Column>
														</Grid.Row>
													</Grid>
												</Segment>
											</Grid.Column>
										</Grid.Row>
									);
								})}
							</Grid>
						}
					</div>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		pairCompetency: state.pairCompetency
	};
}

IndicatorAdministration.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getPairCompetencysByCourse: PropTypes.func.isRequired,
	setPairCompetencys: PropTypes.func.isRequired,
	delIndicatorsChild: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getPairCompetencysByCourse,
	setPairCompetencys,
	delIndicatorsChild
}) (IndicatorAdministration);
