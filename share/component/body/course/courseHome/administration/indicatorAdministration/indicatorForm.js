import React, {Component} from 'react';
import {Input, Form, Label, TextArea} from 'semantic-ui-react';
import debug from 'debug';
import validator from 'validator';
import isEmpty from 'is-empty';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	postIndicator,
	putIndicator
} from '../../../../../../action/indicator.js';

import {
	setPairCompetencys
} from '../../../../../../action/pairCompetency.js';

const logName = 'IndicatorForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class IndicatorForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		const {mode} = this.props;

		if(mode === 'add') {
			this.props.setAddEditLoading(false);
		} else if(mode === 'edit') {
			const indicatorUpdated = this.props.getIndicatorUpdated();
			var {data} = this.state;

			data.number = indicatorUpdated.number;
			data.indicator = indicatorUpdated.content;

			this.setState({data: data});

			this.props.setAddEditLoading(false);
		}
	}

	/** Validation */
	validateNumber() {
		log.trace('validateNumber');
		const {number} = this.state.data;
		var {error} = this.state;

		if(isEmpty(number)) {
			error.number = 'Anda belum mengisi nomor indikator!';
		} else {
			if(!validator.isNumeric(number.toString())) {
				error.number = 'Format nomor indikator yang Anda isi tidak valid!';
			} else {
				delete error['number'];
			}
		}

		this.setState({error: error});
	}

	validateIndicator() {
		log.trace('validateIndicator');
		const {indicator} = this.state.data;
		var {error} = this.state;

		if(isEmpty(indicator)) {
			error.indicator = 'Anda belum mengisi indikator!';
		} else {
			delete error['indicator'];
		}

		this.setState({error: error});
	}

	validate() {
		log.trace('validate');

		this.validateNumber();
		this.validateIndicator();
	}
	/** End */

	/** Form */
	changeNumber(e) {
		log.trace('changeNumber');

		var {data} = this.state;
		data.number = e.target.value;
		this.setState({data: data});
	}

	changeIndicator(e) {
		log.trace('changeIndicator');

		var {data} = this.state;
		data.indicator = e.target.value;
		this.setState({data: data});
	}

	submit(e) {
		log.trace('submit');
		e.target.blur();

		const {mode} = this.props;
		if(mode === 'edit') {
			this.editSubmit();
		} else {
			this.addSubmit();
		}
	}

	editSubmit() {
		log.trace('editSubmit');

		this.props.cleanMessage();
		this.props.setAddEditLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {data} = this.state;
			const indicatorUpdated = this.props.getIndicatorUpdated();

			const indicator = {
				_id: indicatorUpdated._id,
				number: data.number,
				content: data.indicator
			};

			this.props.putIndicator(indicator).then(
				(res) => {
					const domainUpdated = this.props.getDomainUpdated();
					const basicCompetencyUpdated = this.props.getBasicCompetencyUpdated();
					var {pairCompetency} = this.props;

					pairCompetency.map((item) => {
						if(domainUpdated === 'cognitive') {
							if(item.cognitive._id === basicCompetencyUpdated._id) {
								item.cognitive.indicator.map((updatedIndicator, index) => {
									if(updatedIndicator._id === indicator._id) {
										item.cognitive.indicator[index] = res.indicator;
									}
								});
							}
						} else if(domainUpdated === 'psychomotor') {
							if(item.psychomotor._id === basicCompetencyUpdated._id) {
								item.psychomotor.indicator.map((updatedIndicator, index) => {
									if(updatedIndicator._id === indicator._id) {
										item.psychomotor.indicator[index] = res.indicator;
									}
								});
							}
						}
					});
					this.props.setPairCompetencys(pairCompetency);

					this.props.setMessage('Indikator berhasil diperbaharui!', 'green');
					this.props.setAddEditLoading(false);
					this.props.closeEditModal();
				},
				(err) => {
					err.response.json().then(({error}) => {
						this.props.setAddEditLoading(false);
						this.setState({error});
					});
				}
			);
			/** End */
		} else {
			this.props.setAddEditLoading(false);
		}
	}

	addSubmit() {
		log.trace('addSubmit');

		this.props.cleanMessage();
		this.props.setAddEditLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {data} = this.state;
			const basicCompetencyUpdated = this.props.getBasicCompetencyUpdated();

			const indicator = {
				number: data.number,
				content: data.indicator,
				basicCompetency: basicCompetencyUpdated._id
			};

			this.props.postIndicator(indicator).then(
				(res) => {
					const domainUpdated = this.props.getDomainUpdated();
					var {pairCompetency} = this.props;

					pairCompetency.map((item) => {
						if(domainUpdated === 'cognitive') {
							if(item.cognitive._id === basicCompetencyUpdated._id) {
								item.cognitive.indicator.push(res.indicator);
							}
						} else if(domainUpdated === 'psychomotor') {
							if(item.psychomotor._id === basicCompetencyUpdated._id) {
								item.psychomotor.indicator.push(res.indicator);
							}
						}
					});
					this.props.setPairCompetencys(pairCompetency);
					this.props.updateList();

					this.props.setMessage('Indikator berhasil ditambahkan!', 'green');
					this.props.setAddEditLoading(false);
					this.props.closeAddModal();
				},
				(err) => {
					err.response.json().then(({error}) => {
						this.props.setAddEditLoading(false);
						this.setState({error});
					});
				}
			);
			/** End */
		} else {
			this.props.setAddEditLoading(false);
		}
	}
	/** End */

	render() {
		log.trace('render');

		var loading = this.props.getAddEditLoading();
		const domainUpdated = this.props.getDomainUpdated();
		const basicCompetencyUpdated = this.props.getBasicCompetencyUpdated();

		return(
			<div>
				<Form
					size='small'
					id='indicatorForm'
					onSubmit={this.submit.bind(this)}
					loading={loading}
				>
					<Form.Field>
						<label>Nomor</label>
						<Input
							label={
								(domainUpdated === 'cognitive' ? '3.' : '4.')
								+ basicCompetencyUpdated.number
							}
							placeholder='Nomor Indikator'
							onChange={this.changeNumber.bind(this)}
							value={isEmpty(this.state.data.number) ?
								'' : this.state.data.number}
						/>
						{this.state.error.number &&
							<Label pointing>{this.state.error.number}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Indikator</label>
						<TextArea
							placeholder='Indikator'
							autoHeight
							spellCheck='false'
							onChange={this.changeIndicator.bind(this)}
							value={this.state.data.indicator}
						/>
						{this.state.error.indicator &&
							<Label pointing>{this.state.error.indicator}</Label>
						}
					</Form.Field>
				</Form>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		pairCompetency: state.pairCompetency
	};
}

IndicatorForm.propTypes = {
	postIndicator: PropTypes.func.isRequired,
	putIndicator: PropTypes.func.isRequired,
	setPairCompetencys: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	postIndicator,
	putIndicator,
	setPairCompetencys
}) (IndicatorForm);
