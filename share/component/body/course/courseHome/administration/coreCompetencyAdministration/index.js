import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Loader, Button, Table, TextArea, Form, Grid, Message, Label} from 'semantic-ui-react';
import isEmpty from 'is-empty';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getCoreCompetencys,
	putPostCoreCompetency,
	setCoreCompetencys
} from '../../../../../../action/coreCompetency.js';

const logName = 'CoreCompetencyAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class CoreCompetencyAdministration extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			saveLoading: false,
			error: {},
			cognitive: {},
			psychomotor: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'CoreCompetency']);
		this.props.changeMenu('coreCompetency');

		const course = this.props.course.reduce(item => item);

		this.props.getCoreCompetencys({
			query: {course: course._id},
			field: 'domain competency'
		}).then((res) => {
			this.props.setCoreCompetencys(res.coreCompetency);

			res.coreCompetency.map((item) => {
				if(item.domain === 'cognitive')
					this.setState({cognitive: item});

				if(item.domain === 'psychomotor')
					this.setState({psychomotor: item});
			});

			this.setState({loading: false});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setCoreCompetencys();
	}

	/** Form */
	validate() {
		log.trace('validate');

		var {error, cognitive, psychomotor} = this.state;

		if(isEmpty(cognitive.competency)) {
			error.cognitive = 'Anda harus mengisi kompetensi pengetahuan!';
		}
		if(isEmpty(psychomotor.competency)) {
			error.psychomotor = 'Anda harus mengisi kompetensi keterampilan!';
		}

		this.setState({error: error});
	}

	change(type, e) {
		log.trace('change');

		const {cognitive, psychomotor} = this.state;

		if(type === 'cognitive') {
			cognitive.competency = e.target.value;
			this.setState({cognitive: cognitive});
		}

		if(type === 'psychomotor') {
			psychomotor.competency = e.target.value;
			this.setState({psychomotor: psychomotor});
		}
	}

	save(e) {
		log.trace('save');
		e.target.blur();

		this.setState({saveLoading: true});
		this.validate();

		const course = this.props.course.reduce(item => item);
		const {cognitive, psychomotor} = this.state;

		if(isEmpty(this.state.error.cognitive)
		&& isEmpty(this.state.error.psychomotor)) {
			const putPostingCognitive = new Promise((resolve, reject) => {
				this.props.putPostCoreCompetency({
					query: {
						course: course._id,
						domain: 'cognitive'
					},
					data: {
						post: {
							domain: 'cognitive',
							competency: cognitive.competency
						},
						put: {competency: cognitive.competency}
					}
				}).then(() => {
					resolve();
				}).catch(() => {
					reject();
				});
			});

			const putPostingPsychomotor = new Promise((resolve, reject) => {
				this.props.putPostCoreCompetency({
					query: {
						course: course._id,
						domain: 'psychomotor'
					},
					data: {
						post: {
							domain: 'psychomotor',
							competency: psychomotor.competency
						},
						put: {competency: psychomotor.competency}
					}
				}).then(() => {
					resolve();
				}).catch(() => {
					reject();
				});
			});

			Promise.all([putPostingCognitive, putPostingPsychomotor]).then(() => {
				this.props.getCoreCompetencys({
					query: {course: course._id},
					field: 'domain competency'
				}).then((res) => {
					this.props.setCoreCompetencys(res.coreCompetency);

					res.coreCompetency.map((item) => {
						if(item.domain === 'cognitive')
							this.setState({cognitive: item});

						if(item.domain === 'psychomotor')
							this.setState({psychomotor: item});
					});

					const message = {
						content: 'Berhasil menyimpan kompetensi inti!',
						color: 'green'
					};
					this.setState({
						message: message,
						saveLoading: false
					});

					var messageId = setTimeout(() => {
						this.setState({message: undefined});
					}, 3000);
					this.setState({messageId: messageId});
				});
			}).catch(() => {
				const message = {
					content: 'Gagal menyimpan kompetensi inti!',
					color: 'red'
				};
				this.setState({
					message: message,
					saveLoading: false
				});

				var messageId = setTimeout(() => {
					this.setState({message: undefined});
				}, 3000);
				this.setState({messageId: messageId});
			});
		} else {
			this.setState({saveLoading: false});
		}
	}
	/** End */

	render() {
		log.trace('render');

		const {loading, saveLoading, cognitive, psychomotor} = this.state;

		return(
			<div>
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					<div>
						{!isEmpty(this.state.message) &&
							<Message color={this.state.message.color} size='tiny'>
								{this.state.message.content}
							</Message>
						}
						<Grid>
							<Grid.Row>
								<Grid.Column>
									<Table size='small' compact unstackable basic='very'>
										<Table.Header>
											<Table.Row>
												<Table.HeaderCell>No.</Table.HeaderCell>
												<Table.HeaderCell>Ranah</Table.HeaderCell>
												<Table.HeaderCell>Kompetensi</Table.HeaderCell>
											</Table.Row>
										</Table.Header>
										<Table.Body>
											<Table.Row>
												<Table.Cell style={{paddingTop: '15px', width: '25px'}} verticalAlign='top'>3</Table.Cell>
												<Table.Cell style={{paddingTop: '15px', }} width={2} verticalAlign='top'>Pengetahuan</Table.Cell>
												<Table.Cell>
													<Form>
														<TextArea
															placeholder='Kompetensi Inti Ranah Pengetahuan'
															autoHeight
															value={cognitive.competency}
															onChange={this.change.bind(this, 'cognitive')}
															spellCheck='false'
														/>
														{this.state.error.cognitive &&
															<Label pointing>{this.state.error.cognitive}</Label>
														}
													</Form>
												</Table.Cell>
											</Table.Row>
											<Table.Row>
												<Table.Cell style={{paddingTop: '15px', width: '25px'}} verticalAlign='top'>4</Table.Cell>
												<Table.Cell style={{paddingTop: '15px', }} width={2} verticalAlign='top'>Keterampilan</Table.Cell>
												<Table.Cell>
													<Form>
														<TextArea
															placeholder='Kompetensi Inti Ranah Keterampilan'
															autoHeight
															value={psychomotor.competency}
															onChange={this.change.bind(this, 'psychomotor')}
															spellCheck='false'
														/>
														{this.state.error.psychomotor &&
															<Label pointing>{this.state.error.psychomotor}</Label>
														}
													</Form>
												</Table.Cell>
											</Table.Row>
										</Table.Body>
										<Table.Footer>
											<Table.Row>
												<Table.HeaderCell colSpan='3' textAlign='center'>
													<Button
														content='Simpan'
														size='small'
														color='teal'
														icon='checkmark'
														loading={saveLoading}
														onClick={saveLoading ? undefined : this.save.bind(this)}
													/>
												</Table.HeaderCell>
											</Table.Row>
										</Table.Footer>
									</Table>
								</Grid.Column>
							</Grid.Row>
						</Grid>
					</div>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		coreCompetency: state.coreCompetency
	};
}

CoreCompetencyAdministration.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getCoreCompetencys: PropTypes.func.isRequired,
	putPostCoreCompetency: PropTypes.func.isRequired,
	setCoreCompetencys: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getCoreCompetencys,
	putPostCoreCompetency,
	setCoreCompetencys
}) (CoreCompetencyAdministration);
