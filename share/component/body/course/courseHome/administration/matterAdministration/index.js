import React, {Component} from 'react';
import debug from 'debug';
import {Route, withRouter, Redirect, Switch} from 'react-router-dom';

import MatterGroupList from './matterGroupList.js';
import MatterEditor from './matterEditor';

const logName = 'MatterAdministration';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class MatterAdministration extends Component {
	changeMenu(item) {
		log.trace('changeMenu');

		this.props.changeMenu(item);
	}

	getActiveMenu() {
		log.trace('getActiveMenu');

		return this.props.getActiveMenu();
	}

	render() {
		log.trace('render');

		return(
			<div>
				<Switch>
					<Route
						path='/course/:courseId/administration/matter'
						exact
						render={(props) => (
							<MatterGroupList {...props}
								changeMenu={this.changeMenu.bind(this)}
								getActiveMenu={this.getActiveMenu.bind(this)}
							/>
						)}
					/>
					<Route
						path='/course/:courseId/administration/matter/list'
						exact
						render={(props) => (
							<MatterGroupList {...props}
								changeMenu={this.changeMenu.bind(this)}
								getActiveMenu={this.getActiveMenu.bind(this)}
							/>
						)}
					/>
					<Route
						path='/course/:courseId/administration/matter/add'
						exact
						render={(props) => (
							<MatterEditor {...props}
								changeMenu={this.changeMenu.bind(this)}
								getActiveMenu={this.getActiveMenu.bind(this)}
							/>
						)}
					/>

					<Redirect from='*' to='/404' />
				</Switch>
			</div>
		);
	}
}

export default withRouter(MatterAdministration);
