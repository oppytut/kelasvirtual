import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Loader, Message, Button, Segment, Grid, Table, Popup} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {Link} from 'react-router-dom';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../action/courseHome.js';

import {
	getMatterGroupsByCourse,
	setMatterGroups
} from '../../../../../../action/matterGroup.js';

const logName = 'MatterGroupList';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class MatterGroupList extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Matter', 'List']);
		this.props.changeMenu('matter');

		const course = this.props.course.reduce(item => item);
		const student = this.props.student.reduce(item => item);

		this.props.getMatterGroupsByCourse({
			query: {
				course: course._id, // Exception
				pairCompetency: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}},
				indicator: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}}
			},
			field: 'name',
			populate: [{
				path: 'matter',
				select: 'number title',
				populate: [
					{
						path: 'matterRequire',
						select: 'content',
						populate: [
							{
								path: 'studentMatter',
								select: 'pass',
								match: {student: student._id}
							}
						]
					},
					{
						path: 'studentMatter',
						select: 'pass',
						match: {student: student._id}
					}
				]
			}]
		}).then((res) => {
			const matterGroup = res.matterGroup;

			this.props.setMatterGroups(matterGroup);

			const count = 5;
			var list = [];

			matterGroup.map((item, index) => {
				list[index] = {
					count: count,
					page: 1,
					countPage: Math.ceil(Object.keys(item.matter).length/count)
				};
			});

			this.setState({
				loading: false,
				list: list
			});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setMatterGroups();
	}

	/** Pagination */
	nextPage(idx, e) {
		log.trace('nextPage');
		e.target.blur();

		var {list} = this.state;

		if(list[idx].page < list[idx].countPage) {
			list[idx].page = list[idx].page+1;
			this.setState({list: list});
		}
	}

	prevPage(idx, e) {
		log.trace('prevPage');
		e.target.blur();

		var {list} = this.state;

		if(list[idx].page > 1) {
			list[idx].page = list[idx].page-1;
			this.setState({list: list});
		}
	}

	goPage(idx, page, e) {
		log.trace('goPage');
		e.target.blur();

		var {list} = this.state;
		list[idx].page = page;
		this.setState({list});
	}
	/** End */

	render() {
		log.trace('render');

		const {matterGroup} = this.props;
		const {loading} = this.state;
		const course = this.props.course.reduce(item => item);

		const list = isEmpty(this.state.list) ?
			{} : this.state.list;

		/** PageButtion Component */
		const pageButton = (idx) => {
			var pageButton = [];
			for(var i = 1; i <= list[idx].countPage; i++) {
				pageButton.push(<Button
					key={i}
					content={i}
					active={i == list[idx].page ? true : undefined}
					onClick={this.goPage.bind(this, idx, i)}
				/>);
			}

			return (pageButton);
		};
		/** End */

		return(
			<div>
				{loading ?
					<div>
						<Loader active size='small' />
					</div>
					:
					isEmpty(matterGroup) ?
						<Message>
							<Message.Header>
								Belum ada materi pembelajaran!
							</Message.Header>
							<p>
								Silahkan tambahkan materi pembelajaran terlebih dahulu.
								<br />
								<Button
									style={{marginTop: '15px'}}
									content='Tambah Materi'
									size='tiny'
									color='teal'
									icon='add'
									compact
									as={Link}
									to={'/course/'+course._id+'/administration/matter/add'}
								/>
							</p>
						</Message>
						:
						<Grid>
							<Grid.Row style={{paddingBottom: '0px'}}>
								<Grid.Column>
									<Popup
										size='tiny'
										content='Tambah Grup Materi'
										trigger={
											<Button
												size='tiny'
												color='teal'
												icon='add'
												compact
												as={Link}
												to={'/course/'+course._id+'/administration/matter/add'}
											/>
										}
									/>
								</Grid.Column>
							</Grid.Row>
							{matterGroup.map((item, index) => {
								return(
									<Grid.Row key={index}>
										<Grid.Column>
											<Segment style={{
												borderRadius: '0px',
												boxShadow: 'none'
											}}>
												<Grid>
													<Grid.Row columns='equal'>
														<Grid.Column>
															{!isEmpty(item.name) &&
																<div>
																	<div style={{fontSize: '12px'}}>
																		Grup
																	</div>
																	<div style={{fontSize: '15px'}}>
																		<b>{item.name}</b>
																	</div>
																</div>
															}
														</Grid.Column>
														<Grid.Column textAlign='right'>
															<Popup
																size='tiny'
																content='Edit Grup Materi'
																trigger={
																	<Button
																		size='tiny'
																		color='teal'
																		icon='edit'
																		compact
																	/>
																}
															/>
															<Popup
																size='tiny'
																content='Hapus Grup Materi'
																trigger={
																	<Button
																		size='tiny'
																		color='teal'
																		icon='delete'
																		compact
																	/>
																}
															/>
														</Grid.Column>
													</Grid.Row>
													<Grid.Row>
														<Grid.Column>
															<Table size='small' compact unstackable basic='very'>
																<Table.Header>
																	<Table.Row>
																		<Table.HeaderCell>No.</Table.HeaderCell>
																		<Table.HeaderCell colSpan='3'>Judul</Table.HeaderCell>
																	</Table.Row>
																</Table.Header>
																<Table.Body>
																	{item.matter.map((matter, idxMatter) => {
																		if(
																			(list[index].count*(list[index].page-1))+1 <= idxMatter+1
																			&& idxMatter+1 <= list[index].count*list[index].page
																		)
																			return(
																				<Table.Row key={idxMatter}>
																					<Table.Cell style={{width: '25px'}}>{idxMatter+1}</Table.Cell>
																					<Table.Cell colSpan='2'>{matter.title}</Table.Cell>
																					<Table.Cell width={3} textAlign='right'>
																						<Button
																							content='Edit'
																							size='tiny'
																							color='teal'
																							icon='edit'
																							basic
																							compact
																						/>
																						<Button
																							content='Hapus'
																							size='tiny'
																							color='teal'
																							icon='delete'
																							basic
																							compact
																						/>
																					</Table.Cell>
																				</Table.Row>
																			);
																	})}
																</Table.Body>
																<Table.Footer>
																	<Table.Row>
																		<Table.HeaderCell colSpan='2' textAlign='left'>
																			<Button
																				content='Tambah'
																				size='tiny'
																				color='teal'
																				icon='add'
																				compact
																				as={Link}
																				to={'/course/'+course._id+'/administration/matter/add'}
																			/>
																		</Table.HeaderCell>
																		<Table.HeaderCell colSpan='2' textAlign='right'>
																			<Button.Group size='mini' color='teal' compact>
																				<Button
																					icon='left chevron'
																					onClick={this.prevPage.bind(this, index)}
																					disabled={list[index].page > 1 ? undefined : true}
																				/>
																				{pageButton(index)}
																				<Button
																					icon='right chevron'
																					onClick={this.nextPage.bind(this, index)}
																					disabled={list[index].page < list[index].countPage ? undefined : true}
																				/>
																			</Button.Group>
																		</Table.HeaderCell>
																	</Table.Row>
																</Table.Footer>
															</Table>
														</Grid.Column>
													</Grid.Row>
												</Grid>
											</Segment>
										</Grid.Column>
									</Grid.Row>
								);
							})}
						</Grid>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		matterGroup: state.matterGroup,
		course: state.course,
		student: state.student
	};
}

MatterGroupList.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getMatterGroupsByCourse: PropTypes.func.isRequired,
	setMatterGroups: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getMatterGroupsByCourse,
	setMatterGroups
}) (MatterGroupList);
