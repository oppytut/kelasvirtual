import React, {Component} from 'react';
import {Segment, Grid} from 'semantic-ui-react';
import {connect} from 'react-redux';
import debug from 'debug';
import PropTypes from 'prop-types';

import MatterForm from './matterForm.js';

import {
	courseHomeActiveBreadcrumb
} from '../../../../../../../action/courseHome.js';

import {
	getMatterGroupsByCourse,
	setMatterGroups
} from '../../../../../../../action/matterGroup.js';

const logName = 'MatterEditor';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class MatterEditor extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveBreadcrumb(['Administration', 'Matter', 'Add']);
		this.props.changeMenu('matter');

		const course = this.props.course.reduce(item => item);

		this.props.getMatterGroupsByCourse({
			query: {
				course: course._id, // Exception
				pairCompetency: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}},
				indicator: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}}
			},
			field: '_id name'
		}).then((res) => {
			const matterGroup = res.matterGroup;

			this.props.setMatterGroups(matterGroup);

			const count = 5;
			var list = [];

			matterGroup.map((item, index) => {
				list[index] = {
					count: count,
					page: 1,
					countPage: Math.ceil(Object.keys(item.matter).length/count)
				};
			});

			this.setState({
				loading: false,
				list: list
			});
		});
	}

	render() {
		log.trace('render');

		return(
			<div>
				<Grid>
					<Grid.Row>
						<Grid.Column>
							<Segment style={{
								borderRadius: '0px',
								boxShadow: 'none'
							}}>
								<MatterForm />
							</Segment>
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		matterGroup: state.matterGroup
	};
}

MatterEditor.propTypes = {
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getMatterGroupsByCourse: PropTypes.func.isRequired,
	setMatterGroups: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveBreadcrumb,
	getMatterGroupsByCourse,
	setMatterGroups
}) (MatterEditor);
