import React, {Component} from 'react';
import debug from 'debug';
import {Form, Label, Button} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import ReactQuill from 'react-quill';

import 'react-quill/dist/quill.snow.css';

const logName = 'MatterForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class MatterForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {}
		};
	}

	/** Form */
	submit(e) {
		log.trace('submit');
		e.target.blur();

	}

	changeContent(value) {
		log.trace('changeContent');

		var {data} = this.state;
		data.content = value;
		this.setState({data: data});
	}

	changeVideo() {
		log.trace('changeVideo');

		
	}
	/** End */

	render() {
		log.trace('render');

		const course = this.props.course.reduce(item => item);

		return(
			<div>
				<Form size='small'>
					<Form.Field>
						<label>Judul</label>
						<input
							style={{borderRadius: '0px'}}
							placeholder='Judul'
							name='title'
						/>
						{this.state.error.title &&
							<Label pointing>{this.state.error.title}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Konten</label>
						<ReactQuill
							value={this.state.data.content}
							onChange={this.changeContent.bind(this)}
							theme='snow'
						/>
						{this.state.error.content &&
							<Label pointing>{this.state.error.content}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Nomor Urut Tampil</label>
						<input
							style={{borderRadius: '0px'}}
							placeholder='Nomor Urut Tampil'
							name='number'
						/>
						{this.state.error.number &&
							<Label pointing>{this.state.error.number}</Label>
						}
					</Form.Field>
					<Form.Field>
						<label>Video</label>
						<input
							style={{borderRadius: '0px'}}
							placeholder='Video'
							name='video'
							type='file'
							onChange={this.changeVideo.bind(this)}
						/>
						{this.state.error.video &&
							<Label pointing>{this.state.error.video}</Label>
						}
					</Form.Field>
					<Form.Field>
						<center>
							<Button
								content='Batal'
								size='small'
								as={Link}
								to={'/course/'+course._id+'/administration/matter'}
							/>
							<Button
								type='submit'
								onClick={this.submit.bind(this)}
								size='small'
								color='teal'
							>Simpan</Button>
						</center>
					</Form.Field>
				</Form>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course
	};
}

export default connect(mapStateToProps, {}) (MatterForm);
