import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
} from '../../../../action/courseHome.js';

const logName = 'Exam';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Exam extends Component {
	componentWillMount() {
		log.trace('componentWillMount');

		this.props.courseHomeActiveMenu('Exam');
		this.props.courseHomeActiveBreadcrumb(['Exam']);
	}

	render() {
		log.trace('render');

		return(
			<div>
				Exam
			</div>
		);
	}
}

Exam.propTypes = {
	courseHomeActiveMenu: PropTypes.func.isRequired,
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired
};

export default connect(null, {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
}) (Exam);
