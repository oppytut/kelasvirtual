import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Table, Grid, Header, Loader, Message} from 'semantic-ui-react';

import MatterGroupList from './matterGroupList.js';

import {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
} from '../../../../../action/courseHome.js';

import {
	getPairCompetencysByCourse,
	setPairCompetencys
} from '../../../../../action/pairCompetency.js';

import {
	getMatterGroupsByCourse,
	setMatterGroups
} from '../../../../../action/matterGroup.js';

const logName = 'Overview';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Overview extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveMenu('Home');
		this.props.courseHomeActiveBreadcrumb(['Home']);

		const course = this.props.course.reduce(item => item);
		const student = this.props.student.reduce(item => item);

		/** Get PairCompetency */
		const gettingPairCompetencysByCourse = new Promise((resolve) => {
			this.props.getPairCompetencysByCourse({
				query: {
					basicCompetency: {coreCompetency: {course: {_id: course._id}}}
				},
				field: '_id',
				populate: [
					{
						path: 'cognitive',
						select: 'competency number'
					},
					{
						path: 'psychomotor',
						select: 'competency number'
					}
				]
			}).then((res) => {
				this.props.setPairCompetencys(res.pairCompetency);
				resolve();
			});
		});
		/** End */

		/** Get MatterGroup */
		const gettingMatterGroupsByCourse = new Promise((resolve) => {
			this.props.getMatterGroupsByCourse({
				query: {
					course: course._id, // Exception
					pairCompetency: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}},
					indicator: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}}
				},
				field: 'name',
				populate: [{
					path: 'matter',
					select: 'number title',
					populate: [
						{
							path: 'matterRequire',
							select: 'content',
							populate: [
								{
									path: 'studentMatter',
									select: 'pass',
									match: {student: student._id}
								}
							]
						},
						{
							path: 'studentMatter',
							select: 'pass',
							match: {student: student._id}
						}
					]
				}]
			}).then((res) => {
				this.props.setMatterGroups(res.matterGroup);
				resolve();
			});
		});
		/** End */

		Promise.all([
			gettingPairCompetencysByCourse,
			gettingMatterGroupsByCourse
		]).then(() => {
			this.setState({loading: false});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setPairCompetencys();
		this.props.setMatterGroups();
	}

	render() {
		log.trace('render');

		const {loading} = this.state;

		const course = this.props.course.reduce(item => item);
		const {pairCompetency, matterGroup} = this.props;

		return(
			<div>
				{loading ?
					<Grid>
						<Grid.Row style={{paddingTop: '200px', paddingBottom: '200px'}}>
							<Grid.Column>
								<Loader active />
							</Grid.Column>
						</Grid.Row>
					</Grid>
					:
					<Grid stackable textAlign='left'>
						<Grid.Row columns={2}>
							<Grid.Column>
								<Table size='small' compact unstackable style={{borderRadius: '0px'}}>
									<Table.Body>
										<Table.Row>
											<Table.Cell style={{width: '120px'}}>Mata Pelajaran</Table.Cell>
											<Table.Cell>{course.name}</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>Kelas</Table.Cell>
											<Table.Cell>{course.classname}</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>Tahun Pelajaran</Table.Cell>
											<Table.Cell>{course.teachingYear.year}</Table.Cell>
										</Table.Row>
									</Table.Body>
								</Table>
							</Grid.Column>

							<Grid.Column>
								<Table size='small' compact unstackable style={{borderRadius: '0px'}}>
									<Table.Body>
										<Table.Row>
											<Table.Cell style={{width: '120px'}}>Paket Keahlian</Table.Cell>
											<Table.Cell>{course.department.name}</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>Tingkat</Table.Cell>
											<Table.Cell>{course.grade.name}</Table.Cell>
										</Table.Row>
										<Table.Row>
											<Table.Cell>Sekolah</Table.Cell>
											<Table.Cell>{course.school.name}</Table.Cell>
										</Table.Row>
									</Table.Body>
								</Table>
							</Grid.Column>

							<Grid.Column style={{marginTop: '28px'}}>
								<Header size='tiny' style={{margin: '0px 0px 0px 0px'}}>Kompetensi Dasar</Header>
								{Object.keys(pairCompetency).length === 0 ?
									<Message size='tiny' style={{marginTop: '13px'}}>
										<Message.Header>
											Kompetensi dasar belum tersedia!
										</Message.Header>
										<p>
											Silahkan hubungi pendidik untuk menambahkan kompetensi dasar.
										</p>
									</Message>
									:
									pairCompetency.map((pairCompetency, index) => {
										return (
											<Table size='small' compact key={index} style={{marginTop: '7px'}} unstackable style={{borderRadius: '0px'}}>
												<Table.Body>
													<Table.Row>
														<Table.Cell style={{width: '25px'}}>3.{pairCompetency.cognitive.number}</Table.Cell>
														<Table.Cell><div dangerouslySetInnerHTML={{__html: pairCompetency.cognitive.competency}} /></Table.Cell>
													</Table.Row>
													<Table.Row>
														<Table.Cell>4.{pairCompetency.psychomotor.number}</Table.Cell>
														<Table.Cell><div dangerouslySetInnerHTML={{__html: pairCompetency.psychomotor.competency}} /></Table.Cell>
													</Table.Row>
												</Table.Body>
											</Table>
										);
									})
								}
							</Grid.Column>

							<Grid.Column style={{marginTop: '28px'}}>
								<Header size='tiny' style={{margin: '0px 0px 0px 0px'}}>Materi</Header>
								{Object.keys(matterGroup).length === 0 ?
									<Message size='tiny' style={{marginTop: '13px'}}>
										<Message.Header>
											Materi belum tersedia!
										</Message.Header>
										<p>
											Silahkan hubungi pendidik untuk menambahkan materi.
										</p>
									</Message>
									:
									matterGroup.map((matterGroup, index) => {
										return (
											<MatterGroupList key={index} matterGroup={matterGroup} />
										);
									})
								}
							</Grid.Column>
						</Grid.Row>
					</Grid>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		pairCompetency: state.pairCompetency,
		matterGroup: state.matterGroup,
		student: state.student
	};
}

Overview.propTypes = {
	courseHomeActiveMenu: PropTypes.func.isRequired,
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getPairCompetencysByCourse: PropTypes.func.isRequired,
	setPairCompetencys: PropTypes.func.isRequired,
	getMatterGroupsByCourse: PropTypes.func.isRequired,
	setMatterGroups: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb,
	getPairCompetencysByCourse,
	setPairCompetencys,
	getMatterGroupsByCourse,
	setMatterGroups
}) (Overview);
