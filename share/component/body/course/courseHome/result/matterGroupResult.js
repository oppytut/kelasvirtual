import React, {Component} from 'react';
import debug from 'debug';
import {Loader, Table, Button, Label} from 'semantic-ui-react';
import {connect} from 'react-redux';
import isEmpty from 'is-empty';
import Case from 'case';

const logName = 'GroupMatterResult';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class MatterGroupResult extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			show: {
				count: 5,
				page: 1,
				countPage: 0
			}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		const matterGroup = this.props.matterGroup;
		var show = this.state.show;

		show.countPage = Math.round(Object.keys(matterGroup.matter).length/show.count);

		this.setState({
			loading: false,
			show: show
		});
	}

	nextMatter(e) {
		log.trace('nextMatter');

		var show = this.state.show;

		if(show.page < show.countPage) {
			show.page = show.page+1;
			this.setState({show: show});
			e.target.blur();
		}
	}

	prevMatter(e) {
		log.trace('prevMatter');

		var show = this.state.show;

		if(show.page > 1) {
			show.page = show.page-1;
			this.setState({show: show});
			e.target.blur();
		}
	}

	goPage(page, e) {
		log.trace('goPage');

		var show = this.state.show;
		show.page = page;
		this.setState({show: show});

		e.target.blur();
	}

	isLocked(matterRequire) {
		log.trace('isLocked');

		if(isEmpty(matterRequire)) {
			return false;
		} else {
			for(var i in matterRequire) {
				if(isEmpty(matterRequire[i].studentMatter)) {
					return true;
				} else {
					if(!matterRequire[i].studentMatter[0].pass) {
						return true;
					}
				}
			}
			return false;
		}
	}

	isClear(studentMatter) {
		log.trace('isClear');

		if(isEmpty(studentMatter)) {
			return false;
		} else {
			if(studentMatter.pass) {
				return true;
			} else {
				return false;
			}
		}
	}

	render() {
		const loading = this.state.loading;

		const matterGroup = this.props.matterGroup;

		const show = this.state.show;

		var pageButton = [];
		for(var i = 1; i <= show.countPage; i++) {
			pageButton.push(<Button
				key={i}
				content={i}
				active={i === show.page ? true : undefined}
				onClick={this.goPage.bind(this, i)}
			/>);
		}

		return(
			<div>
				{loading ?
					<div style={{paddingTop: '125px', paddingBottom: '125px'}}>
						<Loader active size='small' />
					</div>
					:
					<div>
						<div style={{fontSize: '12px'}}>({matterGroup.name})</div>
						<Table size='small' compact style={{marginTop: '7px'}} unstackable style={{borderRadius: '0px'}}>
							<Table.Body>
								{matterGroup.matter.map((matter, index) => {
									if((show.count*(show.page-1))+1 <= index+1 && index+1 <= show.count*show.page)
										return (
											<Table.Row key={index}>
												<Table.Cell style={{width: '25px'}}>{matter.number}</Table.Cell>
												<Table.Cell><div dangerouslySetInnerHTML={{__html: Case.sentence(matter.title)}} /></Table.Cell>
												<Table.Cell width={3} textAlign='right'>
													{this.isClear(matter.studentMatter.reduce((prev, item) => item, {})) ?
														<Label
															size='small'
															icon='checkmark'
															color='green'
															content='Lulus'
														/>
														:
														<Label
															size='small'
															color='yellow'
															content='Belum Lulus'
														/>
													}
												</Table.Cell>
											</Table.Row>
										);
								})}
							</Table.Body>
							<Table.Footer>
								<Table.Row>
									<Table.HeaderCell colSpan='3' textAlign='right'>
										<Button.Group size='mini' color='teal' compact>
											<Button
												icon='left chevron'
												onClick={this.prevMatter.bind(this)}
												disabled={show.page > 1 ? undefined : true}
											/>
											{pageButton}
											<Button
												icon='right chevron'
												onClick={this.nextMatter.bind(this)}
												disabled={show.page < show.countPage ? undefined : true}
											/>
										</Button.Group>
									</Table.HeaderCell>
								</Table.Row>
							</Table.Footer>
						</Table>
					</div>
				}
			</div>
		);
	}
}

export default connect(null, {}) (MatterGroupResult);
