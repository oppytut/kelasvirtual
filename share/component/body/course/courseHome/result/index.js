import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import debug from 'debug';
import {Progress, Grid, Loader, Header, Message} from 'semantic-ui-react';

const logName = 'Result';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

import MatterGroupResult from './matterGroupResult.js';

import {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
} from '../../../../../action/courseHome.js';

import {
	getMatterGroupsByCourse,
	setMatterGroups
} from '../../../../../action/matterGroup.js';

class Result extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			progress: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveMenu('Result');
		this.props.courseHomeActiveBreadcrumb(['Result']);

		const course = this.props.course.reduce(item => item);
		const student = this.props.student.reduce(item => item);

		this.props.getMatterGroupsByCourse({
			query: {
				course: course._id, // Exception
				pairCompetency: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}},
				indicator: {basicCompetency: {coreCompetency: {course: {_id: course._id}}}}
			},
			field: 'name',
			populate: [{
				path: 'matter',
				select: 'number title',
				populate: [
					{
						path: 'matterRequire',
						select: 'content',
						populate: [
							{
								path: 'studentMatter',
								select: 'pass',
								match: {student: student._id}
							}
						]
					},
					{
						path: 'studentMatter',
						select: 'pass',
						match: {student: student._id}
					}
				]
			}]
		}).then((res) => {
			const matterGroup = res.matterGroup;
			var progress = {
				count: 0,
				total: 1 // prevent error
			};

			matterGroup.reduce((prev, item) => item, {matter: []}).matter.map((item) => {
				if(item.studentMatter.reduce((prev, item) => item, {pass: false}).pass) {
					progress.count += 1;
				}

				progress.total += 1;
			});

			this.setState({
				loading: false,
				progress: progress
			});

			this.props.setMatterGroups(matterGroup);
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setMatterGroups();
	}

	render() {
		log.trace('render');

		const {loading, progress} = this.state;
		const {matterGroup} = this.props;

		return(
			<div>
				{loading ?
					<Grid>
						<Grid.Row style={{paddingTop: '200px', paddingBottom: '200px'}}>
							<Grid.Column>
								<Loader active />
							</Grid.Column>
						</Grid.Row>
					</Grid>
					:
					<Grid stackable>
						<Grid.Row>
							<Grid.Column>
								<center>
									<div style={{
										maxWidth: '700px',
										textAlign: 'left'
									}}>
										<Header size='tiny' style={{margin: '0px 0px 7px 0px'}}>Progres Belajar</Header>
										<Progress
											value={progress.count}
											total={progress.total}
											active
											progress='percent'
											color='green'
											style={{marginBottom: '0px'}}
										/>
									</div>
								</center>
							</Grid.Column>
						</Grid.Row>
						<Grid.Row>
							<Grid.Column>
								<center>
									<div style={{
										maxWidth: '700px',
										textAlign: 'left'
									}}>
										<Header size='tiny' style={{margin: '0px 0px 0px 0px'}}>Materi</Header>
										{Object.keys(matterGroup).length === 0 ?
											<Message size='tiny' style={{marginTop: '13px'}}>
												<Message.Header>
													Materi belum tersedia!
												</Message.Header>
												<p>
													Silahkan hubungi pendidik untuk menambahkan materi.
												</p>
											</Message>
											:
											matterGroup.map((matterGroup, index) => {
												return (
													<MatterGroupResult key={index} matterGroup={matterGroup} />
												);
											})
										}
									</div>
								</center>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				}
			</div>
		);
	}
}

Result.propTypes = {
	courseHomeActiveMenu: PropTypes.func.isRequired,
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getMatterGroupsByCourse: PropTypes.func.isRequired,
	setMatterGroups: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		course: state.course,
		student: state.student,
		matterGroup: state.matterGroup
	};
}

export default connect(mapStateToProps, {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb,
	getMatterGroupsByCourse,
	setMatterGroups
}) (Result);
