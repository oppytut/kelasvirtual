import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
} from '../../../../action/courseHome.js';

const logName = 'Discussion';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Discussion extends Component {
	componentWillMount() {
		log.trace('componentWillMount');

		this.props.courseHomeActiveMenu('Discussion');
		this.props.courseHomeActiveBreadcrumb(['Discussion']);
	}

	render() {
		log.trace('render');

		return(
			<div>
				Discussion
			</div>
		);
	}
}

Discussion.propTypes = {
	courseHomeActiveMenu: PropTypes.func.isRequired,
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired
};

export default connect(null, {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
}) (Discussion);
