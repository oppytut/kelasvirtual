import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'is-empty';
import {Table, Button, Header} from 'semantic-ui-react';
import {Link} from 'react-router-dom';
import Case from 'case';

import debug from 'debug';

import {
	learningActiveMatter,
	learningMatterList
} from '../../../../../action/learning.js';

const logName = 'MatterList';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class MatterList extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		const count = 8;
		const matterGroup = this.props.matterGroup.reduce(item => item);
		const {activeMatter} = this.props.learning;

		var matterList = {
			count: count,
			page: 1, // Initiale default
			countPage: Math.ceil(Object.keys(matterGroup.matter).length/count) // count is used in learningNavigation.js
		};

		for(var i in matterGroup.matter) {
			if(matterGroup.matter[i]._id === activeMatter) {
				matterList.page = Math.ceil((parseInt(i)+2)/count);
				break;
			}
		}

		this.props.learningMatterList(matterList);
	}

	nextPage(e) {
		log.trace('nextPage');
		e.target.blur();

		var {matterList} = this.props.learning;

		if(matterList.page < matterList.countPage) {
			matterList.page = matterList.page+1;
			this.props.learningMatterList(matterList);
		}
	}

	prevPage(e) {
		log.trace('prevPage');
		e.target.blur();

		var {matterList} = this.props.learning;

		if(matterList.page > 1) {
			matterList.page = matterList.page-1;
			this.props.learningMatterList(matterList);
		}
	}

	goPage(page, e) {
		log.trace('goPage');
		e.target.blur();

		var {matterList} = this.props.learning;
		matterList.page = page;
		this.props.learningMatterList(matterList);
	}

	isLocked(matterRequire) {
		log.trace('isLocked');

		if(isEmpty(matterRequire)) {
			return false;
		} else {
			for(var i in matterRequire) {
				if(isEmpty(matterRequire[i].studentMatter)) {
					return true;
				} else {
					if(!matterRequire[i].studentMatter[0].pass) {
						return true;
					}
				}
			}
			return false;
		}
	}

	pelajari(matterId) {
		log.trace('pelajari');

		this.props.learningActiveMatter(matterId);
		window.scrollTo(0,0);
	}

	render() {
		log.trace('render');

		const course = this.props.course.reduce(item => item);
		const {activeMatter} = this.props.learning;
		const matterGroup = this.props.matterGroup.reduce(item => item);
		const matterList = isEmpty(this.props.learning.matterList) ?
			{} : this.props.learning.matterList;

		/** PageButtion Component */
		var pageButton = [];
		for(var i = 1; i <= matterList.countPage; i++) {
			pageButton.push(<Button
				key={i}
				content={i}
				active={i == matterList.page ? true : undefined}
				onClick={this.goPage.bind(this, i)}
			/>);
		}
		/** End */

		return(
			<div>
				<Header size='tiny' style={{margin: '0px 0px 0px 0px'}}>Materi</Header>
				<div style={{fontSize: '12px'}}>({matterGroup.name})</div>
				<Table size='small' compact style={{marginTop: '7px', borderRadius: '0px'}} unstackable>
					<Table.Body>
						{matterGroup.matter.map((matter, index) => {
							if((matterList.count*(matterList.page-1))+1 <= index+1 && index+1 <= matterList.count*matterList.page)
								return (
									<Table.Row key={index} active={matter._id === activeMatter ? true : undefined}>
										<Table.Cell style={{width: '25px'}}>{matter.number}</Table.Cell>
										<Table.Cell><div dangerouslySetInnerHTML={{__html: Case.sentence(matter.title)}} /></Table.Cell>
										<Table.Cell width={3} textAlign='right'>
											{this.isLocked(matter.matterRequire) ?
												<Button
													icon='lock'
													basic
													compact
													color='teal'
													size='tiny'
													content='Pelajari'
													disabled
												/>
												:
												matter._id === activeMatter ?
													<Button
														icon='unlock'
														basic
														compact
														color='teal'
														size='tiny'
														content='Pelajari'
													/>
													:
													<Button
														icon='unlock'
														basic
														compact
														color='teal'
														size='tiny'
														content='Pelajari'
														as={Link}
														to={'/course/'+course._id+'/learning/'+matter._id}
														onClick={this.pelajari.bind(this, matter._id)}
													/>
											}
										</Table.Cell>
									</Table.Row>
								);
						})}
					</Table.Body>
					<Table.Footer>
						<Table.Row>
							<Table.HeaderCell colSpan='3' textAlign='right'>
								<Button.Group size='mini' color='teal' compact>
									<Button
										icon='left chevron'
										onClick={this.prevPage.bind(this)}
										disabled={matterList.page > 1 ? undefined : true}
									/>
									{pageButton}
									<Button
										icon='right chevron'
										onClick={this.nextPage.bind(this)}
										disabled={matterList.page < matterList.countPage ? undefined : true}
									/>
								</Button.Group>
							</Table.HeaderCell>
						</Table.Row>
					</Table.Footer>
				</Table>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		learning: state.learning,
		matterGroup: state.matterGroup,
		course: state.course
	};
}

MatterList.propTypes = {
	learningActiveMatter: PropTypes.func.isRequired,
	learningMatterList: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	learningActiveMatter,
	learningMatterList
}) (MatterList);
