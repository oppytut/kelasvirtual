import React, {Component} from 'react';
import {Segment, Button, Grid, Divider, Responsive} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import isEmpty from 'is-empty';
import {Link} from 'react-router-dom';

import {
	learningActiveMatter,
	learningMatterList
} from '../../../../../action/learning.js';
import {
	updateMatterGroup
} from '../../../../../action/matterGroup.js';
import {
	postStudentMatter,
	putStudentMatter
} from '../../../../../action/studentMatter.js';

import debug from 'debug';

const logName = 'LearningNavigation';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class LearningNavigation extends Component {
	saveLearning(matterId) {
		log.trace('saveLearning');

		var matterGroup = this.props.matterGroup.reduce(item => item);

		new Promise((resolve) => {
			const student = this.props.student.reduce(item => item);

			var studentMatter = {};

			matterGroup.matter.map((matter) => {
				// Find in MatterGroup
				if(matter._id === matterId) {
					if(isEmpty(matter.studentMatter)) {
						// Data not found
						studentMatter = {
							pass: true,
							student: student._id,
							matter: matterId
						};

						// POST data
						this.props.postStudentMatter(studentMatter).then((res) => {
							matter.studentMatter.push(res.studentMatter);
							resolve(res.studentMatter);
						});
					} else {
						// Data found
						studentMatter = matter.studentMatter[0];
						studentMatter.pass = true;

						// PUT data
						this.props.putStudentMatter(studentMatter).then((res) => {
							matter.studentMatter[0] = res.studentMatter;
							resolve(res.studentMatter);
						});
					}
				}
			});
		}).then((item) => {
			matterGroup.matter.map((matter) => {
			// Find in MatterRequire
				matter.matterRequire.map((matterRequire) => {
					if(matterRequire.content === matterId) {
						if(isEmpty(matterRequire.studentMatter)) {
							matterRequire.studentMatter.push(item);
						} else {
							matterRequire.studentMatter[0] = item;
						}
					}
				});
			});

			this.props.updateMatterGroup(matterGroup);
		});
	}

	changeMatter(matter, page, saveMatter, e) {
		log.trace('changeMatter', page);
		e.target.blur();

		var {matterList} = this.props.learning;

		matterList.page = page;

		this.props.learningMatterList(matterList);
		this.props.learningActiveMatter(matter._id);

		if(!isEmpty(saveMatter)) {
			this.saveLearning(saveMatter._id);
		}

		window.scrollTo(0,0);
	}


	render() {
		log.trace('render');

		const {activeMatter, matterList} = this.props.learning;
		const matterGroup = this.props.matterGroup.reduce(item => item);
		const course = this.props.course.reduce(item => item);

		var studentMatter = {};
		var studentTest = {};

		var saveMatter = {};
		var prevMatter = {};
		var nextMatter = {};
		var prevPage = {};
		var nextPage = {};

		matterGroup.matter.map((matter, index) => {
			if(matter._id === activeMatter) {
				const studentMatterArr = matter.studentMatter;

				studentMatter = isEmpty(studentMatterArr) ?
					{} : studentMatterArr.reduce(item => item);

				const studentTestArr = matter.testGroup.reduce(item => item)
					.test.reduce(item => item)
					.studentTest;

				studentTest = isEmpty(studentTestArr) ?
					{} : studentTestArr.reduce(item => item);

				saveMatter = matter;

				if(index > 0) {
					prevMatter = matterGroup.matter[index-1];
					prevPage = Math.ceil((parseInt(index))/matterList.count);
				}

				if(index !== Object.keys(matterGroup.matter).length-1) {
					nextMatter = matterGroup.matter[index+1];
					nextPage = Math.ceil((parseInt(index)+2)/matterList.count);
				}
			}
		});

		const NextButton = (
			<Button
				content='Selanjutnya'
				size='tiny'
				color='teal'
				icon='right chevron'
				labelPosition='right'
				onClick={this.changeMatter.bind(this, nextMatter, nextPage, saveMatter)}
				disabled={!(!isEmpty(nextMatter)
					&& studentMatter.videoPass === true
					&& studentTest.cognitivePass === true
					&& studentTest.psychomotorPass === true
				)}
				as={isEmpty(nextMatter) ? undefined : Link}
				to={isEmpty(nextMatter) ? undefined : '/course/'+course._id+'/learning/'+nextMatter._id}
			/>
		);

		return(
			<div>
				<Segment style={{boxShadow: 'none', borderRadius: '0px'}}>
					<Grid>
						<Grid.Row columns='equal'>
							<Grid.Column textAlign='left'>
								<Button
									content='Sebelumnya'
									size='tiny'
									color='teal'
									icon='left chevron'
									labelPosition='left'
									onClick={this.changeMatter.bind(this, prevMatter, prevPage, undefined)}
									disabled={isEmpty(prevMatter)}
									as={isEmpty(prevMatter) ? undefined : Link}
									to={isEmpty(prevMatter) ? undefined : '/course/'+course._id+'/learning/'+prevMatter._id}
								/>
								<Responsive maxWidth={375}>
									<Divider fitted hidden />
									{NextButton}
								</Responsive>
							</Grid.Column>
							<Responsive minWidth={376}>
								<Grid.Column textAlign='right' style={{paddingRight: '10px'}}>
									{NextButton}
								</Grid.Column>
							</Responsive>
						</Grid.Row>
					</Grid>
				</Segment>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		student: state.student,
		learning: state.learning,
		matterGroup: state.matterGroup,
		course: state.course
	};
}

LearningNavigation.propTypes = {
	learningActiveMatter: PropTypes.func.isRequired,
	learningMatterList: PropTypes.func.isRequired,
	postStudentMatter: PropTypes.func.isRequired,
	putStudentMatter: PropTypes.func.isRequired,
	updateMatterGroup: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	learningActiveMatter,
	learningMatterList,
	postStudentMatter,
	putStudentMatter,
	updateMatterGroup
}) (LearningNavigation);
