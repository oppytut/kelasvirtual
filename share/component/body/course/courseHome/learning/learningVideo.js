import React, {Component} from 'react';
import {Segment, Label, Header} from 'semantic-ui-react';
import {connect} from 'react-redux';
import isEmpty from 'is-empty';
import ReactPlayer from 'react-player';
import PropTypes from 'prop-types';

import debug from 'debug';

import {
	updateMatterGroup
} from '../../../../../action/matterGroup.js';

import {
	postStudentMatter,
	putStudentMatter
} from '../../../../../action/studentMatter.js';

const logName = 'LearningVideo';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class LearningVideo extends Component {
	constructor() {
		super();

		this.state = {
			playing: false
		};
	}

	ended() {
		log.trace('ended');
		
		var matterGroup = this.props.matterGroup.reduce(item => item);
		
		new Promise((resolve) => {
			const {activeMatter} = this.props.learning;
			
			// Updating data
			matterGroup.matter.map((matter) => {
				if(matter._id === activeMatter) {
					var studentMatter = isEmpty(matter.studentMatter) ? 
						{} : matter.studentMatter.reduce(item => item);

					studentMatter.videoPass = true;

					if(isEmpty(studentMatter._id)) {
						// Data not found
						studentMatter.student = this.props.student.reduce(item => item)._id;
						studentMatter.matter = matter._id;
						
						// POST data
						this.props.postStudentMatter(studentMatter).then((res) => {
							matter.studentMatter.push(res.studentMatter);
							resolve();
						});
					} else {
						// Data found
						// PUT data
						this.props.putStudentMatter(studentMatter).then((res) => {
							matter.studentMatter[0] = res.studentMatter;
							resolve();
						});
					}
				}
			});
		}).then(() => {
			this.props.updateMatterGroup(matterGroup);
		});
	}

	render() {
		log.trace('render');

		const {activeMatter} = this.props.learning;
		
		const matter = this.props.matterGroup.reduce(item => item)
			.matter.filter((item) => {
				return item._id === activeMatter;
			}).reduce(item => item);

		const studentMatter = isEmpty(matter.studentMatter) ? 
			{} : matter.studentMatter.reduce(item => item);

		return(
			<div>
				<Segment style={{boxShadow: 'none', borderRadius: '0px'}}>
					<Label 
						color={studentMatter.videoPass ? 'teal' : undefined} 
						ribbon='right'
					>
						{studentMatter.videoPass ? 'Sudah Dipelajari' : 'Belum Dipelajari'}
					</Label>
					
					<Header style={{margin: '-27px 0px 0px 0px'}} as='h3'>Video</Header>
					<Header style={{margin: '0px 0px 5px 0px'}} as='h5'>Pembelajaran</Header>

					{isEmpty(matter.video) ? 
						<div>
							Maaf, belum ada video pembelajaran untuk materi ini.
						</div>
						:
						<center style={{marginTop: '20px'}}>
							<ReactPlayer 
								url={'/media/matter/' + matter.video}
								onEnded={this.ended.bind(this)}
								playing={this.state.playing}
								controls={true}
								width='100%'
								height='100%'
							/>
						</center>
					}
				</Segment>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		learning: state.learning,
		matterGroup: state.matterGroup,
		student: state.student
	};
}

LearningVideo.propTypes = {
	updateMatterGroup: PropTypes.func.isRequired,
	postStudentMatter: PropTypes.func.isRequired,
	putStudentMatter: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	updateMatterGroup,
	postStudentMatter,
	putStudentMatter
}) (LearningVideo);