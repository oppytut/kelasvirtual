import React, {Component} from 'react';
import {Segment, Header, Form, Button, Label, Message} from 'semantic-ui-react';
import {connect} from 'react-redux';
import debug from 'debug';
import isEmpty from 'is-empty';
import PropTypes from 'prop-types';

import {
	updateMatterGroup
} from '../../../../../../action/matterGroup.js';

import {
	postStudentTest,
	putStudentTest
} from '../../../../../../action/studentTest.js';

const logName = 'CognitiveTestLearning';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class CognitiveTestLearning extends Component {
	constructor() {
		super();

		this.state = {
			checked: false,
			loading: false
		};
	}

	componentWillReceiveProps(nextProps) {
		log.trace('componentWillReceiveProps');

		if(this.state.activeMatter !== nextProps.learning.activeMatter) {
			this.setState({
				message: undefined,
				answer: undefined,
				saveCognitiveTest: undefined,
				checked: false,
				activeMatter: undefined
			});
		}
	}

	changeAnswer(e, {value}) {
		log.trace('changeAnswer');

		if(!this.state.checked)
			this.setState({answer: value});
	}

	checkAnswer(cognitiveTest, e) {
		log.trace('checkAnswer');
		e.target.blur();

		this.setState({loading: true});

		const {answer} = this.state;
		const {activeMatter} = this.props.learning;

		var matterGroup = this.props.matterGroup.reduce(item => item);
		var message = {};

		if(cognitiveTest.mcQuestion.letterAnswer === answer) {
			// Answer true
			message = {
				text: 'Selamat, jawaban Anda benar!',
				color: 'green'
			};

			// Updating data
			new Promise((resolve) => {
				matterGroup.matter.map((matter) => {
					if(matter._id === activeMatter) {
						const studentTestArr = matter.testGroup.reduce(item => item)
							.test.reduce(item => item)
							.studentTest;
						var studentTest = isEmpty(studentTestArr) ?
							{} : studentTestArr.reduce(item => item);

						studentTest.cognitivePass = true;

						if(isEmpty(studentTest._id)) {
							// Data not found
							studentTest.student = this.props.student.reduce(item => item)._id;
							studentTest.test = cognitiveTest.test;

							// POST data
							this.props.postStudentTest(studentTest).then((res) => {
								matter.testGroup.reduce(item => item)
									.test.reduce(item => item)
									.studentTest.push(res.studentTest);
								resolve();
							});
						} else {
							// Data found
							// PUT data
							this.props.putStudentTest(studentTest).then((res) => {
								matter.testGroup.reduce(item => item)
									.test.reduce(item => item)
									.studentTest[0] = res.studentTest;
								resolve();
							});
						}
					}
				});
			}).then(() => {
				this.setState({
					saveCognitiveTest: cognitiveTest,
					message: message,
					checked: true,
					activeMatter: activeMatter,
					loading: false
				});

				this.props.updateMatterGroup(matterGroup);
			});
		} else {
			// Answer false
			message = {
				text: 'Maaf, jawaban Anda salah!',
				color: 'yellow'
			};

			this.setState({
				saveCognitiveTest: cognitiveTest,
				message: message,
				checked: true,
				activeMatter: activeMatter,
				loading: false
			});
		}
	}

	close() {
		log.trace('close');

		this.setState({message: undefined});
	}

	another(e) {
		log.trace('another');
		e.target.blur();

		const {cognitiveTestArr} = this.props;
		const cognitiveTest = isEmpty(cognitiveTestArr) ?
			{} : cognitiveTestArr[
				Math.floor(Math.random()*Object.keys(cognitiveTestArr).length)
			];

		this.setState({
			message: undefined,
			answer: undefined,
			checked: false,
			saveCognitiveTest: cognitiveTest
		});
	}

	render() {
		log.trace('render');

		const {loading} = this.state;
		const {studentTest} = this.props;
		const {message, saveCognitiveTest} = this.state;
		const cognitiveTest = !isEmpty(saveCognitiveTest) ?
			saveCognitiveTest
			: this.props.cognitiveTest;

		return(
			<div>
				<Segment style={{boxShadow: 'none', borderRadius: '0px'}}>
					<Label
						color={studentTest.cognitivePass ? 'teal' : undefined}
						ribbon='right'
					>
						{studentTest.cognitivePass ? 'Lulus' : 'Belum Lulus'}
					</Label>

					<Header style={{margin: '-27px 0px 0px 0px'}} as='h3'>Latihan</Header>
					<Header style={{margin: '0px 0px 5px 0px'}} as='h5'>Pengetahuan</Header>
					{isEmpty(cognitiveTest) ?
						<div>
							Maaf, belum ada latihan soal pengetahuan untuk materi ini.
						</div>
						:
						<div>
							<Form style={{marginTop: '20px', marginBottom: '20px'}}>
								{!isEmpty(message) &&
									<Message
										size='mini'
										color={message.color}
										onDismiss={this.close.bind(this)}
										content={message.text}
									/>
								}

								<Form.Field>
									<div dangerouslySetInnerHTML={{__html: cognitiveTest.mcQuestion.question}} />
								</Form.Field>
								{cognitiveTest.mcQuestion.mcQuestionOption.map((mcQuestionOption, index) => {
									return (
										<Form.Group key={index}>
											<Form.Radio
												value={mcQuestionOption.letter}
												checked={mcQuestionOption.letter === this.state.answer}
												onChange={this.changeAnswer.bind(this)}
											/>
											<label>
												<div dangerouslySetInnerHTML={{__html:
													'<div style="display: inline-block; width: 25px; vertical-align: top;">' +
													String.fromCharCode(97 + index).toUpperCase() + '.'  +
													'</div>' +
													mcQuestionOption.content
												}} />
											</label>
										</Form.Group>
									);
								})}
							</Form>
							<Button
								loading={loading}
								size='mini'
								color='teal'
								onClick={loading ? undefined : this.checkAnswer.bind(this, cognitiveTest)}
								content='Periksa'
								disabled={!(!this.state.checked && !isEmpty(this.state.answer))}
							/>
							<Button
								size='mini'
								icon='refresh'
								content='Muat Ulang'
								disabled={!this.state.checked}
								onClick={this.another.bind(this)}
							/>
						</div>
					}
				</Segment>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		student: state.student,
		learning: state.learning,
		matterGroup: state.matterGroup
	};
}

CognitiveTestLearning.propTypes = {
	updateMatterGroup: PropTypes.func.isRequired,
	postStudentTest: PropTypes.func.isRequired,
	putStudentTest: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	updateMatterGroup,
	postStudentTest,
	putStudentTest
}) (CognitiveTestLearning);
