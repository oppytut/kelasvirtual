import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import isEmpty from 'is-empty';

import CognitiveTestLearning from './cognitiveTestLearning.js';
import PsychomotorTestLearning from './psychomotorTestLearning';

const logName = 'LearningTest';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class LearningTest extends Component {
	render() {
		log.trace('render');

		const {activeMatter} = this.props.learning;
		const matterGroup = this.props.matterGroup.reduce(item => item);
		
		// TestGroup in Array
		const testGroupArr = matterGroup.matter.filter((item) => {
			return item._id === activeMatter;
		}).reduce(item => item)
			.testGroup;
		
		// TestGroup
		const testGroup = isEmpty(testGroupArr)	?
			{} : testGroupArr.reduce(item => item);
		
		// Test
		const test = isEmpty(testGroup.test) ? 
			{} : testGroup.test.reduce(item => item);
		
		// CoginitiveTest
		const cognitiveTest = isEmpty(test.cognitiveTest) ?
			{} : test.cognitiveTest[
				Math.floor(Math.random()*Object.keys(test.cognitiveTest).length)
			];
		
		// PsychomotorTest
		const psychomotorTest =  isEmpty(test.psychomotorTest) ?
			{} : test.psychomotorTest[
				Math.floor(Math.random()*Object.keys(test.psychomotorTest).length)
			];
		
		// StudentTest
		const studentTest = isEmpty(test.studentTest) ?
			{} : test.studentTest.reduce(item => item);

		return(
			<div>
				<CognitiveTestLearning 
					cognitiveTest={cognitiveTest}
					cognitiveTestArr={test.cognitiveTest}
					studentTest={studentTest}
				/>
				<PsychomotorTestLearning 
					psychomotorTest={psychomotorTest}
					psychomotorTestArr={test.psychomotorTest}
					studentTest={studentTest}
				/>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		matterGroup: state.matterGroup,
		learning: state.learning
	};
}

export default connect(mapStateToProps, {}) (LearningTest);