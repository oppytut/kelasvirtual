import React, {Component} from 'react';
import debug from 'debug';
import {UnControlled as CodeMirror} from 'react-codemirror2';

import './editor.css';
import 'codemirror/lib/codemirror.css';
import 'codemirror/addon/hint/show-hint.css';
import 'codemirror/mode/sql/sql.js';
import 'codemirror/addon/hint/show-hint.js';

var logName = 'Editor';
var log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Editor extends Component {
	change(editor, data, value) {
		log.trace('change');
		
		this.props.changeAnswer(value);
	}

	render() {
		log.trace('render');

		const answer = this.props.getAnswer();
		const options = {
			mode: 'text/x-mysql',
			lineNumbers: true
		};

		return(
			<div style={{marginTop: '10px'}}>
				<CodeMirror 
					value={answer}
					onChange={this.change.bind(this)} 
					options={options}
					autoFocus={false}
				/>
			</div>
		);
	}
}

export default Editor;