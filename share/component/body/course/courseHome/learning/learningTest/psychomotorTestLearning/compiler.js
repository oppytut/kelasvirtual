import debug from 'debug';
import isEmpty from 'is-empty';
import validator from 'validator';

const logName = 'Compiler';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

function generateOutput(data) {
	log.trace('generateOutput');

	var output = {};
	const cmdOutput = 'mysql> ' + data.cmd + '\n';
	const errOutput = 'You have an error or not permitted command in your SQL syntax.';
	const type = data.cmd.split(' ');

	const set = (o) => {
		output.console = cmdOutput + o;
	};

	switch(type[0]) {
	/** CREATE */
	case 'create':
		switch(type[1]) {
		/** DATABASE */
		case 'database':
			if(type[2].slice(-1) === ';' && type[2].length > 1
			&& isEmpty(type[3])) {
				set('Query OK, 1 row affected (0,00 sec)');
				return output;
			}
			break;
		/** End */

		/** TABLE */
		case 'table':
			if(data.cmd === data.correctAnswer) {
				set('Query OK, 0 row affected (0,00 sec)');
				return output;
			} else {
				set('Query OK, 0 row affected (0,00 sec)');
				/*set('Your answer is wrong.');*/
				return output;
			}
		}
		break;
	/** End */

	/** SHOW */
	case 'show':
		if(type[1] === 'databases;'
		&& isEmpty(type[2])) {
			set('+--------------------+\n| Database           |\n+--------------------+\n| siswa              |\n| guru               |\n+--------------------+\n2 rows in set (0,00 sec)');
			return output;
		}
		break;
	/** End */

	/** USE */
	case 'use':
		if(type[1].slice(-1) === ';' && type[1].length > 1
		&& isEmpty(type[2])) {
			if(data.cmd === data.correctAnswer) {
				set('Database changed');
				return output;
			} else {
				set('Your answer is wrong.');
				return output;
			}
		}
		break;
	/** End */

	/** DROP */
	case 'drop':
		switch(type[1]) {
		/** DATABASE */
		case 'database':
			if(type[2].slice(-1) === ';' && type[2].length > 1
			&& isEmpty(type[3])) {
				if(data.cmd === data.correctAnswer) {
					set('Query OK, 0 row affected (0,00 sec)');
					return output;
				} else {
					set('Your answer is wrong.');
					return output;
				}
			}
			break;
		/** End */

		/** TABLE */
		case 'table':
			if(type[2].slice(-1) === ';' && type[2].length > 1
			&& isEmpty(type[3])) {
				if(data.cmd === data.correctAnswer) {
					set('Query OK, 0 row affected (0,00 sec)');
					return output;
				} else {
					set('Your answer is wrong.');
					return output;
				}
			}
			break;
		/** End */
		}
		break;
	/** End */

	/** SELECT */
	case 'select':
		if(data.cmd === data.correctAnswer) {
			set('Correct, Query OK, 0 row affected (0,00 sec)');
			return output;
		} else {
			set('Your answer is wrong.');
			return output;
		}
	/** End */

	/** ALTER */
	case 'alter':
		switch(type[1]) {
		/** TABLE */
		case 'table':
			if(validator.isAlphanumeric(type[2])
			&& type[3] === 'rename'
			&& type[4] === 'to'
			&& type[5].slice(-1) === ';' && type[5].length > 1
			&& isEmpty(type[6])) {
				if(data.cmd === data.correctAnswer) {
					set('Query OK, 1 row affected (0,00 sec)');
					return output;
				} else {
					set('Your answer is wrong.');
					return output;
				}
			}
			break;
		/** End */
		}
		break;
	/** End */

	/** UPADATE */
	case 'update':
		if(validator.isAlphanumeric(type[1])
		&& type[2] === 'set'
		&& type[4] === 'where'
		&& type[5].slice(-1) === ';' && type[5].length > 1
		&& isEmpty(type[6])) {
			if(data.cmd === data.correctAnswer) {
				set('Query OK, 1 row affected (0,00 sec)');
				return output;
			} else {
				set('Your answer is wrong.');
				return output;
			}
		}
		break;
	/** End */

	/** DELETE FROM */
	case 'delete':
		if(type[1] === 'from'
			&& validator.isAlphanumeric(type[2])
			&& type[3] === 'where'
			&& type[4].slice(-1) === ';' && type[4].length > 1
			&& isEmpty(type[5])) {
			if(data.cmd === data.correctAnswer) {
				set('Query OK, 1 row affected (0,00 sec)');
				return output;
			} else {
				set('Your answer is wrong.');
				return output;
			}
		}
		break;
	/** End */
	}

	set(errOutput);
	return output;
}

function isPermitted(cmd) {
	log.trace('isPermitted');

	if(cmd.indexOf(';') === -1) {
		log.warn('not have semicolon');
		return false;
	} else if(cmd.match(/;/gi).length !== 1) {
		log.warn('multiple semicolon');
		return false;
	}	else if(cmd.slice(-1) !== ';') {
		log.warn('last character is not semicolon');
		return false;
	}

	const type = cmd.substr(0, cmd.indexOf(' '));
	const permitted = [
		'create',
		'drop',
		'show',
		'use',
		'alter',
		'insert',
		'select',
		'update',
		'delete'
	];

	for(var i in permitted) {
		if(type == permitted[i]) {
			log.trace('permitted');

			return true;
		}
	}

	log.warn('not permitted');
	return false;
}

export function compile(data) {
	return new Promise((resolve) => {
		var results = {};

		new Promise(async (resolve) => {
			if(isPermitted(data.cmd)) {
				results = await generateOutput(data);
				resolve();
			} else {
				results = {
					console: 'mysql> ' + data.cmd + '\n' + 'You have an error or not permitted command in your SQL syntax.'
				};
				resolve();
			}
		}).then(() => {
			resolve(results);
		});
	});
}
