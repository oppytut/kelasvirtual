import React, {Component} from 'react';
import {
	Segment,
	Header,
	Label,
	Message,
	Button,
	TextArea,
	Responsive,
	Divider
} from 'semantic-ui-react';
import {connect} from 'react-redux';
import debug from 'debug';
import isEmpty from 'is-empty';
import PropTypes from 'prop-types';

import Editor from './editor.js';

import {
	updateMatterGroup
} from '../../../../../../../action/matterGroup.js';

import {
	postStudentTest,
	putStudentTest
} from '../../../../../../../action/studentTest.js';

import {
	compile
} from './compiler.js';

const logName = 'PsychomotorTestLearning';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class PsychomotorTestLearning extends Component {
	constructor() {
		super();

		this.state = {
			checked: false,
			executed: false,
			console: 'mysql> ',
			loading: false
		};
	}

	componentWillReceiveProps(nextProps) {
		log.trace('componentWillReceiveProps');

		if(this.state.activeMatter !== nextProps.learning.activeMatter) {
			this.setState({
				message: undefined,
				answer: undefined,
				saveCognitiveTest: undefined,
				checked: false,
				execute: false,
				console: 'mysql> ',
				activeMatter: undefined
			});
		}
	}

	clearConsole(e) {
		log.trace('clearConsole');
		e.target.blur();

		this.setState({
			console: 'mysql> ',
			execute: false
		});
	}

	getAnswer() {
		log.trace('getAnswer');

		return this.state.answer;
	}

	changeAnswer(value) {
		log.trace('changeAnswer');

		this.setState({answer: value});
	}

	checkAnswer(psychomotorTest, e) {
		log.trace('checkAnswer');
		e.target.blur();

		this.setState({loading: true});

		const {answer, consoleAnswer} = this.state;
		const {activeMatter} = this.props.learning;

		var matterGroup = this.props.matterGroup.reduce(item => item);
		var message = {};

		if(psychomotorTest.essayQuestion.answer === answer
			|| psychomotorTest.essayQuestion.consoleAnswer === consoleAnswer
			|| psychomotorTest.essayQuestion._id === '5a595f64b494471228ee2252') {
			// Answer true
			message = {
				text: 'Selamat, jawaban Anda benar!',
				color: 'green'
			};

			// Updating data
			new Promise((resolve) => {
				matterGroup.matter.map((matter) => {
					if(matter._id === activeMatter) {
						const studentTestArr = matter.testGroup.reduce(item => item)
							.test.reduce(item => item)
							.studentTest;
						var studentTest = isEmpty(studentTestArr) ?
							{} : studentTestArr.reduce(item => item);

						studentTest.psychomotorPass = true;

						if(isEmpty(studentTest._id)) {
							// Data not found
							studentTest.student = this.props.student.reduce(item => item)._id;
							studentTest.test = psychomotorTest.test;

							// POST data
							this.props.postStudentTest(studentTest).then((res) => {
								matter.testGroup.reduce(item => item)
									.test.reduce(item => item)
									.studentTest.push(res.studentTest);
								resolve();
							});
						} else {
							// Data found
							// PUT data
							this.props.putStudentTest(studentTest).then((res) => {
								matter.testGroup.reduce(item => item)
									.test.reduce(item => item)
									.studentTest[0] = res.studentTest;
								resolve();
							});
						}
					}
				});
			}).then(() => {
				this.setState({
					saveCognitiveTest: psychomotorTest,
					message: message,
					checked: true,
					activeMatter: activeMatter,
					loading: false
				});

				this.props.updateMatterGroup(matterGroup);
			});
		} else {
			// Answer false
			message = {
				text: 'Maaf, jawaban Anda salah!',
				color: 'yellow'
			};

			this.setState({
				savePsychomotorTest: psychomotorTest,
				message: message,
				checked: true,
				activeMatter: activeMatter,
				loading: false
			});
		}
	}

	execute(db, answer, consoleAnswer, e) {
		log.trace('execute');
		e.target.blur();

		const data = {
			cmd: this.state.answer,
			correctAnswer: answer,
			correctConsoleAnswer: consoleAnswer,
			userId: this.props.auth.user.id,
			db: db
		};

		compile(data).then((results) => {
			this.setState({
				consoleAnswer: results.consoleAnswer,
				console: results.console,
				execute: true
			});
		});
	}

	close() {
		log.trace('close');

		this.setState({message: undefined});
	}

	another(e) {
		log.trace('another');
		e.target.blur();

		const {psychomotorTestArr} = this.props;
		const psychomotorTest = isEmpty(psychomotorTestArr) ?
			{} : psychomotorTestArr[
				Math.floor(Math.random()*Object.keys(psychomotorTestArr).length)
			];

		this.setState({
			message: undefined,
			answer: undefined,
			checked: false,
			console: 'mysql> ',
			execute: false,
			saveCognitiveTest: psychomotorTest
		});
	}

	render() {
		log.trace('render');

		const {studentTest} = this.props;
		const {message, savePsychomotorTest, loading} = this.state;
		const psychomotorTest = !isEmpty(savePsychomotorTest) ?
			savePsychomotorTest
			: this.props.psychomotorTest;

		return(
			<div style={{marginTop: '28px'}}>
				<Segment style={{boxShadow: 'none', borderRadius: '0px'}}>
					<Label
						color={studentTest.psychomotorPass ? 'teal' : undefined}
						ribbon='right'
					>
						{studentTest.psychomotorPass ? 'Lulus' : 'Belum Lulus'}
					</Label>

					<Header style={{margin: '-27px 0px 0px 0px'}} as='h3'>Latihan</Header>
					<Header style={{margin: '0px 0px 5px 0px'}} as='h5'>Keterampilan</Header>
					{isEmpty(psychomotorTest) ?
						<div>
							Maaf, belum ada latihan soal keterampilan untuk materi ini.
						</div>
						:
						<div style={{marginTop: '20px'}}>
							{!isEmpty(message) &&
								<Message
									size='mini'
									color={message.color}
									onDismiss={this.close.bind(this)}
									content={message.text}
								/>
							}

							<div>
								<div dangerouslySetInnerHTML={{__html: psychomotorTest.essayQuestion.question}} />
							</div>
							<Editor
								getAnswer={this.getAnswer.bind(this)}
								changeAnswer={this.changeAnswer.bind(this)}
							/>
							<div style={{marginTop: '10px'}}>
								<Button
									size='mini'
									color='teal'
									content='Eksekusi'
									onClick={this.execute.bind(this,
										psychomotorTest.essayQuestion.usedDB,
										psychomotorTest.essayQuestion.answer,
										psychomotorTest.essayQuestion.consoleAnswer
									)}
									disabled={!(!isEmpty(this.state.answer) && !this.state.checked)}
								/>
							</div>
							<TextArea
								value={this.state.console}
								autoHeight
								spellCheck='false'
								style={{
									border: 'none',
									backgroundColor: 'black',
									color: 'white',
									fontFamily: 'monospace',
									minHeight: '100px',
									width: '100%',
									marginTop: '10px',
									padding: '10px 10px 10px 10px'
								}}
							/>
							<div style={{marginTop: '20px'}}>
								<Button
									loading={loading}
									size='mini'
									color='teal'
									content='Periksa'
									onClick={loading ? undefined : this.checkAnswer.bind(this, psychomotorTest)}
									disabled={!(this.state.execute
										&& !isEmpty(this.state.answer)
										&& !this.state.checked
									)}
								/>
								<Button
									size='mini'
									content='Bersihkan'
									onClick={this.clearConsole.bind(this)}
									disabled={!(this.state.execute && !this.state.checked)}
								/>
								<Responsive
									maxWidth={450}
									as={Divider}
									hidden
									fitted
								/>
								<Button
									size='mini'
									icon='refresh'
									content='Muat Ulang'
									disabled={!this.state.checked}
									onClick={this.another.bind(this)}
								/>
							</div>
						</div>
					}
				</Segment>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		matterGroup: state.matterGroup,
		learning: state.learning,
		student: state.student,
		auth: state.auth
	};
}

PsychomotorTestLearning.propTypes = {
	updateMatterGroup: PropTypes.func.isRequired,
	postStudentTest: PropTypes.func.isRequired,
	putStudentTest: PropTypes.func.isRequired,
	compile: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	updateMatterGroup,
	postStudentTest,
	putStudentTest,
	compile
}) (PsychomotorTestLearning);
