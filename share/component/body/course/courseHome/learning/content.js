import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Segment, Header, Divider} from 'semantic-ui-react';

import debug from 'debug';

const logName = 'Content';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Content extends Component {
	render() {
		log.trace('render');

		const {activeMatter} = this.props.learning;
		const matter = this.props.matterGroup.reduce(item => item)
			.matter.filter((item) => {
				return item._id === activeMatter;
			}).reduce(item => item);

		return(
			<div>
				<Segment style={{boxShadow: 'none', borderRadius: '0px'}}>
					<Header size='large'>{matter.title}</Header>
					<div dangerouslySetInnerHTML={{__html: matter.content}} />
					<Divider />
					<Header size='medium' style={{margin: '20px 0px 5px 0px'}}>Referesi</Header>
					{matter.reference.map((reference, index) => {
						return (
							<div key={index} dangerouslySetInnerHTML={{__html: reference.content}} />
						);
					})}
				</Segment>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		learning: state.learning,
		matterGroup: state.matterGroup
	};
}

export default connect(mapStateToProps, {}) (Content);