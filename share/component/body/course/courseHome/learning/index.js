import React, {Component} from 'react';
import debug from 'debug';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {Grid, Loader, Sticky, Responsive, Message} from 'semantic-ui-react';
import isEmpty from 'is-empty';
import {withRouter} from 'react-router-dom';

import MatterList from './matterList.js';
import Content from './content.js';
import LearningVideo from './learningVideo.js';
import LearningTest from './learningTest';
import LearningNavigation from './learningNavigation.js';

import {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
} from '../../../../../action/courseHome.js';
import {
	learningActiveMatter,
	learningMatterList
} from '../../../../../action/learning.js';
import {
	getMatterGroupsByCourse,
	getMatterGroupsByMatter,
	setMatterGroups
} from '../../../../../action/matterGroup.js';

const logName = 'Learning';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Learning extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');
		this.props.courseHomeActiveMenu('Learning');
		this.props.courseHomeActiveBreadcrumb(['Learning']);

		const {courseId, matterId} = this.props.match.params;
		const student = this.props.student.reduce(item => item);

		var rule = {
			query: {
				course: courseId,
				pairCompetency: {basicCompetency: {coreCompetency: {course: {_id: courseId}}}},
				indicator: {basicCompetency: {coreCompetency: {course: {_id: courseId}}}}
			},
			field: 'name',
			populate: [{
				path: 'matter',
				select: 'number title content video',
				populate: [
					{
						path: 'reference',
						select: 'content'
					},
					{
						path: 'matterRequire',
						select: 'content',
						populate: [
							{
								path: 'studentMatter',
								select: 'pass videoPass',
								match: {student: student._id}
							}
						]
					},
					{
						path: 'studentMatter',
						select: 'pass videoPass',
						match: {student: student._id}
					},
					{
						path: 'testGroup',
						select: '_id',
						populate: [
							{
								path: 'test',
								select: '_id',
								populate: [
									{
										path: 'cognitiveTest',
										select: '_id',
										populate: [
											{
												path: 'mcQuestion',
												select: 'question answer letterAnswer',
												populate: [
													{
														path: 'mcQuestionOption',
														select: 'content answer letter'
													}
												]
											},
											{
												path: 'studentCognitiveTest',
												select: 'answer letterAnswer',
												match: {student: student._id}
											}
										]
									},
									{
										path: 'psychomotorTest',
										select: '_id',
										populate: [
											{
												path: 'essayQuestion',
												select: 'question answer consoleAnswer usedDB'
											},
											{
												path: 'studentPsychomotorTest',
												select: 'answer consoleAnswer',
												match: {student: student._id}
											}
										]
									},
									{
										path: 'studentTest',
										select: 'cognitivePass psychomotorPass',
										match: {student: student._id}
									}
								]
							}
						]
					}
				]
			}],
			limit: 1
		};

		/** Get MatterGroup */
		const gettingMatterGroups = new Promise((resolve) => {
			if(!isEmpty(matterId)) {
				rule.query.matter = {_id: matterId};
				// By Matter
				this.props.getMatterGroupsByMatter(rule).then(
					(res) => {
						this.props.setMatterGroups(res.matterGroup);
						this.props.learningActiveMatter(matterId);
						resolve();
					},
					() => {
						this.props.history.push('/400');
					}
				);
			} else {
				// By Course
				this.props.getMatterGroupsByCourse(rule).then(
					(res) => {
						if(!isEmpty(res.matterGroup)) {
							this.props.setMatterGroups(res.matterGroup);
							const matterGroup = res.matterGroup.reduce(item => item);
							this.props.learningActiveMatter(matterGroup.matter[0]._id);
						}
						resolve();
					},
					() => {
						this.props.history.push('/400');
					}
				);
			}
		});
		/** End */

		Promise.all([gettingMatterGroups]).then(() => {
			this.setState({loading: false});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.learningActiveMatter();
		this.props.learningMatterList();
		this.props.setMatterGroups();
	}

	render() {
		log.trace('render');

		const {loading} = this.state;
		const learningComp = this.learningComp;
		const {matterGroup} = this.props;

		return(
			<div ref={learningComp => this.learningComp = learningComp}>
				{loading ?
					<Grid>
						<Grid.Row style={{paddingTop: '200px', paddingBottom: '200px'}}>
							<Grid.Column>
								<Loader active />
							</Grid.Column>
						</Grid.Row>
					</Grid>
					:
					isEmpty(matterGroup) ?
						<Grid stackable textAlign='left'>
							<Grid.Row>
								<Grid.Column>
									<Message>
										<Message.Header>
											Materi belum tersedia!
										</Message.Header>
										<p>
											Silahkan hubungi pendidik untuk menambahkan materi.
										</p>
									</Message>
								</Grid.Column>
							</Grid.Row>
						</Grid>
						:
						<Grid stackable textAlign='left'>
							<Grid.Row columns={2}>
								<Grid.Column width={5}>
									<Responsive minWidth={769}>
										<Sticky offset={10} context={learningComp}>
											<div style={{paddingBottom: '14px'}}>
												<MatterList />
											</div>
										</Sticky>
									</Responsive>
									<Responsive maxWidth={768}>
										<MatterList />
									</Responsive>
								</Grid.Column>
								<Grid.Column width={11}>
									<Grid>
										<Grid.Row>
											<Grid.Column>
												<Content />
											</Grid.Column>
										</Grid.Row>

										<Grid.Row>
											<Grid.Column>
												<LearningVideo />
											</Grid.Column>
										</Grid.Row>

										<Grid.Row>
											<Grid.Column>
												<LearningTest />
											</Grid.Column>
										</Grid.Row>

										<Grid.Row>
											<Grid.Column>
												<LearningNavigation />
											</Grid.Column>
										</Grid.Row>
									</Grid>
								</Grid.Column>
							</Grid.Row>
						</Grid>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		student: state.student,
		matterGroup: state.matterGroup
	};
}

Learning.propTypes = {
	courseHomeActiveMenu: PropTypes.func.isRequired,
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	learningActiveMatter: PropTypes.func.isRequired,
	learningMatterList: PropTypes.func.isRequired,
	getMatterGroupsByCourse: PropTypes.func.isRequired,
	getMatterGroupsByMatter: PropTypes.func.isRequired,
	setMatterGroups: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps, {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb,
	learningActiveMatter,
	learningMatterList,
	getMatterGroupsByCourse,
	getMatterGroupsByMatter,
	setMatterGroups
}) (Learning));
