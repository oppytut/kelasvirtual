import React, {Component} from 'react';
import {Menu} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import isEmpty from 'is-empty';
import debug from 'debug';

const logName = 'CourseHomeMenu';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class CourseHomeMenu extends Component {
	render() {
		log.trace('render');

		const course = this.props.course.reduce(item => item);
		var {activeMenu} = this.props.courseHome;

		if(isEmpty(activeMenu))
			activeMenu = 'Home';

		return(
			<div>
				<Menu
					size='tiny'
					stackable
					widths={10}
					color='teal'
					inverted
					borderless
				>
					<Menu.Item
						name='Home'
						active={activeMenu === 'Home'}
						as={Link}
						to={'/course/'+course._id}
						content='Beranda'
					/>
					<Menu.Item
						name='Learning'
						active={activeMenu === 'Learning'}
						as={Link}
						to={'/course/'+course._id+'/learning'}
						content='Belajar'
					/>
					{/*
						<Menu.Item
						name='Discussion'
						active={activeMenu === 'Discussion'}
						as={Link}
						to={'/course/'+course._id+'/discussion'}
						content='Diskusi'
					/>
					<Menu.Item
						name='Exam'
						active={activeMenu === 'Exam'}
						as={Link}
						to={'/course/'+course._id+'/exam'}
						content='Ujian'
					/>
					*/}
					{!isEmpty(course.teacher) ?
						<Menu.Item
							name='Administration'
							active={activeMenu === 'Administration'}
							as={Link}
							to={'/course/'+course._id+'/administration'}
							content='Administrasi'
						/>
						:
						<Menu.Item
							name='Result'
							active={activeMenu === 'Result'}
							as={Link}
							to={'/course/'+course._id+'/result'}
							content='Hasil Belajar'
						/>
					}
				</Menu>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		courseHome: state.courseHome,
		course: state.course
	};
}

export default connect (mapStateToProps, {}) (CourseHomeMenu);
