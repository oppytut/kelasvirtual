import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Grid, Breadcrumb, Loader, Button, Responsive} from 'semantic-ui-react';
import {Route, withRouter, Switch, Link, Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import debug from 'debug';
import isEmpty from 'is-empty';

import CourseHomeMenu from './courseHomeMenu.js';
import Overview from './overview';
import Learning from './learning';
import Discussion from './discussion.js';
import Exam from './exam.js';
import Administration from './administration';
import Result from './result';

import {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb
} from '../../../../action/courseHome.js';
import {
	getCourses,
	setCourses
} from '../../../../action/course.js';
import {
	getStudents,
	setStudents
} from '../../../../action/student.js';

const logName = 'CourseHome';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class CourseHome extends Component {
	constructor() {
		super();

		this.state = {
			loading: true
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		const courseId = this.props.match.params.courseId;
		const userId = this.props.auth.user.id;

		/** Get Course */
		const gettingCourse = new Promise((resolve) => {
			this.props.getCourses({
				query: {_id: courseId},
				field: 'name classname password',
				populate: [
					{
						path: 'school',
						select: 'name'
					},
					{
						path: 'department',
						select: 'name'
					},
					{
						path: 'grade',
						select: 'name'
					},
					{
						path: 'teachingYear',
						select: 'year'
					},
					{
						path: 'teacher',
						select: '_id',
						match: {user: userId}
					}
				]
			}).then(
				(res) => {
					this.props.setCourses(res.course);
					resolve();
				},
				() => {
					this.props.history.push('/400');
				}
			);
		});
		/** End */

		/** Get Student */
		const gettingStudents = new Promise((resolve) => {
			this.props.getStudents({
				query: {course: courseId, user: userId},
				field: '_id'
			}).then(
				(res) => {
					this.props.setStudents(res.student);
					resolve();
				}
			);
		});
		/** End */

		Promise.all([gettingCourse, gettingStudents]).then(() => {
			this.setState({loading: false});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setCourses();
		this.props.setStudents();
		this.props.courseHomeActiveMenu();
		this.props.courseHomeActiveBreadcrumb();
	}

	render() {
		log.trace('render');

		const loading = this.state.loading;

		const course = loading ? {} : this.props.course.reduce(item => item);
		const student = loading ? {} : this.props.student.reduce(item => item);

		var {activeBreadcrumb} = this.props.courseHome;
		var breadcrumbLoading = true;
		if(!isEmpty(activeBreadcrumb)) {
			breadcrumbLoading = false;
		}

		/** learningButton Component */
		var learningButton = (<Loader size='tiny' inverted active />);
		if(!loading && !breadcrumbLoading) {
			learningButton = (
				<Button
					animated='fade'
					size='small'
					as={Link}
					to={isEmpty(student.lastStudied) ?
						('/course/'+course._id+'/learning')
						: ('/course/'+course._id+'/learning/'+student.lastStudied)
					}
					inverted
				>
					<Button.Content visible>
						Belajar Sekarang
					</Button.Content>
					<Button.Content hidden>
						Belajar
					</Button.Content>
				</Button>
			);
		}
		/** End */

		return(
			<div>
				{loading ?
					<Grid style={{paddingTop: '70px', paddingBottom: '70px'}}>
						<Grid.Row style={{padding: '100px 13px 100px 13px'}}>
							<Grid.Column>
								<Loader active />
							</Grid.Column>
						</Grid.Row>
					</Grid>
					:
					<Grid style={{paddingTop: '70px'}}>
						{!breadcrumbLoading &&
							<Grid.Row style={{padding: '0px 13px 0px 13px'}}>
								<Grid.Column>
									<b>
										<Breadcrumb size='mini'>
											{/* 0 */}
											<Breadcrumb.Section
												as={Link}
												to='/course'
												link
											>
												Daftar Kelas
											</Breadcrumb.Section>

											{/* 1 */}
											<Breadcrumb.Divider icon='right chevron' />
											{activeBreadcrumb[0] === 'Home' ?
												<Breadcrumb.Section active>
													Kelas
												</Breadcrumb.Section>
												:
												<Breadcrumb.Section
													as={Link}
													to={'/course/'+course._id}
												>
													Kelas ({course.name} - {course.classname} - {course.school.name})
												</Breadcrumb.Section>
											}

											{/* 2 */}
											{activeBreadcrumb[0] === 'Learning' &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{activeBreadcrumb[0] === 'Learning' &&
												<Breadcrumb.Section active>
													Belajar
												</Breadcrumb.Section>
											}

											{activeBreadcrumb[0] === 'Discussion' &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{activeBreadcrumb[0] === 'Discussion' &&
												<Breadcrumb.Section active>
													Diskusi
												</Breadcrumb.Section>
											}

											{activeBreadcrumb[0] === 'Exam' &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{activeBreadcrumb[0] === 'Exam' &&
												<Breadcrumb.Section active>
													Ujian
												</Breadcrumb.Section>
											}

											{activeBreadcrumb[0] === 'Result' &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{activeBreadcrumb[0] === 'Result' &&
												<Breadcrumb.Section active>
													Hasil Belajar
												</Breadcrumb.Section>
											}

											{activeBreadcrumb[0] === 'Administration' &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Statistic') &&
												<Breadcrumb.Section active>
													Administrasi
												</Breadcrumb.Section>
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] !== 'Statistic') &&
												<Breadcrumb.Section
													as={Link}
													to={'/course/'+course._id+'/administration'}
												>
													Administrasi
												</Breadcrumb.Section>
											}

											{/* 4 */}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Student') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Student') &&
												<Breadcrumb.Section active>
													Peserta Didik
												</Breadcrumb.Section>
											}

											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Request') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Request') &&
												<Breadcrumb.Section active>
													Permintaan
												</Breadcrumb.Section>
											}

											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Teacher') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Teacher') &&
												<Breadcrumb.Section active>
													Pendidik
												</Breadcrumb.Section>
											}

											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'CoreCompetency') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'CoreCompetency') &&
												<Breadcrumb.Section active>
													Kompetensi Inti
												</Breadcrumb.Section>
											}

											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'BasicCompetency') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'BasicCompetency') &&
												<Breadcrumb.Section active>
													Kompetensi Dasar
												</Breadcrumb.Section>
											}

											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Indicator') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Indicator') &&
												<Breadcrumb.Section active>
													Indikator
												</Breadcrumb.Section>
											}

											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Objective') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Objective') &&
												<Breadcrumb.Section active>
													Tujuan Pembelajaran
												</Breadcrumb.Section>
											}

											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Matter') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Matter'
												&& activeBreadcrumb[2] === 'List') &&
												<Breadcrumb.Section active>
													Materi
												</Breadcrumb.Section>
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Matter'
												&& activeBreadcrumb[2] !== 'List') &&
												<Breadcrumb.Section
													as={Link}
													to={'/course/'+course._id+'/administration/matter'}
												>
													Materi
												</Breadcrumb.Section>
											}

											{/* 5 */}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Matter'
												&& activeBreadcrumb[2] === 'Add') &&
												<Breadcrumb.Divider icon='right chevron' />
											}
											{(activeBreadcrumb[0] === 'Administration'
												&& activeBreadcrumb[1] === 'Matter'
												&& activeBreadcrumb[2] === 'Add') &&
												<Breadcrumb.Section active>
													Tambah Materi
												</Breadcrumb.Section>
											}

										</Breadcrumb>
									</b>
								</Grid.Column>
							</Grid.Row>
						}

						{!breadcrumbLoading &&
							activeBreadcrumb[0] === 'Home' &&
								<Grid.Row columns='equal' style={{
									paddingLeft: '13px',
									paddingRight: '13px',
									marginTop: '5px',
									minHeight: '68px',
									background: '#00B5AD'
								}}>
									<Grid.Column style={{color: 'white'}}>
										<div style={{fontSize: '20px'}}>
											{course.name}
										</div>
										<div style={{fontSize: '12px'}}>
											{course.classname} - {course.school.name}
										</div>
									</Grid.Column>
									<Responsive
										minWidth={551}
										as={Grid.Column}
										verticalAlign='middle'
										textAlign='right'
									>
										{learningButton}
									</Responsive>
								</Grid.Row>
						}

						{!breadcrumbLoading &&
							activeBreadcrumb[0] === 'Home' &&
								<Responsive
									maxWidth={550}
									as={Grid.Row}
									style={{background: '#00B5AD', paddingTop: '0px'}}
								>
									<Grid.Column textAlign='center'>
										{learningButton}
									</Grid.Column>
								</Responsive>
						}

						{!breadcrumbLoading &&
							<Grid.Row style={{
								padding: '0px 13px 0px 13px',
								marginTop: (activeBreadcrumb[0] !== 'Home' ? '5px' : '10px')
							}}>
								<Grid.Column>
									<CourseHomeMenu />
								</Grid.Column>
							</Grid.Row>
						}

						<Grid.Row style={{paddingLeft: '13px', paddingRight: '13px'}}>
							<Grid.Column>
								<center>
									<div style={{maxWidth: '1340px'}}>
										<Switch>
											<Route
												path='/course/:courseId'
												exact
												component={Overview}
											/>
											<Route
												path='/course/:courseId/learning'
												exact
												component={Learning}
											/>
											<Route
												path='/course/:courseId/learning/:matterId'
												exact
												component={Learning}
											/>
											<Route
												path='/course/:courseId/discussion'
												exact
												component={Discussion}
											/>
											<Route
												path='/course/:courseId/exam'
												exact
												component={Exam}
											/>
											<Route
												path='/course/:courseId/administration'
												component={Administration}
											/>
											<Route
												path='/course/:courseId/result'
												component={Result}
												exact
											/>

											<Redirect from='*' to='/404' />
										</Switch>
									</div>
								</center>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				}
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth,
		course: state.course,
		student: state.student,
		courseHome: state.courseHome
	};
}

CourseHome.propTypes = {
	courseHomeActiveMenu: PropTypes.func.isRequired,
	courseHomeActiveBreadcrumb: PropTypes.func.isRequired,
	getCourses: PropTypes.func.isRequired,
	setCourses: PropTypes.func.isRequired,
	getStudents: PropTypes.func.isRequired,
	setStudents: PropTypes.func.isRequired
};

export default withRouter(connect(mapStateToProps, {
	courseHomeActiveMenu,
	courseHomeActiveBreadcrumb,
	getCourses,
	setCourses,
	getStudents,
	setStudents
}) (CourseHome));
