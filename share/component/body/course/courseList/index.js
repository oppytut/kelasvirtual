import React, {Component} from 'react';
import {Grid, Card, Image, Button, Loader, Icon, Modal} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import debug from 'debug';
import isEmpty from 'is-empty';

import EnrollPassForm from './enrollPassForm.js';

import {
	getCourses,
	setCourses
} from '../../../../action/course.js';

import {
	postRequestStudent,
	delRequestStudents
} from '../../../../action/requestStudent.js';

import {
	getStudents,
	postStudent,
	putStudent
} from '../../../../action/student.js';

const logName = 'CourseList';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class CourseList extends Component {
	constructor() {
		super();

		this.state = {
			loading: true,
			confirmModalOpen: false,
			passModalOpen: false,
			enroll: {},
			enrollButtonLoading: {},
			requestButtonLoading: {},
			notifyCard: {},
			error: {}
		};
	}

	componentWillMount() {
		log.trace('componentWillMount');

		const userId = this.props.auth.user.id;

		this.props.getCourses({
			field: 'name classname password',
			populate: [
				{
					path: 'school',
					select: 'name'
				},
				{
					path: 'department',
					select: 'name'
				},
				{
					path: 'grade',
					select: 'name'
				},
				{
					path: 'teachingYear',
					select: 'year'
				},
				{
					path: 'student',
					select: 'deleted',
					match: {user: userId}
				},
				{
					path: 'requestStudent',
					select: '_id',
					match: {user: userId}
				}
			]
		}).then((res) => {
			this.props.setCourses(res.course);
			this.setState({loading: false});
		});
	}

	componentWillUnmount() {
		log.trace('componentWillUnmount');

		this.props.setCourses();
	}

	/** API */
	postingStudent(courseId, userId, idx) {
		log.trace('postingStudent');

		const data = {
			course: courseId,
			user: userId
		};

		this.props.postStudent(data).then(
			(res) => {
				this.updateCourseByStudent(res.student);

				this.deletingRequestStudent({
					course: res.student.course,
					user: res.student.user
				}, idx);
			},
			(err) => {
				err.response.json().then(({error}) => {
					this.changeRequestButtonLoading(idx, false);
					this.setNotifyCard(idx, 'warn', error.student);
				});
			}
		);
	}

	puttingStudent(student, idx) {
		log.trace('puttingStudent');

		student.deleted = false;

		this.props.putStudent(student).then(
			(res) => {
				this.updateCourseByStudent(res.student);

				this.deletingRequestStudent({
					course: student.course,
					user: student.user
				}, idx);
			},
			(err) => {
				err.response.json().then(({error}) => {
					this.changeRequestButtonLoading(idx, false);
					this.setNotifyCard(idx, 'warn', error.student);
				});
			}
		);
	}

	deletingRequestStudent(requestStudent, idx) {
		log.trace('deletingRequestStudent');

		this.props.delRequestStudents({query: requestStudent}).then(
			(res) => {
				this.deleteCourseByRequestStudent(res.requestStudent);

				this.changeEnrollButtonLoading(idx, false);
				this.setNotifyCard(idx, 'success', 'Berhasil menjadi anggota!');
			},
			(err) => {
				this.changeEnrollButtonLoading(idx, false);
				this.setNotifyCard(idx, 'error', err.requestStudent);
			}
		);
	}
	/** End */

	/** Store */
	updateCourseByRequestStudent(requestStudent) {
		log.trace('updateCourseByRequestStudent');

		var courses = this.props.course;

		courses.map((item) => {
			if(item._id === requestStudent.course) {
				item.requestStudent = [];
				item.requestStudent.push(requestStudent);
			}
		});

		this.props.setCourses(courses);
	}

	deleteCourseByRequestStudent(requestStudent) {
		log.trace('updateCourseByRequestStudent');

		if(!isEmpty(requestStudent)) {
			var courses = this.props.course;

			courses.map((item) => {
				if(item._id === requestStudent.reduce(item => item).course) {
					item.requestStudent = [];
				}
			});

			this.props.setCourses(courses);
		}
	}

	updateCourseByStudent(student) {
		log.trace('updateCourseByStudent');

		var courses = this.props.course;

		courses.map((item) => {
			if(item._id === student.course) {
				item.student = [];
				item.student.push(student);
			}
		});

		this.props.setCourses(courses);
	}
	/** End */

	/** Request */
	request(courseId, idx) {
		log.trace('request');

		if(!isEmpty(this.state.notifyCardId)) {
			clearTimeout(this.state.notifyCardId);

			this.setState({
				notifyCardId: undefined,
				notifyCard: {}
			});
		}

		this.changeRequestButtonLoading(idx, true);

		const userId = this.props.auth.user.id;
		const data = {
			course: courseId,
			user: userId
		};

		/** Post Request */
		this.props.postRequestStudent(data).then(
			(res) => {
				this.updateCourseByRequestStudent(res.requestStudent);

				this.changeRequestButtonLoading(idx, false);
				this.setNotifyCard(idx, 'success', 'Permintaan telah terkirim!');
			},
			(err) => {
				err.response.json().then(({error}) => {
					this.changeRequestButtonLoading(idx, false);
					this.setNotifyCard(idx, 'warn', error.user);
				});
			}
		);
		/** End */
	}

	changeRequestButtonLoading(idx, sts) {
		log.trace('changeRequesButtonLoading');

		var {requestButtonLoading} = this.state;
		requestButtonLoading[idx] = sts;
		this.setState({requestButtonLoading: requestButtonLoading});
	}
	/** End */

	/** Enroll */
	enroll() {
		log.trace('enroll');

		const {courseId, userId, idx} = this.state.enroll;

		/** Get Student */
		this.props.getStudents({
			query: {
				course: courseId,
				user: userId
			},
			field: '_id'
		}).then(
			(res) => {
				if(isEmpty(res.student)) {
					this.postingStudent(courseId, userId, idx);
				} else {
					this.puttingStudent(res.student.reduce(item => item), idx);
				}
			},
			(err) => {
				err.response.json().then(({error}) => {
					this.changeRequestButtonLoading(idx, false);
					this.setNotifyCard(idx, 'warn', error.student);
				});
			}
		);
		/** End */
	}

	changeEnrollButtonLoading(idx, sts) {
		log.trace('changeEnrollButtonLoading');

		var enrollButtonLoading = this.state.enrollButtonLoading;
		enrollButtonLoading[idx] = sts;
		this.setState({enrollButtonLoading: enrollButtonLoading});
	}

	handleEnrollButton(courseId, passCourse, idx) {
		log.trace('enroll');

		this.changeEnrollButtonLoading(idx, true);

		new Promise((resolve) => {
			const enroll = {
				courseId: courseId,
				userId: this.props.auth.user.id,
				idx: idx
			};
			this.setState({enroll: enroll});

			resolve();
		}).then(() => {
			if(!isEmpty(passCourse)) {
				this.setState({passModalOpen: true});
			} else {
				this.setState({confirmModalOpen: true});
			}
		});
	}
	/** End */

	/** passModal */
	closePassModal() {
		log.trace('closePassModal');

		this.setState({passModalOpen: false});
	}

	getEnrollData() {
		log.trace('getEnrollData');

		return this.state.enroll;
	}
	/** End */

	/** confirmModal */
	closeConfirmModal() {
		log.trace('closeConfirmModal');

		this.setState({confirmModalOpen: false});
	}

	handleConfirmModal() {
		log.trace('handleConfirmModal');

		this.enroll();
		this.closeConfirmModal();
	}
	/** End */

	setNotifyCard(idx, type, info) {
		log.trace('setNotifyCard');

		var color = '#E0E1E2';

		if(type === 'success') {
			color = '#21BA45';
		} else if(type === 'warn') {
			color = '#FBBD08';
		} else if(type === 'error') {
			color = '#DB2828';
		}

		var notifyCard = this.state.notifyCard;
		notifyCard[idx] = {
			color: color,
			info: info
		};
		this.setState({notifyCard: notifyCard});

		// Deleted
		notifyCard = {};
		var notifyCardId = setTimeout(() => {
			this.setState({notifyCard: notifyCard});
		}, 3000);
		this.setState({notifyCardId: notifyCardId});
	}

	render() {
		log.trace('render');

		const courses = this.props.course;

		const confirmModal = (
			<Modal
				size='tiny'
				open={this.state.confirmModalOpen}
				onClose={this.closeConfirmModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					Anda yakin ingin menjadi anggota dari kelas ini?
				</Modal.Content>

				<Modal.Actions>
					<Button size='small' onClick={this.closeConfirmModal.bind(this)}>
						Batal
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Ya'
						onClick={this.handleConfirmModal.bind(this)}
					/>
				</Modal.Actions>
			</Modal>
		);

		const passModal = (
			<Modal
				size='tiny'
				open={this.state.passModalOpen}
				onClose={this.closePassModal.bind(this)}
				closeIcon
			>
				<Modal.Content>
					<EnrollPassForm
						closePassModal={this.closePassModal.bind(this)}
						enroll={this.enroll.bind(this)}
						getEnrollData={this.getEnrollData.bind(this)}
					/>
				</Modal.Content>

				<Modal.Actions>
					<Button size='small' onClick={this.closePassModal.bind(this)}>
						Batal
					</Button>

					<Button
						size='small'
						positive
						icon='checkmark'
						labelPosition='right'
						content='Lanjut'
						type='submit'
						form='enrollPassForm'
					/>
				</Modal.Actions>
			</Modal>
		);

		const memberCourseCard = (idx, courseId) => {
			return(
				<Card.Content key={idx} extra>
					<div className='ui two buttons'>
						<Button
							style={{padding: '10px 10px 10px 10px'}}
							basic
							color='teal'
							as={Link}
							to={'/course/'+courseId}
						>
							Masuk
						</Button>
					</div>
				</Card.Content>
			);
		};

		const unmemberCourseCard = (idx, courseId, coursePassword) => {
			return(
				<Card.Content key={idx} extra>
					<div className='ui two buttons'>
						<Button
							style={{padding: '10px 10px 10px 10px'}}
							basic
							color='teal'
							onClick={this.handleEnrollButton.bind(this, courseId, coursePassword, idx)}
						>
							<Icon name='add' /> Daftarkan
						</Button>
					</div>
				</Card.Content>
			);
		};

		const unmemberCourseCardPassword = (idx, courseId, coursePassword) => {
			return(
				<Card.Content key={idx} extra>
					<div className='ui two buttons'>
						<Button
							style={{padding: '10px 10px 10px 10px'}}
							basic
							color='teal'
							onClick={this.handleEnrollButton.bind(this, courseId, coursePassword, idx)}
						>
							<Icon name='add' /> Daftarkan
						</Button>
						<Button
							style={{padding: '10px 10px 10px 10px'}}
							basic
							color='grey'
							loading={this.state.requestButtonLoading[idx]}
							disabled={this.state.requestButtonLoading[idx]}
							onClick={this.request.bind(this, courseId, idx)}
						>
							<Icon name='mail' /> Ajukan
						</Button>
					</div>
				</Card.Content>
			);
		};

		return (
			<div>
				<Grid style={{paddingTop: '70px', paddingBottom: '70px'}}>
					<Grid.Row style={{background: '#00B5AD', paddingLeft: '13px', paddingRight: '13px'}}>
						<Grid.Column style={{color: 'white'}}>
							<div style={{fontSize: '20px'}}>
								Pilih Kelas
							</div>
							<div style={{fontSize: '12px'}}>
								Silahkan pilih kelas lalu ikuti proses pembelajarannya!
							</div>
						</Grid.Column>
					</Grid.Row>

					<Grid.Row>
						{passModal}
						{confirmModal}
						{this.state.loading ?
							(<Grid.Column style={{paddingTop: '200px', paddingBottom: '200px'}}>
								<Loader active/>
							</Grid.Column>)
							:
							(<Grid.Column textAlign='center'>
								{courses.map((course, index) => {
									return (
										<Card key={index} style={{
											display: 'inline-block',
											margin: '0px 10px 20px 10px',
											textAlign: 'left',
											boxShadow: 'none',
											borderStyle: 'solid',
											borderWidth: '1px',
											borderColor: '#E1E1E1'
										}}>
											<Card.Content style={{paddingBottom: '7px'}}>
												<Image floated='right' size='mini' src='/media/icon/45x45-square.png' />
												<Card.Header>
													{course.name}
												</Card.Header>
												<Card.Meta>
													<div style={{fontSize: '12px'}}>
														{course.classname} - {course.teachingYear.year}
													</div>
												</Card.Meta>
												<Card.Description style={{marginTop: '15px'}}>
													{course.grade.name} {course.department.name} <br /> {course.school.name}
												</Card.Description>
											</Card.Content>

											{!isEmpty(this.state.notifyCard[index]) ?
												<Card.Content style={{
													background: this.state.notifyCard[index].color,
													paddingTop: '2px',
													border: 'none',
													color: 'white',
													height: '24px',
													fontSize: '12px'
												}}>
													{this.state.notifyCard[index].info}
												</Card.Content>
												:
												<Card.Content style={{
													paddingTop: '2px',
													border: 'none',
													height: '24px'
												}}></Card.Content>
											}

											{!isEmpty(course.student) ?
												course.student.reduce(item => item).deleted ?
													isEmpty(course.password) ?
														unmemberCourseCard(index, course._id, course.password)
														:
														unmemberCourseCardPassword(index, course._id, course.password)
													:
													memberCourseCard(index, course._id)
												:
												isEmpty(course.password) ?
													unmemberCourseCard(index, course._id, course.password)
													:
													unmemberCourseCardPassword(index, course._id, course.password)
											}

										</Card>
									);
								})}
							</Grid.Column>)
						}
					</Grid.Row>
				</Grid>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		course: state.course,
		auth: state.auth
	};
}

CourseList.propTypes = {
	getCourses: PropTypes.func.isRequired,
	setCourses: PropTypes.func.isRequired,
	postRequestStudent: PropTypes.func.isRequired,
	delRequestStudents: PropTypes.func.isRequired,
	getStudents: PropTypes.func.isRequired,
	postStudent: PropTypes.func.isRequired,
	putStudent: PropTypes.func.isRequired
};

export default connect(mapStateToProps, {
	getCourses,
	setCourses,
	postRequestStudent,
	delRequestStudents,
	getStudents,
	postStudent,
	putStudent
}) (CourseList);
