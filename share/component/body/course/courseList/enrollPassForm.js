import React, {Component} from 'react';
import debug from 'debug';
import isEmpty from 'is-empty';
import {Form, Input, Label} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';

import {
	checkPassword
} from '../../../../action/course.js';

const logName = 'EnrollPassForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class EnrollPassForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {},
			loading: false
		};
	}

	change(e) {
		log.trace('change');

		var data = this.state.data;
		data[e.target.name] = e.target.value;
		this.setState({data});

		this.validatePassword();
	}

	validatePassword() {
		log.trace('validatePassword');

		const password = this.state.data.password;
		var error = this.state.error;

		if(isEmpty(password)) {
			error.password = 'Anda harus mengisi kata sandi!';
		} else {
			delete error['password'];
		}

		this.setState({error: error});
	}

	validate() {
		log.trace('validate');

		this.validatePassword();
	}

	submit() {
		log.trace('submit');

		this.setState({loading: true});

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {password} = this.state.data;
			const enroll = this.props.getEnrollData();

			const passwordData = {
				_id: enroll.courseId,
				password: password
			};

			/** Check Password */
			this.props.checkPassword(passwordData).then(
				(res) => {
					if(res.isTrue) {
						this.props.enroll();
						this.props.closePassModal();
						this.setState({loading: false});
					} else {
						var error = this.state.error;
						error.password = 'Kata sandi yang Anda masukan salah!';
						this.setState({error: error});
						this.setState({loading: false});
					}
				}
			);
			/** End */
		}
	}

	render() {
		log.trace('render');

		const {loading} = this.state;

		return(
			<Form
				id='enrollPassForm'
				loading={loading}
				onSubmit={loading ? undefined : this.submit.bind(this)}
				size='small'
			>
				<Form.Field>
					<label>Kata Sandi</label>
					<Input
						name='password'
						value={this.state.password}
						onChange={this.change.bind(this)}
						type='password'
						placeholder='Kata Sandi'
					/>
					{this.state.error.password &&
						<Label pointing>{this.state.error.password}</Label>
					}
				</Form.Field>
			</Form>
		);
	}
}

EnrollPassForm.propTypes = {
	checkPassword: PropTypes.func.isRequired
};

export default connect (null, {
	checkPassword
}) (EnrollPassForm);
