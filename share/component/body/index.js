import React, {Component} from 'react';
import {Route, Redirect, Switch} from 'react-router-dom';
import debug from 'debug';

import Home from './home.js';
import Course from './course';
import Account from './account';
import ErrorResponse from './errorResponse.js';

const logName = 'Body';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};
 
class Body extends Component {
	render() {
		log.trace('render');

		return(
			<div>
				<Switch>
					<Route path='/' exact component={Home} />
					<Route path='/course' component={Course} />
					<Route path='/account' exact component={Account} />
					<Route path='/404' exact render={props => (
						<ErrorResponse {...props}
							title='Error 404!'
							message='Maaf, halaman yang Anda inginkan tidak tersedia.'
						/>
					)} />
					<Route path='/401' exact render={props => (
						<ErrorResponse {...props}
							title='Error 401!'
							message='Maaf, Anda harus masuk terlebih dahulu.'
						/>
					)} />
					<Route path='/400' exact render={props => (
						<ErrorResponse {...props}
							title='Error 400!'
							message='Maaf, halaman yang Anda inginkan tidak tersedia.'
						/>
					)} />
					<Redirect from='*' to='/404' />
				</Switch>
			</div>
		);
	}
}

export default Body;