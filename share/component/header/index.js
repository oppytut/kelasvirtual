import React, {Component} from 'react';
import debug from 'debug';

import Menu from './menu';

const logName = 'Header';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Header extends Component {
	render() {
		log.trace('render');

		return (
			<div>
				<Menu />
			</div>
		);
	}
}

export default Header;