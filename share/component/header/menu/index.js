import React, {Component} from 'react';
import {Image, Header, Menu, Button, Transition, Icon} from 'semantic-ui-react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import MediaQuery from 'react-responsive';
import isEmpty from 'is-empty';
import debug from 'debug';

import LoginButton from './login';
import RegisterButton from './register';
import LogoutButton from './logout.js';

const logName = 'Menu';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class HeaderMenu extends Component {
	constructor() {
		super();

		this.state = {
			visible: false
		};
	}

	changeVisibility() {
		log.trace('changeVisibility');

		this.setState({visible: !this.state.visible});
	}

	render() {
		log.trace('render');

		const {isAuthenticated} = this.props.auth;
		var {activeMenu} = this.props.header;

		if(isEmpty(activeMenu))
			activeMenu = 'home';

		const headerItem = (
			<Menu.Item as={Link} to='/'>
				<div>
					<Image src='/media/icon/25x25-square.png' />
				</div>
				<div style={{marginLeft: '10px'}}>
					<Header as='h4'>Kelas Virtual</Header>
				</div>
			</Menu.Item>
		);

		const controlItem = (
			<Menu.Menu position='right'>
				<Menu.Item>
					<Button
						icon
						basic
						color='teal'
						size='mini'
						onClick={this.changeVisibility.bind(this)}
					>
						<Icon name='sidebar' />
					</Button>
				</Menu.Item>
			</Menu.Menu>
		);

		const contentItem = (
			<MediaQuery minWidth={451}>
				{(matches) => {
					if(matches) {
						if(isAuthenticated) {
							return memberItem;
						} else {
							return guestItem;
						}
					} else {
						return controlItem;
					}
				}}
			</MediaQuery>
		);

		const contentItemTrans = (
			<MediaQuery minWidth={451}>
				{(matches) => {
					if(!matches) {
						if(isAuthenticated) {
							return memberItemTrans;
						} else {
							return guestItemTrans;
						}
					} else {
						return '';
					}
				}}
			</MediaQuery>
		);

		const generateGuestItem = (fitted) => {
			return (
				<Menu.Menu position='right'>
					<Menu.Item fitted={fitted}>
						<RegisterButton />
					</Menu.Item>
					<Menu.Item fitted={fitted}>
						<LoginButton />
					</Menu.Item>
				</Menu.Menu>
			);
		};

		const generateMemberItem = (fitted) => {
			return (
				<Menu.Menu position='right'>
					<Menu.Item
						name='home'
						active={activeMenu === 'home'}
						as={Link}
						to='/'
					>
						Beranda
					</Menu.Item>
					<Menu.Item
						name='course'
						active={activeMenu === 'course'}
						as={Link}
						to='/course'
					>
						Kelas
					</Menu.Item>
					<Menu.Item
						name='account'
						active={activeMenu === 'account'}
						as={Link}
						to='/account'
					>
						Akun
					</Menu.Item>

					<Menu.Item fitted={fitted}>
						<LogoutButton />
					</Menu.Item>
				</Menu.Menu>
			);
		};

		const guestItem = (
			generateGuestItem('vertically')
		);

		const guestItemTrans = (
			generateGuestItem()
		);

		const memberItem = (
			generateMemberItem('vertically')
		);

		const memberItemTrans = (
			generateMemberItem()
		);

		return(
			<div>
				<Transition
					visible={this.state.visible}
					animation='slide down'
					duration={300}
				>
					<Menu
						size='tiny'
						fixed='top'
						stackable={true}
						borderless={true}
						style={{boxShadow: 'none'}}
					>
						{headerItem}
						{contentItemTrans}
					</Menu>
				</Transition>

				<Menu
					size='tiny'
					fixed='top'
					borderless={true}
					style={{boxShadow: 'none'}}
				>
					{headerItem}
					{contentItem}
				</Menu>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		auth: state.auth,
		header: state.header
	};
}

export default connect(mapStateToProps, {}) (HeaderMenu);
