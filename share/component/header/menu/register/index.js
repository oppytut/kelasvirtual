import React, {Component} from 'react';
import {Button, Modal} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import debug from 'debug';

import RegisterForm from './registerForm.js';

import {
	headerRegisterModalOpen
} from '../../../../action/header.js';

const logName = 'Register';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class RegisterModal extends Component {
	constructor() {
		super();

		this.state = {
			register: false,
			loading: false
		};
	}

	changeRegisterStatus(status) {
		log.trace('changeRegisterStatus');

		this.setState({
			register: status
		});
	}

	closeModal() {
		log.trace('closeModal');

		this.props.headerRegisterModalOpen(false);
	}

	register(e) {
		log.trace('register');
		e.target.blur();

		this.props.headerRegisterModalOpen(true);
		this.changeRegisterStatus.bind(this)(false);
	}

	setLoading(value) {
		log.trace('setLoading');

		this.setState({loading: value});
	}

	getLoading() {
		log.trace('getLoading');

		return this.state.loading;
	}

	handleRegisterModalButton(e) {
		log.trace('registerModalButtonHandle');
		e.target.blur();

	}

	render() {
		log.trace('render');

		const {registerModalOpen} = this.props.header;

		return (
			<div>
				<Button
					onClick={this.register.bind(this)}
					size='mini'
					color='teal'
				>
					Daftar
				</Button>

				<Modal
					size='tiny'
					open={registerModalOpen}
					onClose={this.closeModal.bind(this)}
					closeIcon
				>
					<Modal.Content>
						<RegisterForm
							changeRegisterStatus={this.changeRegisterStatus.bind(this)}
							getLoading={this.getLoading.bind(this)}
							setLoading={this.setLoading.bind(this)}
						/>
					</Modal.Content>

					<Modal.Actions>
						{
							this.state.register ?
								<Button
									size='small'
									onClick={this.closeModal.bind(this)}
								>
									Tutup
								</Button>
								:
								<div>
									<Button
										size='small'
										onClick={this.closeModal.bind(this)}
										disabled={this.state.loading}
									>
										Batal
									</Button>
									<Button
										size='small'
										positive
										icon='checkmark'
										labelPosition='right'
										content='Daftar'
										type='submit'
										form='registerForm'
										onClick={this.handleRegisterModalButton.bind(this)}
									/>
								</div>
						}
					</Modal.Actions>
				</Modal>
			</div>
		);
	}
}

RegisterModal.propTypes = {
	headerRegisterModalOpen: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		header: state.header
	};
}

export default connect(mapStateToProps, {
	headerRegisterModalOpen
}) (RegisterModal);
