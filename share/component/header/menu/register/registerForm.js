import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Label, Input, Message, Form} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import validator from 'validator';
import isEmpty from 'is-empty';
import debug from 'debug';

import {
	postUser
} from '../../../../action/user.js';

const logName = 'RegisterForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class RegisterForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {},
			success: false
		};
	}

	change(e) {
		log.trace('change');

		var data = this.state.data;
		data[e.target.name] = e.target.value;
		this.setState({data});
	}

	validateEmail() {
		log.trace('validateEmail');

		const email = this.state.data.email;
		var {error} = this.state;

		if(isEmpty(email)) {
			error.email = 'Anda harus mengisi email!';
		} else if(!validator.isEmail(email)) {
			error.email = 'Format email yang Anda isi tidak valid!';
		} else {
			delete error['email'];
		}

		this.setState({error: error});
	}

	validateUsername() {
		log.trace('validateUsername');

		const username = this.state.data.username;
		var {error} = this.state;

		if(isEmpty(username)) {
			error.username = 'Anda harus mengisi username!';
		} else if(!validator.isAlphanumeric(username)) {
			error.username = 'Username hanya boleh menggunakan huruf dan angka!';
		} else if(!validator.isLength(username, {min: 5, max: 20})) {
			error.username = 'Username harus 5 sampai 20 karakter!';
		} else {
			delete error['username'];
		}

		this.setState({error: error});
	}

	validatePassword() {
		log.trace('validatePassword');

		const password = this.state.data.password;
		var {error} = this.state;

		if(isEmpty(password)) {
			error.password = 'Anda harus mengisi password!';
		} else if(!validator.isLength(password, {min: 8, max: 20})) {
			error.password = 'Password harus 8 sampai 20 karakter!';
		} else if(/\s/.test(password)) {
			error.password = 'Password tidak boleh mengandung spasi!';
		} else {
			delete error['password'];
		}

		this.setState({error: error});
	}

	validateRePassword() {
		log.trace('validatePassword');

		const password = this.state.data.password;
		const repassword = this.state.data.repassword;
		var {error} = this.state;

		if(isEmpty(repassword)) {
			error.repassword = 'Anda harus mengisi ulang password!';
		} else if(repassword != password) {
			error.repassword = 'Password yang Anda isi tidak sama!';
		} else {
			delete error['repassword'];
		}

		this.setState({error: error});
	}

	validate() {
		log.trace('validate');

		this.validateEmail();
		this.validatePassword();
		this.validateUsername();
		this.validateRePassword();
	}

	submit() {
		log.trace('submit');

		this.props.setLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const {data} = this.state;

			/** Post User */
			this.props.postUser(data).then(
				() => {
					this.props.setLoading(false);
					this.setState({
						success: true
					});

					this.props.changeRegisterStatus(true);
					this.props.history.push('/');
				},
				(err) => {
					err.response.json().then(({error}) => {
						this.props.setLoading(false);
						this.setState({error});
					});
				}
			);
			/** End */
		} else {
			this.props.setLoading(false);
		}
	}

	render() {
		log.trace('render');

		const loading = this.props.getLoading();

		const formElement = (
			<Form
				id='registerForm'
				loading={loading}
				onSubmit={loading ? undefined : this.submit.bind(this)}
				size='small'
			>
				<Form.Field>
					<label>Email</label>
					<Input
						name='email'
						value={this.state.data.email}
						onChange={this.change.bind(this)}
						placeholder='Email'
					/>
					{this.state.error.email &&
						<Label pointing>{this.state.error.email}</Label>
					}
				</Form.Field>

				<Form.Field>
					<label>Username</label>
					<Input
						name='username'
						value={this.state.username}
						onChange={this.change.bind(this)}
						placeholder='Username'
					/>
					{this.state.error.username &&
						<Label pointing>{this.state.error.username}</Label>
					}
				</Form.Field>

				<Form.Field>
					<label>Password</label>
					<Input
						name='password'
						value={this.state.password}
						onChange={this.change.bind(this)}
						type='password'
						placeholder='Password'
					/>
					{this.state.error.password &&
						<Label pointing>{this.state.error.password}</Label>
					}
				</Form.Field>

				<Form.Field>
					<Input
						name='repassword'
						value={this.state.repassword}
						onChange={this.change.bind(this)}
						type='password'
						placeholder='Masukan Ulang Password'
					/>
					{this.state.error.repassword &&
						<Label pointing>{this.state.error.repassword}</Label>
					}
				</Form.Field>
			</Form>
		);

		return(
			<div>
				{
					this.state.success ?
						<Message style={{maxHeight: '100px'}} positive>
							<Message.Header>Pendaftaran berhasil!</Message.Header>
							<p>Silahkan <b>masuk</b> dan mulai menggunakan Kelas Virtual.</p>
						</Message>
						:
						formElement
				}
			</div>
		);
	}
}

RegisterForm.propTypes = {
	postUser: PropTypes.func.isRequired
};

export default withRouter(connect(null, {
	postUser
})(RegisterForm));
