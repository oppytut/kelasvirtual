import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Label, Input, Form} from 'semantic-ui-react';
import {connect} from 'react-redux';
import isEmpty from 'is-empty';
import PropTypes from 'prop-types';
import debug from 'debug';

import {
	login
} from '../../../../action/auth.js';

const logName = 'LoginForm';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class LoginForm extends Component {
	constructor() {
		super();

		this.state = {
			data: {},
			error: {}
		};
	}

	change(e) {
		log.trace('change');

		var data = this.state.data;
		data[e.target.name] = e.target.value;
		this.setState({data});
	}

	validateId() {
		log.trace('validateId');

		const id = this.state.data.id;
		var error = this.state.error;

		if(isEmpty(id)) {
			error.id = 'Anda harus mengisi Email atau Username!';
		} else {
			delete error['id'];
		}

		this.setState({error: error});
	}

	validatePassword() {
		log.trace('validatePassword');

		var password = this.state.data.password;
		var error = this.state.error;

		if(isEmpty(password)) {
			error.password = 'Anda harus mengisi password!';
		} else {
			delete error['password'];
		}

		this.setState({error: error});
	}

	validate() {
		log.trace('validate');

		this.validateId();
		this.validatePassword();
	}

	submit() {
		log.trace('submit');

		this.props.setLoading(true);

		this.validate();
		const validation = isEmpty(this.state.error);

		if(validation) {
			log.info('data is valid');

			const data = this.state.data;

			/** Do Login */
			this.props.login(data).then(
				() => {
					this.props.setLoading(false);
					this.props.closeModal();
					this.props.history.push('/');
				},
				(err) => {
					err.response.json().then(({error}) => {
						this.props.setLoading(false);
						this.setState({error});
					});
				}
			);
			/** End */
		} else {
			this.props.setLoading(false);
		}
	}

	render() {
		log.trace('render');

		const loading = this.props.getLoading();

		return(
			<Form
				id='loginForm'
				loading={loading}
				onSubmit={loading ? undefined : this.submit.bind(this)}
				size='small'
			>
				<Form.Field>
					<label>Email atau Username</label>
					<Input
						name='id'
						value={this.state.id}
						onChange={this.change.bind(this)}
						placeholder='Email atau Username'
					/>
					{this.state.error.id &&
						<Label pointing>{this.state.error.id}</Label>
					}
				</Form.Field>

				<Form.Field>
					<label>Password</label>
					<Input
						name='password'
						value={this.state.password}
						onChange={this.change.bind(this)}
						type='password'
						placeholder='Password'
					/>
					{this.state.error.password &&
						<Label pointing>{this.state.error.password}</Label>
					}
				</Form.Field>
			</Form>
		);
	}
}

LoginForm.propTypes = {
	login: PropTypes.func.isRequired
};

export default withRouter(connect(null, {
	login
})(LoginForm));
