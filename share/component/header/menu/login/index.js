import React, {Component} from 'react';
import {Button, Modal} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import debug from 'debug';

import LoginForm from './loginForm.js';

import {
	headerLoginModalOpen
} from '../../../../action/header.js';

const logName = 'Login';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class LoginModal extends Component {
	constructor() {
		super();

		this.state = {
			open: false,
			loading: false
		};
	}

	closeModal() {
		log.trace('closeModal');

		this.props.headerLoginModalOpen(false);
	}

	login(e) {
		log.trace('login');
		e.target.blur();

		this.props.headerLoginModalOpen(true);
	}

	setLoading(value) {
		log.trace('setLoading');

		this.setState({loading: value});
	}

	getLoading() {
		log.trace('getLoading');

		return this.state.loading;
	}

	handleLoginModalButton(e) {
		log.trace('handleLoginModalButton');
		e.target.blur();

	}

	render() {
		log.trace('render');

		const {loginModalOpen} = this.props.header;

		return(
			<div>
				<Button
					onClick={this.login.bind(this)}
					size='mini'
				>Masuk</Button>

				<Modal
					size='tiny'
					open={loginModalOpen}
					onClose={this.closeModal.bind(this)}
					closeIcon
				>
					<Modal.Content>
						<LoginForm
							closeModal={this.closeModal.bind(this)}
							setLoading={this.setLoading.bind(this)}
							getLoading={this.getLoading.bind(this)}
						/>
					</Modal.Content>

					<Modal.Actions>
						<Button
							size='small'
							onClick={this.closeModal.bind(this)}
							disabled={this.state.loading}
						>
							Batal
						</Button>

						<Button
							size='small'
							positive
							icon='checkmark'
							labelPosition='right'
							content='Masuk'
							type='submit'
							form='loginForm'
							onClick={this.handleLoginModalButton.bind(this)}
						/>
					</Modal.Actions>
				</Modal>
			</div>
		);
	}
}

LoginModal.propTypes = {
	headerLoginModalOpen: PropTypes.func.isRequired
};

function mapStateToProps(state) {
	return {
		header: state.header
	};
}

export default connect(mapStateToProps, {
	headerLoginModalOpen
}) (LoginModal);
