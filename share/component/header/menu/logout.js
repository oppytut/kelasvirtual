import React, {Component} from 'react';
import {withRouter} from 'react-router-dom';
import {Button} from 'semantic-ui-react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import debug from 'debug';

import {
	logout
} from '../../../action/auth.js';

const logName = 'Logout';
const log = {
	trace: debug(`${logName}:trace`),
	debug: debug(`${logName}:debug`),
	log: debug(`${logName}:log`),
	info: debug(`${logName}:info`),
	warn: debug(`${logName}:warn`),
	error: debug(`${logName}:error`)
};

class Logout extends Component {
	logout(e) {
		log.trace('logout');
		e.target.blur();

		this.props.logout();
		this.props.history.push('/');
	}

	render() {
		log.trace('render');

		return(
			<div>
				<Button 
					onClick={this.logout.bind(this)} 
					size='mini'
					basic
					color='teal'
				>
					Keluar
				</Button>
			</div>
		);
	}
}

Logout.propTypes = {
	logout: PropTypes.func.isRequired
};

export default withRouter(connect(null, {
	logout
}) (Logout));