const autoprefixer = require('autoprefixer');
const nodeExternals = require('webpack-node-externals');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const ReloadServerPlugin = require('reload-server-webpack-plugin');

const browserConfig = {
	devtool: 'cheap-module-source-map',
	entry: {
		app: ['babel-polyfill', './client/app.js']
	},
	module: {
		rules: [
			{
				test: /\.(png|jpg|gif|svg|jpeg|ttf|woff|woff2|eot)$/,
				loader: 'url-loader',
				options: {
					name: './public/bundle/media/[name].[ext]'
				}
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: [
						{
							loader: 'css-loader',
							options: {importLoaders: 1}
						},
						{
							loader: 'postcss-loader',
							options: {plugins: [autoprefixer()]}
						}
					]
				})
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {presets: ['env', 'react']}
			}
		]
	},
	output: {
		filename: './public/bundle/js/[name].js'
	},
	plugins: [
		new BrowserSyncPlugin ({
			host: 'localhost',
			port: 9002,
			proxy: 'http://localhost:9001/',
			files: [
				'./share/*',
				'./server/*',
				'./client/*'
			],
			reloadDelay: 2000,
			browser: ['chromium-browser']
		}),
		new ExtractTextPlugin({
			filename: './public/bundle/css/[name].css'
		})
	]
};

const serverConfig = {
	devtool: 'cheap-module-source-map',
	entry: ['babel-polyfill', './server/app.js'],
	externals: [nodeExternals()],
	target: 'node',
	output: {
		filename: './app.js',
		libraryTarget: 'commonjs2'
	},
	module: {
		rules: [
			{
				test: /\.(png|jpg|gif|svg|jpeg|ttf|woff|woff2|eot)$/,
				loader: 'url-loader',
				options: {
					name: './public/bundle/server/media/[name].[ext]'
				}
			},
			{
				test: /\.css$/,
				use: ExtractTextPlugin.extract({
					use: [
						{
							loader: 'css-loader',
							options: {importLoaders: 1}
						},
						{
							loader: 'postcss-loader',
							options: {plugins: [autoprefixer()]}
						}
					]
				})
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				loader: 'babel-loader',
				query: {presets: ['env', 'react']}
			}
		]
	},
	plugins: [
		new ReloadServerPlugin({
			script: 'app.js',
		}),
		new ExtractTextPlugin({
			filename: './public/bundle/server/css/[name].css'
		})
	]
};

module.exports = [browserConfig, serverConfig];
